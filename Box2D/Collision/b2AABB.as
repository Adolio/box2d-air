﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Collision
{
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Vec2;

	/**
	* An axis aligned bounding box.
	*/
	final public class b2AABB
	{	
		final public function b2AABB()
		{
		}
		
		private static var dX:Number;
		private static var dY:Number;
		private static var isValid:Boolean;
		private static var containsResult:Boolean;
		
		/** The lower vertex */
		public var lowerBound:b2Vec2 = new b2Vec2();
		/** The upper vertex */
		public var upperBound:b2Vec2 = new b2Vec2();

		/**
		* Verify that the bounds are sorted.
		*/
		[Inline]
		final public function IsValid():Boolean
		{
			//b2Vec2 d = upperBound - lowerBound;;
			dX = upperBound.x - lowerBound.x;
			dY = upperBound.y - lowerBound.y;

			isValid = dX >= 0.0 && dY >= 0.0;
			isValid = isValid && lowerBound.IsValid() && upperBound.IsValid();

			return isValid;
		}
		
		/** Get the center of the AABB. */
		[Inline]
		final public function GetCenter(result:b2Vec2):b2Vec2
		{
			result.x = (lowerBound.x + upperBound.x) * 0.5;
			result.y = (lowerBound.y + upperBound.y) * 0.5;

			return result;
		}
		
		/** Get the extents of the AABB (half-widths). */
		[Inline]
		final public function GetExtents(result:b2Vec2):b2Vec2
		{
			result.x = (upperBound.x - lowerBound.x) * 0.5;
			result.y = (upperBound.y - lowerBound.y) * 0.5;

			return result;
		}
		
		/**
		 * Is an AABB contained within this one.
		 */
		[Inline]
		final public function Contains(aabb:b2AABB):Boolean
		{
			containsResult = true;
			containsResult &&= lowerBound.x <= aabb.lowerBound.x;
			containsResult &&= lowerBound.y <= aabb.lowerBound.y;
			containsResult &&= aabb.upperBound.x <= upperBound.x;
			containsResult &&= aabb.upperBound.y <= upperBound.y;

			return containsResult;
		}
		
		// From Real-time Collision Detection, p179.
		/**
		 * Perform a precise raycast against the AABB.
		 */
		[Inline]
		final public function RayCast(output:b2RayCastOutput, input:b2RayCastInput):Boolean
		{
			var tmin:Number = -Number.MAX_VALUE;
			var tmax:Number = Number.MAX_VALUE;
			
			var pX:Number = input.p1.x;
			var pY:Number = input.p1.y;
			var dX:Number = input.p2.x - input.p1.x;
			var dY:Number = input.p2.y - input.p1.y;
			var absDX:Number = Math.abs(dX);
			var absDY:Number = Math.abs(dY);
			
			var normal:b2Vec2 = output.normal;
			
			var inv_d:Number;
			var t1:Number;
			var t2:Number;
			var t3:Number;
			var s:Number;
			
			
			if (absDX < Number.MIN_VALUE)
			{
				// Parallel.
				if (pX < lowerBound.x || upperBound.x < pX)
				{
					return false;					
				}
			}
			else
			{
				inv_d = 1.0 / dX;
				t1 = (lowerBound.x - pX) * inv_d;
				t2 = (upperBound.x - pX) * inv_d;
				
				// Sign of the normal vector
				s = -1.0;
				
				if (t1 > t2)
				{
					t3 = t1;
					t1 = t2;
					t2 = t3;
					s = 1.0;
				}
				
				// Push the min up
				if (t1 > tmin)
				{
					normal.x = s;
					normal.y = 0;
					tmin = t1;
				}
				
				// Pull the max down
				tmax = Math.min(tmax, t2);
				
				if(tmin > tmax)
				{
					return false;					
				}
			}
			
			if(absDY < Number.MIN_VALUE)
			{
				// Parallel.
				if(pY < lowerBound.y || upperBound.y < pY)
				{
					return false;					
				}
			}
			else
			{
				inv_d = 1.0 / dY;
				t1 = (lowerBound.y - pY) * inv_d;
				t2 = (upperBound.y - pY) * inv_d;
				
				// Sign of the normal vector
				s = -1.0;
				
				if (t1 > t2)
				{
					t3 = t1;
					t1 = t2;
					t2 = t3;
					s = 1.0;
				}
				
				// Push the min up
				if (t1 > tmin)
				{
					normal.y = s;
					normal.x = 0;
					tmin = t1;
				}
				
				// Pull the max down
				tmax = Math.min(tmax, t2);
				
				if(tmin > tmax)
				{
					return false;					
				}
			}
			
			output.fraction = tmin;
			return true;
		}
		
		
		private static var to_d1X:Number;
		private static var to_d1Y:Number;
		
		private static var to_d2X:Number;
		private static var to_d2Y:Number;
		
		/**
		 * Tests if another AABB overlaps this one.
		 */
		[Inline]
		final public function TestOverlap(other:b2AABB):Boolean
		{
			to_d1X = other.lowerBound.x - upperBound.x;
			to_d1Y = other.lowerBound.y - upperBound.y;
			to_d2X = lowerBound.x - other.upperBound.x;
			to_d2Y = lowerBound.y - other.upperBound.y;
	
			if(to_d1X > b2Settings.DECIMAL_ZERO || to_d1Y > b2Settings.DECIMAL_ZERO)
			{
				return false;				
			}
	
			if(to_d2X > b2Settings.DECIMAL_ZERO || to_d2Y > b2Settings.DECIMAL_ZERO)
			{
				return false;				
			}
	
			return true;
		}
		
		/** Combine two AABBs into one. */
		[Inline]
		public static function Combine(aabb1:b2AABB, aabb2:b2AABB, result:b2AABB):b2AABB
		{
			result.lowerBound.x = aabb1.lowerBound.x < aabb2.lowerBound.x ? aabb1.lowerBound.x : aabb2.lowerBound.x;
			result.lowerBound.y = aabb1.lowerBound.y < aabb2.lowerBound.y ? aabb1.lowerBound.y : aabb2.lowerBound.y;
			result.upperBound.x = aabb1.upperBound.x > aabb2.upperBound.x ? aabb1.upperBound.x : aabb2.upperBound.x;
			result.upperBound.y = aabb1.upperBound.y > aabb2.upperBound.y ? aabb1.upperBound.y : aabb2.upperBound.y;

			return result;
		}
	}
}