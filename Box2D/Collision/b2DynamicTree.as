﻿/*
* Copyright (c) 2009 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Collision 
{
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Fixture;
	
	// A dynamic AABB tree broad-phase, inspired by Nathanael Presson's btDbvt.
	
	/**
	 * A dynamic tree arranges data in a binary tree to accelerate
	 * queries such as volume queries and ray casts. Leafs are proxies
	 * with an AABB. In the tree we expand the proxy AABB by b2_fatAABBFactor
	 * so that the proxy AABB is bigger than the client object. This allows the client
	 * object to move by small amounts without triggering a tree update.
	 * 
	 * Nodes are pooled.
	 */
	public class b2DynamicTree 
	{
		/**
		 * Constructing the tree initializes the node pool.
		 */
		public function b2DynamicTree() 
		{
			m_root = null;
			
			// TODO: Maybe allocate some free nodes?
			m_freeList = null;
			m_path = 0;
		}
		
		/**
		 * Create a proxy. Provide a tight fitting AABB and a userData.
		 */
		
		private var cp_node:b2DynamicTreeNode;
		
		private var cp_extendX:Number;
		private var cp_extendY:Number;
		
		public function CreateDynamicTreeNodeProxyForFixture(aabb:b2AABB, userData:b2Fixture):b2DynamicTreeNode
		{
			cp_node = AllocateNode();
			
			// Fatten the aabb.
			cp_extendX = b2Settings.b2_aabbExtension;
			cp_extendY = b2Settings.b2_aabbExtension;

			cp_node.aabb.lowerBound.x = aabb.lowerBound.x - cp_extendX;
			cp_node.aabb.lowerBound.y = aabb.lowerBound.y - cp_extendY;
			cp_node.aabb.upperBound.x = aabb.upperBound.x + cp_extendX;
			cp_node.aabb.upperBound.y = aabb.upperBound.y + cp_extendY;
			
			cp_node.userData = userData;
			
			InsertLeaf(cp_node);

			return cp_node;
		}
		
		/**
		 * Destroy a proxy. This asserts if the id is invalid.
		 */
		public function DestroyProxy(proxy:b2DynamicTreeNode):void
		{
			//b2Settings.b2Assert(proxy.IsLeaf());
			RemoveLeaf(proxy);
			FreeNode(proxy);
		}
		
		/**
		 * Move a proxy with a swept AABB. If the proxy has moved outside of its fattened AABB,
		 * then the proxy is removed from the tree and re-inserted. Otherwise
		 * the function returns immediately.
		 */
		
		private var mp_extendX:Number;
		private var mp_extendY:Number;
		private static var containsAabb:Boolean;
		
		public function MoveProxy(proxy:b2DynamicTreeNode, aabb:b2AABB, displacement:b2Vec2):Boolean
		{
			// isLeaf
			//b2Settings.b2Assert(proxy.child1 == null);
			if(proxy.child1)
			{
				throw Error('Child is already a leaf');
			}
			
			/***
			if(proxy.aabb.Contains(aabb))
			{
				return false;
			}
			***/
			
			containsAabb = true;
			containsAabb &&= proxy.aabb.lowerBound.x <= aabb.lowerBound.x;
			containsAabb &&= proxy.aabb.lowerBound.y <= aabb.lowerBound.y;
			containsAabb &&= aabb.upperBound.x <= proxy.aabb.upperBound.x;
			containsAabb &&= aabb.upperBound.y <= proxy.aabb.upperBound.y;
			
			if(containsAabb) return false;
			
			
			
			
			RemoveLeaf(proxy);
			
			// Extend AABB
			mp_extendX = b2Settings.b2_aabbExtension + b2Settings.b2_aabbMultiplier * (displacement.x > 0?displacement.x: -displacement.x);
			mp_extendY = b2Settings.b2_aabbExtension + b2Settings.b2_aabbMultiplier * (displacement.y > 0?displacement.y: -displacement.y);
			
			proxy.aabb.lowerBound.x = aabb.lowerBound.x - mp_extendX;
			proxy.aabb.lowerBound.y = aabb.lowerBound.y - mp_extendY;
			proxy.aabb.upperBound.x = aabb.upperBound.x + mp_extendX;
			proxy.aabb.upperBound.y = aabb.upperBound.y + mp_extendY;
			
			InsertLeaf(proxy);

			return true;
		}
		
		/**
		 * Perform some iterations to re-balance the tree.
		 */
		[Inline]
		
		private var rb_i:int;
		private var rb_bit:uint;
		
		private var rb_node:b2DynamicTreeNode;
		
		final public function Rebalance(iterations:int):void
		{
			if(m_root == null)
			{
				return;				
			}

			for (rb_i = 0; rb_i < iterations; rb_i++)
			{
				rb_node = m_root;
				rb_bit = 0;

				while(rb_node.child1 != null)
				{
					rb_node = (m_path >> rb_bit) & 1 ? rb_node.child2 : rb_node.child1;
					rb_bit = (rb_bit + 1) & 31; // 0-31 bits in a uint
				}

				++m_path;
				
				RemoveLeaf(rb_node);
				InsertLeaf(rb_node);
			}
		}
		
		[Inline]
		final public function GetFatAABB(proxy:b2DynamicTreeNode):b2AABB
		{
			return proxy.aabb;
		}

		/**
		 * Get user data from a proxy. Returns null if the proxy is invalid.
		 */
		[Inline]
		final public function GetDynamicTreeNodeUserData(proxy:b2DynamicTreeNode):b2Fixture
		{
			return proxy.userData;
		}
		
		private var _queryStackVector:Vector.<b2DynamicTreeNode> = new Vector.<b2DynamicTreeNode>;
		
		/**
		 * Query an AABB for overlapping proxies. The callback
		 * is called for each proxy that overlaps the supplied AABB.
		 * The callback should match function signature
		 * <code>function callback(proxy:b2DynamicTreeNode):Boolean</code>
		 * and should return false to trigger premature termination.
		 */

		private var q_count:int;
		
		private var q_node:b2DynamicTreeNode;
		private var q_proceed:Boolean;
		
		[Inline]
		final public function Query(callback:IDynamicTreeCallback, aabb:b2AABB):void
		{
			if(m_root == null)
			{
				return;
			}

			_queryStackVector.length = 0;

			q_count = 0;

			_queryStackVector[q_count++] = m_root;

			while(q_count > 0)
			{
				q_node = _queryStackVector[--q_count];

				if(q_node.aabb.TestOverlap(aabb))
				{
					// isLeaf
					if(q_node.child1 == null)
					{
						q_proceed = callback.QueryDynamicTreeNodeCallback(q_node);
						
						if(!q_proceed)
						{
							return;
						}
					}
					else
					{
						// No stack limit, so no assert
						_queryStackVector[q_count++] = q_node.child1;
						_queryStackVector[q_count++] = q_node.child2;
					}
				}
			}
		}
		
		private var _raycastSegmentAABB:b2AABB = new b2AABB();
		private var _rayCastSubInput:b2RayCastInput = new b2RayCastInput();
		private var _rayCastStack:Vector.<b2DynamicTreeNode> = new Vector.<b2DynamicTreeNode>;
		
		private var rc_p1:b2Vec2;
		private var rc_p2:b2Vec2;
		
		private var r:b2Vec2 = new b2Vec2(0, 0); 
		private var v:b2Vec2 = new b2Vec2(0, 0);
		private var c:b2Vec2 = new b2Vec2(0, 0);
		private var h:b2Vec2 = new b2Vec2(0, 0);
		private var abs_v:b2Vec2 = new b2Vec2(0, 0); 
		
		private var maxFraction:Number;
		private var tX:Number;
		private var tY:Number;		
		private var rc_count:int;
		private var rc_node:b2DynamicTreeNode;
		private var separation:Number;
		
		private static var separationResult:Number;
		
		/**
		 * Ray-cast against the proxies in the tree. This relies on the callback
		 * to perform a exact ray-cast in the case were the proxy contains a shape.
		 * The callback also performs the any collision filtering. This has performance
		 * roughly equal to k * log(n), where k is the number of collisions and n is the
		 * number of proxies in the tree.
		 * @param input the ray-cast input data. The ray extends from p1 to p1 + maxFraction * (p2 - p1).
		 * @param callback a callback class that is called for each proxy that is hit by the ray.
		 * It should be of signature:
		 * <code>function callback(input:b2RayCastInput, proxy:*):Number</code>
		 */
		[Inline]
		final public function RayCast(callback:IBroadPhaseRayCastCallback, input:b2RayCastInput):void
		{
			if(m_root == null)
			{
				return;
			}
				
			rc_p1 = input.p1;
			rc_p2 = input.p2;
			
			r.x = 0;
			r.y = 0;
				
			//b2Math.SubtractVV(rc_p1, rc_p2, r);
			
			r.x = rc_p1.x - rc_p2.x;
			r.y = rc_p1.y - rc_p2.y;

			r.Normalize();

			v.x = 0; 
			v.y = 0;

			// b2Math.CrossFV(1.0, r, v);
			v.x = -1 * r.y;
			v.y = 1 * r.x;
			
			abs_v.x = 0;
			abs_v.y = 0;
				
			//b2Math.AbsV(v, abs_v);
			abs_v.x = v.x > b2Settings.DECIMAL_ZERO ? v.x : -v.x;
			abs_v.y = v.y > b2Settings.DECIMAL_ZERO ? v.y : -v.y;

			maxFraction = input.maxFraction;

			tX = rc_p1.x + maxFraction * (rc_p2.x - rc_p1.x);
			tY = rc_p1.y + maxFraction * (rc_p2.y - rc_p1.y);

			_raycastSegmentAABB.lowerBound.x = rc_p1.x < tX ? rc_p1.x : tX;
			_raycastSegmentAABB.lowerBound.y = rc_p1.y < tY ? rc_p1.y : tY;
			_raycastSegmentAABB.upperBound.x = rc_p1.x > tX ? rc_p1.x : tX;
			_raycastSegmentAABB.upperBound.y = rc_p1.y > tY ? rc_p1.y : tY;
			
			rc_count = 0;
			_rayCastStack[rc_count++] = m_root;

			c.x = 0;
			c.y = 0;
			
			h.x = 0;
			h.y = 0;
			
			separation;
			
			while(rc_count > 0)
			{
				rc_node = _rayCastStack[--rc_count];
				
				if(rc_node.aabb.TestOverlap(_raycastSegmentAABB) == false)
				{
					continue;
				}
				
				// Separating axis for segment (Gino, p80)
				// |dot(v, p1 - c)| > dot(|v|,h)
				
				// rc_node.aabb.GetCenter(c);
				c.x = (rc_node.aabb.lowerBound.x + rc_node.aabb.upperBound.x) * 0.5;
				c.y = (rc_node.aabb.lowerBound.y + rc_node.aabb.upperBound.y) * 0.5;
				
				
				// rc_node.aabb.GetExtents(h);
				h.x = (rc_node.aabb.upperBound.x - rc_node.aabb.lowerBound.x) * 0.5;
				h.y = (rc_node.aabb.upperBound.y - rc_node.aabb.lowerBound.y) * 0.5;
				
				separationResult = v.x * (rc_p1.x - c.x) + v.y * (rc_p1.y - c.y);
				separationResult = separationResult < 0 ? -separationResult : separationResult;
				
				separation = separationResult - abs_v.x * h.x - abs_v.y * h.y;
				
				if(separation > b2Settings.DECIMAL_ZERO)
				{
					continue;
				}
				
				//if(rc_node.IsLeaf())
				if(rc_node.child1 == null)
				{
					_rayCastSubInput.p1.x = input.p1.x;
					_rayCastSubInput.p1.y = input.p1.y;
					
					_rayCastSubInput.p2.x = input.p2.x;
					_rayCastSubInput.p2.y = input.p2.y;
					
					_rayCastSubInput.maxFraction = input.maxFraction;
					
					maxFraction = callback.RayCastCallback(_rayCastSubInput, rc_node);
					
					if(maxFraction == b2Settings.DECIMAL_ZERO)
					{
						break;
					}
						
					//Update the segment bounding box
					{
						tX = rc_p1.x + maxFraction * (rc_p2.x - rc_p1.x);
						tY = rc_p1.y + maxFraction * (rc_p2.y - rc_p1.y);

						_raycastSegmentAABB.lowerBound.x = rc_p1.x < tX ? rc_p1.x : tX;
						_raycastSegmentAABB.lowerBound.y = rc_p1.y < tY ? rc_p1.y : tY;
						_raycastSegmentAABB.upperBound.x = rc_p1.x > tX ? rc_p1.x : tX;
						_raycastSegmentAABB.upperBound.y = rc_p1.y > tY ? rc_p1.y : tY;
					}
				}
				else
				{
					// No stack limit, so no assert
					_rayCastStack[rc_count++] = rc_node.child1;
					_rayCastStack[rc_count++] = rc_node.child2;
				}
			}

			_rayCastStack.length = 0;
		}
		
		private var al_node:b2DynamicTreeNode;
		private var newNode:b2DynamicTreeNode;
		
		private function AllocateNode():b2DynamicTreeNode
		{
			// Peel a node off the free list
			// this is Box2D Pooling b2DynamicTreeNode objects
			if (m_freeList)
			{
				al_node = m_freeList;
				m_freeList = al_node.parent;
				al_node.parent = null;
				al_node.child1 = null;
				al_node.child2 = null;
				newNode = al_node;
				newNode.nodeId = ++m_nodeCount;
			}
			else
			{
				// Ignore length pool expansion and relocation found in the C++
				// As we are using heap allocation
				newNode = new b2DynamicTreeNode(++m_nodeCount);
			}

			return newNode;
		}

		[Inline]
		final private function FreeNode(node:b2DynamicTreeNode):void
		{
			node.parent = m_freeList;
			m_freeList = node;
			--m_nodeCount;
		}
		
		private var center:b2Vec2 = new b2Vec2(); 
		private var sibling:b2DynamicTreeNode;
		private var child1:b2DynamicTreeNode;
		private var child2:b2DynamicTreeNode;
		private var norm1:Number;
		private var norm2:Number;
		private var node1:b2DynamicTreeNode;
		private var node2:b2DynamicTreeNode;
		private var oldAABB:b2AABB;
		private var isLeaf:Boolean;

		private static var normalResult1:Number, normalResult2:Number;

		[Inline]
		final private function InsertLeaf(leaf:b2DynamicTreeNode):void
		{
			if (m_root == null)
			{
				m_root = leaf;
				m_root.parent = null;
				return;
			} 
				
			// leaf.aabb.GetCenter(center);			
			center.x = (leaf.aabb.lowerBound.x + leaf.aabb.upperBound.x) * 0.5;
			center.y = (leaf.aabb.lowerBound.y + leaf.aabb.upperBound.y) * 0.5;
			
			sibling = m_root;
			
			isLeaf = sibling.child1 == null;
			
			if(isLeaf == false)
			{
				do
				{
					child1 = sibling.child1;
					child2 = sibling.child2;

					/*
					norm1 = 
						Math.abs(
							((child1.aabb.lowerBound.x + child1.aabb.upperBound.x) * 0.5) - center.x
						) +
						Math.abs(
							((child1.aabb.lowerBound.y + child1.aabb.upperBound.y) * 0.5) - center.y
						);
					*/
					normalResult1 = ((child1.aabb.lowerBound.x + child1.aabb.upperBound.x) * 0.5) - center.x;
					normalResult1 = normalResult1 < 0 ? -normalResult1 : normalResult1;
					
					normalResult2 = ((child1.aabb.lowerBound.y + child1.aabb.upperBound.y) * 0.5) - center.y;
					normalResult2 = normalResult2 < 0 ? -normalResult2 : normalResult2;
					
					norm1 = normalResult1 + normalResult2;

					/*
					norm2 = 
						Math.abs(
							((child2.aabb.lowerBound.x + child2.aabb.upperBound.x) * 0.5) - center.x
						) + 
						Math.abs(
							((child2.aabb.lowerBound.y + child2.aabb.upperBound.y) * 0.5) - center.y
						);
					*/
					
					normalResult1 = ((child2.aabb.lowerBound.x + child2.aabb.upperBound.x) * 0.5) - center.x;
					normalResult1 = normalResult1 < 0 ? -normalResult1 : normalResult1;
					
					normalResult2 = ((child2.aabb.lowerBound.y + child2.aabb.upperBound.y) * 0.5) - center.y;
					normalResult2 = normalResult2 < 0 ? -normalResult2 : normalResult2;
					
					norm2 = normalResult1 + normalResult2;					

					if(norm1 < norm2)
					{
						sibling = child1;
					}
					else
					{
						sibling = child2;
					}

					isLeaf = sibling.child1 == null;
				}

				while(isLeaf == false);
			}

			node1 = sibling.parent;
			node2 = AllocateNode();

			node2.parent = node1;
			node2.userData = null;
			
			// b2AABB.Combine(leaf.aabb, sibling.aabb, node2.aabb);
			node2.aabb.lowerBound.x = leaf.aabb.lowerBound.x < sibling.aabb.lowerBound.x ? leaf.aabb.lowerBound.x : sibling.aabb.lowerBound.x;
			node2.aabb.lowerBound.y = leaf.aabb.lowerBound.y < sibling.aabb.lowerBound.y ? leaf.aabb.lowerBound.y : sibling.aabb.lowerBound.y;
			node2.aabb.upperBound.x = leaf.aabb.upperBound.x > sibling.aabb.upperBound.x ? leaf.aabb.upperBound.x : sibling.aabb.upperBound.x;
			node2.aabb.upperBound.y = leaf.aabb.upperBound.y > sibling.aabb.upperBound.y ? leaf.aabb.upperBound.y : sibling.aabb.upperBound.y;
			
			if(node1)
			{
				if (sibling.parent.child1 == sibling)
				{
					node1.child1 = node2;
				}
				else
				{
					node1.child2 = node2;
				}
				
				node2.child1 = sibling;
				node2.child2 = leaf;
				sibling.parent = node2;
				leaf.parent = node2;

				do
				{
					if (node1.aabb.Contains(node2.aabb))
					{
						break;						
					}
					
					// b2AABB.Combine(node1.child1.aabb, node1.child2.aabb, node1.aabb);
					node1.aabb.lowerBound.x = node1.child1.aabb.lowerBound.x < node1.child2.aabb.lowerBound.x ? node1.child1.aabb.lowerBound.x : node1.child2.aabb.lowerBound.x;
					node1.aabb.lowerBound.y = node1.child1.aabb.lowerBound.y < node1.child2.aabb.lowerBound.y ? node1.child1.aabb.lowerBound.y : node1.child2.aabb.lowerBound.y;
					node1.aabb.upperBound.x = node1.child1.aabb.upperBound.x > node1.child2.aabb.upperBound.x ? node1.child1.aabb.upperBound.x : node1.child2.aabb.upperBound.x;
					node1.aabb.upperBound.y = node1.child1.aabb.upperBound.y > node1.child2.aabb.upperBound.y ? node1.child1.aabb.upperBound.y : node1.child2.aabb.upperBound.y;
					
					node2 = node1;
					node1 = node1.parent;
				}
				while (node1);
			}
			else
			{
				node2.child1 = sibling;
				node2.child2 = leaf;
				sibling.parent = node2;
				leaf.parent = node2;
				m_root = node2;
			}
		}
		
		[Inline]
		final private function RemoveLeaf(leaf:b2DynamicTreeNode):void
		{
			if(leaf == m_root)
			{
				m_root = null;
				return;
			}
			
			node2 = leaf.parent;
			node1 = node2.parent;

			if(node2.child1 == leaf)
			{
				sibling = node2.child2;
			}
			else
			{
				sibling = node2.child1;
			}
			
			if(node1)
			{
				// Destroy node2 and connect node1 to sibling
				if (node1.child1 == node2)
				{
					node1.child1 = sibling;
				}
				else
				{
					node1.child2 = sibling;
				}

				sibling.parent = node1;
				FreeNode(node2);
				
				// Adjust the ancestor bounds
				while(node1)
				{
					oldAABB = node1.aabb;

					// b2AABB.Combine(node1.child1.aabb, node1.child2.aabb, node1.aabb);
					node1.aabb.lowerBound.x = node1.child1.aabb.lowerBound.x < node1.child2.aabb.lowerBound.x ? node1.child1.aabb.lowerBound.x : node1.child2.aabb.lowerBound.x;
					node1.aabb.lowerBound.y = node1.child1.aabb.lowerBound.y < node1.child2.aabb.lowerBound.y ? node1.child1.aabb.lowerBound.y : node1.child2.aabb.lowerBound.y;
					node1.aabb.upperBound.x = node1.child1.aabb.upperBound.x > node1.child2.aabb.upperBound.x ? node1.child1.aabb.upperBound.x : node1.child2.aabb.upperBound.x;
					node1.aabb.upperBound.y = node1.child1.aabb.upperBound.y > node1.child2.aabb.upperBound.y ? node1.child1.aabb.upperBound.y : node1.child2.aabb.upperBound.y;
					
					
					if(oldAABB.Contains(node1.aabb))
					{
						break;						
					}

					node1 = node1.parent;
				}
			}
			else
			{
				m_root = sibling;
				sibling.parent = null;
				FreeNode(node2);
			}
		}
		
		public var m_root:b2DynamicTreeNode;
		private var m_freeList:b2DynamicTreeNode;
		
		/** This is used for incrementally traverse the tree for rebalancing */
		private var m_path:uint;
		
		private var m_nodeCount:uint = 0;
	}	
}