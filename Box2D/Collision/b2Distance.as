﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Collision
{
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Transform;
	import Box2D.Common.Math.b2Vec2;
	
	/**
	* @private
	*/
	public class b2Distance
	{
	
		// GJK using Voronoi regions (Christer Ericson) and Barycentric coordinates.
		private static const k_maxIters:int = 20;
		private static const DOUBLE_MIN_VALUE:Number = Number.MIN_VALUE * Number.MIN_VALUE;

		private static var b2_gjkCalls:int;
		private static var b2_gjkIters:int;
		private static var b2_gjkMaxIters:int;
		
		private static var proxyA:b2DistanceProxy;
		private static var proxyB:b2DistanceProxy;
		
		private static var transformA:b2Transform;
		private static var transformB:b2Transform;
		
		private static var s_simplex:b2Simplex = new b2Simplex();
		private static var s_saveA:Vector.<int> = new Vector.<int>(3);
		private static var s_saveB:Vector.<int> = new Vector.<int>(3);
		
		private static var saveA:Vector.<int>;
		private static var saveB:Vector.<int>;
		private static var saveCount:int = 0;
		
		private static var vertices:Vector.<b2SimplexVertex>;
		
		private static var closestPoint:b2Vec2 = new b2Vec2();
		
		private static var distanceSqr1:Number;
		private static var distanceSqr2:Number;
		
		private static var i:int;
		private static var p:b2Vec2 = new b2Vec2();
		
		// Main iteration loop
		private static var iter:int = 0;
		private static var vertex:b2SimplexVertex;
		
		private static var multA:b2Vec2 = new b2Vec2();
		private static var multB:b2Vec2 = new b2Vec2();
		
		private static var negative:b2Vec2 = new b2Vec2();
		
		private static var d:b2Vec2 = new b2Vec2();
		private static var subtractedVector:b2Vec2 = new b2Vec2();
		
		private static var duplicate:Boolean = false;
		
		private static var normal:b2Vec2 = new b2Vec2();
		
		private static var rA:Number;
		private static var rB:Number;
		
		public static function Distance(output:b2DistanceOutput, cache:b2SimplexCache, input:b2DistanceInput):void
		{
			++b2_gjkCalls;
			
			proxyA = input.proxyA;
			proxyB = input.proxyB;
			
			transformA = input.transformA;
			transformB = input.transformB;
			
			// Initialize the simplex
			s_simplex.ReadCache(cache, proxyA, transformA, proxyB, transformB);
			
			// Get simplex vertices as an vector.
			vertices = s_simplex.m_vertices;
			
			// These store the vertices of the last simplex so that we
			// can check for duplicates and prevent cycling
			saveA = s_saveA;
			saveB = s_saveB;
			saveCount = 0;
				
			i = 0;
			iter = 0;

			closestPoint.x = 0;
			closestPoint.y = 0;

			p.x = 0;
			p.y = 0;
			
			d.x = 0;
			d.y = 0;
			
			s_simplex.GetClosestPoint(closestPoint);

			// closestPoint.LengthSquared()
			distanceSqr1 = (closestPoint.x * closestPoint.x + closestPoint.y * closestPoint.y);
			distanceSqr2 = distanceSqr1;
			
			while(iter < k_maxIters)
			{
				// Copy the simplex so that we can identify duplicates
				saveCount = s_simplex.m_count;

				for (i = 0; i < saveCount; i++)
				{
					vertex = vertices[i];

					saveA[i] = vertex.indexA;
					saveB[i] = vertex.indexB;
				}
				
				switch(s_simplex.m_count)
				{
					case 2:
						s_simplex.Solve2();
					break;
					
					case 3:
						s_simplex.Solve3();
					break;
				}
				
				// If we have 3 points, then the origin is in the corresponding triangle.
				if(s_simplex.m_count == 3)
				{
					break;
				}
				
				// Compute the closest point.
				s_simplex.GetClosestPoint(p);
				distanceSqr2 = (p.x * p.x + p.y * p.y);
				
				/*
				// Ensure progress
				if(distanceSqr2 > distanceSqr1)
				{
					//break;
				}
				*/

				distanceSqr1 = distanceSqr2;
				
				// Get search direction.
				
				s_simplex.GetSearchDirection(d);
				
				// Ensure the search direction is numerically fit.
				if((d.x * d.x + d.y * d.y) < DOUBLE_MIN_VALUE)
				{
					// THe origin is probably contained by a line segment or triangle.
					// Thus the shapes are overlapped.
					
					// We can't return zero here even though there may be overlap.
					// In case the simplex is a point, segment or triangle it is very difficult
					// to determine if the origin is contained in the CSO or very close to it
					break;
				}
				
				/***
				 *
				 * ORIGINAL CODE
				 * 
				var vertex:b2SimplexVertex = vertices[simplex.m_count];
		
					vertex.indexA = proxyA.GetSupport(b2Math.MulTMV(transformA.R, d.GetNegative()));
					vertex.wA = b2Math.MulX(transformA, proxyA.GetVertex(vertex.indexA));
					vertex.indexB = proxyB.GetSupport(b2Math.MulTMV(transformB.R, d));
					vertex.wB = b2Math.MulX(transformB, proxyB.GetVertex(vertex.indexB));
					vertex.w = b2Math.SubtractVV(vertex.wB, vertex.wA);
				****/
				
				// Compute a tentative new simplex vertex using support points
				vertex = vertices[s_simplex.m_count];
				
				negative.x = -d.x;
				negative.y = -d.y;
				
				b2Math.MulTMV(transformA.R, negative, multA);
				
				vertex.indexA = proxyA.GetSupport(multA);
				
				b2Math.MulX(transformA, proxyA.m_vertices[vertex.indexA], vertex.wA);
				
				b2Math.MulTMV(transformB.R, d, multB);
				
				vertex.indexB = proxyB.GetSupport(multB);
				
				b2Math.MulX(transformB, proxyB.m_vertices[vertex.indexB], vertex.wB);
				
				vertex.w.x = vertex.wB.x - vertex.wA.x;
				vertex.w.y = vertex.wB.y - vertex.wA.y;
				
				// Iteration count is equated to the number of support point calls.
				++iter;
				++b2_gjkIters;
				
				// Check for duplicate support points. This is the main termination criteria.
				duplicate = false;

				for(i = 0; i < saveCount; i++)
				{
					if (vertex.indexA == saveA[i] && vertex.indexB == saveB[i])
					{
						duplicate = true;
						break;
					}
				}
				
				// If we found a duplicate support point we must exit to avoid cycling
				if(duplicate)
				{
					break;
				}
				
				// New vertex is ok and needed.
				++s_simplex.m_count;
				
			}// end while
			
			b2_gjkMaxIters = b2Math.Max(b2_gjkMaxIters, iter);
			
			// Prepare output
			s_simplex.GetWitnessPoints(output.pointA, output.pointB);
				
			subtractedVector.x = output.pointA.x - output.pointB.x;
			subtractedVector.y = output.pointA.y - output.pointB.y;
			
			// subtractedVector.Length()
			output.distance = Math.sqrt(subtractedVector.x * subtractedVector.x + subtractedVector.y * subtractedVector.y);
			
			output.iterations = iter;
			
			// Cache the simplex
			s_simplex.WriteCache(cache);
			
			// Apply radii if requested.
			if(input.useRadii)
			{
				rA = proxyA.m_radius;
				rB = proxyB.m_radius;
				
				if(output.distance > rA + rB && output.distance > Number.MIN_VALUE)
				{
					// Shapes are still not overlapped.
					// Move the witness points to the outer surface.
					output.distance -= rA + rB;
					
					normal.x = output.pointB.x - output.pointA.x;
					normal.y = output.pointB.y - output.pointA.y;
					
					normal.Normalize();

					output.pointA.x += rA * normal.x;
					output.pointA.y += rA * normal.y;
					output.pointB.x -= rB * normal.x;
					output.pointB.y -= rB * normal.y;
				}
				else
				{
					// Shapes are overlapped when radii are considered.
					// Move the witness points to the middle.
					p.x = .5 * (output.pointA.x + output.pointB.x);
					p.y = .5 * (output.pointA.y + output.pointB.y);
		
					output.pointA.x = output.pointB.x = p.x;
					output.pointA.y = output.pointB.y = p.y;
		
					output.distance = b2Settings.DECIMAL_ZERO;
				}
			}
			
			vertices = null;
			
			proxyA = null;
			proxyB = null;
			
			transformA = null;
			transformB = null;
			
			saveA = null;
			saveB = null;
		}
	}
}