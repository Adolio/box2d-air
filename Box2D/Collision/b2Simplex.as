﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Collision 
{
	import Box2D.Common.b2Pool;
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Transform;
	import Box2D.Common.Math.b2Vec2;
	
	internal class b2Simplex
	{
		
		public function b2Simplex()
		{
			m_vertices[0] = m_v1;
			m_vertices[1] = m_v2;
			m_vertices[2] = m_v3;
		}
		
		private static var wALocal:b2Vec2;
		private static var wBLocal:b2Vec2;
		
		private static var i:int;
		
		private static var v:b2SimplexVertex;
		
		private static var metric1:Number;
		private static var metric2:Number;
		
		[Inline]
		final public function ReadCache(cache:b2SimplexCache, 
									    proxyA:b2DistanceProxy,
									    transformA:b2Transform,
									    proxyB:b2DistanceProxy,
									    transformB:b2Transform):void
		{
			// b2Settings.b2Assert(0 <= cache.count && cache.count <= 3);
			if(!(0 <= cache.count && cache.count <= 3))
			{
				throw Error('Cache count invalid');	
			}
			
			// Copy data from cache.
			m_count = cache.count;
			
			for(i = 0; i < m_count; i++)
			{
				v = m_vertices[i];

				v.indexA = cache.indexA[i];
				v.indexB = cache.indexB[i];
				
				/*
				wALocal = proxyA.GetVertex(v.indexA);
				wBLocal = proxyB.GetVertex(v.indexB);
				*/
				
				wALocal = proxyA.m_vertices[v.indexA];
				wBLocal = proxyB.m_vertices[v.indexB];
				
				b2Math.MulX(transformA, wALocal, v.wA);
				b2Math.MulX(transformB, wBLocal, v.wB);
				
				v.w.x = v.wB.x - v.wA.x;
				v.w.y = v.wB.y - v.wA.y;
		
				v.a = 0;
			}
			
			// Compute the new simplex metric, if it substantially different than
			// old metric then flush the simplex
			if (m_count > 1)
			{
				metric1 = cache.metric;
				metric2 = GetMetric();
				
				if (metric2 < .5 * metric1 || 2.0 * metric1 < metric2 || metric2 < Number.MIN_VALUE)
				{
					// Reset the simplex
					m_count = 0;
				}
			}
			
			// If the cache is empty or invalid
			if (m_count == 0)
			{
				v = m_vertices[0];

				v.indexA = 0;
				v.indexB = 0;

				/*
				wALocal = proxyA.GetVertex(v.indexA);
				wBLocal = proxyB.GetVertex(v.indexB);
				*/

				wALocal = proxyA.m_vertices[0];
				wBLocal = proxyB.m_vertices[0];
				
				b2Math.MulX(transformA, wALocal, v.wA);
				b2Math.MulX(transformB, wBLocal, v.wB);
				
				v.w.x = v.wB.x - v.wA.x;
				v.w.y = v.wB.y - v.wA.y;
		
				m_count = 1;
			}
		}
		
		private var wc_i:int;
		
		public function WriteCache(cache:b2SimplexCache):void
		{
			cache.metric = GetMetric();
			cache.count = uint(m_count);

			for(wc_i = 0; wc_i < m_count; wc_i++)
			{
				cache.indexA[wc_i] = m_vertices[wc_i].indexA;
				cache.indexB[wc_i] = m_vertices[wc_i].indexB;
			}
		}
		
		private static var negative:b2Vec2 = b2Pool.getVector2(0, 0);
		private static var e12:b2Vec2 = b2Pool.getVector2(0, 0);
		private static var sign:Number;
		
		public function GetSearchDirection(result:b2Vec2):void
		{
			switch(m_count)
			{
				case 1:
					result.x = -m_v1.w.x;
					result.y = -m_v1.w.y;
				break;
		
				case 2:
		
					e12.x = m_v2.w.x - m_v1.w.x;
					e12.y = m_v2.w.y - m_v1.w.y;
					
					negative.x = -m_v1.w.x;
					negative.y = -m_v1.w.y;
					
					// b2Math.CrossVV(e12, negative)
					sign = (e12.x * negative.y) - (e12.y * negative.x);
		
					if(sign > b2Settings.DECIMAL_ZERO)
					{
						// Origin is left of e12.
						// Math.CrossFV(s:Number, a:b2Vec2, result:b2Vec2)
						/**
						 * result.x = -s * a.y;
						   result.y = s * a.x;
						 */
						
						// b2Math.CrossFV(b2Settings.DECIMAL_ONE, e12, result);
						
						result.x = -b2Settings.DECIMAL_ONE * e12.y;
						result.y = b2Settings.DECIMAL_ONE * e12.x;
					}
					else
					{
						// Math.CrossVF(a:b2Vec2, s:Number, result:b2Vec2)
						/**
						 * result.x = s * a.y;
						   result.y = -s * a.x;
						 */
						
						// Origin is right of e12.
						// b2Math.CrossVF(e12, b2Settings.DECIMAL_ONE, result);
						
						result.x = b2Settings.DECIMAL_ONE * e12.y;
						result.y = -b2Settings.DECIMAL_ONE * e12.x;
					}

				break;
			}
		}

		[Inline]
		final public function GetClosestPoint(result:b2Vec2):void
		{
			switch(m_count)
			{
				case 0:
					b2Settings.b2Assert(false);
				break;
				case 1:
					result.x = m_v1.w.x;
					result.y = m_v1.w.y;
				break;
				case 2:
					
					result.x = m_v1.a * m_v1.w.x + m_v2.a * m_v2.w.x;
					result.y = m_v1.a * m_v1.w.y + m_v2.a * m_v2.w.y;
				break;
			}
		}
		
		[Inline]
		final public function GetWitnessPoints(pA:b2Vec2, pB:b2Vec2):void
		{
			switch(m_count)
			{
				case 0:
					b2Settings.b2Assert(false);
				break;
				
				case 1:

					pA.x = m_v1.wA.x;
					pA.y = m_v1.wA.y;
					
					pB.x = m_v1.wB.x;
					pB.y = m_v1.wB.y;
					
				break;
				
				case 2:
					pA.x = m_v1.a * m_v1.wA.x + m_v2.a * m_v2.wA.x;
					pA.y = m_v1.a * m_v1.wA.y + m_v2.a * m_v2.wA.y;
					pB.x = m_v1.a * m_v1.wB.x + m_v2.a * m_v2.wB.x;
					pB.y = m_v1.a * m_v1.wB.y + m_v2.a * m_v2.wB.y;
				break;
				
				case 3:
					pB.x = pA.x = m_v1.a * m_v1.wA.x + m_v2.a * m_v2.wA.x + m_v3.a * m_v3.wA.x;
					pB.y = pA.y = m_v1.a * m_v1.wA.y + m_v2.a * m_v2.wA.y + m_v3.a * m_v3.wA.y;
				break;
				
				default:
					b2Settings.b2Assert(false);
				break;
			}
		}
		
		private static var metricVector:b2Vec2 = b2Pool.getVector2(0, 0);
		
		private static var subtractA:b2Vec2 = b2Pool.getVector2(0, 0);
		private static var subtractB:b2Vec2 = b2Pool.getVector2(0, 0);
		
		private static var metric:Number;
		
		[Inline]
		final public function GetMetric():Number
		{
			switch(m_count)
			{
				case 0:
					b2Settings.b2Assert(false);
					metric = b2Settings.DECIMAL_ZERO;
				break;
			
				case 1:
					metric = b2Settings.DECIMAL_ZERO;
				break;
			
				case 2:
					
					metricVector.x = m_v1.w.x - m_v2.w.x;
					metricVector.y = m_v1.w.y - m_v2.w.y;
					
					// metricVector.Length();
					metric = Math.sqrt(metricVector.x * metricVector.x + metricVector.y * metricVector.y);

				break;
			
				case 3:
					
					subtractA.x = m_v2.w.x - m_v1.w.x;
					subtractA.y = m_v2.w.y - m_v1.w.y;
					
					subtractB.x = m_v3.w.x - m_v1.w.x;
					subtractB.y = m_v3.w.y - m_v1.w.y;
					
					// b2Math.CrossVV(subtractA, subtractB)
					metric = (subtractA.x * subtractB.y) - (subtractA.y * subtractB.x);
					
				break;
			
				default:
					b2Settings.b2Assert(false);
					metric =  b2Settings.DECIMAL_ZERO;
				break;
			}
			
			return metric;
		}
		
		// Solve a line segment using barycentric coordinates.
		//
		// p = a1 * w1 + a2 * w2
		// a1 + a2 = 1
		//
		// The vector from the origin to the closest point on the line is
		// perpendicular to the line.
		// e12 = w2 - w1
		// dot(p, e) = 0
		// a1 * dot(w1, e) + a2 * dot(w2, e) = 0
		//
		// 2-by-2 linear system
		// [1      1     ][a1] = [1]
		// [w1.e12 w2.e12][a2] = [0]
		//
		// Define
		// d12_1 =  dot(w2, e12)
		// d12_2 = -dot(w1, e12)
		// d12 = d12_1 + d12_2
		//
		// Solution
		// a1 = d12_1 / d12
		// a2 = d12_2 / d12
		
		private static var w1:b2Vec2;
		private static var w2:b2Vec2;
		private static var d12_2:Number;
		private static var inv_d12:Number;
		private static var d12_1:Number;
		
		[Inline]
		final public function Solve2():void
		{
			w1 = m_v1.w;
			w2 = m_v2.w;

			e12.x = w2.x - w1.x;
			e12.y = w2.y - w1.y;
			
			// w1 region
			d12_2 = -(w1.x * e12.x + w1.y * e12.y);
			
			if(d12_2 <= b2Settings.DECIMAL_ZERO)
			{
				// a2 <= 0, so we clamp it to 0
				m_v1.a = b2Settings.DECIMAL_ONE;
				m_count = 1;

				return;
			}
			
			// w2 region
			d12_1 = (w2.x * e12.x + w2.y * e12.y);
		
			if (d12_1 <= b2Settings.DECIMAL_ZERO)
			{
				// a1 <= 0, so we clamp it to 0
				m_v2.a = b2Settings.DECIMAL_ONE;
				m_count = 1;
				m_v1.Set(m_v2);
		
				return;
			}
		
			// Must be in e12 region.
			inv_d12 = b2Settings.DECIMAL_ONE / (d12_1 + d12_2);
			
			m_v1.a = d12_1 * inv_d12;
			m_v2.a = d12_2 * inv_d12;
			
			m_count = 2;
		}
		
		private static var w3:b2Vec2;
		private static var w1e12:Number;
		private static var w2e12:Number;
		private static var e13:b2Vec2 = b2Pool.getVector2(0, 0);
		
		private static var w1e13:Number;
		private static var w3e13:Number;
		
		private static var d13_1:Number;
		private static var d13_2:Number;
		
		private static var e23:b2Vec2 = b2Pool.getVector2(0, 0);
		
		private static var w2e23:Number;
		private static var w3e23:Number;
		private static var d23_1:Number;
		private static var d23_2:Number;
		private static var n123:Number;
		
		private static var d123_1:Number;
		private static var d123_2:Number;
		private static var d123_3:Number;

		private static var inv_d13:Number;
		private static var inv_d23:Number;
		private static var inv_d123:Number;
		
		[Inline]
		final public function Solve3():void
		{
			w1 = m_v1.w;
			w2 = m_v2.w;
			w3 = m_v3.w;

			e12.x = w2.x - w1.x;
			e12.y = w2.y - w1.y;
		
			// Math.Dot(a, b)
			// a.x * b.x + a.y * b.y

			w1e12 = w1.x * e12.x + w1.y * e12.y; // b2Math.Dot(w1, e12);
			w2e12 = w2.x * e12.x + w2.y * e12.y; // b2Math.Dot(w2, e12);
			
			d12_1 = w2e12;
			d12_2 = -w1e12;

			e13.x = w3.x - w1.x;
			e13.y = w3.y - w1.y;
			
			w1e13 = w1.x * e13.x + w1.y * e13.y; //b2Math.Dot(w1, e13);
			w3e13 = w3.x * e13.x + w3.y * e13.y; // b2Math.Dot(w3, e13);
			
			d13_1 = w3e13;
			d13_2 = -w1e13;

			e23.x = w3.x - w2.x;
			e23.y = w3.y - w2.y;

			w2e23 = w2.x * e23.x + w2.y * e23.y; // b2Math.Dot(w2, e23); 
			w3e23 = w3.x * e23.x + w3.y * e23.y; // b2Math.Dot(w3, e23);
			
			d23_1 = w3e23;
			d23_2 = -w2e23;
			
			// Triangle123
			// Math.CrossVV(a, b)
			// a.x * b.y - a.y * b.x;
			
			n123 = (e12.x * e13.y) - (e12.y * e13.x); // b2Math.CrossVV(e12, e13);
		
			d123_1 = n123 * ((w2.x * w3.y) - (w2.y * w3.x)); // b2Math.CrossVV(w2, w3);
			d123_2 = n123 * ((w3.x * w1.y) - (w3.y * w1.x)); // b2Math.CrossVV(w3, w1);
			d123_3 = n123 * ((w1.x * w2.y) - (w1.y * w2.x)); // b2Math.CrossVV(w1, w2);

			
			// w1 region
			if (d12_2 <= b2Settings.DECIMAL_ZERO && d13_2 <= b2Settings.DECIMAL_ZERO)
			{
				m_v1.a = b2Settings.DECIMAL_ONE;
				m_count = 1;
				return;
			}
		
			// e12
			if (d12_1 > b2Settings.DECIMAL_ZERO && d12_2 > b2Settings.DECIMAL_ZERO && d123_3 <= b2Settings.DECIMAL_ZERO)
			{
				inv_d12 = b2Settings.DECIMAL_ONE / (d12_1 + d12_2);
				m_v1.a = d12_1 * inv_d12;
				m_v2.a = d12_2 * inv_d12;
				m_count = 2;
				return;
			}

			// e13
			if (d13_1 > b2Settings.DECIMAL_ZERO && d13_2 > b2Settings.DECIMAL_ZERO && d123_2 <= b2Settings.DECIMAL_ZERO)
			{
				inv_d13 = b2Settings.DECIMAL_ONE / (d13_1 + d13_2);
				m_v1.a = d13_1 * inv_d13;
				m_v3.a = d13_2 * inv_d13;
				m_count = 2;
				m_v2.Set(m_v3);
				return;
			}

			// w2 region
			if (d12_1 <= b2Settings.DECIMAL_ZERO && d23_2 <= b2Settings.DECIMAL_ZERO)
			{
				m_v2.a = 1.0;
				m_count = 1;
				m_v1.Set(m_v2);
				return;
			}

			// w3 region
			if (d13_1 <= b2Settings.DECIMAL_ZERO && d23_1 <= b2Settings.DECIMAL_ZERO)
			{
				m_v3.a = b2Settings.DECIMAL_ONE;
				m_count = 1;
				m_v1.Set(m_v3);
				return;
			}

			// e23
			if (d23_1 > b2Settings.DECIMAL_ZERO && d23_2 > b2Settings.DECIMAL_ZERO && d123_1 <= b2Settings.DECIMAL_ZERO)
			{
				inv_d23 = b2Settings.DECIMAL_ONE / (d23_1 + d23_2);
				m_v2.a = d23_1 * inv_d23;
				m_v3.a = d23_2 * inv_d23;
				m_count = 2;
				m_v1.Set(m_v3);
				return;
			}
		
			// Must be in triangle123
			inv_d123 = b2Settings.DECIMAL_ONE / (d123_1 + d123_2 + d123_3);
			m_v1.a = d123_1 * inv_d123;
			m_v2.a = d123_2 * inv_d123;
			m_v3.a = d123_3 * inv_d123;
			m_count = 3;
		}
		
		public var m_v1:b2SimplexVertex = new b2SimplexVertex();
		public var m_v2:b2SimplexVertex = new b2SimplexVertex();
		public var m_v3:b2SimplexVertex = new b2SimplexVertex();
		public var m_vertices:Vector.<b2SimplexVertex> = new Vector.<b2SimplexVertex>(3);
		public var m_count:int;
	}
}