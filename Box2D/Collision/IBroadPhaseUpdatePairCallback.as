package Box2D.Collision
{
	import Box2D.Dynamics.b2Fixture;

	public interface IBroadPhaseUpdatePairCallback
	{
		function updatePairsCallback(proxyUserDataA:b2Fixture, proxyUserDataB:b2Fixture):void;
	}
}