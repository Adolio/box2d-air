﻿package Box2D.Collision 
{
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2Fixture;
	import Box2D.Dynamics.Contacts.b2ContactEdge;

	/**
	 * The broad-phase is used for computing pairs and performing volume queries and ray casts.
	 * This broad-phase does not persist pairs. Instead, this reports potentially new pairs.
	 * It is up to the client to consume the new pairs and to track subsequent overlap.
	 */
	public class b2DynamicTreeBroadPhase implements IBroadPhase, IDynamicTreeCallback
	{
		public function b2DynamicTreeBroadPhase()
		{	
		}
		
		/**
		 * Create a proxy with an initial AABB. Pairs are not reported until
		 * UpdatePairs is called.
		 */
		
		private var c_proxy:b2DynamicTreeNode;
		
		public function CreateDynamicTreeNodeProxyForFixture(aabb:b2AABB, userData:b2Fixture):b2DynamicTreeNode
		{
			c_proxy = m_tree.CreateDynamicTreeNodeProxyForFixture(aabb, userData);
			++m_proxyCount;
			m_moveBuffer[m_moveBuffer.length] = c_proxy;
				
			return c_proxy;
		}
		
		/**
		 * Destroy a proxy. It is up to the client to remove any pairs.
		 */
		public function DestroyDynamicTreeNodeProxy(node:b2DynamicTreeNode):void
		{
			ub_i = m_moveBuffer.indexOf(node);
			
			if(ub_i > -1)
			{
				m_moveBuffer.removeAt(ub_i);
			}
			
			--m_proxyCount;
			m_tree.DestroyProxy(node);
		}
		
		/**
		 * Call MoveProxy as many times as you like, then when you are done
		 * call UpdatePairs to finalized the proxy pairs (for your time step).
		 */
		[Inline]
		final public function MoveDynamicTreeNodeProxy(proxy:b2DynamicTreeNode, aabb:b2AABB, displacement:b2Vec2):void
		{
			if(m_tree.MoveProxy(proxy, aabb, displacement))
			{
				m_moveBuffer[m_moveBuffer.length] = proxy;
			}
		}
		
		[Inline]
		final public function TestDynamicTreeNodeOverlap(proxyA:b2DynamicTreeNode, proxyB:b2DynamicTreeNode):Boolean
		{
			return proxyA.aabb.TestOverlap(proxyB.aabb);
		}
		
		/**
		 * Get user data from a proxy. Returns null if the proxy is invalid.
		 */
		public function GetDynamicTreeNodeUserData(proxy:b2DynamicTreeNode):b2Fixture
		{
			return proxy.userData;
		}
		
		/**
		 * Get the AABB for a proxy.
		 */
		public function GetFatAABB(proxy:*):b2AABB
		{
			return proxy.aabb;
		}
		
		/**
		 * Get the number of proxies.
		 */
		public function GetProxyCount():int
		{
			return m_proxyCount;
		}
		
		public function QueryProxy(node:b2Proxy):Boolean
		{
			return false;
		}
			
		private var _currentQueryProxy:b2DynamicTreeNode;
		private var upc_pair:b2DynamicTreePair;
		
		[Inline]
		final public function QueryDynamicTreeNodeCallback(node:b2DynamicTreeNode):Boolean
		{
			// A proxy cannot form a pair with itself.
			if(node == _currentQueryProxy)
			{
				return true;			
			}
			
			// Grow the pair buffer as needed
			if(m_pairCount == m_pairBuffer.length)
			{
				m_pairBuffer[m_pairCount] = new b2DynamicTreePair();
			}
			
			/***
			 * OLD CODE
				pair.proxyA = proxy < _currentQueryProxy ? proxy : _currentQueryProxy;
				pair.proxyB = proxy >= _currentQueryProxy ? proxy : _currentQueryProxy;
			****/
	
			upc_pair = m_pairBuffer[m_pairCount];
			upc_pair.proxyA = _currentQueryProxy;
			upc_pair.proxyB = node;
			
			if(node.nodeId < _currentQueryProxy.nodeId)
			{
				upc_pair.proxyA = node;
				upc_pair.proxyB = _currentQueryProxy;
			}
			else
			{
				upc_pair.proxyA = _currentQueryProxy;
				upc_pair.proxyB = node;
			}
			
			++m_pairCount;
	
			return true;
		}
		
		/**
		 * Update the pairs. This results in pair callbacks. This can only add pairs.
		 */
		
		private var up_i:int;
		private var up_len:int;
		
		private var up_pair:b2DynamicTreePair;
		private var up_primaryPair:b2DynamicTreePair;
		
		private static var fixtureA:b2Fixture;
		private static var fixtureB:b2Fixture;
		
		private static var ap_otherFixtureA:b2Fixture;
		private static var ap_otherFixtureB:b2Fixture;

		private static var ap_bodyA:b2Body;
		private static var ap_bodyB:b2Body;

		private static var ap_edge:b2ContactEdge;

		private static var updatePairs:Boolean;
		private static var isDuplicate:Boolean;

		private var _queryStackVector:Vector.<b2DynamicTreeNode> = new Vector.<b2DynamicTreeNode>;

		private static var to_d1X:Number;
		private static var to_d1Y:Number;

		private static var to_d2X:Number;
		private static var to_d2Y:Number;

		private static var isOverlapping:Boolean;

		private var q_count:int;

		private var q_node:b2DynamicTreeNode;
		private var q_proceed:Boolean;

		[Inline]
		final public function UpdatePairs(callback:IBroadPhaseUpdatePairCallback):void
		{
			m_pairCount = 0;

			// Perform tree queries for all moving queries
			for(up_i = 0, up_len = m_moveBuffer.length; up_i < up_len; up_i++)
			{
				_currentQueryProxy = m_moveBuffer[up_i];

				// We have to query the tree with the fat AABB so that
				// we don't fail to create a pair that may touch later.
				
				
				//m_tree.Query(this, _currentQueryProxy.aabb);
				
				if(m_tree.m_root == null)
				{
					continue;				
				}
				
				_queryStackVector.length = 0;
				
				q_count = 0;
				
				_queryStackVector[q_count++] = m_tree.m_root;
				
				while(q_count > 0)
				{
					q_node = _queryStackVector[--q_count];
					
					//if(q_node.aabb.TestOverlap(aabb))
					
					isOverlapping = true;
					
					to_d1X = _currentQueryProxy.aabb.lowerBound.x - q_node.aabb.upperBound.x;
					to_d1Y = _currentQueryProxy.aabb.lowerBound.y - q_node.aabb.upperBound.y;
					to_d2X = q_node.aabb.lowerBound.x - _currentQueryProxy.aabb.upperBound.x;
					to_d2Y = q_node.aabb.lowerBound.y - _currentQueryProxy.aabb.upperBound.y;
					
					
					if(to_d1X > b2Settings.DECIMAL_ZERO || to_d1Y > b2Settings.DECIMAL_ZERO)
					{
						isOverlapping = false;				
					}
					
					if(to_d2X > b2Settings.DECIMAL_ZERO || to_d2Y > b2Settings.DECIMAL_ZERO)
					{
						isOverlapping = false;				
					}				
					
					/**
					 *  
					 * END OF q_node.aabb.TestOverlap(aabb)
					 * 
					 */
					
					
					if(isOverlapping)
					{
						// isLeaf
						if(q_node.child1 == null)
						{
							//q_proceed = QueryDynamicTreeNodeCallback(q_node);
							
							q_proceed = true;
							
							// A proxy cannot form a pair with itself.
							if(q_node.nodeId == _currentQueryProxy.nodeId)
							{
								// it looks like this needs to be false, in original code this is true
								q_proceed = false;	
							}
							
							// Grow the pair buffer as needed
							if(m_pairCount == m_pairBuffer.length)
							{
								m_pairBuffer[m_pairCount] = new b2DynamicTreePair();
							}
							
							upc_pair = m_pairBuffer[m_pairCount];
							
							//upc_pair.proxyA = _currentQueryProxy;
							//upc_pair.proxyB = q_node;
							
							if(q_node.nodeId < _currentQueryProxy.nodeId)
							{
								upc_pair.proxyA = q_node;
								upc_pair.proxyB = _currentQueryProxy;
							}
							else
							{
								upc_pair.proxyA = _currentQueryProxy;
								upc_pair.proxyB = q_node;
							}
							
							++m_pairCount;
							
							q_proceed = true;
							
							/**
							 *  
							 * END OF QueryDynamicTreeNodeCallback(q_node);
							 * 
							 */
							
							
							if(!q_proceed)
							{
								// Break out of while(q_count > 0) loop
								break;
							}
						}
						else
						{
							// No stack limit, so no assert
							_queryStackVector[q_count++] = q_node.child1;
							_queryStackVector[q_count++] = q_node.child2;
						}
					}
				}
				
				
				/**
				 *  
				 * END OF m_tree.Query(this, _currentQueryProxy.aabb);
				 * 
				 */
			}


			// Reset move buffer
			m_moveBuffer.length = 0;

			// Sort the pair buffer to expose duplicates.
			// TODO: Something more sensible
			//m_pairBuffer.sort(ComparePairs);

			//m_pairBuffer.sort(ComparePairs);

			// Send the pair buffer
			for(up_i = 0; up_i < m_pairCount;)
			{
				up_primaryPair = m_pairBuffer[up_i];

				fixtureA = up_primaryPair.proxyA.userData;
				fixtureB = up_primaryPair.proxyB.userData;

				ap_bodyA = fixtureA.m_body;
				ap_bodyB = fixtureB.m_body;

				if(ap_bodyA != ap_bodyB)
				{
					// Does a contact already exist?
					ap_edge = ap_bodyB.m_contactList;

					updatePairs = true;

					while(ap_edge)
					{
						if(ap_edge.other == ap_bodyA)
						{
							ap_otherFixtureA = ap_edge.contact.m_fixtureA;
							ap_otherFixtureB = ap_edge.contact.m_fixtureB;

							if(ap_otherFixtureA == fixtureA && ap_otherFixtureB == fixtureB)
							{
								updatePairs = false;
								break;
							}

							if(ap_otherFixtureA == fixtureB && ap_otherFixtureB == fixtureA)
							{
								updatePairs = false;
								break;
							}
						}

						ap_edge = ap_edge.next;
					}

					if(updatePairs)
					{
						callback.updatePairsCallback(
							fixtureA,
							fixtureB
						);
					}
				}

				++up_i;
				
				
				/*****
				
				 * 
				 * THIS SEAMS TO NEVER FIND ANY DUPLICATES
				 */ 

				// Skip any duplicate pairs
				/*while(up_i < m_pairCount)
				{
					up_pair = m_pairBuffer[up_i];

					if(up_pair.proxyA.nodeId != up_primaryPair.proxyA.nodeId || 
						up_pair.proxyB.nodeId != up_primaryPair.proxyB.nodeId)
					{
						break;
					}

					++up_i;
				}*/

			} // END OF for(up_i = 0; up_i < m_pairCount;)
		}

		/**
		 * @inheritDoc
		 */
		[Inline]
		final public function Query(callback:IDynamicTreeCallback, aabb:b2AABB):void
		{
			m_tree.Query(callback, aabb);
		}
		
		/**
		 * @inheritDoc
		 */
		[Inline]
		final public function RayCast(callback:IBroadPhaseRayCastCallback, input:b2RayCastInput):void
		{
			m_tree.RayCast(callback, input);
		}
		
		
		public function Validate():void
		{
			//TODO_BORIS
		}
		
		[Inline]
		final public function Rebalance(iterations:int):void
		{
			m_tree.Rebalance(iterations);
		}
		
		
		// Private ///////////////
		
		private var ub_i:int;

		private static function ComparePairs(pair1:b2DynamicTreePair, pair2:b2DynamicTreePair):int
		{
			//TODO_BORIS:
			// We cannot consistently sort objects easily in AS3
			// The caller of this needs replacing with a different method.

			if(pair1.proxyA.nodeId < pair2.proxyA.nodeId)
			{
				return -1;
			}

			if(pair1.proxyA.nodeId == pair2.proxyA.nodeId)
			{
				//return pair1.proxyB.nodeId < pair2.proxyB.nodeId ? -1 : pair1.proxyB.nodeId == pair2.proxyB.nodeId ? 0 : 1;
				
				if(pair1.proxyB.nodeId < pair2.proxyB.nodeId)
				{
					return -1;
				}
				else 
				{
					if(pair1.proxyB.nodeId == pair2.proxyB.nodeId)
					{
						return 0;
					}
					else
					{
						return 1;
					}
				}
			}

			return 1;
		}

		public var m_tree:b2DynamicTree = new b2DynamicTree();

		private var m_proxyCount:int;
		private var m_moveBuffer:Vector.<b2DynamicTreeNode> = new Vector.<b2DynamicTreeNode>();
		
		private var m_pairBuffer:Vector.<b2DynamicTreePair> = new Vector.<b2DynamicTreePair>();
		private var m_pairCount:uint = 0;
	}
}