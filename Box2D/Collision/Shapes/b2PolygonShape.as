﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Collision.Shapes
{
	import Box2D.Collision.b2AABB;
	import Box2D.Collision.b2OBB;
	import Box2D.Collision.b2RayCastInput;
	import Box2D.Collision.b2RayCastOutput;
	import Box2D.Common.b2Pool;
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Mat22;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Transform;
	import Box2D.Common.Math.b2Vec2;
	
	/**
	* Convex polygon. The vertices must be in CCW order for a right-handed
	* coordinate system with the z-axis coming out of the screen.
	* @see b2PolygonDef
	*/
	
	public class b2PolygonShape extends b2Shape
	{
		public override function Set(other:b2Shape):void 
		{
			super.Set(other);

			if(other is b2PolygonShape)
			{
				var other2:b2PolygonShape = other as b2PolygonShape;

				m_centroid.SetV(other2.m_centroid);
				m_vertexCount = other2.m_vertexCount;

				var vert:b2Vec2;
				var normal:b2Vec2;

				for(var i:int = 0; i < m_vertexCount; i++)
				{
					vert = other2.m_vertices[i];
					normal = other2.m_normals[i];
					
					m_vertices[m_vertices.length] = b2Pool.getVector2(
						vert.x,
						vert.y
					);
						
					m_normals[m_normals.length] = b2Pool.getVector2(
						normal.x,
						normal.y
					);
				}
			}
		}
	
		private static var count:int = 0;

		final override public function Destroy():void
		{
			b2Pool.putVector2(m_centroid);

			count = m_vertexCount;

			while(--count > -1)
			{
				b2Pool.putVector2(m_vertices.pop());
				b2Pool.putVector2(m_normals.pop());
			}

			if(m_distanceProxy.m_vertices != m_normals)
			{
				b2Pool.putVector2Vector(m_normals);
			}

			if(m_distanceProxy.m_vertices != m_vertices)
			{
				b2Pool.putVector2Vector(m_vertices);					
			}

			m_vertices = null;
			m_normals = null;

			m_centroid = null;

			super.Destroy();
		}
		
		override public function Reset():void
		{
			super.Reset();
			
			m_type = e_polygonShape;

			m_vertexCount = 0;

			c_tVec = null;
			
			c_lowerX = 0;
			c_lowerY = 0;
			
			c_upperX = 0;
			c_upperY = 0;
			
			c_vX = 0;
			c_vY = 0;
			
			c_i = 0;
			
			rc_lower = 0;
			rc_upper = 0;
			
			rc_tX = 0;
			rc_tY = 0;
			
			
			rc_tMat = null;
			rc_tVec = null;
			
			rc_p1X = 0;
			rc_p1Y = 0;
			
			rc_p2X = 0;
			rc_p2Y = 0;
			
			rc_dX = 0;
			rc_dY = 0;
			
			rc_index = 0;
			
			rc_i = 0;
			
			rc_numerator = 0;
			rc_denominator = 0;
			
			c_lowerX = 0;
			c_lowerY = 0;
			
			c_upperX = 0;
			c_upperY = 0;
			
			c_vX = 0;
			c_vY = 0;
			
			c_i = 0;
			
			
			cm_centerX = 0;
			cm_centerY = 0;
			cm_area = 0;
			cm_I = 0;
			
			cm_p1X = 0;
			cm_p1Y = 0;
			
			cm_p2 = null;
			cm_p3 = null;
			
			cm_e1X = 0;
			cm_e1Y = 0;
			
			cm_e2X = 0;
			cm_e2Y = 0;
			
			cm_D = 0;
			cm_triangleArea = 0;
			
			cm_intx2 = 0;
			cm_inty2 = 0;
			
			cm_i = 0;
		}
		
		/**
		 * Copy vertices. This assumes the vertices define a convex polygon.
		 * It is assumed that the exterior is the the right of each edge.
		 */
		public function SetAsArray(vertices:Array, vertexCount:Number = 0):void
		{
			var v:Vector.<b2Vec2> = new Vector.<b2Vec2>();
			
			for each(var tVec:b2Vec2 in vertices)
			{
				v[v.length] = tVec;
			}
			
			SetAsVector(v, vertexCount);
		}
		
		public static function AsArray(vertices:Array, vertexCount:Number):b2PolygonShape
		{
			var polygonShape:b2PolygonShape = new b2PolygonShape();
			polygonShape.SetAsArray(vertices, vertexCount);
			return polygonShape;
		}
		
		private static var i1:int;
		private static var i2:int;
		
		private static var edge:b2Vec2 = new b2Vec2(0, 0);
		private static var crossVF:b2Vec2 = new b2Vec2(0, 0);
		
		private static var vertice:b2Vec2, normal:b2Vec2, currentVertexLen:uint, currentVertice:b2Vec2, currentNormal:b2Vec2;
		
		private static var verticeI1:b2Vec2;
		private static var verticeI2:b2Vec2;
		
		private static var lengthSquared:Number;

		/**
		 * Copy vertices. This assumes the vertices define a convex polygon.
		 * It is assumed that the exterior is the the right of each edge.
		 */
		public function SetAsVector(vertices:Vector.<b2Vec2>, vertexCount:uint = 0):void
		{
			if(vertexCount == 0)
			{
				vertexCount = vertices.length;			
			}

			// b2Settings.b2Assert(2 <= vertexCount);
			if(vertexCount < 3)
			{
				throw Error("Polygon shape needs at least 3 vertices");
			}

			m_vertexCount = vertexCount;
			currentVertexLen = m_vertices.length;

			// Copy vertices
			for(i = 0; i < m_vertexCount; i++)
			{
				vertice = vertices[i];

				if(i < currentVertexLen)
				{
					currentVertice = m_vertices[i];
					currentVertice.x = vertice.x;
					currentVertice.y = vertice.y;
				
					currentNormal = m_normals[i];
					currentNormal.x = 0;
					currentNormal.y = 0;
				}
				else
				{
					m_vertices[i] = b2Pool.getVector2(vertice.x, vertice.y);
					m_normals[i] = b2Pool.getVector2(0, 0);
				}
			}

			// Compute normals. Ensure the edges have non-zero length.
			for(i = 0; i < m_vertexCount; ++i)
			{
				i1 = i;
				i2 = i + 1 < m_vertexCount ? i + 1 : 0;

				verticeI1 = m_vertices[i1];
				verticeI2 = m_vertices[i2];
				
				edge.x = verticeI2.x - verticeI1.x;
				edge.y = verticeI2.y - verticeI1.y;

				// LengthSquared():Number
				lengthSquared = (edge.x * edge.x) + (edge.y * edge.y);

				//b2Settings.b2Assert(lengthSquared > Number.MIN_VALUE);
				if(lengthSquared <= Number.MIN_VALUE)
				{
					throw Error("Polygon vertex data invalid");
				}	

				//b2Math.CrossVF(edge, b2Settings.DECIMAL_ONE, crossVF);
				crossVF.x = b2Settings.DECIMAL_ONE * edge.y;
				crossVF.y = -b2Settings.DECIMAL_ONE * edge.x;	
				
				vertice = m_normals[i];

				vertice.x = crossVF.x;
				vertice.y = crossVF.y;

				vertice.Normalize();
			}

			verticeI1 = null;
			verticeI2 = null;

			ComputeCentroid(m_vertices, m_vertexCount, m_centroid);
		}
		
		public static function AsVector(vertices:Vector.<b2Vec2>, vertexCount:Number):b2PolygonShape
		{
			var polygonShape:b2PolygonShape = new b2PolygonShape();
			polygonShape.SetAsVector(vertices, vertexCount);
			return polygonShape;
		}
		
		/**
		* Build vertices to represent an axis-aligned box.
		* @param hx the half-width.
		* @param hy the half-height.
		*/
		public function SetAsBox(hx:Number, hy:Number) : void 
		{
			m_vertexCount = 4;
			Reserve(4);
			m_vertices[0].Set(-hx, -hy);
			m_vertices[1].Set( hx, -hy);
			m_vertices[2].Set( hx,  hy);
			m_vertices[3].Set(-hx,  hy);
			m_normals[0].Set(b2Settings.DECIMAL_ZERO, -1.0);
			m_normals[1].Set(1.0, b2Settings.DECIMAL_ZERO);
			m_normals[2].Set(b2Settings.DECIMAL_ZERO, 1.0);
			m_normals[3].Set(-1.0, b2Settings.DECIMAL_ZERO);
			m_centroid.SetZero();
		}
		
		public static function AsBox(hx:Number, hy:Number):b2PolygonShape
		{
			var polygonShape:b2PolygonShape = new b2PolygonShape();
			polygonShape.SetAsBox(hx, hy);
			return polygonShape;
		}
		
		/**
		* Build vertices to represent an oriented box.
		* @param hx the half-width.
		* @param hy the half-height.
		* @param center the center of the box in local coordinates.
		* @param angle the rotation of the box in local coordinates.
		*/

		private static var sob_transform:b2Transform
		private static var sob_verticeResult:b2Vec2 = new b2Vec2();
		private static var sob_normalResult:b2Vec2 = new b2Vec2();
		private static var reserveVerticesCount:int;
		private static var currentVerticesListCount:uint;
		
		public function SetAsOrientedBox(hx:Number, hy:Number, center:b2Vec2 = null, angle:Number = 0.0):void
		{
			m_vertexCount = 4;

			currentVerticesListCount = m_vertices.length;
			
			if(currentVerticesListCount < 4)
			{
				reserveVerticesCount = 4 - currentVerticesListCount;

				for(i = currentVerticesListCount; i < reserveVerticesCount; i++)
				{
					m_vertices[i] = b2Pool.getVector2(0, 0);
					m_normals[i] = b2Pool.getVector2(0, 0);
				}
			}

			vertice = m_vertices[0];
			vertice.x = -hx;
			vertice.y = -hy;
			
			vertice = m_vertices[1];
			vertice.x = hx;
			vertice.y = -hy;
			
			vertice = m_vertices[2];
			vertice.x = hx;
			vertice.y = hy;
			
			vertice = m_vertices[3];
			vertice.x = -hx;
			vertice.y = hy;
			
			
			normal = m_normals[0]; 
			normal.x = b2Settings.DECIMAL_ZERO;
			normal.y = -b2Settings.DECIMAL_ONE;
			
			normal = m_normals[1]; 
			normal.x = b2Settings.DECIMAL_ONE;
			normal.y = b2Settings.DECIMAL_ZERO;
			
			normal = m_normals[2]; 
			normal.x = b2Settings.DECIMAL_ZERO;
			normal.y = b2Settings.DECIMAL_ONE;
			
			normal = m_normals[3]; 
			normal.x = -b2Settings.DECIMAL_ONE;
			normal.y = b2Settings.DECIMAL_ZERO;


			m_centroid.x = center.x;
			m_centroid.y = center.y;

			sob_transform.position.x = center.x;
			sob_transform.position.y = center.y;	
			sob_transform.R.Set(angle);

			// Transform vertices and normals.
			for(i = 0; i < m_vertexCount; ++i)
			{
				vertice = m_vertices[i];

				b2Math.MulX(sob_transform, vertice, sob_verticeResult);

				vertice.x = sob_verticeResult.x;
				vertice.y = sob_verticeResult.y;


				normal = m_normals[i];

				//b2Math.MulMV(sob_transform.R, normal, sob_normalResult);				
				sob_normalResult.x = sob_transform.R.col1.x * normal.x + sob_transform.R.col2.x * normal.y;
				sob_normalResult.y = sob_transform.R.col1.y * normal.x + sob_transform.R.col2.y * normal.y;

				normal.x = sob_normalResult.x;
				normal.y = sob_normalResult.y;
			}
		}
		
		public function calculateVertexNormal(vertexIndex:uint):void
		{
			i1 = vertexIndex;
			i2 = vertexIndex + 1 < m_vertexCount ? vertexIndex + 1 : 0;
			
			verticeI1 = m_vertices[i1];
			verticeI2 = m_vertices[i2];
			
			edge.x = verticeI2.x - verticeI1.x;
			edge.y = verticeI2.y - verticeI1.y;
			
			// LengthSquared():Number
			lengthSquared = (edge.x * edge.x) + (edge.y * edge.y);
			
			//b2Settings.b2Assert(lengthSquared > Number.MIN_VALUE);
			if(lengthSquared <= Number.MIN_VALUE)
			{
				throw Error("Polygon vertex data invalid");
			}

			//b2Math.CrossVF(edge, b2Settings.DECIMAL_ONE, crossVF);
			crossVF.x = b2Settings.DECIMAL_ONE * edge.y;
			crossVF.y = -b2Settings.DECIMAL_ONE * edge.x;	
			
			vertice = m_normals[vertexIndex];
			
			vertice.x = crossVF.x;
			vertice.y = crossVF.y;
			
			vertice.Normalize();
		}

		public static function AsOrientedBox(hx:Number, hy:Number, center:b2Vec2 = null, angle:Number = 0.0):b2PolygonShape
		{
			var polygonShape:b2PolygonShape = new b2PolygonShape();
			polygonShape.SetAsOrientedBox(hx, hy, center, angle);
			return polygonShape;
		}
		
		/**
		 * Set this as a single edge.
		 */
		public function SetAsEdge(v1:b2Vec2, v2:b2Vec2):void
		{
			m_vertexCount = 2;
			Reserve(2);
			m_vertices[0].SetV(v1);
			m_vertices[1].SetV(v2);
			m_centroid.x = 0.5 * (v1.x + v2.x);
			m_centroid.y = 0.5 * (v1.y + v2.y);
			
			var subtractedVector:b2Vec2 = b2Pool.getVector2(0, 0);
			
			b2Math.SubtractVV(v2, v1, subtractedVector);
				
			b2Math.CrossVF(subtractedVector, 1.0, m_normals[0]); // is m_normals[0] null?
			
			b2Pool.putVector2(subtractedVector);
			
			m_normals[0].Normalize();
			m_normals[1].x = -m_normals[0].x;
			m_normals[1].y = -m_normals[0].y;
		}
		
		/**
		 * Set this as a single edge.
		 */
		static public function AsEdge(v1:b2Vec2, v2:b2Vec2):b2PolygonShape
		{
			var polygonShape:b2PolygonShape = new b2PolygonShape();
			polygonShape.SetAsEdge(v1, v2);
			return polygonShape;
		}

		private static var dot:Number;
		private static var tX:Number;
		private static var tY:Number;
		private static var pLocalX:Number;
		private static var pLocalY:Number;
		
		private static var tVec:b2Vec2;
		private static var tMat:b2Mat22;
		
		/**
		* @inheritDoc
		*/
		public override function TestPoint(xf:b2Transform, p:b2Vec2):Boolean
		{
			tMat = xf.R;
			
			tX = p.x - xf.position.x;
			tY = p.y - xf.position.y;
			
			pLocalX = (tX*tMat.col1.x + tY*tMat.col1.y);
			pLocalY = (tX*tMat.col2.x + tY*tMat.col2.y);
			
			for(i = 0; i < m_vertexCount; ++i)
			{
				//float32 dot = b2Dot(m_normals[i], pLocal - m_vertices[i]);
				tVec = m_vertices[i];
				tX = pLocalX - tVec.x;
				tY = pLocalY - tVec.y;
				tVec = m_normals[i];
				
				dot = (tVec.x * tX + tVec.y * tY);

				if(dot > b2Settings.DECIMAL_ZERO)
				{
					return false;
				}
			}
			
			return true;
		}
		
		
		private static var rc_lower:Number;
		private static var rc_upper:Number;
		
		private static var rc_tX:Number;
		private static var rc_tY:Number;
		
		
		private static var rc_tMat:b2Mat22;
		private static var rc_tVec:b2Vec2;
		
		private static var rc_p1X:Number;
		private static var rc_p1Y:Number;
		
		private static var rc_p2X:Number;
		private static var rc_p2Y:Number;
		
		private static var rc_dX:Number;
		private static var rc_dY:Number;
		
		private static var rc_index:int;
		
		private static var rc_i:int;
		
		private static var rc_numerator:Number;
		private static var rc_denominator:Number;
		
		/**
		 * @inheritDoc
		 */
		[Inline]
		final public override function RayCast(output:b2RayCastOutput, input:b2RayCastInput, transform:b2Transform):Boolean
		{
			rc_lower = b2Settings.DECIMAL_ZERO;
			rc_upper = input.maxFraction;

			rc_tX = input.p1.x - transform.position.x;
			rc_tY = input.p1.y - transform.position.y;
			rc_tMat = transform.R;
			
			rc_p1X = (rc_tX * rc_tMat.col1.x + rc_tY * rc_tMat.col1.y);
			rc_p1Y = (rc_tX * rc_tMat.col2.x + rc_tY * rc_tMat.col2.y);

			rc_tX = input.p2.x - transform.position.x;
			rc_tY = input.p2.y - transform.position.y;
			rc_tMat = transform.R;

			rc_p2X = (rc_tX * rc_tMat.col1.x + rc_tY * rc_tMat.col1.y);
			rc_p2Y = (rc_tX * rc_tMat.col2.x + rc_tY * rc_tMat.col2.y);

			rc_dX = rc_p2X - rc_p1X;
			rc_dY = rc_p2Y - rc_p1Y;
			rc_index = -1;
			
			for (rc_i = 0; rc_i < m_vertexCount; ++rc_i)
			{
				rc_tVec = m_vertices[rc_i];
				rc_tX = rc_tVec.x - rc_p1X;
				rc_tY = rc_tVec.y - rc_p1Y;
				rc_tVec = m_normals[rc_i];
				
				rc_numerator = (rc_tVec.x*rc_tX + rc_tVec.y*rc_tY);

				rc_denominator = (rc_tVec.x * rc_dX + rc_tVec.y * rc_dY);
				
				if (rc_denominator == b2Settings.DECIMAL_ZERO)
				{
					if (rc_numerator < b2Settings.DECIMAL_ZERO)
					{
						return false;
					}
				}
				else
				{
					// Note: we want this predicate without division:
					// lower < numerator / denominator, where denominator < 0
					// Since denominator < 0, we have to flip the inequality:
					// lower < numerator / denominator <==> denominator * lower > numerator.
					if (rc_denominator < b2Settings.DECIMAL_ZERO && rc_numerator < rc_lower * rc_denominator)
					{
						// Increase lower.
						// The segment enters this half-space.
						rc_lower = rc_numerator / rc_denominator;
						rc_index = rc_i;
					}
					else if (rc_denominator > b2Settings.DECIMAL_ZERO && rc_numerator < rc_upper * rc_denominator)
					{
						// Decrease upper.
						// The segment exits this half-space.
						rc_upper = rc_numerator / rc_denominator;
					}
				}
				
				if (rc_upper < rc_lower - Number.MIN_VALUE)
				{
					return false;
				}
			}
			
			if (rc_index >= 0)
			{
				output.fraction = rc_lower;

				rc_tMat = transform.R;
				rc_tVec = m_normals[rc_index];
				output.normal.x = (rc_tMat.col1.x * rc_tVec.x + rc_tMat.col2.x * rc_tVec.y);
				output.normal.y = (rc_tMat.col1.y * rc_tVec.x + rc_tMat.col2.y * rc_tVec.y);
				return true;
			}
			
			return false;
		}
	

		private static var c_tVec:b2Vec2;
		
		private static var c_lowerX:Number;
		private static var c_lowerY:Number;
		
		private static var c_upperX:Number;
		private static var c_upperY:Number;
		
		private static var c_vX:Number;
		private static var c_vY:Number;
		
		private static var c_i:int;
		

		/**
		 * @inheritDoc
		 */
		[Inline]
		final public override function ComputeAABB(aabb:b2AABB, xf:b2Transform):void
		{
			c_tVec = m_vertices[0];

			c_lowerX = xf.position.x + (xf.R.col1.x * c_tVec.x + xf.R.col2.x * c_tVec.y);
			c_lowerY = xf.position.y + (xf.R.col1.y * c_tVec.x + xf.R.col2.y * c_tVec.y);
			c_upperX = c_lowerX;
			c_upperY = c_lowerY;

			for(c_i = 1; c_i < m_vertexCount; ++c_i)
			{
				c_tVec = m_vertices[c_i];
				
				c_vX = xf.position.x + (xf.R.col1.x * c_tVec.x + xf.R.col2.x * c_tVec.y);
				c_vY = xf.position.y + (xf.R.col1.y * c_tVec.x + xf.R.col2.y * c_tVec.y);

				c_lowerX = c_lowerX < c_vX ? c_lowerX : c_vX;
				c_lowerY = c_lowerY < c_vY ? c_lowerY : c_vY;
				c_upperX = c_upperX > c_vX ? c_upperX : c_vX;
				c_upperY = c_upperY > c_vY ? c_upperY : c_vY;
			}

			aabb.lowerBound.x = c_lowerX - m_radius;
			aabb.lowerBound.y = c_lowerY - m_radius;
			aabb.upperBound.x = c_upperX + m_radius;
			aabb.upperBound.y = c_upperY + m_radius;
		}
		
		private static const k_inv3:Number = 1.0 / 3.0;
	
		private static var cm_centerX:Number;
		private static var cm_centerY:Number;
		private static var cm_area:Number;
		private static var cm_I:Number;
		
		private static var cm_p1X:Number;
		private static var cm_p1Y:Number;
		
		private static var cm_p2:b2Vec2;
		private static var cm_p3:b2Vec2;
		
		private static var cm_e1X:Number;
		private static var cm_e1Y:Number;
		
		private static var cm_e2X:Number;
		private static var cm_e2Y:Number;
		
		private static var cm_D:Number;
		private static var cm_triangleArea:Number;
		
		private static var cm_intx2:Number;
		private static var cm_inty2:Number;
		
		private static var cm_i:int;
		
		private static var _vertice1:b2Vec2;
		private static var _vertice2:b2Vec2;
		
		/**
		* @inheritDoc
		*/
		[Inline]
		final public override function ComputeMass(massData:b2MassData, density:Number):void
		{
			// shape is destroyed
			if(m_vertexCount == 0)
			{
				return;
			}
			
			// A line segment has zero mass.
			if(m_vertexCount == 2)
			{
				_vertice1 = m_vertices[0];
				_vertice2 = m_vertices[1];
					
				massData.center.x = 0.5 * (_vertice1.x + _vertice2.x);
				massData.center.y = 0.5 * (_vertice1.y + _vertice2.y);

				massData.mass = b2Settings.DECIMAL_ZERO;
				massData.I = b2Settings.DECIMAL_ZERO;
				
				_vertice1 = null;
				_vertice2 = null;
				
				vertice
				return;
			}

			//b2Vec2 center; center.Set(0.0f, 0.0f);
			cm_centerX = b2Settings.DECIMAL_ZERO;
			cm_centerY = b2Settings.DECIMAL_ZERO;

			cm_area = b2Settings.DECIMAL_ZERO;
			cm_I = b2Settings.DECIMAL_ZERO;

			cm_p1X = b2Settings.DECIMAL_ZERO;
			cm_p1Y = b2Settings.DECIMAL_ZERO;
			
			for(cm_i = 0; cm_i < m_vertexCount; ++cm_i)
			{
				cm_p2 = m_vertices[cm_i];

				cm_p3 = cm_i + 1 < m_vertexCount ? m_vertices[cm_i+1] : m_vertices[0];

				cm_e1X = cm_p2.x - cm_p1X;
				cm_e1Y = cm_p2.y - cm_p1Y;

				cm_e2X = cm_p3.x - cm_p1X;
				cm_e2Y = cm_p3.y - cm_p1Y;

				cm_D = cm_e1X * cm_e2Y - cm_e1Y * cm_e2X;
				
				//float32 triangleArea = 0.5f * D;
				cm_triangleArea = 0.5 * cm_D;
				cm_area += cm_triangleArea;
				
				// Area weighted centroid
				//center += triangleArea * k_inv3 * (p1 + p2 + p3);
				cm_centerX += cm_triangleArea * k_inv3 * (cm_p1X + cm_p2.x + cm_p3.x);
				cm_centerY += cm_triangleArea * k_inv3 * (cm_p1Y + cm_p2.y + cm_p3.y);
				
				cm_intx2 = k_inv3 * (0.25 * (cm_e1X*cm_e1X + cm_e2X*cm_e1X + cm_e2X*cm_e2X) + (cm_p1X*cm_e1X + cm_p1X*cm_e2X)) + 0.5*cm_p1X*cm_p1X;
				cm_inty2 = k_inv3 * (0.25 * (cm_e1Y*cm_e1Y + cm_e2Y*cm_e1Y + cm_e2Y*cm_e2Y) + (cm_p1Y*cm_e1Y + cm_p1Y*cm_e2Y)) + 0.5*cm_p1Y*cm_p1Y;
				
				cm_I += cm_D * (cm_intx2 + cm_inty2);
			}

			// Total mass
			massData.mass = density * cm_area;

			cm_centerX *= 1.0 / cm_area;
			cm_centerY *= 1.0 / cm_area;

			massData.center.x = cm_centerX;
			massData.center.y = cm_centerY;
			
			// Inertia tensor relative to the local origin.
			massData.I = density * cm_I;
		}
	
		/**
		* @inheritDoc
		*/
		
		private static var normalL:b2Vec2 = new b2Vec2();
		private static var depths:Vector.<Number> = new Vector.<Number>;

		private static var offsetL:Number;
		private static var diveCount:int;
		private static var intoIndex:int;
		private static var outoIndex:int;
		private static var lastSubmerged:Boolean;
		private static var isSubmerged:Boolean;
		private static var submergedArea:Number;
		
		private static var massData:b2MassData = new b2MassData();
		
		private static var i:int;

		private static var intoIndex2:int;
		private static var outoIndex2:int;
		private static var intoLamdda:Number;
		private static var outoLamdda:Number;
		
		private static var intoVec:b2Vec2 = new b2Vec2();
		private static var outoVec:b2Vec2 = new b2Vec2();
		
		private static var area:Number;
		private static var center:b2Vec2 = new b2Vec2();
		
		private static var p2:b2Vec2;
		private static var p3:b2Vec2;

		private static var triangleArea:Number;
		private static var vertext:b2Vec2;

		public override function ComputeSubmergedArea(normal:b2Vec2,
													  offset:Number,
													  xf:b2Transform,
													  c:b2Vec2):Number
		{
			depths.length = 0;
			
			// Transform plane into shape co-ordinates	
			b2Math.MulTMV(xf.R, normal, normalL);
			
			//offsetL = offset - b2Math.Dot(normal, xf.position);
			offsetL = offset - (normal.x * xf.position.x + normal.y * xf.position.y);			

			
			diveCount = 0;
			intoIndex = -1;
			outoIndex = -1;
			
			lastSubmerged = false;

			for (i = 0; i < m_vertexCount;++i)
			{
				vertext = m_vertices[i];

				//depths[i] = b2Math.Dot(normalL, m_vertices[i]) - offsetL;
				depths[i] = (normalL.x * vertext.x + normalL.y * vertext.y) - offsetL;

				isSubmerged = depths[i] < -Number.MIN_VALUE;
	
				if (i > 0)
				{
					if (isSubmerged)
					{
						if (!lastSubmerged)
						{
							intoIndex = i - 1;
							diveCount++;
						}
					}
					else
					{
						if (lastSubmerged)
						{
							outoIndex = i - 1;
							diveCount++;
						}
					}
				}
	
				lastSubmerged = isSubmerged;
			}
	
			switch(diveCount)
			{
				case 0:

					if(lastSubmerged)
					{
						// Completely submerged
						ComputeMass(massData, 1);
						b2Math.MulX(xf, massData.center, c);
						submergedArea = massData.mass;
					}
					else
					{
						//Completely dry
						submergedArea = 0;
					}
					
					return submergedArea;
	
				break;
				
				case 1:
					if (intoIndex == -1)
					{
						intoIndex = m_vertexCount - 1;
					}
					else
					{
						outoIndex = m_vertexCount - 1;
					}
				break;
			}

			intoIndex2 = (intoIndex + 1) % m_vertexCount;
			outoIndex2 = (outoIndex + 1) % m_vertexCount;
			intoLamdda = (0 - depths[intoIndex]) / (depths[intoIndex2] - depths[intoIndex]);
			outoLamdda = (0 - depths[outoIndex]) / (depths[outoIndex2] - depths[outoIndex]);

			intoVec.x = m_vertices[intoIndex].x * (1 - intoLamdda) + m_vertices[intoIndex2].x * intoLamdda;
			intoVec.y = m_vertices[intoIndex].y * (1 - intoLamdda) + m_vertices[intoIndex2].y * intoLamdda

			outoVec.x = m_vertices[outoIndex].x * (1 - outoLamdda) + m_vertices[outoIndex2].x * outoLamdda;
			outoVec.y = m_vertices[outoIndex].y * (1 - outoLamdda) + m_vertices[outoIndex2].y * outoLamdda;

			// Initialize accumulator
			area = 0;

			center.x = 0;
			center.y = 0;
			
			p2 = m_vertices[intoIndex2];
			p3;
			
			// An awkward loop from intoIndex2+1 to outIndex2
			i = intoIndex2;

			while (i != outoIndex2)
			{
				i = (i + 1) % m_vertexCount;

				if(i == outoIndex2)
				{
					p3 = outoVec
				}
				else
				{
					p3 = m_vertices[i];
				}
					
				
				triangleArea = 0.5 * ( (p2.x - intoVec.x) * (p3.y - intoVec.y) - (p2.y - intoVec.y) * (p3.x - intoVec.x) );
				area += triangleArea;

				// Area weighted centroid
				center.x += triangleArea * (intoVec.x + p2.x + p3.x) / 3;
				center.y += triangleArea * (intoVec.y + p2.y + p3.y) / 3;

				p2 = p3;
			}

			//Normalize and transform centroid
			center.Multiply(1 / area);
			b2Math.MulX(xf, center, c);

			return area;
		}
		
		/**
		* Get the vertex count.
		*/
		public function GetVertexCount():int
		{
			return m_vertexCount;
		}
	
		/**
		* Get the vertices in local coordinates.
		*/
		[Inline]
		final public function GetVertices():Array
		{
			return m_vertices;
		}
		
		/**
		* Get the edge normal vectors. There is one for each vertex.
		*/
		[Inline]
		final public function GetNormals():Array
		{
			return m_normals;
		}
		
		/**
		 * Get the supporting vertex index in the given direction.
		 */
		public function GetSupport(d:b2Vec2):int
		{
			var bestIndex:int = 0;
			var bestValue:Number = m_vertices[0].x * d.x + m_vertices[0].y * d.y;
			for (var i:int= 1; i < m_vertexCount; ++i)
			{
				var value:Number = m_vertices[i].x * d.x + m_vertices[i].y * d.y;
				if (value > bestValue)
				{
					bestIndex = i;
					bestValue = value;
				}
			}
			return bestIndex;
		}
		
		public function GetSupportVertex(d:b2Vec2):b2Vec2
		{
			var bestIndex:int = 0;
			var bestValue:Number = m_vertices[0].x * d.x + m_vertices[0].y * d.y;
			for (var i:int= 1; i < m_vertexCount; ++i)
			{
				var value:Number = m_vertices[i].x * d.x + m_vertices[i].y * d.y;
				if (value > bestValue)
				{
					bestIndex = i;
					bestValue = value;
				}
			}
			return m_vertices[bestIndex];
		}
	
		// TODO: Expose this
		private function Validate():Boolean
		{
			/*
			// Ensure the polygon is convex.
			for (int32 i = 0; i < m_vertexCount; ++i)
			{
				for (int32 j = 0; j < m_vertexCount; ++j)
				{
					// Don't check vertices on the current edge.
					if (j == i || j == (i + 1) % m_vertexCount)
					{
						continue;
					}
					
					// Your polygon is non-convex (it has an indentation).
					// Or your polygon is too skinny.
					float32 s = b2Dot(m_normals[i], m_vertices[j] - m_vertices[i]);
					b2Assert(s < -b2_linearSlop);
				}
			}
			
			// Ensure the polygon is counter-clockwise.
			for (i = 1; i < m_vertexCount; ++i)
			{
				var cross:Number = b2Math.b2CrossVV(m_normals[int(i-1)], m_normals[i]);
				
				// Keep asinf happy.
				cross = b2Math.b2Clamp(cross, -1.0, 1.0);
				
				// You have consecutive edges that are almost parallel on your polygon.
				var angle:Number = Math.asin(cross);
				//b2Assert(angle > b2_angularSlop);
				trace(angle > b2Settings.b2_angularSlop);
			}
			*/
			return false;
		}
		//--------------- Internals Below -------------------
		
		/**
		 * @private
		 */

		public function b2PolygonShape()
		{
			super();

			m_type = e_polygonShape;
			
			m_centroid = b2Pool.getVector2(0, 0);

			m_vertices = b2Pool.getVector2Array();
			m_normals = b2Pool.getVector2Array();
			
			if(!sob_transform)
			{
				sob_transform = new b2Transform();
			}
		}

		private function Reserve(count:int):void
		{
			for(i = m_vertices.length; i < count; i++)
			{
				m_vertices[i] = b2Pool.getVector2(0, 0);
				m_normals[i] = b2Pool.getVector2(0, 0);
			}
		}
	
		// Local position of the polygon centroid.
		public var m_centroid:b2Vec2;
	
		public var m_vertices:Array;
		public var m_normals:Array;
		
		public var m_vertexCount:uint;
		
		
		private static var cc_area:Number;
		
		private static var cc_p1X:Number;
		private static var cc_p1Y:Number;
		
		private static var cc_inv3:Number;
		
		private static var cc_i:Number;
		
		private static var cc_p2:b2Vec2;
		private static var cc_p3:b2Vec2;
		
		private static var cc_e1X:Number;
		private static var cc_e1Y:Number;
		
		private static var cc_e2X:Number;
		private static var cc_e2Y:Number;
		
		private static var cc_D:Number;
		
		private static var cc_triangleArea:Number;
		
		/**
		 * Computes the centroid of the given polygon
		 * @param	vs		vector of b2Vec specifying a polygon
		 * @param	count	length of vs
		 * @return the polygon centroid
		 */
		[Inline]
		static public function ComputeCentroid(vs:Array, count:uint, result:b2Vec2):void
		{
			cc_area = b2Settings.DECIMAL_ZERO;

			cc_p1X = b2Settings.DECIMAL_ZERO;
			cc_p1Y = b2Settings.DECIMAL_ZERO;
			
			cc_inv3 = b2Settings.DECIMAL_ONE / 3.0;
			
			for (cc_i = 0; cc_i < count; ++cc_i)
			{
				cc_p2 = vs[cc_i];
				cc_p3 = cc_i + 1 < count ? vs[cc_i+1] : vs[0];
				

				cc_e1X = cc_p2.x - cc_p1X;
				cc_e1Y = cc_p2.y - cc_p1Y;

				cc_e2X = cc_p3.x - cc_p1X;
				cc_e2Y = cc_p3.y - cc_p1Y;

				cc_D = (cc_e1X * cc_e2Y - cc_e1Y * cc_e2X);

				cc_triangleArea = 0.5 * cc_D;
				cc_area += cc_triangleArea;

				result.x += cc_triangleArea * cc_inv3 * (cc_p1X + cc_p2.x + cc_p3.x);
				result.y += cc_triangleArea * cc_inv3 * (cc_p1Y + cc_p2.y + cc_p3.y);
			}

			result.x *= b2Settings.DECIMAL_ONE / cc_area;
			result.y *= b2Settings.DECIMAL_ONE / cc_area;
		}
	
		/**
		 * Computes a polygon's OBB
		 * @see http://www.geometrictools.com/Documentation/MinimumAreaRectangle.pdf
		 */
		
		static public function ComputeOBB(obb:b2OBB, vs:Vector.<b2Vec2>, count:int) : void
		{
			var i:int;
			var p:Vector.<b2Vec2> = new Vector.<b2Vec2>(count + 1);
			for (i = 0; i < count; ++i)
			{
				p[i] = vs[i];
			}
			p[count] = p[0];
			
			var minArea:Number = Number.MAX_VALUE;
			
			for (i = 1; i <= count; ++i)
			{
				var root:b2Vec2 = p[int(i-1)];
				//b2Vec2 ux = p[i] - root;
				var uxX:Number = p[i].x - root.x;
				var uxY:Number = p[i].y - root.y;
				//var length:Number = ux.Normalize();
				var length:Number = Math.sqrt(uxX*uxX + uxY*uxY);
				uxX /= length;
				uxY /= length;
				//b2Settings.b2Assert(length > Number.MIN_VALUE);
				//b2Vec2 uy(-ux.y, ux.x);
				var uyX:Number = -uxY;
				var uyY:Number = uxX;
				//b2Vec2 lower(FLT_MAX, FLT_MAX);
				var lowerX:Number = Number.MAX_VALUE;
				var lowerY:Number = Number.MAX_VALUE;
				//b2Vec2 upper(-FLT_MAX, -FLT_MAX);
				var upperX:Number = -Number.MAX_VALUE;
				var upperY:Number = -Number.MAX_VALUE;
				
				for (var j:int = 0; j < count; ++j)
				{
					//b2Vec2 d = p[j] - root;
					var dX:Number = p[j].x - root.x;
					var dY:Number = p[j].y - root.y;
					//b2Vec2 r;
					//var rX:Number = b2Dot(ux, d);
					var rX:Number = (uxX*dX + uxY*dY);
					//var rY:Number = b2Dot(uy, d);
					var rY:Number = (uyX*dX + uyY*dY);
					//lower = b2Min(lower, r);
					if (rX < lowerX) lowerX = rX;
					if (rY < lowerY) lowerY = rY;
					//upper = b2Max(upper, r);
					if (rX > upperX) upperX = rX;
					if (rY > upperY) upperY = rY;
				}
				
				var area:Number = (upperX - lowerX) * (upperY - lowerY);
				if (area < 0.95 * minArea)
				{
					minArea = area;
					//obb->R.col1 = ux;
					obb.R.col1.x = uxX;
					obb.R.col1.y = uxY;
					//obb->R.col2 = uy;
					obb.R.col2.x = uyX;
					obb.R.col2.y = uyY;
					//b2Vec2 center = 0.5f * (lower + upper);
					var centerX:Number = 0.5 * (lowerX + upperX);
					var centerY:Number = 0.5 * (lowerY + upperY);
					//obb->center = root + b2Mul(obb->R, center);
					var tMat:b2Mat22 = obb.R;
					obb.center.x = root.x + (tMat.col1.x * centerX + tMat.col2.x * centerY);
					obb.center.y = root.y + (tMat.col1.y * centerX + tMat.col2.y * centerY);
					//obb->extents = 0.5f * (upper - lower);
					obb.extents.x = 0.5 * (upperX - lowerX);
					obb.extents.y = 0.5 * (upperY - lowerY);
				}
			}
			
			//b2Settings.b2Assert(minArea < Number.MAX_VALUE);
		}
	}
}
