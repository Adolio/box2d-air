﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Collision.Shapes
{	
	import Box2D.Collision.b2AABB;
	import Box2D.Collision.b2RayCastInput;
	import Box2D.Collision.b2RayCastOutput;
	import Box2D.Common.b2Pool;
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Transform;
	import Box2D.Common.Math.b2Vec2;

	
	/**
	* A circle shape.
	* @see b2CircleDef
	*/
	public class b2CircleShape extends b2Shape
	{
		override public function Copy():b2Shape 
		{
			var s:b2Shape = new b2CircleShape();
			s.Set(this);
			return s;
		}
		
		override public function Set(other:b2Shape):void 
		{
			super.Set(other);
			if (other is b2CircleShape)
			{
				var other2:b2CircleShape = other as b2CircleShape;
				m_p.SetV(other2.m_p);
			}
		}
		
	
		private static var tp_dX:Number;
		private static var tp_dY:Number;
		
		/**
		* @inheritDoc
		*/
		public override function TestPoint(transform:b2Transform, p:b2Vec2):Boolean
		{
			tp_dX = transform.position.x + (transform.R.col1.x * m_p.x + transform.R.col2.x * m_p.y);
			tp_dY = transform.position.y + (transform.R.col1.y * m_p.x + transform.R.col2.y * m_p.y);
			
			tp_dX = p.x - tp_dX;
			tp_dY = p.y - tp_dY;
	
			return (tp_dX*tp_dX + tp_dY*tp_dY) <= m_radius * m_radius;
		}
	

		private static var rc_positionX:Number;
		private static var rc_positionY:Number;

		private static var rc_sX:Number;
		private static var rc_sY:Number;

		private static var rc_b:Number;
		
		private static var rc_rX:Number;
		private static var rc_rY:Number;

		private static var rc_c:Number;

		private static var rc_rr:Number;
		private static var rc_sigma:Number;
		
		private static var rc_a:Number;
		
		/**
		* @inheritDoc
		*/
		[Inline]
		final public override function RayCast(output:b2RayCastOutput, input:b2RayCastInput, transform:b2Transform):Boolean
		{
			rc_positionX = transform.position.x + (transform.R.col1.x * m_p.x + transform.R.col2.x * m_p.y);
			rc_positionY = transform.position.y + (transform.R.col1.y * m_p.x + transform.R.col2.y * m_p.y);

			rc_sX = input.p1.x - rc_positionX;
			rc_sY = input.p1.y - rc_positionY;			

			rc_b = (rc_sX*rc_sX + rc_sY*rc_sY) - m_radius * m_radius;

			rc_rX = input.p2.x - input.p1.x;
			rc_rY = input.p2.y - input.p1.y;

			rc_c =  (rc_sX*rc_rX + rc_sY*rc_rY);

			rc_rr = (rc_rX*rc_rX + rc_rY*rc_rY);
			rc_sigma = rc_c * rc_c - rc_rr * rc_b;
			
			// Check for negative discriminant and short segment.
			if (rc_sigma < b2Settings.DECIMAL_ZERO || rc_rr < Number.MIN_VALUE)
			{
				return false;
			}
			
			// Find the point of intersection of the line with the circle.
			rc_a = -(rc_c + Math.sqrt(rc_sigma));
			
			// Is the intersection point on the segment?
			if (b2Settings.DECIMAL_ZERO <= rc_a && rc_a <= input.maxFraction * rc_rr)
			{
				rc_a /= rc_rr;
				output.fraction = rc_a;

				output.normal.x = rc_sX + rc_a * rc_rX;
				output.normal.y = rc_sY + rc_a * rc_rY;
				output.normal.Normalize();

				return true;
			}
			
			return false;
		}
	
		private static var ca_pX:Number;
		private static var ca_pY:Number;
		
		
		/**
		* @inheritDoc
		*/
		public override function ComputeAABB(aabb:b2AABB, transform:b2Transform):void
		{
			//b2Vec2 p = transform.position + b2Mul(transform.R, m_p);

			ca_pX = transform.position.x + (transform.R.col1.x * m_p.x + transform.R.col2.x * m_p.y);
			ca_pY = transform.position.y + (transform.R.col1.y * m_p.x + transform.R.col2.y * m_p.y);
			
			aabb.lowerBound.x = ca_pX - m_radius;
			aabb.lowerBound.y = ca_pY - m_radius;

			aabb.upperBound.x = ca_pX + m_radius;
			aabb.upperBound.y = ca_pY + m_radius;
		}
	
		/**
		* @inheritDoc
		*/
		[Inline]
		final public override function ComputeMass(massData:b2MassData, density:Number):void
		{
			massData.mass = density * b2Settings.b2_pi * m_radius * m_radius;
			massData.center.x = m_p.x;
			massData.center.y = m_p.y;
			
			// inertia about the local origin
			//massData.I = massData.mass * (0.5 * m_radius * m_radius + b2Dot(m_p, m_p));
			massData.I = massData.mass * (0.5 * m_radius * m_radius + (m_p.x*m_p.x + m_p.y*m_p.y));
		}
		
		/**
		* @inheritDoc
		*/
		
		private static var position:b2Vec2 = new b2Vec2();
		
		private static var length:Number;
		
		private static var r2:Number;
		private static var l2:Number;
		private static var area:Number;
		private static var com:Number;
		
		public override function ComputeSubmergedArea(
				normal:b2Vec2,
				offset:Number,
				xf:b2Transform,
				c:b2Vec2):Number
		{
			b2Math.MulX(xf, m_p, position);
 			
			length = -(b2Math.Dot(normal, position) - offset);
			
			if(length < -m_radius + Number.MIN_VALUE)
			{
				//Completely dry
				return 0;
			}
			else if (length > m_radius)
			{
				//Completely wet
				c.x = position.x;
				c.y = position.y;
				return Math.PI * m_radius * m_radius;
			}
			else
			{
				r2 = m_radius * m_radius;
				l2 = length * length;

				area = r2 * ( 
					Math.asin(length / m_radius) + b2Settings.HALF_PI
				) + length * Math.sqrt( r2 - l2 );
				
				com = -2 / 3 * Math.pow(r2 - l2, 1.5) / area;
				
				c.x = position.x + normal.x * com;
				c.y = position.y + normal.y * com;
				
				return area;
			}
		}
	
		/**
		 * Get the local position of this circle in its parent body.
		 */
		public function GetLocalPosition() : b2Vec2{
			return m_p;
		}
		
		/**
		 * Set the local position of this circle in its parent body.
		 */
		public function SetLocalPosition(xPos:Number, yPos:Number):void
		{
			m_p.x = xPos;
			m_p.y = yPos;
		}
		
		/**
		 * Get the radius of the circle
		 */
		public function GetRadius():Number
		{
			return m_radius;
		}
		
		/**
		 * Set the radius of the circle
		 */
		public function SetRadius(radius:Number):void
		{
			m_radius = radius;
		}
	
		public function b2CircleShape(radius:Number = 0)
		{
			super();
			
			m_radius = radius;

			m_p = b2Pool.getVector2(0, 0);
			m_type = e_circleShape;
		}
		
		[Inline]
		final override public function Destroy():void
		{
			super.Destroy();
			
			b2Pool.putVector2(m_p);
			
			m_p = null;
		}
	
		// Local position in parent body
		public var m_p:b2Vec2;
	};
}
