package Box2D.Collision
{
	public interface IDynamicTreeCallback
	{
		function QueryDynamicTreeNodeCallback(proxy:b2DynamicTreeNode):Boolean;
		function QueryProxy(proxy:b2Proxy):Boolean;
	}
}