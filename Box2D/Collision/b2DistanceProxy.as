﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Collision 
{
	import Box2D.Collision.Shapes.b2CircleShape;
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Common.b2Pool;
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Vec2;

	/**
	 * A distance proxy is used by the GJK algorithm.
 	 * It encapsulates any shape.
 	 */
	public class b2DistanceProxy 
	{
		public function b2DistanceProxy()
		{
			m_circleVertices = b2Pool.getVector2Array();
			m_circleVertices.push(b2Pool.getVector2(0, 0));
		}
		
		private static var proxyVertice:b2Vec2;
		private static var circleShape:b2CircleShape;
		private static var polygonShape:b2PolygonShape;
		
 		/**
 		 * Initialize the proxy using the given shape. The shape
 		 * must remain in scope while the proxy is in use.
 		 */
		[Inline]
 		final public function Set(shape:b2Shape):void
		{
			switch(shape.m_type)
			{
				case b2Shape.e_circleShape:
					
					circleShape = shape as b2CircleShape;
					
					proxyVertice = m_circleVertices[0];
					
					proxyVertice.x = circleShape.m_p.x;
					proxyVertice.y = circleShape.m_p.y;
					
					m_count = 1;
					m_radius = circleShape.m_radius;
					
					m_vertices = m_circleVertices;

					proxyVertice = null;
					
				break;
				
				case b2Shape.e_polygonShape:

					polygonShape =  shape as b2PolygonShape;
					
					m_vertices = polygonShape.m_vertices;
					m_count = polygonShape.m_vertexCount;
					m_radius = polygonShape.m_radius;

				break;
			}
			
			circleShape = null;
			polygonShape = null;
		}

		private static var _bestIndex:int;
		private static var _bestValue:Number;
		private static var _currentValue:Number;
		private static var _i:int;
		private static var _vertice:b2Vec2;


 		/**
 		 * Get the supporting vertex index in the given direction.
 		 */
 		public function GetSupport(d:b2Vec2):Number
		{
			_bestIndex = 0;
			
			_vertice = m_vertices[0];
			
			_bestValue = _vertice.x * d.x + _vertice.y * d.y;

			for(_i = 1; _i < m_count; ++_i)
			{
				_vertice = m_vertices[_i];

				_currentValue = (_vertice.x * d.x) + (_vertice.y * d.y);

				if(_currentValue > _bestValue)
				{
					_bestIndex = _i;
					_bestValue = _currentValue;
				}
			}

			_vertice = null;

			return _bestIndex;
		}
		
		private var sv_vertex:b2Vec2;
		private var sv_i:int;
		private var sv_bestIndex:int;
		private var sv_currentValue:Number;
		private var sv_bestValue:Number;
		
 		/**
 		 * Get the supporting vertex in the given direction.
 		 */
 		public function GetSupportVertex(d:b2Vec2, result:b2Vec2):void
		{
			sv_bestIndex = 0;
			
			sv_vertex = m_vertices[0];
			
			sv_bestValue = sv_vertex.x * d.x + sv_vertex.y * d.y;
				
			for(sv_i = 1; sv_i < m_count; ++sv_i)
			{
				sv_vertex = m_vertices[sv_i];
				
				sv_currentValue = sv_vertex.x * d.x + sv_vertex.y * d.y;
				
				if(sv_currentValue > sv_bestValue)
				{
					sv_bestIndex = sv_i;
					sv_bestValue = sv_currentValue;
				}
			}

			sv_vertex = m_vertices[sv_bestIndex];
			
			result.x = sv_vertex.x;
			result.y = sv_vertex.y;
			
			sv_vertex = null;
		}


 		/**
 		 * Get the vertex count.
 		 */
 		public function GetVertexCount():int
		{
			return m_count;
		}

		public function Reset():void
		{
			// m_vertices can be null for circle shapes
			if(m_vertices)
			{
				var i:int = m_vertices.length;
				
				while(--i > -1)
				{
					b2Pool.putVector2(m_vertices.pop());
				}
			}
		}
		
 		/**
 		 * Get a vertex by index. Used by b2Distance.
 		 */
		[Inline]
 		final public function GetVertex(index:int):b2Vec2
		{
			b2Settings.b2Assert(0 <= index && index < m_count);
			return m_vertices[index];
		}
		
 		public var m_vertices:Array;
		public var m_circleVertices:Array;
		
 		public var m_count:int;
 		public var m_radius:Number;
	}
}