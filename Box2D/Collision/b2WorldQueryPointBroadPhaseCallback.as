package Box2D.Collision
{
	import Box2D.Common.Math.b2Vec2;

	public class b2WorldQueryPointBroadPhaseCallback implements IDynamicTreeCallback
	{
		public function b2WorldQueryPointBroadPhaseCallback()
		{
		}
		
		public function QueryProxy(proxy:b2Proxy):Boolean
		{
			return false;
		}

		public var broadPhase:IBroadPhase;
		public var point:b2Vec2;
		public var isFixtureFound:Boolean = false;
		public var fixtureFoundCallback:Function;
		public var shouldExcludeFixtureCallback:Function;
		
		public function QueryDynamicTreeNodeCallback(node:b2DynamicTreeNode):Boolean
		{			
			if(node.userData.m_shape.TestPoint(node.userData.m_body.m_xf, point))
			{
				if(shouldExcludeFixtureCallback && shouldExcludeFixtureCallback(node.userData))
				{
					return true;
				}

				isFixtureFound = true;
				return fixtureFoundCallback(node.userData);
			}

			return true;
		}
	}
}