package Box2D.Collision
{
	public interface IBroadPhaseRayCastCallback
	{
		function RayCastCallback(input:b2RayCastInput, proxy:b2DynamicTreeNode):Number
	}
}