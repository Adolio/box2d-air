﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Collision
{
	
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Sweep;
	import Box2D.Common.Math.b2Transform;
	
	
	/**
	* @private
	*/
	public class b2TimeOfImpact
	{
		
		private static const k_maxIterations:int = 1000; //TODO_ERIN b2Settings
		private static const k_maxRootIterations:int = 50; //TODO_ERIN b2Settings
		private static const ALPHA_BUFFER:Number = b2Settings.DECIMAL_ONE + 100.0 * Number.MIN_VALUE;
		
		private static var b2_toiRootIters:int = 0;
		private static var b2_toiMaxRootIters:int = 0;
	
		private static var s_cache:b2SimplexCache = new b2SimplexCache();
		private static var s_distanceInput:b2DistanceInput = new b2DistanceInput();
		private static var s_xfA:b2Transform = new b2Transform();
		private static var s_xfB:b2Transform = new b2Transform();
		private static var s_fcn:b2SeparationFunction = new b2SeparationFunction();
		private static var s_distanceOutput:b2DistanceOutput = new b2DistanceOutput();

		private static var proxyA:b2DistanceProxy;
		private static var proxyB:b2DistanceProxy;
		
		private static var sweepA:b2Sweep;
		private static var sweepB:b2Sweep;
		
		private static var radius:Number;
		
		private static var alpha:Number = b2Settings.DECIMAL_ZERO;
		
		private static var iter:int = 0;
		private static var target:Number = b2Settings.DECIMAL_ZERO;
		
		private static var separation:Number;
		
		private static var newAlpha:Number;
		
		private static var x1:Number;
		private static var x2:Number;
		
		private static var f1:Number;
		private static var f2:Number;
		
		private static var rootIterCount:int;
		private static var rootX:Number;

		private static var rootAlpha:Number
		
		public static function TimeOfImpact(input:b2TOIInput):Number
		{
			proxyA = input.proxyA;
			proxyB = input.proxyB;
			
			sweepA = input.sweepA;
			sweepB = input.sweepB;
			
			// b2Settings.b2Assert(sweepA.t0 == sweepB.t0);
			if(sweepA.t0 != sweepB.t0)
			{
				throw Error('Time of impact invalid');
			}
				
			// b2Settings.b2Assert(b2Settings.DECIMAL_ONE - sweepA.t0 > Number.MIN_VALUE);
			if(b2Settings.DECIMAL_ONE - sweepA.t0 < Number.MIN_VALUE)
			{
				throw Error('Time interval invalid');
			}
			
			radius = proxyA.m_radius + proxyB.m_radius;
			
			alpha= b2Settings.DECIMAL_ZERO;
			
			iter = 0;
			target = b2Settings.DECIMAL_ZERO;
			
			// Prepare input for distance query.
			s_cache.count = 0;
			s_distanceInput.useRadii = false;
			
			for(;;)
			{
				sweepA.GetTransform(s_xfA, alpha);
				sweepB.GetTransform(s_xfB, alpha);
				
				// Get the distance between shapes
				s_distanceInput.proxyA = proxyA;
				s_distanceInput.proxyB = proxyB;
				s_distanceInput.transformA = s_xfA;
				s_distanceInput.transformB = s_xfB;
				
				b2Distance.Distance(s_distanceOutput, s_cache, s_distanceInput);
				
				if(s_distanceOutput.distance <= b2Settings.DECIMAL_ZERO)
				{
					alpha = b2Settings.DECIMAL_ONE;
					break;
				}
				
				s_fcn.Initialize(s_cache, proxyA, s_xfA, proxyB, s_xfB);
				
				separation = s_fcn.Evaluate(s_xfA, s_xfB);
				
				if (separation <= b2Settings.DECIMAL_ZERO)
				{
					alpha = b2Settings.DECIMAL_ONE;
					break;
				}
				
				if(iter == 0)
				{
					// Compute a reasonable target distance to give some breathing room
					// for conservative advancement. We take advantage of the shape radii
					// to create additional clearance
					if (separation > radius)
					{
						target = b2Math.Max(radius - input.tolerance, 0.75 * radius);
					}
					else
					{
						target = b2Math.Max(separation - input.tolerance, 0.02 * radius);
					}
				}
				
				if (separation - target < 0.5 * input.tolerance)
				{
					if(iter == 0)
					{
						alpha = b2Settings.DECIMAL_ONE;
						break;
					}

					break;
				}

				// Compute 1D root of f(x) - target = 0
				newAlpha = alpha;
				
				x1 = alpha;
				x2 = 1.0;
				
				f1 = separation;
				
				sweepA.GetTransform(s_xfA, x2);
				sweepB.GetTransform(s_xfB, x2);
				
				f2 = s_fcn.Evaluate(s_xfA, s_xfB);
				
				// If intervals don't overlap at t2, then we are done
				if(f2 >= target)
				{
					alpha = b2Settings.DECIMAL_ONE;
					break;
				}
				
				// Determine when intervals intersect
				rootIterCount = 0;
				
				for(;;)
				{
					// Use a mix of the secand rule and bisection
					if (rootIterCount & 1)
					{
						// Secant rule to improve convergence
						rootX = x1 + (target - f1) * (x2 - x1) / (f2 - f1);
					}
					else
					{
						// Bisection to guarantee progress
						rootX = 0.5 * (x1 + x2);
					}
					
					sweepA.GetTransform(s_xfA, rootX);
					sweepB.GetTransform(s_xfB, rootX);
					
					rootAlpha = s_fcn.Evaluate(s_xfA, s_xfB);
					
					if (b2Math.Abs(rootAlpha - target) < 0.025 * input.tolerance)
					{
						newAlpha = rootX;
						break;
					}
					
					// Ensure we continue to bracket the root
					if (rootAlpha > target)
					{
						x1 = rootX;
						f1 = rootAlpha;
					}
					else
					{
						x2 = rootX;
						f2 = rootAlpha;
					}
					
					++rootIterCount;
					++b2_toiRootIters;

					if(rootIterCount == k_maxRootIterations)
					{
						break;
					}
				}
				
				b2_toiMaxRootIters = b2Math.Max(b2_toiMaxRootIters, rootIterCount);
				
				// Ensure significant advancement
				if(newAlpha < ALPHA_BUFFER * alpha)
				{
					break;
				}
				
				alpha = newAlpha;
				
				iter++;
				
				if(iter == k_maxIterations)
				{
					break;
				}
			}
	
			return alpha;
		}
	}
}
