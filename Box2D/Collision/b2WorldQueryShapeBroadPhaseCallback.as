package Box2D.Collision
{
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Common.Math.b2Transform;

	public class b2WorldQueryShapeBroadPhaseCallback implements IDynamicTreeCallback
	{
		public var broadPhase:IBroadPhase;
		public var shape:b2Shape;
		public var transform:b2Transform;
		public var isFixtureFound:Boolean = false;
		public var fixtureFoundCallback:Function;

		public function b2WorldQueryShapeBroadPhaseCallback()
		{
		}
		
		public function QueryDynamicTreeNodeCallback(node:b2DynamicTreeNode):Boolean
		{
			if(b2Shape.TestOverlap(
				shape,
				transform,
				node.userData.m_shape,
				node.userData.m_body.m_xf)) 
			{
				isFixtureFound = true;
				return fixtureFoundCallback(node.userData);
			}
			
			return true;
		}
		
		public function QueryProxy(proxy:b2Proxy):Boolean
		{
			return false;
		}
	}
}