﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Collision 
{
	
	
	import Box2D.Common.b2Pool;
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Mat22;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Transform;
	import Box2D.Common.Math.b2Vec2;
	
	public class b2SeparationFunction
	{
		public function b2SeparationFunction()
		{
		}
		
		//enum Type
		public static const e_points:int = 0x01;
		public static const e_faceA:int = 0x02;
		public static const e_faceB:int = 0x04;
		
		private var count:int;
		
		private var pointAX:Number;
		private var pointAY:Number;
		private var pointBX:Number;
		private var pointBY:Number;
		private var normalX:Number;
		private var normalY:Number;
		
		private var tMat:b2Mat22;
		private var tVec:b2Vec2 = new b2Vec2();

		private var s:Number;
		private var sgn:Number;
		
		private var in_localA:b2Vec2 = new b2Vec2();
		private var in_localB:b2Vec2 = new b2Vec2();
		
		private var in_subtractedLocalA:b2Vec2 = new b2Vec2();
		private var in_subtractedLocalB:b2Vec2 = new b2Vec2();
		
		private var in_vertexA:b2Vec2;
		private var in_vertexA1:b2Vec2;
		
		private var in_vertexB:b2Vec2;
		private var in_vertexB1:b2Vec2;
		
		
		private var in_pA:b2Vec2 = new b2Vec2();
		private var in_dA:b2Vec2 = new b2Vec2();
		private var in_pB:b2Vec2 = new b2Vec2();
		private var in_dB:b2Vec2 = new b2Vec2();
		
		private var in_r:b2Vec2 = b2Pool.getVector2(0, 0);
		
		private var in_a:Number;
		private var in_e:Number;
		
		private var in_c:Number;
		private var in_f:Number;
		
		private var in_b:Number;
		private var in_denom:Number;
		
		private var in_t:Number;
		
		public function Initialize(cache:b2SimplexCache,
									proxyA:b2DistanceProxy, transformA:b2Transform,
									proxyB:b2DistanceProxy, transformB:b2Transform):void
		{
			m_proxyA = proxyA;
			m_proxyB = proxyB;
			count = cache.count;
			
			//b2Settings.b2Assert(0 < count && count < 3);
			if(!(0 < count && count < 3))
			{
				throw Error('Cache count is not valid');		
			}
	
			if(count == 1)
			{
				m_type = e_points;
				
				in_vertexA = m_proxyA.m_vertices[cache.indexA[0]];
				in_vertexB = m_proxyB.m_vertices[cache.indexB[0]];

				//pointA = b2Math.b2MulX(transformA, localPointA);
				tMat = transformA.R;
				
				pointAX = transformA.position.x + (tMat.col1.x * in_vertexA.x + tMat.col2.x * in_vertexA.y)
				pointAY = transformA.position.y + (tMat.col1.y * in_vertexA.x + tMat.col2.y * in_vertexA.y)
					
				//pointB = b2Math.b2MulX(transformB, localPointB);
				tMat = transformB.R;
				
				pointBX = transformB.position.x + (tMat.col1.x * in_vertexB.x + tMat.col2.x * in_vertexB.y);
				pointBY = transformB.position.y + (tMat.col1.y * in_vertexB.x + tMat.col2.y * in_vertexB.y);
					
				//m_axis = b2Math.SubtractVV(pointB, pointA);
				m_axis.x = pointBX - pointAX;
				m_axis.y = pointBY - pointAY;

				m_axis.Normalize();
			}
			else if(cache.indexB[0] == cache.indexB[1])
			{
				// Two points on A and one on B
				m_type = e_faceA;

				in_vertexA = m_proxyA.m_vertices[cache.indexA[0]];
				in_vertexA1 = m_proxyA.m_vertices[cache.indexA[1]];
				
				in_vertexB = m_proxyB.m_vertices[cache.indexB[0]];

				m_localPoint.x = 0.5 * (in_vertexA.x + in_vertexA1.x);
				m_localPoint.y = 0.5 * (in_vertexA.y + in_vertexA1.y);
				
				// b2Math.SubtractVV(in_vertexA1, in_vertexA, in_subtractedLocalA);
				in_subtractedLocalA.x = in_vertexA1.x - in_vertexA.x;
				in_subtractedLocalA.y = in_vertexA1.y - in_vertexA.y;

				// b2Math.CrossVF(in_subtractedLocalA, 1.0, m_axis);
				m_axis.x = b2Settings.DECIMAL_ONE * in_subtractedLocalA.y;
				m_axis.y = -b2Settings.DECIMAL_ONE * in_subtractedLocalA.x;
				
				m_axis.Normalize();

				tMat = transformA.R;
				
				normalX = tMat.col1.x * m_axis.x + tMat.col2.x * m_axis.y;
				normalY = tMat.col1.y * m_axis.x + tMat.col2.y * m_axis.y;

				tMat = transformA.R;
				
				pointAX = transformA.position.x + (tMat.col1.x * m_localPoint.x + tMat.col2.x * m_localPoint.y);
				pointAY = transformA.position.y + (tMat.col1.y * m_localPoint.x + tMat.col2.y * m_localPoint.y);
				
				//pointB = b2Math.b2MulX(transformB, localPointB);

				tMat = transformB.R;

				pointBX = transformB.position.x + (tMat.col1.x * in_vertexB.x + tMat.col2.x * in_vertexB.y);
				pointBY = transformB.position.y + (tMat.col1.y * in_vertexB.x + tMat.col2.y * in_vertexB.y);
				
				//float32 s = b2Dot(pointB - pointA, normal);
				s = (pointBX - pointAX) * normalX + (pointBY - pointAY) * normalY;

				if(s < b2Settings.DECIMAL_ZERO)
				{
					m_axis.NegativeSelf();
				}
			}
			else if (cache.indexA[0] == cache.indexA[0])
			{
				// Two points on B and one on A
				m_type = e_faceB;
				
				in_vertexA = m_proxyA.m_vertices[cache.indexA[0]];
				
				in_vertexB = m_proxyB.m_vertices[cache.indexB[0]];
				in_vertexB1 = m_proxyB.m_vertices[cache.indexB[1]];
				
				m_localPoint.x = 0.5 * (in_vertexB.x + in_vertexB1.x);
				m_localPoint.y = 0.5 * (in_vertexB.y + in_vertexB1.y);
				
				// b2Math.SubtractVV(in_vertexB1, in_vertexB, in_subtractedLocalB);
				in_subtractedLocalB.x = in_vertexB1.x - in_vertexB.x;
				in_subtractedLocalB.y = in_vertexB1.y - in_vertexB.y;
					
				// b2Math.CrossVF(in_subtractedLocalB, 1.0, m_axis);
				m_axis.x = b2Settings.DECIMAL_ONE * in_subtractedLocalB.y;
				m_axis.y = -b2Settings.DECIMAL_ONE * in_subtractedLocalB.x;
				
				m_axis.Normalize();

				tMat = transformB.R;
				
				normalX = tMat.col1.x * m_axis.x + tMat.col2.x * m_axis.y;
				normalY = tMat.col1.y * m_axis.x + tMat.col2.y * m_axis.y;

				//pointB = b2Math.b2MulX(transformB, m_localPoint);
				tMat = transformB.R;
				
				pointBX = transformB.position.x + (tMat.col1.x * m_localPoint.x + tMat.col2.x * m_localPoint.y);
				pointBY = transformB.position.y + (tMat.col1.y * m_localPoint.x + tMat.col2.y * m_localPoint.y);

				tMat = transformA.R;

				pointAX = transformA.position.x + (tMat.col1.x * in_vertexA.x + tMat.col2.x * in_vertexA.y);
				pointAY = transformA.position.y + (tMat.col1.y * in_vertexA.x + tMat.col2.y * in_vertexA.y);
				
				//float32 s = b2Dot(pointA - pointB, normal);
				s = (pointAX - pointBX) * normalX + (pointAY - pointBY) * normalY;
				
				if (s < b2Settings.DECIMAL_ZERO)
				{
					m_axis.NegativeSelf();
				}
			}
			else
			{
				// Two points on B and two points on A.
				// The faces are parallel.
				
				in_vertexA = m_proxyA.m_vertices[cache.indexA[0]];
				in_vertexA1 = m_proxyA.m_vertices[cache.indexA[1]];
				
				in_vertexB = m_proxyB.m_vertices[cache.indexB[0]];
				in_vertexB1 = m_proxyB.m_vertices[cache.indexB[1]];
				
				/*
				
				in_localPointA1 = m_proxyA.GetVertex(cache.indexA[0]);
				in_localPointA2 = m_proxyA.GetVertex(cache.indexA[1]);
				
				in_localPointB1 = m_proxyB.GetVertex(cache.indexB[0]);
				in_localPointB2 = m_proxyB.GetVertex(cache.indexB[1]);
				*/

				b2Math.SubtractVV(in_vertexA1, in_vertexA, in_subtractedLocalA);
				b2Math.SubtractVV(in_vertexB1, in_vertexB, in_subtractedLocalB);
				
				
				
				b2Math.MulX(transformA, in_vertexA, in_pA);
				b2Math.MulMV(transformA.R, in_subtractedLocalA, in_dA);
				
				b2Math.MulX(transformB, in_vertexB, in_pB)
				b2Math.MulMV(transformB.R, in_subtractedLocalB, in_dB);
				
				in_a = in_dA.x * in_dA.x + in_dA.y * in_dA.y;
				in_e = in_dB.x * in_dB.x + in_dB.y * in_dB.y;
					
				b2Math.SubtractVV(in_dB, in_dA, in_r);
				
				in_c = in_dA.x * in_r.x + in_dA.y * in_r.y;
				in_f = in_dB.x * in_r.x + in_dB.y * in_r.y;
				
				in_b = in_dA.x * in_dB.x + in_dA.y * in_dB.y;
				in_denom = in_a * in_e - in_b * in_b;
				
				s = b2Settings.DECIMAL_ZERO;
				
				if(in_denom != b2Settings.DECIMAL_ZERO)
				{
					s = b2Math.Clamp(
						(in_b * in_f - in_c * in_e) / in_denom,
						b2Settings.DECIMAL_ZERO,
						b2Settings.DECIMAL_ONE
					);
				}
				
				in_t = (in_b * s + in_f) / in_e;

				if(in_t < b2Settings.DECIMAL_ZERO)
				{
					in_t = b2Settings.DECIMAL_ZERO;
					s = b2Math.Clamp((in_b - in_c) / in_a, 0.0, 1.0);
				}

				in_localA.x = in_vertexA.x + s * (in_vertexA1.x - in_vertexA.x);
				in_localA.y = in_vertexA.y + s * (in_vertexA1.y - in_vertexA.y);

				in_localB.x = in_vertexB.x + s * (in_vertexB1.x - in_vertexB.x);
				in_localB.y = in_vertexB.y + s * (in_vertexB1.y - in_vertexB.y);
				
				if(s == b2Settings.DECIMAL_ZERO || s == b2Settings.DECIMAL_ONE)
				{
					m_type = e_faceB;
					
					b2Math.SubtractVV(in_vertexB1, in_vertexB, in_subtractedLocalB);
						
					b2Math.CrossVF(in_subtractedLocalB, 1.0, m_axis);
					
					m_axis.Normalize();

					tMat = transformB.R;
					
					normalX = tMat.col1.x * m_axis.x + tMat.col2.x * m_axis.y;
					normalY = tMat.col1.y * m_axis.x + tMat.col2.y * m_axis.y;
	
					tMat = transformB.R;
					
					pointBX = transformB.position.x + (tMat.col1.x * m_localPoint.x + tMat.col2.x * m_localPoint.y);
					pointBY = transformB.position.y + (tMat.col1.y * m_localPoint.x + tMat.col2.y * m_localPoint.y);
					
					//pointA = b2Math.b2MulX(transformA, localPointA);
					tMat = transformA.R;
					
					pointAX = transformA.position.x + (tMat.col1.x * in_localA.x + tMat.col2.x * in_localA.y);
					pointAY = transformA.position.y + (tMat.col1.y * in_localA.x + tMat.col2.y * in_localA.y);
					
					//float32 sgn = b2Dot(pointA - pointB, normal);
					sgn = (pointAX - pointBX) * normalX + (pointAY - pointBY) * normalY;
					
					if(s < b2Settings.DECIMAL_ZERO)
					{
						m_axis.NegativeSelf();
					}
				}
				else
				{
					m_type = e_faceA;
					
					b2Math.SubtractVV(in_vertexA1, in_vertexA, in_subtractedLocalA);
					
					b2Math.CrossVF(in_subtractedLocalA, b2Settings.DECIMAL_ONE, m_axis);


					tMat = transformA.R;
					
					normalX = tMat.col1.x * m_axis.x + tMat.col2.x * m_axis.y;
					normalY = tMat.col1.y * m_axis.x + tMat.col2.y * m_axis.y;


					tMat = transformA.R;

					pointAX = transformA.position.x + (tMat.col1.x * m_localPoint.x + tMat.col2.x * m_localPoint.y);
					pointAY = transformA.position.y + (tMat.col1.y * m_localPoint.x + tMat.col2.y * m_localPoint.y);


					tMat = transformB.R;
					
					pointBX = transformB.position.x + (tMat.col1.x * in_localB.x + tMat.col2.x * in_localB.y);
					pointBY = transformB.position.y + (tMat.col1.y * in_localB.x + tMat.col2.y * in_localB.y);
					
					//float32 sgn = b2Dot(pointB - pointA, normal);
					sgn = (pointBX - pointAX) * normalX + (pointBY - pointAY) * normalY;

					if(s < b2Settings.DECIMAL_ZERO)
					{
						m_axis.NegativeSelf();
					}
				}
			}
		}
		
		
		
		private var axisA:b2Vec2 = new b2Vec2();
		private var axisB:b2Vec2 = new b2Vec2();
		
		private var ev_localPointA:b2Vec2 = new b2Vec2();
		private var ev_localPointB:b2Vec2 = new b2Vec2();
		
		private var pointA:b2Vec2 = new b2Vec2();
		private var pointB:b2Vec2 = new b2Vec2();
		private var normal:b2Vec2 = new b2Vec2();
		private var negative:b2Vec2 = new b2Vec2();
		
		private var seperation:Number;
		
		public function Evaluate(transformA:b2Transform, transformB:b2Transform):Number
		{
			switch(m_type)
			{
				case e_points:
	
					// b2Math.MulTMV(transformA.R, m_axis, axisA);					
					axisA.x = m_axis.x * transformA.R.col1.x + m_axis.y * transformA.R.col1.y;
					axisA.y = m_axis.x * transformA.R.col2.x + m_axis.y * transformA.R.col2.y;
					
					negative.x = -m_axis.x;
					negative.y = -m_axis.y;
					
					// b2Math.MulTMV(transformB.R, negative, axisB);
					axisB.x = negative.x * transformB.R.col1.x + negative.y * transformB.R.col1.y;
					axisB.y = negative.x * transformB.R.col2.x + negative.y * transformB.R.col2.y;
					
					
					m_proxyA.GetSupportVertex(axisA, ev_localPointA);
					m_proxyB.GetSupportVertex(axisB, ev_localPointB);

					b2Math.MulX(transformA, ev_localPointA, pointA);
					b2Math.MulX(transformB, ev_localPointB, pointB);

					//float32 separation = b2Dot(pointB - pointA, m_axis);
					seperation = (pointB.x - pointA.x) * m_axis.x + (pointB.y - pointA.y) * m_axis.y;
					
				break;
	
				case e_faceA:
	
					// b2Math.MulMV(transformA.R, m_axis, normal);
					normal.x = transformA.R.col1.x * m_axis.x + transformA.R.col2.x * m_axis.y;
					normal.y = transformA.R.col1.y * m_axis.x + transformA.R.col2.y * m_axis.y;
					
					
					b2Math.MulX(transformA, m_localPoint, pointA);
					
					negative.x = -normal.x;
					negative.y = -normal.y;
					
					// b2Math.MulTMV(transformB.R, negative, axisB);
					axisB.x = negative.x * transformB.R.col1.x + negative.y * transformB.R.col1.y;
					axisB.y = negative.x * transformB.R.col2.x + negative.y * transformB.R.col2.y;
					
					m_proxyB.GetSupportVertex(axisB, ev_localPointB);
					b2Math.MulX(transformB, ev_localPointB, pointB);
					
					//float32 separation = b2Dot(pointB - pointA, normal);
					seperation = (pointB.x - pointA.x) * normal.x + (pointB.y - pointA.y) * normal.y;
	
				break;
				
				case e_faceB:
	
					// b2Math.MulMV(transformB.R, m_axis, normal);
					normal.x = transformB.R.col1.x * m_axis.x + transformB.R.col2.x * m_axis.y;
					normal.y = transformB.R.col1.y * m_axis.x + transformB.R.col2.y * m_axis.y;
					
					b2Math.MulX(transformB, m_localPoint, pointB);
					
					negative.x = -normal.x;
					negative.y = -normal.y;
	
					// b2Math.MulTMV(transformA.R, negative, axisA);
					axisA.x = negative.x * transformA.R.col1.x + negative.y * transformA.R.col1.y;
					axisA.y = negative.x * transformA.R.col2.x + negative.y * transformA.R.col2.y;
					
					m_proxyA.GetSupportVertex(axisA, ev_localPointA);
					b2Math.MulX(transformA, ev_localPointA, pointA);
					
					//float32 separation = b2Dot(pointA - pointB, normal);
					seperation = (pointA.x - pointB.x) * normal.x + (pointA.y - pointB.y) * normal.y;
	
				break;
					
				default:
					b2Settings.b2Assert(false);
					seperation = 0.0;
				break;
			}
			
			return seperation;
		}
		
		public var m_proxyA:b2DistanceProxy;
		public var m_proxyB:b2DistanceProxy;
		public var m_type:int;
		public var m_localPoint:b2Vec2 = new b2Vec2();
		public var m_axis:b2Vec2 = new b2Vec2();
	}
}