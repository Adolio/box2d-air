package Box2D.Collision
{
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Fixture;

	public interface IB2WorldRayCastCallback
	{
		function rayCastCallback(fixture:b2Fixture, hitPoint:b2Vec2, normal:b2Vec2, fraction:Number):Number;
	}
}