﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Common.Math
{
	import Box2D.Common.b2Pool;
	import Box2D.Common.b2Settings;

	/**
	* @private
	*/
	public class b2Math
	{
	
		/**
		* This function is used to ensure that a floating point number is
		* not a NaN or infinity.
		*/
		[Inline]
		static public function IsValid(x:Number) : Boolean
		{
			return isFinite(x);
		}
		
		/*static public function b2InvSqrt(x:Number):Number{
			union
			{
				float32 x;
				int32 i;
			} convert;
			
			convert.x = x;
			float32 xhalf = 0.5f * x;
			convert.i = 0x5f3759df - (convert.i >> 1);
			x = convert.x;
			x = x * (1.5f - xhalf * x * x);
			return x;
		}*/
	
		[Inline]
		static public function Dot(a:b2Vec2, b:b2Vec2):Number
		{
			return a.x * b.x + a.y * b.y;
		}
	
		[Inline]
		static public function CrossVV(a:b2Vec2, b:b2Vec2):Number
		{
			return a.x * b.y - a.y * b.x;
		}
	
		[Inline]
		static public function CrossVF(a:b2Vec2, s:Number, result:b2Vec2):void
		{
			result.x = s * a.y;
			result.y = -s * a.x;
		}
	
		[Inline]
		static public function CrossFV(s:Number, a:b2Vec2, result:b2Vec2):void
		{
			result.x = -s * a.y;
			result.y = s * a.x;
		}
	
		[Inline]
		static public function MulMV(A:b2Mat22, v:b2Vec2, result:b2Vec2):void
		{
			result.x = A.col1.x * v.x + A.col2.x * v.y;
			result.y = A.col1.y * v.x + A.col2.y * v.y;
		}
	
		[Inline]
		static public function MulTMV(A:b2Mat22, v:b2Vec2, result:b2Vec2):void
		{
			result.x = v.x * A.col1.x + v.y * A.col1.y;
			result.y = v.x * A.col2.x + v.y * A.col2.y;
		}
		
		[Inline]
		static public function MulX(T:b2Transform, v:b2Vec2, result:b2Vec2):void
		{			
			// MulMV(T.R, v, result);
			result.x = T.R.col1.x * v.x + T.R.col2.x * v.y;
			result.y = T.R.col1.y * v.x + T.R.col2.y * v.y;
			
			result.x += T.position.x;
			result.y += T.position.y;
		}
	
		private static var mulXTA:b2Vec2 = new b2Vec2(0, 0);
		private static var mulTX:Number;
	
		[Inline]
		static public function MulXT(T:b2Transform, v:b2Vec2, result:b2Vec2):void
		{
			SubtractVV(v, T.position, mulXTA);
	
			mulTX = (mulXTA.x * T.R.col1.x + mulXTA.y * T.R.col1.y );
	
			mulXTA.y = (mulXTA.x * T.R.col2.x + mulXTA.y * T.R.col2.y );
			mulXTA.x = mulTX;
			
			result.x = mulXTA.x;
			result.y = mulXTA.y;
		}
	
		[Inline]
		static public function AddVV(a:b2Vec2, b:b2Vec2, result:b2Vec2):void
		{
			result.x = a.x + b.x;
			result.y = a.y + b.y;
		}
	
		[Inline]
		static public function SubtractVV(a:b2Vec2, b:b2Vec2, result:b2Vec2):void
		{
			result.x = a.x - b.x;
			result.y = a.y - b.y;
		}
		
		private static var cX:Number;
		private static var cY:Number;
		
		[Inline]
		static public function Distance(a:b2Vec2, b:b2Vec2):Number
		{
			cX = a.x-b.x;
			cY = a.y-b.y;
	
			return Math.sqrt(cX*cX + cY*cY);
		}
		
		private static var aX:Number;
		private static var aY:Number;
		
		static  public function AngleBetweenVectors(p1:b2Vec2, p2:b2Vec2):Number
		{
			aX = p2.x - p1.x;
			aY = p2.y - p1.y;
			
			return Math.atan2(aY, aX);
		}
		
		private static var vertice:b2Vec2, i:int, len:int;

		static public function CalculatePolygonCentroidArray(polygon:Array, centroid:b2Vec2):void
		{
			centroid.x = 0;
			centroid.y = 0;
			
			for(i = 0, len = polygon.length; i < len; i++)
			{
				vertice = polygon[i];
				
				centroid.x += vertice.x;
				centroid.y += vertice.y;
			}
			
			centroid.x = centroid.x / len;
			centroid.y = centroid.y / len;
		}
		
		static public function CalculatePolygonCentroidVector(polygon:Vector.<b2Vec2>, centroid:b2Vec2):void
		{
			centroid.x = 0;
			centroid.y = 0;
			
			for(i = 0, len = polygon.length; i < len; i++)
			{
				vertice = polygon[i];
				
				centroid.x += vertice.x;
				centroid.y += vertice.y;
			}
			
			centroid.x = centroid.x / len;
			centroid.y = centroid.y / len;
		}

		public static var SORT_POLYONG_CENTROID:b2Vec2 = new b2Vec2();
		
		static public function SortPolygonCounterClockWiseCompareCallback(a:b2Vec2, b:b2Vec2):int
		{
			return Math.sin(
				b2Math.AngleBetweenVectors(a, SORT_POLYONG_CENTROID) - 
				b2Math.AngleBetweenVectors(b, SORT_POLYONG_CENTROID)
			) > 0 ? 1 : -1;	
		}
		
		private static var denominator:Number, a:Number, b:Number, numerator1:Number, numerator2:Number;
		
		public static function calculateLineIntersectionPoint(line1StartPoint:b2Vec2, line1EndPoint:b2Vec2, line2StartPoint:b2Vec2, line2EndPoint:b2Vec2, resultPoint:b2Vec2):Boolean
		{
			// if the lines intersect, the result contains the x and y of the intersection 
			// (treating the lines as infinite) and booleans for whether 
			// line segment 1 or line segment 2 contain the point
			
			//denominator = ((line2EndY - line2StartY) * (line1EndX - line1StartX)) - 
			//			    ((line2EndX - line2StartX) * (line1EndY - line1StartY));
			
			denominator = ((line2EndPoint.y - line2StartPoint.y) * (line1EndPoint.x - line1StartPoint.x)) - 
						  ((line2EndPoint.x - line2StartPoint.x) * (line1EndPoint.y - line1StartPoint.y));
			
			if (denominator == 0) return false;
			
			a = line1StartPoint.y - line2StartPoint.y;
			b = line1StartPoint.x - line2StartPoint.x;
			
			numerator1 = ((line2EndPoint.x - line2StartPoint.x) * a) - ((line2EndPoint.y - line2StartPoint.y) * b);
			numerator2 = ((line1EndPoint.x - line1StartPoint.x) * a) - ((line1EndPoint.y - line1StartPoint.y) * b);
			
			a = numerator1 / denominator;
			b = numerator2 / denominator;
			
			// if we cast these lines infinitely in both directions, they intersect here:
			resultPoint.x = line1StartPoint.x + (a * (line1EndPoint.x - line1StartPoint.x));
			resultPoint.y = line1StartPoint.y + (a * (line1EndPoint.y - line1StartPoint.y));
			
			/**
			 // if line1 is a segment and line2 is infinite, they intersect if:
			 if (a > 0 && a < 1) return true;
			 
			 // if line2 is a segment and line1 is infinite, they intersect if:
			 if (b > 0 && b < 1) return true;
			 
			 // if line1 and line2 are segments, they intersect if both of the above are true
			 return result;
			 **/
			
			return true;
		}
	
		static public function VectorAngle(centerX:Number, centerY:Number, radius:Number, angle:Number, result:b2Vec2):void
		{
			result.x = centerX + (radius * Math.cos(angle));
			result.y = centerY + (radius * Math.sin(angle));
		}
	
		[Inline]
		static public function DistanceSquared(a:b2Vec2, b:b2Vec2):Number
		{
			cX = a.x-b.x;
			cY = a.y-b.y;
	
			return (cX*cX + cY*cY);
		}
	
		private static var dX:Number;
		private static var dY:Number;
		
		public static function DistanceBetweenVectors(v1:b2Vec2, v2:b2Vec2):Number
		{
			dX = v1.x - v2.x;
			dY = v1.y - v2.y;
			
			return Math.sqrt((dX*dX) + (dY*dY));
		}
		
		[Inline]
		static public function MulFV(s:Number, a:b2Vec2, result:b2Vec2):void
		{
			result.x = s * a.x;
			result.y = s * a.y;
		}
	
		private static var _addV1:b2Vec2 = new b2Vec2();
		private static var _addV2:b2Vec2 = new b2Vec2();
		
		[Inline]
		static public function AddMM(A:b2Mat22, B:b2Mat22, result:b2Mat22):void
		{
			AddVV(A.col1, B.col1, _addV1);
			AddVV(A.col2, B.col2, _addV2); 
				
			b2Mat22.FromVV(_addV1, _addV2, result);
		}
	
		private static var _mulMMC1:b2Vec2 = new b2Vec2(0, 0);
		private static var _mulMMC2:b2Vec2 = new b2Vec2(0, 0);
		
		// A * B
		[Inline]
		static public function MulMM(A:b2Mat22, B:b2Mat22, result:b2Mat22):void
		{
			MulMV(A, B.col1, _mulMMC1);
			MulMV(A, B.col2, _mulMMC2);
	
			b2Mat22.FromVV(_mulMMC1, _mulMMC2, result);
		}
	
		private static var _mulTMMC1:b2Vec2 = new b2Vec2(0, 0);
		private static var _mulTMMC2:b2Vec2 = new b2Vec2(0, 0);
		
		// A^T * B
		[Inline]
		static public function MulTMM(A:b2Mat22, B:b2Mat22, result:b2Mat22):void
		{
			_mulTMMC1.x = Dot(A.col1, B.col1);
			_mulTMMC1.y = Dot(A.col2, B.col1);
			
			_mulTMMC2.x = Dot(A.col1, B.col2);
			_mulTMMC2.y = Dot(A.col2, B.col2);
	
			b2Mat22.FromVV(_mulTMMC1, _mulTMMC2, result);
		}
	
		[Inline]
		static public function Abs(a:Number):Number
		{
			return a > b2Settings.DECIMAL_ZERO ? a : -a;
		}
	
		[Inline]
		static public function AbsV(a:b2Vec2, result:b2Vec2):void
		{		
			result.x = a.x > b2Settings.DECIMAL_ZERO ? a.x : -a.x;
			result.y = a.y > b2Settings.DECIMAL_ZERO ? a.y : -a.y;
		}
	
		private static var _absmCol1:b2Vec2 = new b2Vec2(0, 0);
		private static var _absmCol2:b2Vec2 = new b2Vec2(0, 0);
		
		[Inline]
		static public function AbsM(A:b2Mat22, result:b2Mat22):void
		{
			AbsV(A.col1, _absmCol1);
			AbsV(A.col2, _absmCol2);
			
			b2Mat22.FromVV(_absmCol1, _absmCol2, result);
		}
	
		[Inline]
		static public function Min(a:Number, b:Number):Number
		{
			return a < b ? a : b;
		}
	
		[Inline]
		static public function MinV(a:b2Vec2, b:b2Vec2, result:b2Vec2):void
		{
			result.x = Min(a.x, b.x);
			result.y = Min(a.y, b.y);
		}
	
		[Inline]
		static public function Max(a:Number, b:Number):Number
		{
			return a > b ? a : b;
		}
	
		[Inline]
		static public function MaxV(a:b2Vec2, b:b2Vec2, result:b2Vec2):void
		{
			result.x = Max(a.x, b.x);
			result.y = Max(a.y, b.y);
		}
	
		[Inline]
		static public function Clamp(a:Number, low:Number, high:Number):Number
		{
			return a < low ? low : a > high ? high : a;
		}
	
		private static var _clampMin:b2Vec2 = new b2Vec2(0, 0);
		
		[Inline]
		static public function ClampV(a:b2Vec2, low:b2Vec2, high:b2Vec2, result:b2Vec2):void
		{
			MinV(a, high, _clampMin);
			
			MaxV(low, _clampMin, result);
		}
	
		[Inline]
		static public function Swap(a:Array, b:Array) : void
		{
			var tmp:* = a[0];
			a[0] = b[0];
			b[0] = tmp;
		}
	
		// b2Random number in range [-1,1]
		[Inline]
		static public function Random():Number
		{
			return Math.random() * 2 - 1;
		}
	
		[Inline]
		static public function RandomRange(lo:Number, hi:Number) : Number
		{
			var r:Number = Math.random();
			r = (hi - lo) * r + lo;
			return r;
		}
	
		// "Next Largest Power of 2
		// Given a binary integer value x, the next largest power of 2 can be computed by a SWAR algorithm
		// that recursively "folds" the upper bits into the lower bits. This process yields a bit vector with
		// the same most significant 1 as x, but all 1's below it. Adding 1 to that value yields the next
		// largest power of 2. For a 32-bit value:"
		[Inline]
		static public function NextPowerOfTwo(x:uint):uint
		{
			x |= (x >> 1) & 0x7FFFFFFF;
			x |= (x >> 2) & 0x3FFFFFFF;
			x |= (x >> 4) & 0x0FFFFFFF;
			x |= (x >> 8) & 0x00FFFFFF;
			x |= (x >> 16)& 0x0000FFFF;
			return x + 1;
		}
	
		[Inline]
		static public function IsPowerOfTwo(x:uint):Boolean
		{
			var result:Boolean = x > 0 && (x & (x - 1)) == 0;
			return result;
		}
		
		static public const b2Vec2_zero:b2Vec2 = new b2Vec2(0.0, 0.0);
		
		static public const b2Mat22_identity:b2Mat22 = b2Pool.getMat22(1.0, 0.0, 0.0, 1.0);
		
		static public const b2Transform_identity:b2Transform = new b2Transform(b2Vec2_zero, b2Mat22_identity);
		
	
	}
}
