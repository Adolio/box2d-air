﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Common.Math
{	
	import Box2D.Common.b2Settings;

	/**
	* A 2-by-2 matrix. Stored in column-major order.
	*/
	public class b2Mat22
	{
		public function b2Mat22()
		{
			col1.x = col2.y = 1.0;
		}
		
		[Inline]
		public static function FromVV(c1:b2Vec2, c2:b2Vec2, result:b2Mat22):void
		{
			result.SetVV(c1, c2);
		}
	
		private static var _c:Number;
		private static var _s:Number;
		
		[Inline]
		final public function Set(angle:Number):void
		{
			_c = Math.cos(angle);
			_s = Math.sin(angle);
			
			col1.x = _c; col2.x = -_s;
			col1.y = _s; col2.y = _c;
		}
		
		[Inline]
		final public function SetVV(c1:b2Vec2, c2:b2Vec2):void
		{
			col1.x = c1.x;
			col1.y = c1.y;
			
			col2.x = c2.x;
			col2.y = c2.y;
		}
		
		[Inline]
		final public function Copy():b2Mat22
		{
			var mat:b2Mat22 = new b2Mat22();
			mat.SetM(this);
			return mat;
		}
		
		[Inline]
		final public function SetM(m:b2Mat22):void
		{
			col1.x = m.col1.x;
			col1.y = m.col1.y;
			
			col2.x = m.col2.x;
			col2.y = m.col2.y;
		}
		
		[Inline]
		final public function AddM(m:b2Mat22):void
		{
			col1.x += m.col1.x;
			col1.y += m.col1.y;
			col2.x += m.col2.x;
			col2.y += m.col2.y;
		}
		
		[Inline]
		final public function SetIdentity() : void
		{
			col1.x = 1.0; col2.x = 0.0;
			col1.y = 0.0; col2.y = 1.0;
		}
	
		[Inline]
		final public function SetZero():void
		{
			col1.x = 0.0; col2.x = 0.0;
			col1.y = 0.0; col2.y = 0.0;
		}
		
		[Inline]
		final public function GetAngle():Number
		{
			return Math.atan2(col1.y, col1.x);
		}
	
		/**
		 * Compute the inverse of this matrix, such that inv(A) * A = identity.
		 */
		
		private var i_a:Number;
		private var i_b:Number;
		private var i_c:Number;
		private var i_d:Number;
		
		private var i_det:Number;
		
		[Inline]
		final public function GetInverse(out:b2Mat22):void
		{
			i_a = col1.x; 
			i_b = col2.x; 
			i_c = col1.y; 
			i_d = col2.y;
			
			//var B:b2Mat22 = new b2Mat22();
			i_det = i_a * i_d - i_b * i_c;
			
			if(i_det != 0.0)
			{
				i_det = b2Settings.DECIMAL_ONE / i_det;
			}
			
			out.col1.x =  i_det * i_d;	out.col2.x = -i_det * i_b;
			out.col1.y = -i_det * i_c;	out.col2.y =  i_det * i_a;
		}
		
		private var a11:Number;
		private var a12:Number;
		private var a21:Number;
		private var a22:Number;

		private var det:Number;
		
		// Solve A * x = b
		[Inline]
		final public function Solve(out:b2Vec2, bX:Number, bY:Number):void
		{
			//float32 a11 = col1.x, a12 = col2.x, a21 = col1.y, a22 = col2.y;
			a11 = col1.x;
			a12 = col2.x;
			a21 = col1.y;
			a22 = col2.y;
			
			//float32 det = a11 * a22 - a12 * a21;
			det = a11 * a22 - a12 * a21;
			
			if(det != 0.0)
			{
				det = 1.0 / det;
			}

			out.x = det * (a22 * bX - a12 * bY);
			out.y = det * (a11 * bY - a21 * bX);
		}
		
		[Inline]
		final public function Abs() : void
		{
			col1.Abs();
			col2.Abs();
		}
	
		public var col1:b2Vec2 = new b2Vec2();
		public var col2:b2Vec2 = new b2Vec2();
	};
}