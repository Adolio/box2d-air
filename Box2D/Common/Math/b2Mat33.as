﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Common.Math
{
	import Box2D.Common.b2Settings;

	/**
	* A 3-by-3 matrix. Stored in column-major order.
	*/
	final public class b2Mat33
	{
		public var col1:b2Vec3 = new b2Vec3();
		public var col2:b2Vec3 = new b2Vec3();
		public var col3:b2Vec3 = new b2Vec3();
		
		private static var det:Number;
		
		final public function b2Mat33(c1:b2Vec3=null, c2:b2Vec3=null, c3:b2Vec3=null)
		{
			if (!c1 && !c2 && !c3)
			{
				col1.SetZero();
				col2.SetZero();
				col3.SetZero();
			}
			else
			{
				col1.x = c1.x;
				col1.y = c1.y;
				
				col2.x = c2.x;
				col2.y = c2.y;
				
				col3.x = c3.x;
				col3.y = c3.y;
			}
		}
		
		[Inline]
		final public function SetVVV(c1:b2Vec3, c2:b2Vec3, c3:b2Vec3) : void
		{
			col1.x = c1.x;
			col1.y = c1.y;
		
			col2.x = c2.x;
			col2.y = c2.y;
			
			col3.x = c3.x;
			col3.y = c3.y;
		}
		
		[Inline]
		final public function SetM(m:b2Mat33):void
		{
			col1.x = m.col1.x;
			col1.y = m.col1.y;
			
			col2.x = m.col2.x;
			col2.y = m.col2.y;
			
			col3.x = m.col3.x;
			col3.y = m.col3.y;
		}
		
		[Inline]
		final public function AddM(m:b2Mat33):void
		{
			col1.x += m.col1.x;
			col1.y += m.col1.y;
			col1.z += m.col1.z;
			col2.x += m.col2.x;
			col2.y += m.col2.y;
			col2.z += m.col2.z;
			col3.x += m.col3.x;
			col3.y += m.col3.y;
			col3.z += m.col3.z;
		}
		
		[Inline]
		final public function SetIdentity():void
		{
			col1.x = 1.0; col2.x = 0.0; col3.x = 0.0;
			col1.y = 0.0; col2.y = 1.0; col3.y = 0.0;
			col1.z = 0.0; col2.z = 0.0; col3.z = 1.0;
		}
	
		[Inline]
		final public function SetZero() : void
		{
			col1.x = b2Settings.DECIMAL_ZERO; col2.x = b2Settings.DECIMAL_ZERO; col3.x = b2Settings.DECIMAL_ZERO;
			col1.y = b2Settings.DECIMAL_ZERO; col2.y = b2Settings.DECIMAL_ZERO; col3.y = b2Settings.DECIMAL_ZERO;
			col1.z = b2Settings.DECIMAL_ZERO; col2.z = b2Settings.DECIMAL_ZERO; col3.z = b2Settings.DECIMAL_ZERO;
		}
		
		// Solve A * x = b
		[Inline]
		final public function Solve22(out:b2Vec2, bX:Number, bY:Number):void
		{
			det = col1.x * col2.y - col2.x * col1.y;
	
			if(det != b2Settings.DECIMAL_ZERO)
			{
				det = b2Settings.DECIMAL_ONE / det;
			}
	
			out.x = det * (col2.y * bX - col2.x * bY);
			out.y = det * (col1.x * bY - col1.y * bX);
		}
	
		// Solve A * x = b
		[Inline]
		final public function Solve33(out:b2Vec3, bX:Number, bY:Number, bZ:Number):void
		{
			//float32 det = b2Dot(col1, b2Cross(col2, col3));
			det = col1.x * (col2.y * col3.z - col2.z * col3.y) +
				  col1.y * (col2.z * col3.x - col2.x * col3.z) +
				  col1.z * (col2.x * col3.y - col2.y * col3.x);

			if(det != b2Settings.DECIMAL_ZERO)
			{
				det = b2Settings.DECIMAL_ONE / det;
			}

			//out.x = det * b2Dot(b, b2Cross(col2, col3));
			out.x = det * (	bX * (col2.y * col3.z - col2.z * col3.y) +
							bY * (col2.z * col3.x - col2.x * col3.z) +
							bZ * (col2.x * col3.y - col2.y * col3.x) );

			//out.y = det * b2Dot(col1, b2Cross(b, col3));
			out.y = det * (	col1.x * (bY * col3.z - bZ * col3.y) +
					col1.y * (bZ * col3.x - bX * col3.z) +
					col1.z * (bX * col3.y - bY * col3.x));

			//out.z = det * b2Dot(col1, b2Cross(col2, b));
			out.z = det * (	col1.x * (col2.y * bZ - col2.z * bY) +
					col1.y * (col2.z * bX - col2.x * bZ) +
					col1.z * (col2.x * bY - col2.y * bX));
		}
	}
}