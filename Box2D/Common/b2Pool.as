package Box2D.Common
{
	import Box2D.Collision.b2AABB;
	import Box2D.Collision.b2DistanceProxy;
	import Box2D.Collision.Shapes.b2CircleShape;
	import Box2D.Collision.Shapes.b2MassData;
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Common.Math.b2Mat22;
	import Box2D.Common.Math.b2Sweep;
	import Box2D.Common.Math.b2Transform;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Common.Math.b2Vec3;
	import Box2D.Dynamics.b2FilterData;
	import Box2D.Dynamics.b2Fixture;
	import Box2D.Dynamics.Contacts.Type.b2CircleContact;
	import Box2D.Dynamics.Contacts.Type.b2PolyAndCircleContact;
	import Box2D.Dynamics.Contacts.Type.b2PolygonContact;
	import Box2D.Dynamics.Controllers.b2BuoyancyController;
	import Box2D.Dynamics.Controllers.b2ControllerEdge;

	final public class b2Pool
	{
		private static var _vector2Pool:Vector.<b2Vec2> = new Vector.<b2Vec2>;
		private static var _vector3Pool:Vector.<b2Vec3> = new Vector.<b2Vec3>;

		private static var _mat22Pool:Vector.<b2Mat22> = new Vector.<b2Mat22>;

		private static var _aabbPool:Vector.<b2AABB> = new Vector.<b2AABB>;

		private static var _distanceProxyPool:Vector.<b2DistanceProxy> = new Vector.<b2DistanceProxy>;

		private static var _filterDataPool:Vector.<b2FilterData> = new Vector.<b2FilterData>;

		private static var _massDataPool:Vector.<b2MassData> = new Vector.<b2MassData>;

		private static var _circleContactPool:Vector.<b2CircleContact> = new Vector.<b2CircleContact>;
		private static var _polyAndCircleContactPool:Vector.<b2PolyAndCircleContact> = new Vector.<b2PolyAndCircleContact>;
		private static var _polygonContactPool:Vector.<b2PolygonContact> = new Vector.<b2PolygonContact>;
		
		private static var _polygonShapePool:Vector.<b2PolygonShape> = new Vector.<b2PolygonShape>;
		private static var _circleShapePool:Vector.<b2CircleShape> = new Vector.<b2CircleShape>;
		
		private static var _fixturePool:Vector.<b2Fixture> = new Vector.<b2Fixture>;
		
		private static var _b2TransformPool:Vector.<b2Transform> = new Vector.<b2Transform>;
		private static var _b2SweepPool:Vector.<b2Sweep> = new Vector.<b2Sweep>;
		
		private static var _b2BodyVectorPool:Array = new Array();
		private static var _b2Vec2ArrayPool:Array = new Array();

		private static var _buoyancyControllerPool:Vector.<b2BuoyancyController> = new Vector.<b2BuoyancyController>;
		
		private static var _controllerEdgePool:Vector.<b2ControllerEdge> = new Vector.<b2ControllerEdge>;

		public function b2Pool()
		{
		}
		
		private static var g_vec2:b2Vec2;

		[Inline]
		public static function getVector2(xPos:Number, yPos:Number):b2Vec2
		{
			if(_vector2Pool.length > 0)
			{
				g_vec2 = _vector2Pool.pop();
				g_vec2.x = xPos;
				g_vec2.y = yPos;
			}
			else
			{
				g_vec2 = new b2Vec2(xPos, yPos);
			}
			
			return g_vec2;
		}
		
		[Inline]
		public static function putVector2(vector:b2Vec2):void
		{
			if (vector) _vector2Pool[_vector2Pool.length] = vector;         
		}
		
		private static var g_vec3:b2Vec3;
		
		[Inline]
		public static function getVector3(xPos:Number = 0, yPos:Number = 0, zPos:Number = 0):b2Vec3
		{
			if(_vector3Pool.length > 0)
			{
				g_vec3 = _vector3Pool.pop();
				g_vec3.x = xPos;
				g_vec3.y = yPos;
				g_vec3.z = zPos;
			}
			else
			{
				g_vec3 = new b2Vec3(xPos, yPos, zPos);
			}
			
			return g_vec3;
		}
		
		[Inline]
		public static function putVector3(vector:b2Vec3):void
		{
			if (vector) _vector3Pool[_vector3Pool.length] = vector;
		}
		
		[Inline]
		public static function getMat22(x1:Number, y1:Number, x2:Number, y2:Number):b2Mat22
		{
			var mat:b2Mat22;
			
			if(_mat22Pool.length > 0)
			{
				mat = _mat22Pool.pop();
			}
			else
			{
				mat = new b2Mat22();
			}
			
			mat.col1.x = x1;
			mat.col1.y = y1;
			
			mat.col2.x = x2;
			mat.col2.y = y2;
			
			return mat;
		}
		
		[Inline]
		public static function putMat22(mat:b2Mat22):void
		{
			if (mat) _mat22Pool[_mat22Pool.length] = mat;
		}
		
		[Inline]
		public static function getAABB():b2AABB
		{
			if(_aabbPool.length > 0)
				return _aabbPool.pop();
			
			return new b2AABB();
		}
		
		[Inline]
		public static function putAABB(aabb:b2AABB):void
		{
			if (aabb) _aabbPool[_aabbPool.length] = aabb;
		}
		
		private static var g_distanceProxy:b2DistanceProxy;

		[Inline]
		public static function getDistanceProxy():b2DistanceProxy
		{
			if(_distanceProxyPool.length > 0)
			{
				g_distanceProxy = _distanceProxyPool.pop();

				if(g_distanceProxy.m_vertices == null)
				{
					g_distanceProxy.m_vertices = b2Pool.getVector2Array();
				} 
				else
				{
					g_distanceProxy.m_vertices.length = 0;
				}

				g_distanceProxy.m_vertices[0] = b2Pool.getVector2(0, 0);
			}
			else
			{
				g_distanceProxy = new b2DistanceProxy();
			}

			return g_distanceProxy;
		}
		
		[Inline]
		public static function putDistanceProxy(distanceProxy:b2DistanceProxy):void
		{
			if (distanceProxy) _distanceProxyPool[_distanceProxyPool.length] = distanceProxy;
		}
		
		private static var g_massData:b2MassData;

		[Inline]
		public static function getMassData():b2MassData
		{
			if(_massDataPool.length > 0)
				return _massDataPool.pop();

			return new b2MassData();
		}
		
		[Inline]
		public static function putMassData(massData:b2MassData):void
		{
			if (massData) _massDataPool[_massDataPool.length] = massData;
		}
		
		private static var g_filterData:b2FilterData;

		[Inline]
		public static function getFilterData(categoryBits:uint = 0x0001,
											 maskBits:uint = 0xFFFF,
											 groupIndex:int = 0):b2FilterData
		{
			if(_filterDataPool.length > 0)
			{
				g_filterData = _filterDataPool.pop();
			}
			else
			{
				g_filterData = new b2FilterData();
			}
			
			g_filterData.categoryBits = categoryBits;
			g_filterData.maskBits = maskBits;
			g_filterData.groupIndex = groupIndex;
			
			return g_filterData;
		}
		
		[Inline]
		public static function putFilterData(filterData:b2FilterData):void
		{
			if (filterData) _filterDataPool[_filterDataPool.length] = filterData;
		}
		
		[Inline]
		public static function getCircleContact():b2CircleContact
		{
			if(_circleContactPool.length > 0)
				return _circleContactPool.pop();
			
			return new b2CircleContact();
		}
		
		[Inline]
		public static function putCircleContact(contact:b2CircleContact):void
		{
			if (contact) _circleContactPool[_circleContactPool.length] = contact;
		}
		
		[Inline]
		public static function getPolyAndCircleContact():b2PolyAndCircleContact
		{
			if(_polyAndCircleContactPool.length > 0)
				return _polyAndCircleContactPool.pop();
			
			return new b2PolyAndCircleContact();
		}
		
		[Inline]
		public static function putPolyAndCircleContact(contact:b2PolyAndCircleContact):void
		{
			if (contact) _polyAndCircleContactPool[_polyAndCircleContactPool.length] = contact;
		}
		
		[Inline]
		public static function getPolyContact():b2PolygonContact
		{
			if(_polygonContactPool.length > 0)
				return _polygonContactPool.pop();
			
			return new b2PolygonContact();
		}
		
		[Inline]
		public static function putPolyContact(contact:b2PolygonContact):void
		{
			if (contact) _polygonContactPool[_polygonContactPool.length] = contact;
		}

		[Inline]
		public static function getPolyShape():b2PolygonShape
		{
			var shape:b2PolygonShape;

			if(_polygonShapePool.length > 0)
			{
				shape = _polygonShapePool.pop();

				if(shape.m_distanceProxy == null)
				{
					shape.m_distanceProxy = b2Pool.getDistanceProxy();
				}

				if(shape.m_vertices == null)
				{
					shape.m_vertices = b2Pool.getVector2Array();
				}

				if(shape.m_normals == null)
				{
					shape.m_normals = b2Pool.getVector2Array();
				}

				if(shape.m_centroid == null)
				{
					shape.m_centroid = b2Pool.getVector2(0, 0);
				}

				shape.Reset();
			}
			else
			{
				shape = new b2PolygonShape();
			}

			return shape;
		}
		
		[Inline]
		public static function putPolyShape(shape:b2PolygonShape):void
		{
			if (shape) _polygonShapePool[_polygonShapePool.length] = shape;
		}
		
		
		
		[Inline]
		public static function getCircleShape(radius:Number):b2CircleShape
		{
			var shape:b2CircleShape;
			
			if(_circleShapePool.length > 0)
			{
				shape = _circleShapePool.pop();
				shape.Reset();

				shape.SetRadius(radius);
				
				if(shape.m_p == null)
				{
					shape.m_p = b2Pool.getVector2(0, 0);
				}
				
				if(shape.m_distanceProxy == null)
				{
					shape.m_distanceProxy = b2Pool.getDistanceProxy();
				}				
			}
			else
			{
				shape = new b2CircleShape(radius);
			}
			
			return shape;
		}
		
		[Inline]
		public static function putCircleShape(shape:b2CircleShape):void
		{
			if (shape) _circleShapePool[_circleShapePool.length] = shape;
		}
		
		
		[Inline]
		public static function getFixture():b2Fixture
		{
			var fixture:b2Fixture;
			
			if(_fixturePool.length > 0)
			{
				fixture = _fixturePool.pop();
				fixture.Initialize();
			}
			else
			{
				fixture = new b2Fixture();
			}

			return fixture;
		}
		
		[Inline]
		public static function putFixture(fixture:b2Fixture):void
		{
			if (fixture) _fixturePool[_fixturePool.length] = fixture;
		}
		
		
		
		[Inline]
		public static function getSweep():b2Sweep
		{
			if(_b2SweepPool.length > 0)
				return _b2SweepPool.pop();
			
			return new b2Sweep();
		}
		
		[Inline]
		public static function putSweep(sweep:b2Sweep):void
		{
			if (sweep) _b2SweepPool[_b2SweepPool.length] = sweep;
		}
		
		[Inline]
		public static function getTransform():b2Transform
		{
			if(_b2TransformPool.length > 0)
				return _b2TransformPool.pop();
			
			return new b2Transform();
		}
		
		[Inline]
		public static function putTransform(transform:b2Transform):void
		{
			if (transform) _b2TransformPool[_b2TransformPool.length] = transform;
		}
		
		[Inline]
		public static function getb2BodyArray():Array
		{
			if(_b2BodyVectorPool.length > 0)
				return _b2BodyVectorPool.pop();
			
			return new Array();
		}
		
		[Inline]
		public static function putb2BodyArray(vector:Array):void
		{
			if (vector) _b2BodyVectorPool[_b2BodyVectorPool.length] = vector;
		}

		[Inline]
		public static function getVector2Array():Array
		{
			if(_b2Vec2ArrayPool.length > 0)
				return _b2Vec2ArrayPool.pop();
	
			return new Array();
		}
		
		[Inline]
		public static function putVector2Vector(vector:Array):void
		{
			if (vector) _b2Vec2ArrayPool[_b2Vec2ArrayPool.length] = vector;
		}
		
		public static function getb2BuoyancyController():b2BuoyancyController
		{
			if(_buoyancyControllerPool.length > 0)
				return _buoyancyControllerPool.pop();
			
			return new b2BuoyancyController();
		}
		
		public static function putb2BuoyancyController(value:b2BuoyancyController):void
		{
			if(value) _buoyancyControllerPool[_buoyancyControllerPool.length] = value;	
		}
		
		public static function getControllerEdge():b2ControllerEdge
		{
			if(_controllerEdgePool.length > 0)
				return _controllerEdgePool.pop();
			
			return new b2ControllerEdge();
		}
		
		public static function putb2ControllerEdge(value:b2ControllerEdge):void
		{
			if(value)
			{
				value.body = null;
				value.controller = null;
				value.nextBody = null;
				value.prevBody = null;
				value.nextController = null;
				value.prevController = null;

				_controllerEdgePool[_controllerEdgePool.length] = value;	
			}
		}
	}
}