﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Dynamics.Contacts
{
	import Box2D.Collision.b2Manifold;
	import Box2D.Collision.b2ManifoldPoint;
	import Box2D.Collision.b2WorldManifold;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Mat22;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Fixture;
	import Box2D.Dynamics.b2TimeStep;
	
	
	/**
	* @private
	*/
	public class b2ContactSolver
	{
		public function b2ContactSolver()
		{
			s_worldManifold = new b2WorldManifold();
		}
		
		private static const k_maxConditionNumber:Number = 100.0;
		
		private var s_worldManifold:b2WorldManifold;
		
		private var fixtureA:b2Fixture;
		private var fixtureB:b2Fixture;
		private var shapeA:b2Shape;
		private var shapeB:b2Shape;
		private var radiusA:Number;
		private var radiusB:Number;

		private var manifold:b2Manifold;
		
		private var friction:Number;
		private var restitution:Number;
		
		private var vAX:Number;
		private var vAY:Number;
		
		private var vBX:Number;
		private var vBY:Number;
		
		private var wA:Number;
		private var wB:Number;
		
		private var cc:b2ContactConstraint;
		
		private var cp:b2ManifoldPoint;
		private var ccp:b2ContactConstraintPoint;
		
		private var rAX:Number;
		private var rAY:Number;
		private var rBX:Number;
		private var rBY:Number;
		
		private var rnA:Number;
		private var rnB:Number;
		
		private var k:uint;
		private var kNormal:Number;
		private var kEqualized:Number;
		
		private var tangentX:Number;
		private var tangentY:Number;
		
		private var rtA:Number;
		private var rtB:Number;
		
		private var kTangent:Number;
		
		private var tX:Number;
		private var tY:Number;
		private var vRel:Number;
		
		private var ccp1:b2ContactConstraintPoint;
		private var ccp2:b2ContactConstraintPoint;
		
		private var invMassA:Number;
		private var invMassB:Number;
		
		private var rn1A:Number;
		private var rn1B:Number;
		private var rn2A:Number;
		private var rn2B:Number;
		
		private var k11:Number;
		private var k22:Number;
		private var k12:Number;
		
		private var tVec:b2Vec2;
		private var tMat:b2Mat22;

		private var tVec2:b2Vec2;
		
		private var j:int;
		private var tCount:int;
		
		private var c:b2ContactConstraint
		
		private var PX:Number;
		private var PY:Number;

		private var dvX:Number;
		private var dvY:Number;
		private var vn:Number;
		private var vt:Number;
		private var lambda:Number;
		private var maxFriction:Number;
		private var newImpulse:Number;
		private var dX:Number;
		private var dY:Number;
		private var P1X:Number;
		private var P1Y:Number;
		private var P2X:Number;
		private var P2Y:Number;

		private var vA:b2Vec2;
		private var vB:b2Vec2;
		
		private var cp1:b2ContactConstraintPoint;
		private var cp2:b2ContactConstraintPoint;
		
		private var aX:Number;
		private var aY:Number;
		
		private var dv1X:Number;
		private var dv1Y:Number;
		private var dv2X:Number;
		private var dv2Y:Number;
		
		private var vn1:Number;
		
		private var vn2:Number;
		
		private var bX:Number;
		private var bY:Number;
		
		private var xX:Number;
		private var xY:Number;
		
		private var point:b2Vec2;
		private var separation:Number;
		
		private var C:Number;
		
		private var impulse:Number;
		private var contact:b2Contact;

		private static var _vertice:b2Vec2;

		[Inline]
		final public function Initialize(step:b2TimeStep, contacts:Vector.<b2Contact>, contactCount:int):void
		{			
			m_step.Set(step);

			m_constraintCount = contactCount;
	
			// fill vector to hold enough constraints
			while (m_constraints.length < m_constraintCount)
			{
				m_constraints[m_constraints.length] = new b2ContactConstraint();
			}			
			
			for(i = 0; i < contactCount; ++i)
			{
				contact = contacts[i];
				
				fixtureA = contact.m_fixtureA;
				fixtureB = contact.m_fixtureB;
				shapeA = fixtureA.m_shape;
				shapeB = fixtureB.m_shape;
				radiusA = shapeA.m_radius;
				radiusB = shapeB.m_radius;

				manifold = contact.m_manifold;
				
				friction = b2Settings.b2MixFriction(fixtureA.m_friction, fixtureB.m_friction);
				restitution = b2Settings.b2MixRestitution(fixtureA.m_restitution, fixtureB.m_restitution);

				vAX = fixtureA.m_body.m_linearVelocity.x;
				vAY = fixtureA.m_body.m_linearVelocity.y;

				vBX = fixtureB.m_body.m_linearVelocity.x;
				vBY = fixtureB.m_body.m_linearVelocity.y;
				wA = fixtureA.m_body.m_angularVelocity;
				wB = fixtureB.m_body.m_angularVelocity;
				
				//b2Settings.b2Assert(manifold.m_pointCount > 0);
				if(manifold.m_pointCount <= 0)
				{
					throw Error('Invalid point count');
				}
				
				s_worldManifold.Initialize(manifold, fixtureA.m_body.m_xf, radiusA, fixtureB.m_body.m_xf, radiusB);
				
				cc = m_constraints[i];
				cc.bodyA = fixtureA.m_body; //p
				cc.bodyB = fixtureB.m_body; //p
				cc.manifold = manifold; //p

				cc.normal.x = s_worldManifold.m_normal.x;
				cc.normal.y = s_worldManifold.m_normal.y;

				cc.pointCount = manifold.m_pointCount;
				cc.friction = friction;
				cc.restitution = restitution;
				
				cc.localPlaneNormal.x = manifold.m_localPlaneNormal.x;
				cc.localPlaneNormal.y = manifold.m_localPlaneNormal.y;
				cc.localPoint.x = manifold.m_localPoint.x;
				cc.localPoint.y = manifold.m_localPoint.y;
				cc.radius = radiusA + radiusB;
				cc.type = manifold.m_type;
				
				for(k = 0; k < cc.pointCount; ++k)
				{
					cp = manifold.m_points[ k ];
					ccp = cc.points[ k ];
					
					ccp.normalImpulse = cp.m_normalImpulse;
					ccp.tangentImpulse = cp.m_tangentImpulse;
					
					ccp.localPoint.x = cp.m_localPoint.x;
					ccp.localPoint.y = cp.m_localPoint.y;
					
					_vertice = s_worldManifold.m_points[k];
					
					rAX = ccp.rA.x = _vertice.x - fixtureA.m_body.m_sweep.c.x;
					rAY = ccp.rA.y = _vertice.y - fixtureA.m_body.m_sweep.c.y;
					rBX = ccp.rB.x = _vertice.x - fixtureB.m_body.m_sweep.c.x;
					rBY = ccp.rB.y = _vertice.y - fixtureB.m_body.m_sweep.c.y;
					
					rnA = rAX * s_worldManifold.m_normal.y - rAY * s_worldManifold.m_normal.x;
					rnB = rBX * s_worldManifold.m_normal.y - rBY * s_worldManifold.m_normal.x;
					
					rnA *= rnA;
					rnB *= rnB;
					
					kNormal = fixtureA.m_body.m_invMass + fixtureB.m_body.m_invMass + fixtureA.m_body.m_invI * rnA + fixtureB.m_body.m_invI * rnB;

					ccp.normalMass = b2Settings.DECIMAL_ONE / kNormal;
					
					kEqualized = fixtureA.m_body.m_mass * fixtureA.m_body.m_invMass + fixtureB.m_body.m_mass * fixtureB.m_body.m_invMass;
					kEqualized += fixtureA.m_body.m_mass * fixtureA.m_body.m_invI * rnA + fixtureB.m_body.m_mass * fixtureB.m_body.m_invI * rnB;

					ccp.equalizedMass = b2Settings.DECIMAL_ONE / kEqualized;

					tangentX = s_worldManifold.m_normal.y
					tangentY = -s_worldManifold.m_normal.x;
					
					rtA = rAX*tangentY - rAY*tangentX;
					rtB = rBX*tangentY - rBY*tangentX;
					
					rtA *= rtA;
					rtB *= rtB;
					
					kTangent = fixtureA.m_body.m_invMass + fixtureB.m_body.m_invMass + fixtureA.m_body.m_invI * rtA + fixtureB.m_body.m_invI * rtB;
					
					ccp.tangentMass = b2Settings.DECIMAL_ONE /  kTangent;
					
					ccp.velocityBias = b2Settings.DECIMAL_ZERO;

					tX = vBX + (-wB*rBY) - vAX - (-wA*rAY);
					tY = vBY + (wB*rBX) - vAY - (wA*rAX);
					vRel = cc.normal.x*tX + cc.normal.y*tY;

					if (vRel < -b2Settings.b2_velocityThreshold)
					{
						ccp.velocityBias += -cc.restitution * vRel;
					}
				}
				
				// If we have two points, then prepare the block solver.
				if(cc.pointCount == 2)
				{
					ccp1 = cc.points[0];
					ccp2 = cc.points[1];

					rn1A = ccp1.rA.x * s_worldManifold.m_normal.y - ccp1.rA.y * s_worldManifold.m_normal.x;
					rn1B = ccp1.rB.x * s_worldManifold.m_normal.y - ccp1.rB.y * s_worldManifold.m_normal.x;
					rn2A = ccp2.rA.x * s_worldManifold.m_normal.y - ccp2.rA.y * s_worldManifold.m_normal.x;
					rn2B = ccp2.rB.x * s_worldManifold.m_normal.y - ccp2.rB.y * s_worldManifold.m_normal.x;
					
					k11 = fixtureA.m_body.m_invMass + fixtureB.m_body.m_invMass + fixtureA.m_body.m_invI * rn1A * rn1A + fixtureB.m_body.m_invI * rn1B * rn1B;
					k22 = fixtureA.m_body.m_invMass + fixtureB.m_body.m_invMass + fixtureA.m_body.m_invI * rn2A * rn2A + fixtureB.m_body.m_invI * rn2B * rn2B;
					k12 = fixtureA.m_body.m_invMass + fixtureB.m_body.m_invMass + fixtureA.m_body.m_invI * rn1A * rn2A + fixtureB.m_body.m_invI * rn1B * rn2B;
					
					// Ensure a reasonable condition number.
					if(k11 * k11 < k_maxConditionNumber * (k11 * k22 - k12 * k12))
					{
						// K is safe to invert.
						cc.K.col1.Set(k11, k12);
						cc.K.col2.Set(k12, k22);
						cc.K.GetInverse(cc.normalMass);
					}
					else
					{
						// The constraints are redundant, just use one.
						// TODO_ERIN use deepest?
						cc.pointCount = 1;
					}
				}
				
				_vertice = null;
			}
		}
	
		[Inline]
		final public function InitVelocityConstraints(step:b2TimeStep):void
		{
			// Warm start.
			for (i = 0; i < m_constraintCount; ++i)
			{
				c = m_constraints[i];

				tangentX = c.normal.y;
				tangentY = -c.normal.x;
				
				if (step.warmStarting)
				{
					tCount = c.pointCount;
	
					for (j = 0; j < tCount; ++j)
					{
						ccp = c.points[ j ];
						ccp.normalImpulse *= step.dtRatio;
						ccp.tangentImpulse *= step.dtRatio;
	
						PX = ccp.normalImpulse * c.normal.x + ccp.tangentImpulse * tangentX;
						PY = ccp.normalImpulse * c.normal.y + ccp.tangentImpulse * tangentY;

						c.bodyA.m_angularVelocity -= c.bodyA.m_invI * (ccp.rA.x * PY - ccp.rA.y * PX);

						c.bodyA.m_linearVelocity.x -= c.bodyA.m_invMass * PX;
						c.bodyA.m_linearVelocity.y -= c.bodyA.m_invMass * PY;

						c.bodyB.m_angularVelocity += c.bodyB.m_invI * (ccp.rB.x * PY - ccp.rB.y * PX);

						c.bodyB.m_linearVelocity.x += c.bodyB.m_invMass * PX;
						c.bodyB.m_linearVelocity.y += c.bodyB.m_invMass * PY;
					}
				}
				else
				{
					tCount = c.pointCount;
	
					for (j = 0; j < tCount; ++j)
					{
						ccp2 = c.points[j];
						ccp2.normalImpulse = b2Settings.DECIMAL_ZERO;
						ccp2.tangentImpulse = b2Settings.DECIMAL_ZERO;
					}
				}
			}
		}
		
		public static var i:int;
		public static var j:int;
		
		[Inline]
		final public function SolveVelocityConstraints():void
		{
			for (i = 0; i < m_constraintCount; ++i)
			{
				c = m_constraints[i];

				wA = c.bodyA.m_angularVelocity;
				wB = c.bodyB.m_angularVelocity;
				
				vA = c.bodyA.m_linearVelocity;
				vB = c.bodyB.m_linearVelocity;

				tangentX = c.normal.y;
				tangentY = -c.normal.x;
	
				// Solve the tangent constraints
				for (j = 0; j < c.pointCount; j++)
				{
					ccp = c.points[j];
					
					// Relative velocity at contact
					//b2Vec2 dv = vB + b2Cross(wB, ccp->rB) - vA - b2Cross(wA, ccp->rA);
					dvX = vB.x - wB * ccp.rB.y - vA.x + wA * ccp.rA.y;
					dvY = vB.y + wB * ccp.rB.x - vA.y - wA * ccp.rA.x;
					
					// Compute tangent force
					vt = dvX * tangentX + dvY * tangentY;
					lambda = ccp.tangentMass * -vt;
					
					// b2Clamp the accumulated force
					maxFriction = c.friction * ccp.normalImpulse;
					newImpulse = b2Math.Clamp(ccp.tangentImpulse + lambda, -maxFriction, maxFriction);
					lambda = newImpulse-ccp.tangentImpulse;
					
					// Apply contact impulse
					PX = lambda * tangentX;
					PY = lambda * tangentY;
					
					vA.x -= c.bodyA.m_invMass * PX;
					vA.y -= c.bodyA.m_invMass * PY;
					wA -= c.bodyA.m_invI * (ccp.rA.x * PY - ccp.rA.y * PX);
					
					vB.x += c.bodyB.m_invMass * PX;
					vB.y += c.bodyB.m_invMass * PY;
					wB += c.bodyB.m_invI * (ccp.rB.x * PY - ccp.rB.y * PX);
					
					ccp.tangentImpulse = newImpulse;
				}
				
				// Solve the normal constraints
				tCount = c.pointCount;
				
				if (c.pointCount == 1)
				{
					ccp = c.points[ 0 ];
					
					// Relative velocity at contact
					//b2Vec2 dv = vB + b2Cross(wB, ccp->rB) - vA - b2Cross(wA, ccp->rA);
					dvX = vB.x + (-wB * ccp.rB.y) - vA.x - (-wA * ccp.rA.y);
					dvY = vB.y + (wB * ccp.rB.x) - vA.y - (wA * ccp.rA.x);
					
					// Compute normal impulse
					//var vn:Number = b2Math.b2Dot(dv, normal);
					vn = dvX * c.normal.x + dvY * c.normal.y;
					lambda = -ccp.normalMass * (vn - ccp.velocityBias);
					
					// b2Clamp the accumulated impulse
					//newImpulse = b2Math.b2Max(ccp.normalImpulse + lambda, 0.0);
					newImpulse = ccp.normalImpulse + lambda;
					newImpulse = newImpulse > 0 ? newImpulse : 0.0;
					lambda = newImpulse - ccp.normalImpulse;
					
					// Apply contact impulse
					//b2Vec2 P = lambda * normal;
					PX = lambda * c.normal.x;
					PY = lambda * c.normal.y;
					
					//vA.Subtract( b2Math.MulFV( invMassA, P ) );
					vA.x -= c.bodyA.m_invMass * PX;
					vA.y -= c.bodyA.m_invMass * PY;
					wA -= c.bodyA.m_invI * (ccp.rA.x * PY - ccp.rA.y * PX);//invIA * b2Math.b2CrossVV(ccp.rA, P);
					
					//vB.Add( b2Math.MulFV( invMass2, P ) );
					vB.x += c.bodyB.m_invMass * PX;
					vB.y += c.bodyB.m_invMass * PY;
					wB += c.bodyB.m_invI * (ccp.rB.x * PY - ccp.rB.y * PX);//invIB * b2Math.b2CrossVV(ccp.rB, P);
					
					ccp.normalImpulse = newImpulse;
				}
				else
				{
					cp1 = c.points[ 0 ];
					cp2 = c.points[ 1 ];
					
					aX = cp1.normalImpulse;
					aY = cp2.normalImpulse;
	
					dv1X = vB.x - wB * cp1.rB.y - vA.x + wA * cp1.rA.y;
					dv1Y = vB.y + wB * cp1.rB.x - vA.y - wA * cp1.rA.x;
	
					dv2X = vB.x - wB * cp2.rB.y - vA.x + wA * cp2.rA.y;
					dv2Y = vB.y + wB * cp2.rB.x - vA.y - wA * cp2.rA.x;
	
					vn1 = dv1X * c.normal.x + dv1Y * c.normal.y;
	
					vn2 = dv2X * c.normal.x + dv2Y * c.normal.y;
					
					bX = vn1 - cp1.velocityBias;
					bY = vn2 - cp2.velocityBias;
					
					//b -= b2Mul(c.K,a);
					tMat = c.K;
					bX -= tMat.col1.x * aX + tMat.col2.x * aY;
					bY -= tMat.col1.y * aX + tMat.col2.y * aY;
	
					for (;; )
					{
						//
						// Case 1: vn = 0
						//
						// 0 = A * x' + b'
						//
						// Solve for x':
						//
						// x' = -inv(A) * b'
						//
						
						//var x:b2Vec2 = - b2Mul(c->normalMass, b);
						tMat = c.normalMass;
						
						xX = - (tMat.col1.x * bX + tMat.col2.x * bY);
						xY = - (tMat.col1.y * bX + tMat.col2.y * bY);
						
						if (xX >= b2Settings.DECIMAL_ZERO && xY >= b2Settings.DECIMAL_ZERO)
						{
							// Resubstitute for the incremental impulse
							//d = x - a;
							dX = xX - aX;
							dY = xY - aY;
							
							//Aply incremental impulse
							//P1 = d.x * normal;
							P1X = dX * c.normal.x;
							P1Y = dX * c.normal.y;
							//P2 = d.y * normal;
							P2X = dY * c.normal.x;
							P2Y = dY * c.normal.y;
							
							//vA -= invMass1 * (P1 + P2)
							vA.x -= c.bodyA.m_invMass * (P1X + P2X);
							vA.y -= c.bodyA.m_invMass * (P1Y + P2Y);
							//wA -= invIA * (b2Cross(cp1.rA, P1) + b2Cross(cp2.rA, P2));
							wA -= c.bodyA.m_invI * ( cp1.rA.x * P1Y - cp1.rA.y * P1X + cp2.rA.x * P2Y - cp2.rA.y * P2X);
							
							//vB += invMassB * (P1 + P2)
							vB.x += c.bodyB.m_invMass * (P1X + P2X);
							vB.y += c.bodyB.m_invMass * (P1Y + P2Y);
							//wB += invIB * (b2Cross(cp1.rB, P1) + b2Cross(cp2.rB, P2));
							wB   += c.bodyB.m_invI * ( cp1.rB.x * P1Y - cp1.rB.y * P1X + cp2.rB.x * P2Y - cp2.rB.y * P2X);
							
							// Accumulate
							cp1.normalImpulse = xX;
							cp2.normalImpulse = xY;

							break;
						}
						
						//
						// Case 2: vn1 = 0  and x2 = 0
						//
						//   0 = a11 * x1' + a12 * 0 + b1'
						// vn2 = a21 * x1' + a22 * 0 + b2'
						//
						
						xX = - cp1.normalMass * bX;
						xY = b2Settings.DECIMAL_ZERO;
						vn1 = b2Settings.DECIMAL_ZERO;
						vn2 = c.K.col1.y * xX + bY;
						
						if (xX >= b2Settings.DECIMAL_ZERO && vn2 >= b2Settings.DECIMAL_ZERO)
						{
							// Resubstitute for the incremental impulse

							dX = xX - aX;
							dY = xY - aY;

							P1X = dX * c.normal.x;
							P1Y = dX * c.normal.y;

							P2X = dY * c.normal.x;
							P2Y = dY * c.normal.y;

							vA.x -= c.bodyA.m_invMass * (P1X + P2X);
							vA.y -= c.bodyA.m_invMass * (P1Y + P2Y);

							wA -= c.bodyA.m_invI * ( cp1.rA.x * P1Y - cp1.rA.y * P1X + cp2.rA.x * P2Y - cp2.rA.y * P2X);

							vB.x += c.bodyB.m_invMass * (P1X + P2X);
							vB.y += c.bodyB.m_invMass * (P1Y + P2Y);

							wB   += c.bodyB.m_invI * ( cp1.rB.x * P1Y - cp1.rB.y * P1X + cp2.rB.x * P2Y - cp2.rB.y * P2X);
							
							// Accumulate
							cp1.normalImpulse = xX;
							cp2.normalImpulse = xY;

							break;
						}
						
						//
						// Case 3: wB = 0 and x1 = 0
						//
						// vn1 = a11 * 0 + a12 * x2' + b1'
						//   0 = a21 * 0 + a22 * x2' + b2'
						//
						
						xX = b2Settings.DECIMAL_ZERO;
						xY = -cp2.normalMass * bY;
						vn1 = c.K.col2.x * xY + bX;
						vn2 = b2Settings.DECIMAL_ZERO;
						
						if (xY >= b2Settings.DECIMAL_ZERO && vn1 >= b2Settings.DECIMAL_ZERO)
						{
							// Resubstitute for the incremental impulse
							//d = x - a;
							dX = xX - aX;
							dY = xY - aY;
							
							//Aply incremental impulse
							//P1 = d.x * normal;
							P1X = dX * c.normal.x;
							P1Y = dX * c.normal.y;
							//P2 = d.y * normal;
							P2X = dY * c.normal.x;
							P2Y = dY * c.normal.y;
							
							//vA -= invMassA * (P1 + P2)
							vA.x -= c.bodyA.m_invMass * (P1X + P2X);
							vA.y -= c.bodyA.m_invMass * (P1Y + P2Y);
							//wA -= invIA * (b2Cross(cp1.rA, P1) + b2Cross(cp2.rA, P2));
							wA -= c.bodyA.m_invI * ( cp1.rA.x * P1Y - cp1.rA.y * P1X + cp2.rA.x * P2Y - cp2.rA.y * P2X);
							
							//vB += invMassB * (P1 + P2)
							vB.x += c.bodyB.m_invMass * (P1X + P2X);
							vB.y += c.bodyB.m_invMass * (P1Y + P2Y);
							//wB += invIB * (b2Cross(cp1.rB, P1) + b2Cross(cp2.rB, P2));
							wB   += c.bodyB.m_invI * ( cp1.rB.x * P1Y - cp1.rB.y * P1X + cp2.rB.x * P2Y - cp2.rB.y * P2X);
							
							// Accumulate
							cp1.normalImpulse = xX;
							cp2.normalImpulse = xY;

							break;
						}
						
						//
						// Case 4: x1 = 0 and x2 = 0
						//
						// vn1 = b1
						// vn2 = b2
						
						xX = b2Settings.DECIMAL_ZERO;
						xY = b2Settings.DECIMAL_ZERO;
						vn1 = bX;
						vn2 = bY;
						
						if (vn1 >= b2Settings.DECIMAL_ZERO && vn2 >= b2Settings.DECIMAL_ZERO)
						{
							// Resubstitute for the incremental impulse
							//d = x - a;
							dX = xX - aX;
							dY = xY - aY;
							
							//Aply incremental impulse
							//P1 = d.x * normal;
							P1X = dX * c.normal.x;
							P1Y = dX * c.normal.y;
							//P2 = d.y * normal;
							P2X = dY * c.normal.x;
							P2Y = dY * c.normal.y;
							
							//vA -= invMassA * (P1 + P2)
							vA.x -= c.bodyA.m_invMass * (P1X + P2X);
							vA.y -= c.bodyA.m_invMass * (P1Y + P2Y);
							//wA -= invIA * (b2Cross(cp1.rA, P1) + b2Cross(cp2.rA, P2));
							wA -= c.bodyA.m_invI * ( cp1.rA.x * P1Y - cp1.rA.y * P1X + cp2.rA.x * P2Y - cp2.rA.y * P2X);
							
							//vB += invMassB * (P1 + P2)
							vB.x += c.bodyB.m_invMass * (P1X + P2X);
							vB.y += c.bodyB.m_invMass * (P1Y + P2Y);
							//wB += invIB * (b2Cross(cp1.rB, P1) + b2Cross(cp2.rB, P2));
							wB   += c.bodyB.m_invI * ( cp1.rB.x * P1Y - cp1.rB.y * P1X + cp2.rB.x * P2Y - cp2.rB.y * P2X);
							
							// Accumulate
							cp1.normalImpulse = xX;
							cp2.normalImpulse = xY;

							break;
						}
						
						// No solution, give up. This is hit sometimes, but it doesn't seem to matter.
						break;
					}
				}

				c.bodyA.m_angularVelocity = wA;
				c.bodyB.m_angularVelocity = wB;
			}
		}

		private var m:b2Manifold;
		
		private var point1:b2ManifoldPoint;
		private var point2:b2ContactConstraintPoint;
		
		private var i:int;
		
		public function FinalizeVelocityConstraints() : void
		{
			for(i = 0; i < m_constraintCount; ++i)
			{
				c = m_constraints[i];
				m = c.manifold;
				
				for(j = 0; j < c.pointCount; ++j)
				{
					point1 = m.m_points[j];
					point2 = c.points[j];

					point1.m_normalImpulse = point2.normalImpulse;
					point1.m_tangentImpulse = point2.tangentImpulse;
				}
			}
		}

		// Sequential solver.
		private static var s_psm:b2PositionSolverManifold = new b2PositionSolverManifold();
		private static var minSeparation:Number;
		private static var LINEAR_SLOPE:Number = -1.5;
		
		[Inline]
		final public function SolvePositionConstraints(baumgarte:Number):Boolean
		{
			minSeparation = 0.0;
			
			for(i = 0; i < m_constraintCount; i++)
			{
				c = m_constraints[i];

				invMassA = c.bodyA.m_mass * c.bodyA.m_invMass;
				invMassB = c.bodyB.m_mass * c.bodyB.m_invMass;
				
				s_psm.Initialize(c);
				
				// Solve normal constraints
				for(j = 0; j < c.pointCount; j++)
				{
					ccp = c.points[j];
					
					point = s_psm.m_points[j];
					separation = s_psm.m_separations[j];
					
					rAX = point.x - c.bodyA.m_sweep.c.x;
					rAY = point.y - c.bodyA.m_sweep.c.y;
					rBX = point.x - c.bodyB.m_sweep.c.x;
					rBY = point.y - c.bodyB.m_sweep.c.y;
					
					// Track max constraint error.
					minSeparation = minSeparation < separation ? minSeparation:separation;
					
					// Prevent large corrections and allow slop.
					C = b2Math.Clamp(
						baumgarte * (separation + b2Settings.b2_linearSlop),
						-b2Settings.b2_maxLinearCorrection,
						b2Settings.DECIMAL_ZERO
					);
					
					// Compute normal impulse
					impulse = -ccp.equalizedMass * C;
					
					PX = impulse * s_psm.m_normal.x;
					PY = impulse * s_psm.m_normal.y;

					c.bodyA.m_sweep.c.x -= invMassA * PX;
					c.bodyA.m_sweep.c.y -= invMassA * PY;

					c.bodyA.m_sweep.a -= (c.bodyA.m_mass * c.bodyA.m_invI) * (rAX * PY - rAY * PX);
					c.bodyA.SynchronizeTransform();
					

					c.bodyB.m_sweep.c.x += invMassB * PX;
					c.bodyB.m_sweep.c.y += invMassB * PY;

					c.bodyB.m_sweep.a += (c.bodyB.m_mass * c.bodyB.m_invI) * (rBX * PY - rBY * PX);
					c.bodyB.SynchronizeTransform();
				}
			}
			
			// We can't expect minSpeparation >= -b2_linearSlop because we don't
			// push the separation above -b2_linearSlop.
			return minSeparation > LINEAR_SLOPE * b2Settings.b2_linearSlop;
		}
		
	//#endif
		private var m_step:b2TimeStep = new b2TimeStep();

		public var m_constraints:Vector.<b2ContactConstraint> = new Vector.<b2ContactConstraint> ();
		private var m_constraintCount:int;
	};
}