/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Dynamics.Contacts
{
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Common.b2Pool;
	import Box2D.Dynamics.b2Fixture;
	import Box2D.Dynamics.Contacts.Type.b2CircleContact;
	import Box2D.Dynamics.Contacts.Type.b2PolyAndCircleContact;
	import Box2D.Dynamics.Contacts.Type.b2PolygonContact;
	
	
	/**
	 * This class manages creation and destruction of b2Contact objects.
	 * @private
	 */
	public class b2ContactFactory
	{
		public function b2ContactFactory()
		{
		}
	
		private static var type1:uint;
		private static var type2:uint;
		
		private static var isAPoly:Boolean;
		private static var isBPoly:Boolean;
		
		private static var c:b2Contact;

		[Inline]
		final public function Create(fixtureA:b2Fixture, fixtureB:b2Fixture):b2Contact
		{
			type1 = fixtureA.m_shape.m_type;
			type2 = fixtureB.m_shape.m_type;
			
			if(type1 == b2Shape.e_circleShape && type2 == b2Shape.e_circleShape)
			{
				c = b2Pool.getCircleContact();
			}
			else if(type1 == b2Shape.e_polygonShape && type2 == b2Shape.e_polygonShape)
			{
				c = b2Pool.getPolyContact();
			}
			else
			{
				c = b2Pool.getPolyAndCircleContact();
			}
			
			isAPoly = fixtureA.m_shape is b2PolygonShape;
			isBPoly = fixtureB.m_shape is b2PolygonShape;
			
			if(isAPoly || isBPoly && type1 != type2)
			{
				if(isAPoly)
				{
					c.Reset(fixtureA, fixtureB);	
				}
				else
				{
					c.Reset(fixtureB, fixtureA);
				}
			}
			else
			{
				c.Reset(fixtureA, fixtureB);
			}
			
			return c;
		}

		[Inline]
		final public function Destroy(c:b2Contact):void
		{
			if (c.m_manifold.m_pointCount > 0)
			{
				if (c.m_fixtureA.m_body)
				{
					c.m_fixtureA.m_body.SetAwake(true);
				}
				
				if (c.m_fixtureB.m_body)
				{
					c.m_fixtureB.m_body.SetAwake(true);
				}
			}
			
			if(c is b2CircleContact)
			{
				b2Pool.putCircleContact(c as b2CircleContact);
			}
			else if(c is b2PolygonContact)
			{
				b2Pool.putPolyContact(c as b2PolygonContact);
			}
			else
			{
				b2Pool.putPolyAndCircleContact(c as b2PolyAndCircleContact);
			}
		}
	}
}
