﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Dynamics.Contacts
{
	import Box2D.Collision.b2ContactID;
	import Box2D.Collision.b2Manifold;
	import Box2D.Collision.b2ManifoldPoint;
	import Box2D.Collision.b2WorldManifold;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Transform;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2ContactListener;
	import Box2D.Dynamics.b2Fixture;
	
	
	//typedef b2Contact* b2ContactCreateFcn(b2Shape* shape1, b2Shape* shape2, b2BlockAllocator* allocator);
	//typedef void b2ContactDestroyFcn(b2Contact* contact, b2BlockAllocator* allocator);
	
	
	
	/**
	* The class manages contact between two shapes. A contact exists for each overlapping
	* AABB in the broad-phase (except if filtered). Therefore a contact object may exist
	* that has no contact points.
	*/
	public class b2Contact
	{
		/**
		 * Get the contact manifold. Do not modify the manifold unless you understand the
		 * internals of Box2D
		 */
		public function GetManifold():b2Manifold
		{
			return m_manifold;
		}
		
		/**
		 * Get the world manifold
		 */
		public function GetWorldManifold(worldManifold:b2WorldManifold):void
		{
			var bodyA:b2Body = m_fixtureA.GetBody();
			var bodyB:b2Body = m_fixtureB.GetBody();
			var shapeA:b2Shape = m_fixtureA.GetShape();
			var shapeB:b2Shape = m_fixtureB.GetShape();
			
			worldManifold.Initialize(m_manifold, bodyA.GetTransform(), shapeA.m_radius, bodyB.GetTransform(), shapeB.m_radius);
		}
		
		/**
		 * Is this contact touching.
		 */
		[Inline]
		final public function IsTouching():Boolean
		{
			return m_isTouching; 
		}
		
		/**
		 * Does this contact generate TOI events for continuous simulation
		 */
		[Inline]
		final public function IsContinuous():Boolean
		{
			return m_isContinuous;
		}
		
		/**
		 * Change this to be a sensor or-non-sensor contact.
		 */
		public function SetSensor(value:Boolean):void
		{
			m_isSensor = value;
		}
		
		/**
		 * Is this contact a sensor?
		 */
		[Inline]
		final public function IsSensor():Boolean
		{
			return m_isSensor;
		}
		
		/**
		 * Enable/disable this contact. This can be used inside the pre-solve
		 * contact listener. The contact is only disabled for the current
		 * time step (or sub-step in continuous collision).
		 */
		[Inline]
		final public function SetEnabled(value:Boolean):void
		{
			m_isEnabled = value;
		}
		
		[Inline]
		final public function GetWasDisabled():Boolean
		{
			return m_wasDisabled;
		}
		
		public function SetWasDisabled(value:Boolean):void
		{
			m_wasDisabled = value;
		}

		/**
		 * Has this contact been disabled?
		 * @return
		 */
		[Inline]
		final public function IsEnabled():Boolean
		{
			return m_isEnabled;
		}
		
		/**
		* Get the next contact in the world's contact list.
		*/
		[Inline]
		final public function GetNext():b2Contact
		{
			return m_next;
		}
		
		/**
		* Get the first fixture in this contact.
		*/
		
		[Inline]
		final public function GetFixtureA():b2Fixture
		{
			return m_fixtureA;
		}
		
		/**
		* Get the second fixture in this contact.
		*/
		[Inline]
		final public function GetFixtureB():b2Fixture
		{
			return m_fixtureB;
		}
		
		/**
		 * Flag this contact for filtering. Filtering will occur the next time step.
		 */
		[Inline]
		final public function FlagForFiltering():void
		{
			m_isFiltering = true;
		}
	
		//--------------- Internals Below -------------------
		
		// m_flags
		// enum
		// This contact should not participate in Solve
		// The contact equivalent of sensors
		static public var e_sensorFlag:uint		= 0x0001;
		// Generate TOI events.
		static public var e_continuousFlag:uint	= 0x0002;
		// Used when crawling contact graph when forming islands.
		static public var e_islandFlag:uint		= 0x0004;
		// Used in SolveTOI to indicate the cached toi value is still valid.
		static public var e_toiFlag:uint		= 0x0008;
		// Set when shapes are touching
		static public var e_touchingFlag:uint	= 0x0010;
		// This contact can be disabled (by user)
		static public var e_enabledFlag:uint	= 0x0020;
		// This contact needs filtering because a fixture filter was changed.
		static public var e_filterFlag:uint		= 0x0040;
	
		public var id:uint = 0;

		public function b2Contact()
		{
			m_nodeA = new b2ContactEdge();
			m_nodeB = new b2ContactEdge();
			
			m_manifold = new b2Manifold();
			m_oldManifold = new b2Manifold();
			
			// Real work is done in Reset
		}
	
		private var r_bodyA:b2Body;
		private var r_bodyB:b2Body;
		
		/** @private */
		final public function Reset(fixtureA:b2Fixture = null, fixtureB:b2Fixture = null):void
		{
			m_isEnabled = true;
			m_isIsland = false;
			m_isTOI = false;
			m_isFiltering = false;
			m_isTouching = false;
			
			if (!fixtureA || !fixtureB)
			{
				m_fixtureA = null;
				m_fixtureB = null;
	
				return;
			}
			
			m_isSensor = fixtureA.m_isSensor || fixtureB.m_isSensor;
			
			r_bodyA = fixtureA.m_body;
			r_bodyB = fixtureB.m_body;
			
			if(r_bodyA.m_type != b2Body.b2_dynamicBody || r_bodyA.m_isBullet || 
				r_bodyB.m_type != b2Body.b2_dynamicBody || r_bodyB.m_isBullet)
			{
				m_isContinuous = true;
			}
			else
			{
				m_isContinuous = false;
			}
			
			m_fixtureA = fixtureA;
			m_fixtureB = fixtureB;
			
			m_manifold.m_pointCount = 0;
			
			m_wasDisabled = false;

			m_prev = null;
			m_next = null;
			
			m_nodeA.contact = null;
			m_nodeA.prev = null;
			m_nodeA.next = null;
			m_nodeA.other = null;
			
			m_nodeB.contact = null;
			m_nodeB.prev = null;
			m_nodeB.next = null;
			m_nodeB.other = null;
		}
		
		private var u_tManifold:b2Manifold;
		private var u_touching:Boolean;
		private var u_wasTouching:Boolean;
		
		private var u_bodyA:b2Body;
		private var u_bodyB:b2Body;
		
		private var u_aabbOverlap:Boolean;
		
		private var u_shapeA:b2Shape;
		private var u_shapeB:b2Shape;
		
		private var u_xfA:b2Transform;
		private var u_xfB:b2Transform;
		
		private var u_mp2:b2ManifoldPoint;
		private var u_id2:b2ContactID;
		
		private var u_mp1:b2ManifoldPoint;
		
		private var u_i:int;
		private var u_j:int;

		final public function Update(listener:b2ContactListener):void
		{
			// Swap old & new manifold
			u_tManifold = m_oldManifold;
			
			m_oldManifold = m_manifold;
			m_manifold = u_tManifold;
			
			// Re-enable this contact			
			m_isEnabled = true;
			
			u_touching = false;
			u_wasTouching = m_isTouching;
			
			u_bodyA = m_fixtureA.m_body;
			u_bodyB = m_fixtureB.m_body;
			
			u_aabbOverlap = m_fixtureA.m_aabb.TestOverlap(m_fixtureB.m_aabb);
			
			// Is this contact a sensor?
			if(m_isSensor)
			{
				if (u_aabbOverlap)
				{
					u_shapeA = m_fixtureA.m_shape;
					u_shapeB = m_fixtureB.m_shape;
					
					u_xfA = u_bodyA.m_xf;
					u_xfB = u_bodyB.m_xf;
					
					u_touching = b2Shape.TestOverlap(u_shapeA, u_xfA, u_shapeB, u_xfB);
				}
				
				// Sensors don't generate manifolds
				m_manifold.m_pointCount = 0;
			}
			else
			{
				// Slow contacts don't generate TOI events.
				if(u_bodyA.m_type != b2Body.b2_dynamicBody || 
					u_bodyA.m_isBullet || 
					u_bodyB.m_type != b2Body.b2_dynamicBody || 
					u_bodyB.m_isBullet)
				{
					m_isContinuous = true;
				}
				else
				{
					m_isContinuous = false;
				}
				
				if(u_aabbOverlap)
				{
					Evaluate();
					
					u_touching = m_manifold.m_pointCount > 0;
					
					u_mp2 = null;
					u_id2 = null;
					
					u_mp1 = null;
					
					// Match old contact ids to new contact ids and copy the
					// stored impulses to warm start the solver.
					for(u_i = 0; u_i < m_manifold.m_pointCount; ++u_i)
					{
						u_mp2 = m_manifold.m_points[u_i];
						
						u_mp2.m_normalImpulse = b2Settings.DECIMAL_ZERO;
						u_mp2.m_tangentImpulse = b2Settings.DECIMAL_ZERO;
						
						u_id2 = u_mp2.m_id;
	
						for(u_j = 0; u_j < m_oldManifold.m_pointCount; ++u_j)
						{
							u_mp1 = m_oldManifold.m_points[u_j];
	
							if (u_mp1.m_id.key == u_id2.key)
							{
								u_mp2.m_normalImpulse = u_mp1.m_normalImpulse;
								u_mp2.m_tangentImpulse = u_mp1.m_tangentImpulse;
								break;
							}
						}
					}
				}
				else
				{
					m_manifold.m_pointCount = 0;
				}

				if (u_touching != u_wasTouching)
				{
					u_bodyA.SetAwake(true);
					u_bodyB.SetAwake(true);
				}
			}
				
			m_isTouching = u_touching;
	
			if(u_wasTouching == false && u_touching == true)
			{
				listener.BeginContact(this);
			}
	
			if(u_wasTouching == true && u_touching == false)
			{
				listener.EndContact(this);
			}
	
			if(!m_isSensor)
			{
				listener.PreSolve(this, m_oldManifold);
			}
		}
	
		//virtual ~b2Contact() {}
	
		public function Evaluate():void{};
	
		
		
		public var m_wasDisabled:Boolean = false;
	
		public var m_isEnabled:Boolean;
		public var m_isSensor:Boolean;
		public var m_isTouching:Boolean;
		public var m_isContinuous:Boolean;
		public var m_isFiltering:Boolean;
		public var m_isIsland:Boolean;
		public var m_isTOI:Boolean;

		// World pool and list pointers.
		public var m_prev:b2Contact;
		public var m_next:b2Contact;
	
		// Nodes for connecting bodies.
		public var m_nodeA:b2ContactEdge;
		public var m_nodeB:b2ContactEdge;
	
		public var m_fixtureA:b2Fixture;
		public var m_fixtureB:b2Fixture;
	
		public var m_manifold:b2Manifold;
		public var m_oldManifold:b2Manifold;
		
		public var m_toi:Number;
	};
}
