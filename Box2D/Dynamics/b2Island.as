﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Dynamics
{
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Mat22;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.Contacts.b2Contact;
	import Box2D.Dynamics.Contacts.b2ContactConstraint;
	import Box2D.Dynamics.Contacts.b2ContactSolver;
	import Box2D.Dynamics.Joints.b2Joint;
	
	
	/*
	Position Correction Notes
	=========================
	I tried the several algorithms for position correction of the 2D revolute joint.
	I looked at these systems:
	- simple pendulum (1m diameter sphere on massless 5m stick) with initial angular velocity of 100 rad/s.
	- suspension bridge with 30 1m long planks of length 1m.
	- multi-link chain with 30 1m long links.
	
	Here are the algorithms:
	
	Baumgarte - A fraction of the position error is added to the velocity error. There is no
	separate position solver.
	
	Pseudo Velocities - After the velocity solver and position integration,
	the position error, Jacobian, and effective mass are recomputed. Then
	the velocity constraints are solved with pseudo velocities and a fraction
	of the position error is added to the pseudo velocity error. The pseudo
	velocities are initialized to zero and there is no warm-starting. After
	the position solver, the pseudo velocities are added to the positions.
	This is also called the First Order World method or the Position LCP method.
	
	Modified Nonlinear Gauss-Seidel (NGS) - Like Pseudo Velocities except the
	position error is re-computed for each constraint and the positions are updated
	after the constraint is solved. The radius vectors (aka Jacobians) are
	re-computed too (otherwise the algorithm has horrible instability). The pseudo
	velocity states are not needed because they are effectively zero at the beginning
	of each iteration. Since we have the current position error, we allow the
	iterations to terminate early if the error becomes smaller than b2_linearSlop.
	
	Full NGS or just NGS - Like Modified NGS except the effective mass are re-computed
	each time a constraint is solved.
	
	Here are the results:
	Baumgarte - this is the cheapest algorithm but it has some stability problems,
	especially with the bridge. The chain links separate easily close to the root
	and they jitter as they struggle to pull together. This is one of the most common
	methods in the field. The big drawback is that the position correction artificially
	affects the momentum, thus leading to instabilities and false bounce. I used a
	bias factor of 0.2. A larger bias factor makes the bridge less stable, a smaller
	factor makes joints and contacts more spongy.
	
	Pseudo Velocities - the is more stable than the Baumgarte method. The bridge is
	stable. However, joints still separate with large angular velocities. Drag the
	simple pendulum in a circle quickly and the joint will separate. The chain separates
	easily and does not recover. I used a bias factor of 0.2. A larger value lead to
	the bridge collapsing when a heavy cube drops on it.
	
	Modified NGS - this algorithm is better in some ways than Baumgarte and Pseudo
	Velocities, but in other ways it is worse. The bridge and chain are much more
	stable, but the simple pendulum goes unstable at high angular velocities.
	
	Full NGS - stable in all tests. The joints display good stiffness. The bridge
	still sags, but this is better than infinite forces.
	
	Recommendations
	Pseudo Velocities are not really worthwhile because the bridge and chain cannot
	recover from joint separation. In other cases the benefit over Baumgarte is small.
	
	Modified NGS is not a robust method for the revolute joint due to the violent
	instability seen in the simple pendulum. Perhaps it is viable with other constraint
	types, especially scalar constraints where the effective mass is a scalar.
	
	This leaves Baumgarte and Full NGS. Baumgarte has small, but manageable instabilities
	and is very fast. I don't think we can escape Baumgarte, especially in highly
	demanding cases where high constraint fidelity is not needed.
	
	Full NGS is robust and easy on the eyes. I recommend this as an option for
	higher fidelity simulation and certainly for suspension bridges and long chains.
	Full NGS might be a good choice for ragdolls, especially motorized ragdolls where
	joint separation can be problematic. The number of NGS iterations can be reduced
	for better performance without harming robustness much.
	
	Each joint in a can be handled differently in the position solver. So I recommend
	a system where the user can select the algorithm on a per joint basis. I would
	probably default to the slower Full NGS and let the user select the faster
	Baumgarte method in performance critical scenarios.
	*/
	
	
	/**
	 * @private
	 */
	public class b2Island
	{
		
		public function b2Island()
		{
			m_bodies = new Vector.<b2Body>();
			m_contacts = new Vector.<b2Contact>();
			m_joints = new Vector.<b2Joint>();
		}
		
		private var in_i:int;
		
		[Inline]
		final public function Initialize(bodyCapacity:int,
										 contactCapacity:int,
										 jointCapacity:int,
										 listener:b2ContactListener,
										 contactSolver:b2ContactSolver):void
		{
			
			in_i = 0;
			
			m_bodyCapacity = bodyCapacity;
			m_contactCapacity = contactCapacity;
			m_jointCapacity	 = jointCapacity;
			m_bodyCount = 0;
			m_contactCount = 0;
			m_jointCount = 0;
			
			m_listener = listener;
			m_contactSolver = contactSolver;
			
			for (in_i = m_bodies.length; in_i < bodyCapacity; in_i++)
				m_bodies[in_i] = null;
			
			for (in_i = m_contacts.length; in_i < contactCapacity; in_i++)
				m_contacts[in_i] = null;
			
			for (in_i = m_joints.length; in_i < jointCapacity; in_i++)
				m_joints[in_i] = null;
			
		}
		//~b2Island();
		
		[Inline]
		final public function Clear():void
		{
			m_bodyCount = 0;
			m_contactCount = 0;
			m_jointCount = 0;
		}
		
		private var s_body:b2Body;
		
		private var translationX:Number;
		private var translationY:Number;
		private var rotation:Number;
		private var contactsOkay:Boolean;
		
		private var jointsOkay:Boolean;
		private var jointOkay:Boolean;
		
		private var minSleepTime:Number = Number.MAX_VALUE;
		
		private var linTolSqr:Number = b2Settings.b2_linearSleepTolerance * b2Settings.b2_linearSleepTolerance;
		private var angTolSqr:Number = b2Settings.b2_angularSleepTolerance * b2Settings.b2_angularSleepTolerance;
		
		private var s_i:int;
		private var s_j:int;
		
		private var dotTranslation:Number;
		private var rotationSquared:Number;
		
		private var dtL:Number;
		private var dtA:Number;
		
		private var velocityClamp:Number;
		
		private static var st_tMat:b2Mat22;
		private static var st_tVec:b2Vec2;
		
		private static var _c:Number;
		private static var _s:Number;

		[Inline]
		final public function Solve(step:b2TimeStep, gravity:b2Vec2, allowSleep:Boolean):void
		{
			// Integrate velocities and apply damping for dynamic objects
			for(s_i = 0; s_i < m_bodyCount; ++s_i)
			{
				s_body = m_bodies[s_i];
				
				if (s_body.m_type != b2Body.b2_dynamicBody)
				{
					continue;				
				}
				
				s_body.m_linearVelocity.x += s_body.m_gravityScale * step.dt * (gravity.x + s_body.m_invMass * s_body.m_force.x);
				s_body.m_linearVelocity.y += s_body.m_gravityScale * step.dt * (gravity.y + s_body.m_invMass * s_body.m_force.y);
				
				s_body.m_angularVelocity += step.dt * s_body.m_invI * s_body.m_torque;
				
				dtL = b2Settings.DECIMAL_ONE - step.dt * s_body.m_linearDamping;
				
				// velocityClamp = b2Math.Clamp(dtL, b2Settings.DECIMAL_ZERO, b2Settings.DECIMAL_ONE);
				velocityClamp = dtL < b2Settings.DECIMAL_ZERO ? 
					b2Settings.DECIMAL_ZERO : dtL > b2Settings.DECIMAL_ONE ? 
					b2Settings.DECIMAL_ONE : dtL;
				
				s_body.m_linearVelocity.x *= velocityClamp;
				s_body.m_linearVelocity.y *= velocityClamp;
				
				dtA = b2Settings.DECIMAL_ONE - step.dt * s_body.m_angularDamping;
				
				//b.m_angularVelocity *= b2Math.Clamp(dtA, b2Settings.DECIMAL_ZERO, b2Settings.DECIMAL_ONE);
				s_body.m_angularVelocity *= dtA < b2Settings.DECIMAL_ZERO ? 
					b2Settings.DECIMAL_ZERO : dtA > b2Settings.DECIMAL_ONE ? 
					b2Settings.DECIMAL_ONE : dtA;
			}
			
			m_contactSolver.Initialize(step, m_contacts, m_contactCount);
			
			// Initialize velocity constraints.
			m_contactSolver.InitVelocityConstraints(step);
			
			for (s_i = 0; s_i < m_jointCount; ++s_i)
			{
				m_joints[s_i].InitVelocityConstraints(step);
			}
			
			// Solve velocity constraints.
			for (s_i = 0; s_i < step.velocityIterations; ++s_i)
			{	
				for (s_j = 0; s_j < m_jointCount; ++s_j)
				{
					m_joints[s_j].SolveVelocityConstraints(step);
				}
				
				m_contactSolver.SolveVelocityConstraints();
			}
			
			m_contactSolver.FinalizeVelocityConstraints();
			
			// Integrate positions.
			for(s_i = 0; s_i < m_bodyCount; ++s_i)
			{
				s_body = m_bodies[s_i];
				
				if(s_body.m_type == b2Body.b2_staticBody)
				{
					continue;				
				}
				
				// Check for large velocities.
				// b2Vec2 translation = step.dt * b.m_linearVelocity;
				translationX = step.dt * s_body.m_linearVelocity.x;
				translationY = step.dt * s_body.m_linearVelocity.y;
				
				//if (b2Dot(translation, translation) > b2_maxTranslationSquared)
				
				dotTranslation = translationX * translationX + translationY * translationY;
				
				if(dotTranslation > b2Settings.b2_maxTranslationSquared)
				{
					s_body.m_linearVelocity.Normalize();
					s_body.m_linearVelocity.x *= b2Settings.b2_maxTranslation * step.inv_dt;
					s_body.m_linearVelocity.y *= b2Settings.b2_maxTranslation * step.inv_dt;
				}
				
				rotation = step.dt * s_body.m_angularVelocity;
				
				rotationSquared = rotation * rotation;
				
				if(rotationSquared > b2Settings.b2_maxRotationSquared)
				{
					if (s_body.m_angularVelocity < b2Settings.DECIMAL_ZERO)
					{
						s_body.m_angularVelocity = -b2Settings.b2_maxRotation * step.inv_dt;
					}
					else
					{
						s_body.m_angularVelocity = b2Settings.b2_maxRotation * step.inv_dt;
					}
				}
				
				// Store positions for continuous collision.
				s_body.m_sweep.c0.x = s_body.m_sweep.c.x;
				s_body.m_sweep.c0.y = s_body.m_sweep.c.y;
				
				s_body.m_sweep.a0 = s_body.m_sweep.a;
				
				// Integrate
				//b.m_sweep.c += step.dt * b.m_linearVelocity;
				s_body.m_sweep.c.x += step.dt * s_body.m_linearVelocity.x;
				s_body.m_sweep.c.y += step.dt * s_body.m_linearVelocity.y;
				s_body.m_sweep.a += step.dt * s_body.m_angularVelocity;
				
				// Compute new transform
				
				/***
				 * START s_body.SynchronizeTransform();
				 **/ 
				// s_body.m_xf.R.Set(s_body.m_sweep.a);
				_c = Math.cos(s_body.m_sweep.a);
				_s = Math.sin(s_body.m_sweep.a);
				
				s_body.m_xf.R.col1.x = _c; s_body.m_xf.R.col2.x = -_s;
				s_body.m_xf.R.col1.y = _s; s_body.m_xf.R.col2.y = _c;
				
				
				
				//m_xf.position = m_sweep.c - b2Mul(m_xf.R, m_sweep.localCenter);
				st_tMat = s_body.m_xf.R;
				st_tVec = s_body.m_sweep.localCenter;
				
				s_body.m_xf.position.x = s_body.m_sweep.c.x - (st_tMat.col1.x * st_tVec.x + st_tMat.col2.x * st_tVec.y);
				s_body.m_xf.position.y = s_body.m_sweep.c.y - (st_tMat.col1.y * st_tVec.x + st_tMat.col2.y * st_tVec.y);
				
				/***
				 *  END OF s_body.SynchronizeTransform();
				 **/
				
				
				// Note: shapes are synchronized later.
			}
			
			// Iterate over constraints.
			for (s_i = 0; s_i < step.positionIterations; ++s_i)
			{
				contactsOkay = m_contactSolver.SolvePositionConstraints(b2Settings.b2_contactBaumgarte);
				
				jointsOkay = true;
				
				for (s_j = 0; s_j < m_jointCount; ++s_j)
				{
					jointOkay = m_joints[s_j].SolvePositionConstraints(b2Settings.b2_contactBaumgarte);
					jointsOkay = jointsOkay && jointOkay;
				}
				
				if(contactsOkay && jointsOkay)
				{
					break;
				}
			}
			
			Report(m_contactSolver.m_constraints);
			
			if(allowSleep)
			{
				minSleepTime = Number.MAX_VALUE;
				
				for(s_i = 0; s_i < m_bodyCount; ++s_i)
				{
					s_body = m_bodies[s_i];
					
					if(s_body.m_type == b2Body.b2_staticBody)
					{
						continue;
					}
					
					if(!s_body.m_allowSleep)
					{
						s_body.m_currentTimeBeforeSleep = b2Settings.DECIMAL_ZERO;
						minSleepTime = b2Settings.DECIMAL_ZERO;
					}
					
					if(!s_body.m_allowSleep ||
						s_body.m_angularVelocity * s_body.m_angularVelocity > angTolSqr ||
						s_body.m_linearVelocity.x * s_body.m_linearVelocity.x + 
						s_body.m_linearVelocity.y * s_body.m_linearVelocity.y > linTolSqr)
					{
						s_body.m_currentTimeBeforeSleep = b2Settings.DECIMAL_ZERO;
						minSleepTime = b2Settings.DECIMAL_ZERO;
					}
					else
					{
						s_body.m_currentTimeBeforeSleep += step.dt;
						minSleepTime = minSleepTime < s_body.m_currentTimeBeforeSleep ? minSleepTime : s_body.m_currentTimeBeforeSleep;
					}
				}
				
				if(minSleepTime >= b2Settings.b2_timeToSleep)
				{
					for(s_i = 0; s_i < m_bodyCount; ++s_i)
					{
						s_body = m_bodies[s_i];
						s_body.SetAwake(false);
					}
				}
			}
		}
		
		private var st_i:int;
		private var st_j:int;
		
		[Inline]
		final public function SolveTOI(subStep:b2TimeStep) : void
		{
			m_contactSolver.Initialize(subStep, m_contacts, m_contactCount);
			
			// No warm starting is needed for TOI events because warm
			// starting impulses were applied in the discrete solver.
			
			// Warm starting for joints is off for now, but we need to
			// call this function to compute Jacobians.
			for (st_i = 0; st_i < m_jointCount;++st_i)
			{
				m_joints[st_i].InitVelocityConstraints(subStep);
			}
			
			
			// Solve velocity constraints.
			for (st_i = 0; st_i < subStep.velocityIterations; ++st_i)
			{
				m_contactSolver.SolveVelocityConstraints();
				
				for (st_j = 0; st_j < m_jointCount;++st_j)
				{
					m_joints[st_j].SolveVelocityConstraints(subStep);
				}
			}
			
			// Don't store the TOI contact forces for warm starting
			// because they can be quite large.
			
			// Integrate positions.
			for (st_i = 0; st_i < m_bodyCount; ++st_i)
			{
				s_body = m_bodies[st_i];
				
				if (s_body.m_type == b2Body.b2_staticBody)
				{
					continue;
				}
				
				translationX = subStep.dt * s_body.m_linearVelocity.x;
				translationY = subStep.dt * s_body.m_linearVelocity.y;				
				
				if ((translationX*translationX+translationY*translationY) > b2Settings.b2_maxTranslationSquared)
				{
					s_body.m_linearVelocity.Normalize();
					s_body.m_linearVelocity.x *= b2Settings.b2_maxTranslation * subStep.inv_dt;
					s_body.m_linearVelocity.y *= b2Settings.b2_maxTranslation * subStep.inv_dt;
				}
				
				rotation = subStep.dt * s_body.m_angularVelocity;
				
				if (rotation * rotation > b2Settings.b2_maxRotationSquared)
				{
					if (s_body.m_angularVelocity < b2Settings.DECIMAL_ZERO)
					{
						s_body.m_angularVelocity = -b2Settings.b2_maxRotation * subStep.inv_dt;
					}
					else
					{
						s_body.m_angularVelocity = b2Settings.b2_maxRotation * subStep.inv_dt;
					}
				}
				
				// Store positions for continuous collision.
				s_body.m_sweep.c0.x = s_body.m_sweep.c.x;
				s_body.m_sweep.c0.y = s_body.m_sweep.c.y;
				
				s_body.m_sweep.a0 = s_body.m_sweep.a;
				
				// Integrate
				s_body.m_sweep.c.x += subStep.dt * s_body.m_linearVelocity.x;
				s_body.m_sweep.c.y += subStep.dt * s_body.m_linearVelocity.y;
				s_body.m_sweep.a += subStep.dt * s_body.m_angularVelocity;
				
				// Compute new transform
				s_body.SynchronizeTransform();
				
				// Note: shapes are synchronized later.
			}
			
			// Solve position constraints.
			for (st_i = 0; st_i < subStep.positionIterations; ++st_i)
			{
				contactsOkay = m_contactSolver.SolvePositionConstraints(k_toiBaumgarte);
				jointsOkay = true;
				
				for (st_j = 0; st_j < m_jointCount;++st_j)
				{
					jointOkay = m_joints[st_j].SolvePositionConstraints(b2Settings.b2_contactBaumgarte);
					jointsOkay = jointsOkay && jointOkay;
				}
				
				if(contactsOkay && jointsOkay)
				{
					break;
				}
			}
			
			Report(m_contactSolver.m_constraints);
		}
		
		private var postSolveContactImpulseData:b2ContactImpulse = new b2ContactImpulse();
		
		private var r_c:b2Contact;
		
		private var r_i:int;
		private var r_j:int;
		
		private var r_cc:b2ContactConstraint;
		
		private static var currentImpulse:Number;
		private static var maxNormalImpulse:Number;
		private static var maxTangentImpulse:Number;
		private static var ipc:int = 0;
		
		[Inline]
		final public function Report(constraints:Vector.<b2ContactConstraint>):void
		{
			if (m_listener == null)
			{
				return;
			}
			
			postSolveContactImpulseData.normalImpulses.length = 0;
			postSolveContactImpulseData.tangentImpulses.length = 0;
			
			maxNormalImpulse = 0;
			maxTangentImpulse = 0;

			ipc = 0;

			for(r_i = 0; r_i < m_contactCount; ++r_i)
			{
				r_c = m_contacts[r_i];
				r_cc = constraints[r_i];
				
				for(r_j = 0; r_j < r_cc.pointCount; ++r_j)
				{
					currentImpulse = r_cc.points[r_j].normalImpulse;

					// Math.abs()
					currentImpulse = currentImpulse < 0 ? -currentImpulse : currentImpulse;

					postSolveContactImpulseData.normalImpulses[
						ipc
					] = currentImpulse;
					
					maxNormalImpulse = maxNormalImpulse > currentImpulse ? maxNormalImpulse : currentImpulse;
					
					
					
					currentImpulse = r_cc.points[r_j].tangentImpulse;
					
					// Math.abs()
					currentImpulse = currentImpulse < 0 ? -currentImpulse : currentImpulse;
					
					postSolveContactImpulseData.tangentImpulses[
						ipc++
					] = currentImpulse;
					
					maxTangentImpulse = maxTangentImpulse > currentImpulse ? maxTangentImpulse : currentImpulse;
				}
				
				m_listener.PostSolve(r_c, postSolveContactImpulseData, maxNormalImpulse, maxTangentImpulse);
			}
		}
		
		
		[Inline]
		final public function AddBody(body:b2Body) : void
		{
			//b2Settings.b2Assert(m_bodyCount < m_bodyCapacity);
			body.m_islandIndex = m_bodyCount;
			m_bodies[m_bodyCount++] = body;
		}
		
		[Inline]
		final public function AddContact(contact:b2Contact) : void
		{
			//b2Settings.b2Assert(m_contactCount < m_contactCapacity);
			m_contacts[m_contactCount++] = contact;
		}
		
		[Inline]
		final public function AddJoint(joint:b2Joint) : void
		{
			//b2Settings.b2Assert(m_jointCount < m_jointCapacity);
			m_joints[m_jointCount++] = joint;
		}
		
		private static const k_toiBaumgarte:Number = 0.75;
		
		private var m_listener:b2ContactListener;
		private var m_contactSolver:b2ContactSolver;
		
		public var m_bodies:Vector.<b2Body>;
		public var m_contacts:Vector.<b2Contact>;
		public var m_joints:Vector.<b2Joint>;
		
		public var m_bodyCount:uint;
		public var m_jointCount:uint;
		public var m_contactCount:uint;
		
		
		private var m_bodyCapacity:uint;
		public var m_contactCapacity:uint;
		public var m_jointCapacity:uint;
	};
}
