﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Dynamics
{
	import Box2D.Collision.IBroadPhase;
	import Box2D.Collision.Shapes.b2MassData;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Common.Math.b2Mat22;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Sweep;
	import Box2D.Common.Math.b2Transform;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Common.b2Pool;
	import Box2D.Common.b2Settings;
	import Box2D.Dynamics.Contacts.b2Contact;
	import Box2D.Dynamics.Contacts.b2ContactEdge;
	import Box2D.Dynamics.Controllers.b2ControllerEdge;
	import Box2D.Dynamics.Joints.b2JointEdge;

	/**
	* A rigid body.
	*/
	public class b2Body
	{

		private var _fixture:b2Fixture;

		/**
		 * Creates a fixture and attach it to this body. Use this function if you need
		 * to set some fixture parameters, like friction. Otherwise you can create the
		 * fixture directly from a shape.
		 * If the density is non-zero, this function automatically updates the mass of the body.
		 * Contacts are not created until the next time step.
		 * @param fixtureDef the fixture definition.
		 * @warning This function is locked during callbacks.
		 */

		public function CreateFixture(def:b2FixtureDef):b2Fixture
		{
			// b2Settings.b2Assert(m_world.IsLocked() == false);
			if(m_world.IsLocked())
			{
				throw Error('Can not create fixture because world is locked');
			}

			_fixture = b2Pool.getFixture();

			_fixture.Create(this, m_xf, def);

			if(m_isActive)
			{
				_broadPhase = m_world.m_contactManager.m_broadPhase;
				_fixture.CreateProxy(_broadPhase, m_xf);
			}

			_fixture.m_next = m_fixtureList;
			m_fixtureList = _fixture;

			++m_fixtureCount;

			_fixture.m_body = this;

			// Adjust mass properties if needed
			if(_fixture.m_density > b2Settings.DECIMAL_ZERO)
			{
				ResetMassData();
			}

			// Let the world know we have a new fixture. This will cause new contacts to be created
			// at the beginning of the next time step.
			m_world.m_flags |= b2World.e_newFixture;

			_broadPhase = null;

			return _fixture;
		}

		/**
		 * Creates a fixture from a shape and attach it to this body.
		 * This is a convenience function. Use b2FixtureDef if you need to set parameters
		 * like friction, restitution, user data, or filtering.
		 * This function automatically updates the mass of the body.
		 * @param shape the shape to be cloned.
		 * @param density the shape density (set to zero for static bodies).
		 * @warning This function is locked during callbacks.
		 */
		public function CreateFixture2(shape:b2Shape, density:Number=0.0):b2Fixture
		{
			var def:b2FixtureDef = new b2FixtureDef();
			def.shape = shape;
			def.density = density;

			return CreateFixture(def);
		}

		private var currentJoint:b2JointEdge;
		private var nextJoint:b2JointEdge;

		public function Destroy():void
		{
			s_tMat = null;
			s_tVec = null;
			s_fixture = null;
			s_broadPhase = null;

			_fixture = null;
			_broadPhase = null;

			m_contactList = null;

			m_controllerList = null;

			m_fixtureList = null;
			m_fixtureCount = 0;

			if(m_force)
			{
				b2Pool.putVector2(m_force);
				m_force = null;
			}

			currentJoint = m_jointList;

			while(currentJoint)
			{
				nextJoint = currentJoint;
				currentJoint = currentJoint.next;

				nextJoint.joint.m_bodyA = null;
				nextJoint.joint.m_bodyB = null;
				nextJoint.other = null;

				nextJoint.next = null;
				nextJoint.prev = null;
			}

			m_jointList = null;

			if(m_linearVelocity)
			{
				b2Pool.putVector2(m_linearVelocity);
				m_linearVelocity = null;
			}

			m_next = null;
			m_prev = null;

			if(m_sweep)
			{
				b2Pool.putSweep(m_sweep);
				m_sweep = null;
			}

			m_userData = null;

			m_world = null;

			if(m_xf)
			{
				b2Pool.putTransform(m_xf);
				m_xf = null;
			}
		}


		private var _node:b2Fixture;
		private var _ppF:b2Fixture;
		private var _found:Boolean;

		private var _edge:b2ContactEdge;
		private var _c:b2Contact;
		private var _fixtureA:b2Fixture;
		private var _fixtureB:b2Fixture;


		/**
		 * Destroy a fixture. This removes the fixture from the broad-phase and
		 * destroys all contacts associated with this fixture. This will
		 * automatically adjust the mass of the body if the body is dynamic and the
		 * fixture has positive density.
		 * All fixtures attached to a body are implicitly destroyed when the body is destroyed.
		 * @param fixture the fixture to be removed.
		 * @warning This function is locked during callbacks.
		 */
		public function DestroyFixture(fixture:b2Fixture):void
		{
			// b2Settings.b2Assert(m_world.IsLocked() == false);
			if(m_world.IsLocked())
			{
				throw Error('Can not destroy fixture because world is locked');
			}

			_node = m_fixtureList;
			_ppF = null;
			_found = false;

			while(_node != null)
			{
				if(_node == fixture)
				{
					if(_ppF)
					{
						_ppF.m_next = fixture.m_next;
					}
					else
					{
						m_fixtureList = fixture.m_next;
					}

					_found = true;
					break;
				}

				_ppF = _node;
				_node = _node.m_next;
			}

			_node = null;
			_ppF = null;

			// You tried to remove a shape that is not attached to this body.
			// b2Settings.b2Assert(_found);
			if(!_found)
			{
				throw Error('You tried to remove a shape that is not attached to this body.');
			}

			// Destroy any contacts associated with the fixture.
			_edge = m_contactList;
			_c = null;
			_fixtureA = null;
			_fixtureB = null;

			while (_edge)
			{
				_c = _edge.contact;
				_edge = _edge.next;

				_fixtureA = _c.GetFixtureA();
				_fixtureB = _c.GetFixtureB();

				if(fixture == _fixtureA || fixture == _fixtureB)
				{
					// This destroys the contact and removes it from
					// this body's contact list
					m_world.m_contactManager.Destroy(_c);
				}
			}

			if(m_isActive)
			{
				_broadPhase = m_world.m_contactManager.m_broadPhase;
				fixture.DestroyProxy(_broadPhase);
			}

			fixture.Destroy();
			fixture.m_body = null;
			fixture.m_next = null;

			--m_fixtureCount;

			// Reset the mass data.
			ResetMassData();

			_edge = null;
			_c = null;
			_fixtureA = null;
			_fixtureB = null;

			b2Pool.putFixture(fixture);
		}

		private static var _broadPhase:IBroadPhase;
		private static var _setPositionFixture:b2Fixture;

		private static var tMat:b2Mat22
		private static var tVec:b2Vec2

		/**
		* Set the position of the body's origin and rotation (radians).
		* This breaks any contacts and wakes the other bodies.
		* @param position the new world position of the body's origin (not necessarily
		* the center of mass).
		* @param angle the new world rotation angle of the body in radians.
		*
		* Setting position is skipped if world is locked.
		*
		*/
		public function SetPositionAndAngle(position:b2Vec2, angle:Number, updateContacts:Boolean = true):void
		{
			// b2Settings.b2Assert(m_world.IsLocked() == false);
			if(m_world.IsLocked())
			{
				throw Error('Can not set position and angle because world is locked');
			}

			m_xf.R.Set(angle);

			m_xf.position.x = position.x;
			m_xf.position.y = position.y;

			//m_sweep.c0 = m_sweep.c = b2Mul(m_xf, m_sweep.localCenter);
			//b2MulMV(m_xf.R, m_sweep.localCenter);

			tMat = m_xf.R;
			tVec = m_sweep.localCenter;

			// (tMat.col1.x * tVec.x + tMat.col2.x * tVec.y)
			m_sweep.c.x = (tMat.col1.x * tVec.x + tMat.col2.x * tVec.y);
			// (tMat.col1.y * tVec.x + tMat.col2.y * tVec.y)
			m_sweep.c.y = (tMat.col1.y * tVec.x + tMat.col2.y * tVec.y);

			//return T.position + b2Mul(T.R, v);
			m_sweep.c.x += m_xf.position.x;
			m_sweep.c.y += m_xf.position.y;

			//m_sweep.c0 = m_sweep.c
			m_sweep.c0.x = m_sweep.c.x;
			m_sweep.c0.y = m_sweep.c.y;

			m_sweep.a0 = m_sweep.a = angle;

			_broadPhase = m_world.m_contactManager.m_broadPhase;

			for(_setPositionFixture = m_fixtureList; _setPositionFixture; _setPositionFixture = _setPositionFixture.m_next)
			{
				_setPositionFixture.Synchronize(_broadPhase, m_xf, m_xf);
			}

			if(updateContacts)
			{
				m_world.m_contactManager.FindNewContacts();
			}

			tMat = null;
			tVec = null;
			_broadPhase = null;
			_setPositionFixture = null;
		}

		/**
		 * Set the position of the body's origin and rotation (radians).
		 * This breaks any contacts and wakes the other bodies.
		 * Note this is less efficient than the other overload - you should use that
		 * if the angle is available.
		 * @param xf the transform of position and angle to set the bdoy to.
		 */
		public function SetTransform(xf:b2Transform):void
		{
			SetPositionAndAngle(xf.position, xf.GetAngle());
		}

		/**
		* Get the body transform for the body's origin.
		* @return the world transform of the body's origin.
		*/
		public function GetTransform():b2Transform
		{
			return m_xf;
		}

		/**
		* Get the world body origin position.
		* @return the world position of the body's origin.
		*/
		[Inline]
		final public function GetPosition():b2Vec2
		{
			return m_xf.position;
		}

		/**
		 * Set the world body origin position.
		 *
		 * Throws exception if world is locked.
		 *
		 * @param position the new position of the body
		 *
		 */
		public function SetPosition(position:b2Vec2):void
		{
			SetPositionAndAngle(position, m_sweep.a);
		}

		/**
		* Get the angle in radians.
		* @return the current world rotation angle in radians.
		*/
		[Inline]
		final public function GetAngle():Number
		{
			return m_sweep.a;
		}

		/**
		 * Set the world body angle
		 * @param angle the new angle of the body.
		 */
		public function SetAngle(angle:Number):void
		{
			SetPositionAndAngle(m_xf.position, angle);
		}


		/**
		* Get the world position of the center of mass.
		*/
		[Inline]
		final public function GetWorldCenter():b2Vec2
		{
			return m_sweep.c;
		}

		/**
		* Get the local position of the center of mass.
		*/
		[Inline]
		final public function GetLocalCenter():b2Vec2
		{
			return m_sweep.localCenter;
		}

		/**
		* Set the linear velocity of the center of mass.
		* @param v the new linear velocity of the center of mass.
		*/

		[Inline]
		final public function SetLinearVelocity(v:b2Vec2):void
		{
			if (m_type == b2_staticBody)
			{
				return;
			}

			m_linearVelocity.x = v.x;
			m_linearVelocity.y = v.y;
		}

		/**
		* Get the linear velocity of the center of mass.
		* @return the linear velocity of the center of mass.
		*/
		[Inline]
		final public function GetLinearVelocity():b2Vec2
		{
			return m_linearVelocity;
		}

		/**
		* Set the angular velocity.
		* @param omega the new angular velocity in radians/second.
		*/
		public function SetAngularVelocity(omega:Number):void
		{
			if(m_type == b2_staticBody)
			{
				return;
			}

			m_angularVelocity = omega;
		}

		/**
		* Get the angular velocity.
		* @return the angular velocity in radians/second.
		*/
		[Inline]
		final public function GetAngularVelocity():Number
		{
			return m_angularVelocity;
		}

		public function GetGravityScale():Number
		{
			return m_gravityScale;
		}

		public function SetGravityScale(value:Number):void
		{
			m_gravityScale = value;
		}

		/**
		 * Get the definition containing the body properties.
		 * @asonly
		 */
		public function GetDefinition():b2BodyDef
		{
			var bd:b2BodyDef = new b2BodyDef();
			bd.type = GetType();
			bd.allowSleep = m_allowSleep;
			bd.angle = GetAngle();
			bd.angularDamping = m_angularDamping;
			bd.angularVelocity = m_angularVelocity;
			bd.fixedRotation = m_isFixedRotation;
			bd.bullet = m_isBullet;
			bd.awake = m_isAwake;
			bd.linearDamping = m_linearDamping;
			bd.linearVelocity.SetV(GetLinearVelocity());
			bd.position = GetPosition();
			bd.userData = GetUserData();
			return bd;
		}

		/**
		* Apply a force at a world point. If the force is not
		* applied at the center of mass, it will generate a torque and
		* affect the angular velocity. This wakes up the body.
		* @param force the world force vector, usually in Newtons (N).
		* @param point the world position of the point of application.
		*/
		public function ApplyForce(force:b2Vec2, point:b2Vec2):void
		{
			if (m_type != b2_dynamicBody)
			{
				return;
			}

			if (IsAwake() == false)
			{
				SetAwake(true);
			}

			//m_force += force;
			m_force.x += force.x;
			m_force.y += force.y;

			//m_torque += b2Cross(point - m_sweep.c, force);
			m_torque += ((point.x - m_sweep.c.x) * force.y - (point.y - m_sweep.c.y) * force.x);
		}

		/**
		* Apply a torque. This affects the angular velocity
		* without affecting the linear velocity of the center of mass.
		* This wakes up the body.
		* @param torque about the z-axis (out of the screen), usually in N-m.
		*/
		public function ApplyTorque(torque:Number):void
		{
			if (m_type != b2_dynamicBody)
			{
				return;
			}

			if (IsAwake() == false)
			{
				SetAwake(true);
			}

			m_torque += torque;
		}

		/**
		* Apply an impulse at a point. This immediately modifies the velocity.
		* It also modifies the angular velocity if the point of application
		* is not at the center of mass. This wakes up the body.
		* @param impulse the world impulse vector, usually in N-seconds or kg-m/s.
		* @param point the world position of the point of application.
		*/
		public function ApplyImpulse(impulse:b2Vec2, point:b2Vec2):void
		{
			if(m_type != b2_dynamicBody)
			{
				return;
			}

			if(IsAwake() == false)
			{
				SetAwake(true);
			}

			//m_linearVelocity += m_invMass * impulse;
			m_linearVelocity.x += m_invMass * impulse.x;
			m_linearVelocity.y += m_invMass * impulse.y;
			//m_angularVelocity += m_invI * b2Cross(point - m_sweep.c, impulse);
			m_angularVelocity += m_invI * ((point.x - m_sweep.c.x) * impulse.y - (point.y - m_sweep.c.y) * impulse.x);
		}

		/**
		 * Splits a body into two, preserving dynamic properties
		 * @param	callback Called once per fixture, return true to move this fixture to the new body
		 * <code>function Callback(fixture:b2Fixture):Boolean</code>
		 * @return The newly created bodies
		 * @asonly
		 */
		public function Split(callback:Function):b2Body
		{
			var linearVelocity:b2Vec2 = b2Pool.getVector2(0, 0);

			linearVelocity.SetV(GetLinearVelocity());

			var angularVelocity:Number = GetAngularVelocity();
			var center:b2Vec2 = GetWorldCenter();
			var body1:b2Body = this;
			var body2:b2Body = m_world.CreateBody(GetDefinition());

			var prev:b2Fixture;

			for(var f:b2Fixture = body1.m_fixtureList; f;)
			{
				if (callback(f))
				{
					var next:b2Fixture = f.m_next;

					// Remove fixture
					if(prev)
					{
						prev.m_next = next;
					}
					else
					{
						body1.m_fixtureList = next;
					}

					body1.m_fixtureCount--;

					// Add fixture
					f.m_next = body2.m_fixtureList;
					body2.m_fixtureList = f;
					body2.m_fixtureCount++;

					f.m_body = body2;

					f = next;
				}
				else
				{
					prev = f;
					f = f.m_next
				}
			}

			body1.ResetMassData();
			body2.ResetMassData();

			// Compute consistent velocites for new bodies based on cached velocity
			var center1:b2Vec2 = body1.GetWorldCenter();
			var center2:b2Vec2 = body2.GetWorldCenter();



			var velocity1:b2Vec2 = b2Pool.getVector2(0, 0);
			var velocity2:b2Vec2 = b2Pool.getVector2(0, 0);

			var velocityCenter:b2Vec2 = b2Pool.getVector2(0, 0);
			var velocityCrossFV:b2Vec2 = b2Pool.getVector2(0, 0);

			b2Math.SubtractVV(center1, center, velocityCenter);

			b2Math.CrossFV(
				angularVelocity,
				velocityCenter,
				velocityCrossFV
			)

			b2Math.AddVV(
				linearVelocity,
				velocityCrossFV,
				velocity1
			);


			b2Math.SubtractVV(center2, center, velocityCenter);

			b2Math.CrossFV(
				angularVelocity,
				velocityCenter,
				velocityCrossFV
			)

			b2Math.AddVV(
				linearVelocity,
				velocityCrossFV,
				velocity2
			);

			body1.SetLinearVelocity(velocity1);
			body2.SetLinearVelocity(velocity2);
			body1.SetAngularVelocity(angularVelocity);
			body2.SetAngularVelocity(angularVelocity);

			body1.SynchronizeFixtures();
			body2.SynchronizeFixtures();

			b2Pool.putVector2(velocity1);
			b2Pool.putVector2(velocity2);

			b2Pool.putVector2(velocityCenter);
			b2Pool.putVector2(velocityCrossFV);

			b2Pool.putVector2(linearVelocity);

			return body2;
		}

		/**
		 * Merges another body into this. Only fixtures, mass and velocity are effected,
		 * Other properties are ignored
		 * @asonly
		 */
		public function Merge(other:b2Body):void
		{
			var f:b2Fixture;
			for (f = other.m_fixtureList; f; )
			{
				var next:b2Fixture = f.m_next;

				// Remove fixture
				other.m_fixtureCount--;

				// Add fixture
				f.m_next = m_fixtureList;
				m_fixtureList = f;
				m_fixtureCount++;

				f.m_body = body2;

				f = next;
			}
			body1.m_fixtureCount = 0;

			// Recalculate velocities
			var body1:b2Body = this;
			var body2:b2Body = other;

			// Compute consistent velocites for new bodies based on cached velocity
			var center1:b2Vec2 = body1.GetWorldCenter();
			var center2:b2Vec2 = body2.GetWorldCenter();

			var velocity1:b2Vec2 = b2Pool.getVector2(0, 0);

			velocity1.SetV(body1.GetLinearVelocity());

			var velocity2:b2Vec2 = b2Pool.getVector2(0, 0);

			velocity2.SetV(body2.GetLinearVelocity());

			var angular1:Number = body1.GetAngularVelocity();
			var angular:Number = body2.GetAngularVelocity();

			// TODO

			body1.ResetMassData();

			SynchronizeFixtures();

			b2Pool.putVector2(velocity1);
			b2Pool.putVector2(velocity2);
		}

		/**
		* Get the total mass of the body.
		* @return the mass, usually in kilograms (kg).
		*/
		public function GetMass():Number
		{
			return m_mass;
		}

		/**
		* Get the central rotational inertia of the body.
		* @return the rotational inertia, usually in kg-m^2.
		*/
		public function GetInertia():Number
		{
			return m_I;
		}

		/**
		 * Get the mass data of the body. The rotational inertial is relative to the center of mass.
		 */
		public function GetMassData(data:b2MassData):void
		{
			data.mass = m_mass;
			data.I = m_I;
			data.center.SetV(m_sweep.localCenter);
		}

		/**
		 * Set the mass properties to override the mass properties of the fixtures
		 * Note that this changes the center of mass position.
		 * Note that creating or destroying fixtures can also alter the mass.
		 * This function has no effect if the body isn't dynamic.
		 * @warning The supplied rotational inertia should be relative to the center of mass
		 * @param	data the mass properties.
		 */
		public function SetMassData(massData:b2MassData):void
		{
			// b2Settings.b2Assert(m_world.IsLocked() == false);
			if(m_world.IsLocked())
			{
				throw Error('Can not set mass data because world is locked');
			}

			if(m_world.IsLocked() == true)
			{
				return;
			}

			if(m_type != b2_dynamicBody)
			{
				return;
			}

			m_invMass = b2Settings.DECIMAL_ZERO;
			m_I = b2Settings.DECIMAL_ZERO;
			m_invI = b2Settings.DECIMAL_ZERO;

			m_mass = massData.mass;

			// Compute the center of mass.
			if(m_mass <= b2Settings.DECIMAL_ZERO)
			{
				m_mass = 1.0;
			}

			m_invMass = 1.0 / m_mass;

			if (massData.I > b2Settings.DECIMAL_ZERO && !m_isFixedRotation)
			{
				// Center the inertia about the center of mass
				m_I = massData.I - m_mass * (massData.center.x * massData.center.x + massData.center.y * massData.center.y);
				m_invI = 1.0 / m_I;
			}

			// Move center of mass
			var oldCenter:b2Vec2 = b2Pool.getVector2(0, 0);

			oldCenter.SetV(m_sweep.c);

			m_sweep.localCenter.SetV(massData.center);

			b2Math.MulX(m_xf, m_sweep.localCenter, m_sweep.c0);

			m_sweep.c.SetV(m_sweep.c0);

			// Update center of mass velocity
			//m_linearVelocity += b2Cross(m_angularVelocity, m_sweep.c - oldCenter);
			m_linearVelocity.x += m_angularVelocity * -(m_sweep.c.y - oldCenter.y);
			m_linearVelocity.y += m_angularVelocity * +(m_sweep.c.x - oldCenter.x);

			b2Pool.putVector2(oldCenter);
		}

		private static var _resetMassCenter:b2Vec2 = new b2Vec2();
		private static var _oldCenter:b2Vec2 = new b2Vec2();


		/**
		 * This resets the mass properties to the sum of the mass properties of the fixtures.
		 * This normally does not need to be called unless you called SetMassData to override
		 * the mass and later you want to reset the mass.
		 */
		[Inline]
		final public function ResetMassData():void
		{
			// Compute mass data from shapes. Each shape has it's own density
			m_mass = b2Settings.DECIMAL_ZERO;
			m_invMass = b2Settings.DECIMAL_ZERO;
			m_I = b2Settings.DECIMAL_ZERO;
			m_invI = b2Settings.DECIMAL_ZERO;

			m_sweep.localCenter.x = b2Settings.DECIMAL_ZERO;
			m_sweep.localCenter.y = b2Settings.DECIMAL_ZERO;

			// Static and kinematic bodies have zero mass.
			if(m_type == b2_staticBody || m_type == b2_kinematicBody)
			{
				return;
			}

			//b2Assert(m_type == b2_dynamicBody);

			// Accumulate mass over all fixtures.
			_resetMassCenter.x = 0;
			_resetMassCenter.y = 0;

			var massData:b2MassData = b2Pool.getMassData();

			for(var f:b2Fixture = m_fixtureList; f; f = f.m_next)
			{
				if(f.m_density == b2Settings.DECIMAL_ZERO)
				{
					continue;
				}

				f.CalculatetMassData(massData);

				m_mass += massData.mass;
				_resetMassCenter.x += massData.center.x * massData.mass;
				_resetMassCenter.y += massData.center.y * massData.mass;
				m_I += massData.I;
			}

			b2Pool.putMassData(massData);

			// Compute the center of mass.
			if(m_mass > b2Settings.DECIMAL_ZERO)
			{
				m_invMass = b2Settings.DECIMAL_ONE / m_mass;
				_resetMassCenter.x *= m_invMass;
				_resetMassCenter.y *= m_invMass;
			}
			else
			{
				// Force all dynamic bodies to have a positive mass.
				m_mass = b2Settings.DECIMAL_ONE;
				m_invMass = b2Settings.DECIMAL_ONE;
			}

			if (m_I > b2Settings.DECIMAL_ZERO && !m_isFixedRotation)
			{
				// Center the inertia about the center of mass
				m_I -= m_mass * (_resetMassCenter.x * _resetMassCenter.x + _resetMassCenter.y * _resetMassCenter.y);
				m_I *= m_inertiaScale;

				// b2Settings.b2Assert(m_I > 0);
				if(m_I <= 0)
				{
					throw Error('Inverse mass is invalid');
				}

				m_invI = b2Settings.DECIMAL_ONE / m_I;
			}
			else
			{
				m_I = b2Settings.DECIMAL_ZERO;
				m_invI = b2Settings.DECIMAL_ZERO;
			}

			_oldCenter.x = m_sweep.c.x;
			_oldCenter.y = m_sweep.c.y;

			m_sweep.localCenter.x = _resetMassCenter.x;
			m_sweep.localCenter.y = _resetMassCenter.y;

			b2Math.MulX(m_xf, m_sweep.localCenter, m_sweep.c0);

			m_sweep.c.x = m_sweep.c0.x;
			m_sweep.c.y = m_sweep.c0.y;

			// Update center of mass velocity
			//m_linearVelocity += b2Cross(m_angularVelocity, m_sweep.c - oldCenter);
			m_linearVelocity.x += m_angularVelocity * -(m_sweep.c.y - _oldCenter.y);
			m_linearVelocity.y += m_angularVelocity * +(m_sweep.c.x - _oldCenter.x);
		}

		private static var wpA:b2Mat22;

		/**
		 * Get the world coordinates of a point given the local coordinates.
		 * @param localPoint a point on the body measured relative the the body's origin.
		 * @return the same point expressed in world coordinates.
		 */
		[Inline]
		final public function GetWorldPoint(localPoint:b2Vec2, result:b2Vec2):void
		{
			//return b2Math.b2MulX(m_xf, localPoint);
			wpA = m_xf.R;

			result.x = wpA.col1.x * localPoint.x + wpA.col2.x * localPoint.y;
			result.y = wpA.col1.y * localPoint.x + wpA.col2.y * localPoint.y;

			result.x += m_xf.position.x;
			result.y += m_xf.position.y;
		}

		/**
		 * Get the world coordinates of a vector given the local coordinates.
		 * @param localVector a vector fixed in the body.
		 * @return the same vector expressed in world coordinates.
		 */
		[Inline]
		final public function GetWorldVector(localVector:b2Vec2, result:b2Vec2):void
		{
			//b2Math.MulMV(m_xf.R, localVector, result);
			result.x = m_xf.R.col1.x * localVector.x + m_xf.R.col2.x * localVector.y;
			result.y = m_xf.R.col1.y * localVector.x + m_xf.R.col2.y * localVector.y;
		}

		/**
		 * Gets a local point relative to the body's origin given a world point.
		 * @param a point in world coordinates.
		 * @return the corresponding local point relative to the body's origin.
		 */
		public function GetLocalPoint(worldPoint:b2Vec2, result:b2Vec2):void
		{
			b2Math.MulXT(m_xf, worldPoint, result);
		}

		/**
		 * Gets a local vector given a world vector.
		 * @param a vector in world coordinates.
		 * @return the corresponding local vector.
		 */
		public function GetLocalVector(worldVector:b2Vec2, result:b2Vec2):void
		{
			b2Math.MulTMV(m_xf.R, worldVector, result);
		}

		/**
		* Get the world linear velocity of a world point attached to this body.
		* @param a point in world coordinates.
		* @return the world velocity of a point.
		*/
		public function GetLinearVelocityFromWorldPoint(worldPoint:b2Vec2, result:b2Vec2):b2Vec2
		{
			//return          m_linearVelocity   + b2Cross(m_angularVelocity,   worldPoint   - m_sweep.c);
			result.x = m_linearVelocity.x - m_angularVelocity * (worldPoint.y - m_sweep.c.y);
			result.y = m_linearVelocity.y + m_angularVelocity * (worldPoint.x - m_sweep.c.x);

			return result;
		}

		/**
		* Get the world velocity of a local point.
		* @param a point in local coordinates.
		* @return the world velocity of a point.
		*/
		public function GetLinearVelocityFromLocalPoint(localPoint:b2Vec2) : b2Vec2
		{
			//return GetLinearVelocityFromWorldPoint(GetWorldPoint(localPoint));
			var A:b2Mat22 = m_xf.R;
			var worldPoint:b2Vec2 = new b2Vec2(A.col1.x * localPoint.x + A.col2.x * localPoint.y,
			                                   A.col1.y * localPoint.x + A.col2.y * localPoint.y);
			worldPoint.x += m_xf.position.x;
			worldPoint.y += m_xf.position.y;
			return new b2Vec2(m_linearVelocity.x -         m_angularVelocity * (worldPoint.y - m_sweep.c.y),
			                  m_linearVelocity.y +         m_angularVelocity * (worldPoint.x - m_sweep.c.x));
		}

		/**
		* Get the linear damping of the body.
		*/
		public function GetLinearDamping():Number
		{
			return m_linearDamping;
		}

		/**
		* Set the linear damping of the body.
		*/
		public function SetLinearDamping(value:Number):void
		{
			m_linearDamping = value;
		}

		/**
		* Get the angular damping of the body
		*/
		public function GetAngularDamping():Number
		{
			return m_angularDamping;
		}

		/**
		* Set the angular damping of the body.
		*/
		public function SetAngularDamping(value:Number):void
		{
			m_angularDamping = value;
		}

		public function SetInertiaScale(value:Number):void
		{
			m_inertiaScale = value;
		}

		public function GetInertiaScale():Number
		{
			return m_inertiaScale;
		}

		/**
		 * Set the type of this body. This may alter the mass and velocity
		 * @param	type - enum stored as a static member of b2Body
		 */
		public function SetType( type:uint ):void
		{
			if ( m_type == type )
			{
				return;
			}

			m_type = type;

			ResetMassData();

			if ( m_type == b2_staticBody )
			{
				m_linearVelocity.SetZero();
				m_angularVelocity = b2Settings.DECIMAL_ZERO;
			}

			SetAwake(true);

			m_force.SetZero();
			m_torque = b2Settings.DECIMAL_ZERO;

			// Since the body type changed, we need to flag contacts for filtering.
			for (var ce:b2ContactEdge = m_contactList; ce; ce = ce.next)
			{
				ce.contact.FlagForFiltering();
			}
		}

		/**
		 * Get the type of this body.
		 * @return type enum as a uint
		 */
		[Inline]
		final public function GetType():uint
		{
			return m_type;
		}

		/**
		* Should this body be treated like a bullet for continuous collision detection?
		*/
		public function SetBullet(value:Boolean):void
		{
			m_isBullet = value;
		}

		/**
		* Is this body treated like a bullet for continuous collision detection?
		*/
		[Inline]
		final public function IsBullet():Boolean
		{
			return m_isBullet
		}

		/**
		 * Is this body allowed to sleep
		 * @param	flag
		 */
		public function SetSleepingAllowed(value:Boolean):void
		{
			m_allowSleep = value;

			if(!value)
			{
				SetAwake(true);
			}
		}

		/**
		 * Set the sleep state of the body. A sleeping body has very low CPU cost.
		 * @param	flag - set to false to put body to sleep, true to wake it
		 */
		[Inline]
		final public function SetAwake(value:Boolean):void
		{
			m_isAwake = value;

			if(value)
			{
				m_currentTimeBeforeSleep = b2Settings.DECIMAL_ZERO;
			}
			else
			{
				m_currentTimeBeforeSleep = b2Settings.DECIMAL_ZERO;

				m_linearVelocity.x = b2Settings.DECIMAL_ZERO;
				m_linearVelocity.y = b2Settings.DECIMAL_ZERO;

				m_angularVelocity = b2Settings.DECIMAL_ZERO;

				m_force.x = b2Settings.DECIMAL_ZERO;
				m_force.y = b2Settings.DECIMAL_ZERO;

				m_torque = b2Settings.DECIMAL_ZERO;
			}
		}

		/**
		 * Get the sleeping state of this body.
		 * @return true if body is sleeping
		 */
		[Inline]
		final public function IsAwake():Boolean
		{
			return m_isAwake;
		}

		/**
		 * Set this body to have fixed rotation. This causes the mass to be reset.
		 * @param	fixed - true means no rotation
		 */
		[Inline]
		final public function SetFixedRotation(value:Boolean):void
		{
			m_isFixedRotation = value;

			ResetMassData();
		}

		/**
		* Does this body have fixed rotation?
		* @return true means fixed rotation
		*/
		[Inline]
		final public function IsFixedRotation():Boolean
		{
			return m_isFixedRotation;
		}

		/** Set the active state of the body. An inactive body is not
		* simulated and cannot be collided with or woken up.
		* If you pass a flag of true, all fixtures will be added to the
		* broad-phase.
		* If you pass a flag of false, all fixtures will be removed from
		* the broad-phase and all contacts will be destroyed.
		* Fixtures and joints are otherwise unaffected. You may continue
		* to create/destroy fixtures and joints on inactive bodies.
		* Fixtures on an inactive body are implicitly inactive and will
		* not participate in collisions, ray-casts, or queries.
		* Joints connected to an inactive body are implicitly inactive.
		* An inactive body is still owned by a b2World object and remains
		* in the body list.
		*/
		public function SetActive(value:Boolean):void
		{
			if(m_isActive == value)
			{
				return;
			}

			// b2Settings.b2Assert(m_world.IsLocked() == false);
			if(m_world.IsLocked())
			{
				throw Error('Can activate object because world is locked');
			}

			m_isActive = value;

			var broadPhase:IBroadPhase;
			var f:b2Fixture;

			if(value)
			{
				// Create all proxies.
				broadPhase = m_world.m_contactManager.m_broadPhase;

				for(f = m_fixtureList; f; f = f.m_next)
				{
					f.CreateProxy(broadPhase, m_xf);
				}
				// Contacts are created the next time step.
			}
			else
			{
				// Destroy all proxies.
				broadPhase = m_world.m_contactManager.m_broadPhase;

				for(f = m_fixtureList; f; f = f.m_next)
				{
					f.DestroyProxy(broadPhase);
				}

				// Destroy the attached contacts.
				var ce:b2ContactEdge = m_contactList;

				while (ce)
				{
					var ce0:b2ContactEdge = ce;
					ce = ce.next;
					m_world.m_contactManager.Destroy(ce0.contact);
				}

				m_contactList = null;
			}
		}

		/**
		 * Get the active state of the body.
		 * @return true if active.
		 */

		[Inline]
		final public function IsActive():Boolean
		{
			return m_isActive;
		}


		/**
		* Is this body allowed to sleep?
		*/
		[Inline]
		final public function IsSleepingAllowed():Boolean
		{
			return m_allowSleep;
		}

		/**
		* Get the list of all fixtures attached to this body.
		*/
		[Inline]
		final public function GetFixtureList():b2Fixture
		{
			return m_fixtureList;
		}

		/**
		* Get the list of all joints attached to this body.
		*/
		[Inline]
		final public function GetJointList():b2JointEdge
		{
			return m_jointList;
		}

		/**
		 * Get the list of all controllers attached to this body.
		 */
		[Inline]
		final public function GetControllerList():b2ControllerEdge
		{
			return m_controllerList;
		}

		/**
		 * Get a list of all contacts attached to this body.
		 */
		[Inline]
		final public function GetContactList():b2ContactEdge
		{
			return m_contactList;
		}

		/**
		* Get the next body in the world's body list.
		*/
		[Inline]
		final public function GetNext():b2Body
		{
			return m_next;
		}

		/**
		* Get the user data pointer that was provided in the body definition.
		*/
		public function GetUserData():*
		{
			return m_userData;
		}

		/**
		* Set the user data. Use this to store your application specific data.
		*/
		public function SetUserData(data:*) : void
		{
			m_userData = data;
		}

		/**
		* Get the parent world of this body.
		*/
		public function GetWorld(): b2World
		{
			return m_world;
		}

		//--------------- Internals Below -------------------


		// Constructor
		/**
		 * @private
		 */
		public function b2Body(bd:b2BodyDef, world:b2World)
		{
			m_id = ++bodyIds;

			//b2Settings.b2Assert(world.IsLocked() == false);

			//b2Settings.b2Assert(bd.position.IsValid());
	 		//b2Settings.b2Assert(bd.linearVelocity.IsValid());
	 		//b2Settings.b2Assert(b2Math.b2IsValid(bd.angle));
	 		//b2Settings.b2Assert(b2Math.b2IsValid(bd.angularVelocity));
	 		//b2Settings.b2Assert(b2Math.b2IsValid(bd.inertiaScale) && bd.inertiaScale >= 0.0);
	 		//b2Settings.b2Assert(b2Math.b2IsValid(bd.angularDamping) && bd.angularDamping >= 0.0);
	 		//b2Settings.b2Assert(b2Math.b2IsValid(bd.linearDamping) && bd.linearDamping >= 0.0);

			m_xf = b2Pool.getTransform();
			m_sweep = b2Pool.getSweep();

			m_isBullet = bd.bullet;
			m_isFixedRotation = bd.fixedRotation;
			m_allowSleep = bd.allowSleep;
			m_isAwake = bd.awake;
			m_isActive = bd.active;

			m_isIsland = false;

			m_world = world;

			m_xf.position.x = bd.position.x;
			m_xf.position.y = bd.position.y;

			m_xf.R.Set(bd.angle);

			m_sweep.localCenter.SetZero();
			m_sweep.t0 = 1.0;
			m_sweep.a0 = m_sweep.a = bd.angle;

			//m_sweep.c0 = m_sweep.c = b2Mul(m_xf, m_sweep.localCenter);
			//b2MulMV(m_xf.R, m_sweep.localCenter);
			var tMat:b2Mat22 = m_xf.R;
			var tVec:b2Vec2 = m_sweep.localCenter;
			// (tMat.col1.x * tVec.x + tMat.col2.x * tVec.y)
			m_sweep.c.x = (tMat.col1.x * tVec.x + tMat.col2.x * tVec.y);
			// (tMat.col1.y * tVec.x + tMat.col2.y * tVec.y)
			m_sweep.c.y = (tMat.col1.y * tVec.x + tMat.col2.y * tVec.y);
			//return T.position + b2Mul(T.R, v);
			m_sweep.c.x += m_xf.position.x;
			m_sweep.c.y += m_xf.position.y;
			//m_sweep.c0 = m_sweep.c
			m_sweep.c0.SetV(m_sweep.c);

			m_jointList = null;
			m_controllerList = null;
			m_contactList = null;
			m_controllerCount = 0;
			m_prev = null;
			m_next = null;

			m_linearVelocity.x = bd.linearVelocity.x;
			m_linearVelocity.y = bd.linearVelocity.y;

			m_angularVelocity = bd.angularVelocity;

			m_linearDamping = bd.linearDamping;
			m_angularDamping = bd.angularDamping;

			m_force.Set(b2Settings.DECIMAL_ZERO, b2Settings.DECIMAL_ZERO);
			m_torque = b2Settings.DECIMAL_ZERO;

			m_currentTimeBeforeSleep = b2Settings.DECIMAL_ZERO;

			m_type = bd.type;

			if (m_type == b2_dynamicBody)
			{
				m_mass = 1.0;
				m_invMass = 1.0;
			}
			else
			{
				m_mass = b2Settings.DECIMAL_ZERO;
				m_invMass = b2Settings.DECIMAL_ZERO;
			}

			m_I = b2Settings.DECIMAL_ZERO;
			m_invI = b2Settings.DECIMAL_ZERO;

			m_inertiaScale = bd.inertiaScale;
			m_gravityScale = bd.gravityScale;

			m_userData = bd.userData;

			m_fixtureList = null;
			m_fixtureCount = 0;
		}

		// Destructor
		//~b2Body();

		private var s_tMat:b2Mat22;
		private var s_tVec:b2Vec2;
		private var s_fixture:b2Fixture;
		private var s_broadPhase:IBroadPhase;

		[Inline]
		final public function SynchronizeFixtures():void
		{
			b2Settings.s_xf1.R.Set(m_sweep.a0);

			s_tMat = b2Settings.s_xf1.R;
			s_tVec = m_sweep.localCenter;

			b2Settings.s_xf1.position.x = m_sweep.c0.x - (s_tMat.col1.x * s_tVec.x + s_tMat.col2.x * s_tVec.y);
			b2Settings.s_xf1.position.y = m_sweep.c0.y - (s_tMat.col1.y * s_tVec.x + s_tMat.col2.y * s_tVec.y);

			s_broadPhase = m_world.m_contactManager.m_broadPhase;

			for (s_fixture = m_fixtureList; s_fixture; s_fixture = s_fixture.m_next)
			{
				s_fixture.Synchronize(s_broadPhase, b2Settings.s_xf1, m_xf);
			}
		}

		private static var _mat22c:Number;
		private static var _mat22s:Number;

		[Inline]
		final public function SynchronizeTransform():void
		{
			//m_xf.R.Set(m_sweep.a);
			_mat22c = Math.cos(m_sweep.a);
			_mat22s = Math.sin(m_sweep.a);

			m_xf.R.col1.x = _mat22c; m_xf.R.col2.x = -_mat22s;
			m_xf.R.col1.y = _mat22s; m_xf.R.col2.y = _mat22c;
			// end of m_xf.R.Set(m_sweep.a);

			//m_xf.position = m_sweep.c - b2Mul(m_xf.R, m_sweep.localCenter);
			m_xf.position.x = m_sweep.c.x - (m_xf.R.col1.x * m_sweep.localCenter.x + m_xf.R.col2.x * m_sweep.localCenter.y);
			m_xf.position.y = m_sweep.c.y - (m_xf.R.col1.y * m_sweep.localCenter.x + m_xf.R.col2.y * m_sweep.localCenter.y);
		}

		private var sc_jointEdge:b2JointEdge;

		// This is used to prevent connected bodies from colliding.
		// It may lie, depending on the collideConnected flag.
		[Inline]
		final public function ShouldCollide(other:b2Body):Boolean
		{
			// At least one body should be dynamic
			if (m_type != b2_dynamicBody && other.m_type != b2_dynamicBody)
			{
				return false;
			}

			// Does a joint prevent collision?
			for(sc_jointEdge = m_jointList; sc_jointEdge; sc_jointEdge = sc_jointEdge.next)
			{
				if(sc_jointEdge.other == other && sc_jointEdge.joint.m_collideConnected == false)
				{
					return false;
				}
			}

			return true;
		}

		[Inline]
		final public function Advance(t:Number):void
		{
			// Advance to the new safe time.
			m_sweep.Advance(t);
			m_sweep.c.SetV(m_sweep.c0);
			m_sweep.a = m_sweep.a0;
			SynchronizeTransform();
		}

		public static var bodyIds:int = 0;

		public var m_type:uint;
		public var m_id:int = 0;

		public var m_allowSleep:Boolean;
		public var m_currentTimeBeforeSleep:Number;

		public var m_isIsland:Boolean;
		public var m_isBullet:Boolean;
		public var m_isFixedRotation:Boolean;
		public var m_isAwake:Boolean;
		public var m_isActive:Boolean;

		public var m_islandIndex:int;

		public var m_xf:b2Transform;// the body origin transform

		public var m_sweep:b2Sweep;	// the swept motion for CCD

		public var m_linearVelocity:b2Vec2 = b2Pool.getVector2(0, 0);
		public var m_angularVelocity:Number;

		public var m_force:b2Vec2 = b2Pool.getVector2(0, 0);
		public var m_torque:Number;

		public var m_world:b2World;
		public var m_prev:b2Body;
		public var m_next:b2Body;

		public var m_fixtureList:b2Fixture;
		public var m_fixtureCount:int;

		public var m_controllerList:b2ControllerEdge;
		public var m_controllerCount:int;

		public var m_jointList:b2JointEdge;
		public var m_contactList:b2ContactEdge;

		public var m_mass:Number;
		public var m_invMass:Number;

		public var m_I:Number, m_invI:Number;

		public var m_inertiaScale:Number = 1;
		public var m_gravityScale:Number = 1;

		public var m_linearDamping:Number;
		public var m_angularDamping:Number;

		public var m_userData:*;

		static public var e_islandFlag:uint				= 0x0001;
		static public var e_awakeFlag:uint				= 0x0002;
		static public var e_allowSleepFlag:uint			= 0x0004;
		static public var e_bulletFlag:uint				= 0x0008;
		static public var e_fixedRotationFlag:uint		= 0x0010;
		static public var e_activeFlag:uint				= 0x0020;

		/// static: zero mass, zero velocity, may be manually moved
		/// kinematic: zero mass, non-zero velocity set by user, moved by solver
		/// dynamic: positive mass, non-zero velocity determined by forces, moved by solver
		static public var b2_staticBody:uint = 0;
		static public var b2_kinematicBody:uint = 1;
		static public var b2_dynamicBody:uint = 2;
	};
}
