﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Dynamics.Joints
{
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Mat22;
	import Box2D.Common.Math.b2Mat33;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Common.Math.b2Vec3;
	import Box2D.Dynamics.b2TimeStep;
	
	// Point-to-point constraint
	// Cdot = v2 - v1
	//      = v2 + cross(w2, r2) - v1 - cross(w1, r1)
	// J = [-I -r1_skew I r2_skew ]
	// Identity used:
	// w k % (rx i + ry j) = w * (-ry i + rx j)
	
	// Angle constraint
	// Cdot = w2 - w1
	// J = [0 0 -1 0 0 1]
	// K = invI1 + invI2
	
	/**
	 * A weld joint essentially glues two bodies together. A weld joint may
	 * distort somewhat because the island constraint solver is approximate.
	 */
	public class b2WeldJoint extends b2Joint
	{
		/** @inheritDoc */
		public override function GetAnchorA(result:b2Vec2):void
		{
			m_bodyA.GetWorldPoint(m_localAnchorA, result);
		}
	
		/** @inheritDoc */
		public override function GetAnchorB(result:b2Vec2):void
		{
			return m_bodyB.GetWorldPoint(m_localAnchorB, result);
		}
		
		/** @inheritDoc */
		public override function GetReactionForce(inv_dt:Number, result:b2Vec2):void
		{
			result.x = inv_dt * m_impulse.x;
			result.y = inv_dt * m_impulse.y;
		}
	
		/** @inheritDoc */
		public override function GetReactionTorque(inv_dt:Number):Number
		{
			return inv_dt * m_impulse.z;
		}
		
		//--------------- Internals Below -------------------
	
		/** @private */
		public function b2WeldJoint(def:b2WeldJointDef)
		{
			super(def);
			
			m_localAnchorA.SetV(def.localAnchorA);
			m_localAnchorB.SetV(def.localAnchorB);
			m_referenceAngle = def.referenceAngle;
	
			m_impulse.SetZero();
			m_mass = new b2Mat33();
		}
	
		[Inline]
		final public override function InitVelocityConstraints(step:b2TimeStep):void
		{
			tMat = m_bodyA.m_xf.R;
			rAX = m_localAnchorA.x - m_bodyA.m_sweep.localCenter.x;
			rAY = m_localAnchorA.y - m_bodyA.m_sweep.localCenter.y;
			
			tX =  (tMat.col1.x * rAX + tMat.col2.x * rAY);
			rAY = (tMat.col1.y * rAX + tMat.col2.y * rAY);
			rAX = tX;

			tMat = m_bodyB.m_xf.R;

			rBX = m_localAnchorB.x - m_bodyB.m_sweep.localCenter.x;
			rBY = m_localAnchorB.y - m_bodyB.m_sweep.localCenter.y;

			tX =  (tMat.col1.x * rBX + tMat.col2.x * rBY);
			rBY = (tMat.col1.y * rBX + tMat.col2.y * rBY);
			rBX = tX;

			mA = m_bodyA.m_invMass
			mB = m_bodyB.m_invMass;
			iA = m_bodyA.m_invI
			iB = m_bodyB.m_invI;
			
			m_mass.col1.x = mA + mB + rAY * rAY * iA + rBY * rBY * iB;
			m_mass.col2.x = -rAY * rAX * iA - rBY * rBX * iB;
			m_mass.col3.x = -rAY * iA - rBY * iB;
			m_mass.col1.y = m_mass.col2.x;
			m_mass.col2.y = mA + mB + rAX * rAX * iA + rBX * rBX * iB;
			m_mass.col3.y = rAX * iA + rBX * iB;
			m_mass.col1.z = m_mass.col3.x;
			m_mass.col2.z = m_mass.col3.y;
			m_mass.col3.z = iA + iB;
			
			if (step.warmStarting)
			{
				// Scale impulses to support a variable time step.
				m_impulse.x *= step.dtRatio;
				m_impulse.y *= step.dtRatio;
				m_impulse.z *= step.dtRatio;
	
				m_bodyA.m_linearVelocity.x -= mA * m_impulse.x;
				m_bodyA.m_linearVelocity.y -= mA * m_impulse.y;
				m_bodyA.m_angularVelocity -= iA * (rAX * m_impulse.y - rAY * m_impulse.x + m_impulse.z);
	
				m_bodyB.m_linearVelocity.x += mB * m_impulse.x;
				m_bodyB.m_linearVelocity.y += mB * m_impulse.y;
				m_bodyB.m_angularVelocity += iB * (rBX * m_impulse.y - rBY * m_impulse.x + m_impulse.z);
			}
			else
			{
				m_impulse.SetZero();
			}
		}
		
		private var _velocityImpulse:b2Vec3 = new b2Vec3(0, 0, 0);
		private var m33Det:Number = 0;

		private var m33bX:Number = 0;
		private var m33bY:Number = 0;
		private var m33bZ:Number = 0;
		
		[Inline]
		final public override function SolveVelocityConstraints(step:b2TimeStep):void
		{
			tMat = m_bodyA.m_xf.R;
			
			rAX = m_localAnchorA.x - m_bodyA.m_sweep.localCenter.x;
			rAY = m_localAnchorA.y - m_bodyA.m_sweep.localCenter.y;
			
			tX =  tMat.col1.x * rAX + tMat.col2.x * rAY;
			rAY = tMat.col1.y * rAX + tMat.col2.y * rAY;
			rAX = tX;

			tMat = m_bodyB.m_xf.R;
			
			rBX = m_localAnchorB.x - m_bodyB.m_sweep.localCenter.x;
			rBY = m_localAnchorB.y - m_bodyB.m_sweep.localCenter.y;
			
			tX =  tMat.col1.x * rBX + tMat.col2.x * rBY;
			rBY = tMat.col1.y * rBX + tMat.col2.y * rBY;
			rBX = tX;
			
			// Solve point-to-point constraint
			Cdot1X = m_bodyB.m_linearVelocity.x - m_bodyB.m_angularVelocity * rBY - m_bodyA.m_linearVelocity.x + m_bodyA.m_angularVelocity * rAY;
			Cdot1Y = m_bodyB.m_linearVelocity.y + m_bodyB.m_angularVelocity * rBX - m_bodyA.m_linearVelocity.y - m_bodyA.m_angularVelocity * rAX;
			Cdot2 = m_bodyB.m_angularVelocity - m_bodyA.m_angularVelocity; 
	
			
			/**
			 * 
			 *  Moved calculating m_mass.Solve33(_velocityImpulse, -Cdot1X, -Cdot1Y, -Cdot2);
			 * 
			 *  here to avoid calling method
			 *  
			 */

			//m_mass.Solve33(_velocityImpulse, -Cdot1X, -Cdot1Y, -Cdot2);
			
			m33bX = -Cdot1X;
			m33bY = -Cdot1Y;
			m33bZ = -Cdot2;
			
			//float32 det = b2Dot(col1, b2Cross(col2, col3));
			m33Det = m_mass.col1.x * (m_mass.col2.y * m_mass.col3.z - m_mass.col2.z * m_mass.col3.y) +
				m_mass.col1.y * (m_mass.col2.z * m_mass.col3.x - m_mass.col2.x * m_mass.col3.z) +
				m_mass.col1.z * (m_mass.col2.x * m_mass.col3.y - m_mass.col2.y * m_mass.col3.x);
			
			if(m33Det != b2Settings.DECIMAL_ZERO)
			{
				m33Det = b2Settings.DECIMAL_ONE / m33Det;
			}

			//out.x = det * b2Dot(b, b2Cross(col2, col3));
			_velocityImpulse.x = m33Det * (m33bX * (m_mass.col2.y * m_mass.col3.z - m_mass.col2.z * m_mass.col3.y) +
										m33bY * (m_mass.col2.z * m_mass.col3.x - m_mass.col2.x * m_mass.col3.z) +
										m33bZ * (m_mass.col2.x * m_mass.col3.y - m_mass.col2.y * m_mass.col3.x) );
			
			//out.y = det * b2Dot(col1, b2Cross(b, col3));
			_velocityImpulse.y = m33Det * (m_mass.col1.x * (m33bY * m_mass.col3.z - m33bZ * m_mass.col3.y) +
										m_mass.col1.y * (m33bZ * m_mass.col3.x - m33bX * m_mass.col3.z) +
										m_mass.col1.z * (m33bX * m_mass.col3.y - m33bY * m_mass.col3.x));
			
			//out.z = det * b2Dot(col1, b2Cross(col2, b));
			_velocityImpulse.z = m33Det * (m_mass.col1.x * (m_mass.col2.y * m33bZ - m_mass.col2.z * m33bY) +
										m_mass.col1.y * (m_mass.col2.z * m33bX - m_mass.col2.x * m33bZ) +
										m_mass.col1.z * (m_mass.col2.x * m33bY - m_mass.col2.y * m33bX));

			
			
			
			m_impulse.x += _velocityImpulse.x;
			m_impulse.y += _velocityImpulse.y;
			m_impulse.z += _velocityImpulse.z;
			
			m_bodyA.m_linearVelocity.x -= m_bodyA.m_invMass * _velocityImpulse.x;
			m_bodyA.m_linearVelocity.y -= m_bodyA.m_invMass * _velocityImpulse.y;

			m_bodyA.m_angularVelocity -= m_bodyA.m_invI * (rAX * _velocityImpulse.y - rAY * _velocityImpulse.x + _velocityImpulse.z);
	
			m_bodyB.m_linearVelocity.x += m_bodyB.m_invMass * _velocityImpulse.x;
			m_bodyB.m_linearVelocity.y += m_bodyB.m_invMass * _velocityImpulse.y;

			m_bodyB.m_angularVelocity += m_bodyB.m_invI * (rBX * _velocityImpulse.y - rBY * _velocityImpulse.x + _velocityImpulse.z);

			m_bodyA.m_angularVelocity = m_bodyA.m_angularVelocity;
			m_bodyB.m_angularVelocity = m_bodyB.m_angularVelocity;
		}
		
		
		private var tMat:b2Mat22;
		private var tX:Number;

		private var rAX:Number;
		private var rAY:Number;
		
		private var rBX:Number;
		private var rBY:Number;
		
		private var mA:Number;
		private var mB:Number;
		private var iA:Number;
		private var iB:Number;
		
		private var C1X:Number;
		private var C1Y:Number;
		private var C2:Number;
		
		// Handle large detachment.
		private var k_allowedStretch:Number;
		private var positionError:Number;
		private var angularError:Number;
		
		private var _positionImpulse:b2Vec3 = new b2Vec3(0, 0, 0);

		private var wA:Number;
		private var wB:Number;
		
		private var Cdot1X:Number;
		private var Cdot1Y:Number;
		private var Cdot2:Number;

		
		[Inline]
		final public override function SolvePositionConstraints(baumgarte:Number):Boolean
		{
			tMat = m_bodyA.m_xf.R;
			
			rAX = m_localAnchorA.x - m_bodyA.m_sweep.localCenter.x;
			rAY = m_localAnchorA.y - m_bodyA.m_sweep.localCenter.y;
			
			tX =  (tMat.col1.x * rAX + tMat.col2.x * rAY);
			rAY = (tMat.col1.y * rAX + tMat.col2.y * rAY);
			rAX = tX;

			tMat = m_bodyB.m_xf.R;
			rBX = m_localAnchorB.x - m_bodyB.m_sweep.localCenter.x;
			rBY = m_localAnchorB.y - m_bodyB.m_sweep.localCenter.y;
			tX =  (tMat.col1.x * rBX + tMat.col2.x * rBY);
			rBY = (tMat.col1.y * rBX + tMat.col2.y * rBY);
			rBX = tX;

			mA = m_bodyA.m_invMass
			mB = m_bodyB.m_invMass;
			iA = m_bodyA.m_invI
			iB = m_bodyB.m_invI;

			C1X =  m_bodyB.m_sweep.c.x + rBX - m_bodyA.m_sweep.c.x - rAX;
			C1Y =  m_bodyB.m_sweep.c.y + rBY - m_bodyA.m_sweep.c.y - rAY;
			C2 = m_bodyB.m_sweep.a - m_bodyA.m_sweep.a - m_referenceAngle;
	
			// Handle large detachment.
			k_allowedStretch = 10.0 * b2Settings.b2_linearSlop;
			positionError = Math.sqrt(C1X * C1X + C1Y * C1Y);
			angularError = b2Math.Abs(C2);
	
			if(positionError > k_allowedStretch)
			{
				iA *= 1.0;
				iB *= 1.0;
			}
			
			m_mass.col1.x = mA + mB + rAY * rAY * iA + rBY * rBY * iB;
			m_mass.col2.x = -rAY * rAX * iA - rBY * rBX * iB;
			m_mass.col3.x = -rAY * iA - rBY * iB;
			m_mass.col1.y = m_mass.col2.x;
			m_mass.col2.y = mA + mB + rAX * rAX * iA + rBX * rBX * iB;
			m_mass.col3.y = rAX * iA + rBX * iB;
			m_mass.col1.z = m_mass.col3.x;
			m_mass.col2.z = m_mass.col3.y;
			m_mass.col3.z = iA + iB;
			
			m_mass.Solve33(_positionImpulse, -C1X, -C1Y, -C2);
			
	
			m_bodyA.m_sweep.c.x -= mA * _positionImpulse.x;
			m_bodyA.m_sweep.c.y -= mA * _positionImpulse.y;
			m_bodyA.m_sweep.a -= iA * (rAX * _positionImpulse.y - rAY * _positionImpulse.x + _positionImpulse.z);
	
			m_bodyB.m_sweep.c.x += mB * _positionImpulse.x;
			m_bodyB.m_sweep.c.y += mB * _positionImpulse.y;
			m_bodyB.m_sweep.a += iB * (rBX * _positionImpulse.y - rBY * _positionImpulse.x + _positionImpulse.z);
	
			m_bodyA.SynchronizeTransform();
			m_bodyB.SynchronizeTransform();
	
			return positionError <= b2Settings.b2_linearSlop && angularError <= b2Settings.b2_angularSlop;
	
		}
	
		private var m_localAnchorA:b2Vec2 = new b2Vec2();
		private var m_localAnchorB:b2Vec2 = new b2Vec2();
		private var m_referenceAngle:Number;
		
		private var m_impulse:b2Vec3 = new b2Vec3();
		private var m_mass:b2Mat33 = new b2Mat33();
	};
}
