﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Dynamics.Joints
{
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Mat22;
	import Box2D.Common.Math.b2Mat33;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Common.Math.b2Vec3;
	import Box2D.Dynamics.b2TimeStep;

	
	// Point-to-point constraint
	// C = p2 - p1
	// Cdot = v2 - v1
	//      = v2 + cross(w2, r2) - v1 - cross(w1, r1)
	// J = [-I -r1_skew I r2_skew ]
	// Identity used:
	// w k % (rx i + ry j) = w * (-ry i + rx j)
	
	// Motor constraint
	// Cdot = w2 - w1
	// J = [0 0 -1 0 0 1]
	// K = invI1 + invI2
	
	/**
	* A revolute joint constrains to bodies to share a common point while they
	* are free to rotate about the point. The relative rotation about the shared
	* point is the joint angle. You can limit the relative rotation with
	* a joint limit that specifies a lower and upper angle. You can use a motor
	* to drive the relative rotation about the shared point. A maximum motor torque
	* is provided so that infinite forces are not generated.
	* @see b2RevoluteJointDef
	*/
	public class b2RevoluteJoint extends b2Joint
	{
		/** @inheritDoc */
		public override function GetAnchorA(result:b2Vec2):void
		{
			return m_bodyA.GetWorldPoint(m_localAnchor1, result);
		}
		
		/** @inheritDoc */
		public override function GetAnchorB(result:b2Vec2):void
		{
			return m_bodyB.GetWorldPoint(m_localAnchor2, result);
		}
	
		/** @inheritDoc */
		public override function GetReactionForce(inv_dt:Number, result:b2Vec2):void
		{
			result.x = inv_dt * m_impulse.x;
			result.y = inv_dt * m_impulse.y;
		}

		/** @inheritDoc */
		public override function GetReactionTorque(inv_dt:Number) :Number{
			return inv_dt * m_impulse.z;
		}
	
		/**
		* Get the current joint angle in radians.
		*/
		[Inline]
		final public function GetJointAngle():Number
		{
			//b2Body* bA = m_bodyA;
			//b2Body* bB = m_bodyB;
			return m_bodyB.m_sweep.a - m_bodyA.m_sweep.a - m_referenceAngle;
		}
	
		/**
		* Get the current joint angle speed in radians per second.
		*/
		[Inline]
		final public function GetJointSpeed():Number
		{
			//b2Body* bA = m_bodyA;
			//b2Body* bB = m_bodyB;
			return m_bodyB.m_angularVelocity - m_bodyA.m_angularVelocity;
		}
	
		/**
		* Is the joint limit enabled?
		*/
		public function IsLimitEnabled() :Boolean{
			return m_enableLimit;
		}
	
		/**
		* Enable/disable the joint limit.
		*/
		public function EnableLimit(flag:Boolean) :void{
			m_enableLimit = flag;
		}
	
		/**
		* Get the lower joint limit in radians.
		*/
		[Inline]
		final public function GetLowerLimit():Number
		{
			return m_lowerAngle;
		}
	
		/**
		* Get the upper joint limit in radians.
		*/
		[Inline]
		final public function GetUpperLimit():Number
		{
			return m_upperAngle;
		}
	
		/**
		* Set the joint limits in radians.
		*/
		public function SetLimits(lower:Number, upper:Number) : void{
			//b2Settings.b2Assert(lower <= upper);
			m_lowerAngle = lower;
			m_upperAngle = upper;
		}
	
		/**
		* Is the joint motor enabled?
		*/
		public function IsMotorEnabled() :Boolean {
			m_bodyA.SetAwake(true);
			m_bodyB.SetAwake(true);
			return m_enableMotor;
		}
	
		/**
		* Enable/disable the joint motor.
		*/
		public function EnableMotor(flag:Boolean) :void{
			m_enableMotor = flag;
		}
	
		/**
		* Set the motor speed in radians per second.
		*/
		[Inline]
		final public function SetMotorSpeed(speed:Number):void
		{
			m_bodyA.SetAwake(true);
			m_bodyB.SetAwake(true);
			m_motorSpeed = speed;
		}
	
		/**
		* Get the motor speed in radians per second.
		*/
		[Inline]
		final public function GetMotorSpeed():Number
		{
			return m_motorSpeed;
		}
	
		/**
		* Set the maximum motor torque, usually in N-m.
		*/
		[Inline]
		final public function SetMaxMotorTorque(torque:Number):void
		{
			m_maxMotorTorque = torque;
		}
	
		public function GetMaxMotorTorque():Number
		{
			return m_maxMotorTorque;
		}
		
		/**
		* Get the current motor torque, usually in N-m.
		*/
		public function GetMotorTorque():Number
		{
			return m_maxMotorTorque;
		}
	
		//--------------- Internals Below -------------------
	
		/** @private */
		public function b2RevoluteJoint(def:b2RevoluteJointDef)
		{
			super(def);
			
			//m_localAnchor1 = def->localAnchorA;
			m_localAnchor1.SetV(def.localAnchorA);
			//m_localAnchor2 = def->localAnchorB;
			m_localAnchor2.SetV(def.localAnchorB);
			
			m_referenceAngle = def.referenceAngle;
			
			m_impulse.SetZero();
			m_motorImpulse = 0.0;
			
			m_lowerAngle = def.lowerAngle;
			m_upperAngle = def.upperAngle;
			m_maxMotorTorque = def.maxMotorTorque;
			m_motorSpeed = def.motorSpeed;
			m_enableLimit = def.enableLimit;
			m_enableMotor = def.enableMotor;
			m_limitState = e_inactiveLimit;
		}
	
		// internal vars
		private var K:b2Mat22 = new b2Mat22();
		private var K1:b2Mat22 = new b2Mat22();
		private var K2:b2Mat22 = new b2Mat22();
		private var K3:b2Mat22 = new b2Mat22();
		
		private var tMat:b2Mat22;
		private var tX:Number;
		
		private var r1X:Number;
		private var r1Y:Number;
		
		private var r2X:Number;
		private var r2Y:Number;

		private var jointAngle:Number;
		
		private var PX:Number;
		private var PY:Number;
		
		private var newImpulse:Number;
		
		private var v1:b2Vec2;
		private var v2:b2Vec2;
		
		private var w1:Number;
		private var w2:Number;
		
		private var Cdot:Number;
		private var impulse:Number;
		private var oldImpulse:Number;
		private var maxImpulse:Number;
		
		private var Cdot1X:Number;
		private var Cdot1Y:Number;
		private var Cdot2:Number;
		
		private var CdotX:Number;
		private var CdotY:Number;
		
		private var angularError:Number;
		private var positionError:Number;
		
		private var oldLimitImpulse:Number;
		private var C:Number;
		
		private var impulseX:Number;
		private var impulseY:Number;
		
		private var angle:Number;
		private var limitImpulse:Number;
		
		private var CX:Number;
		private var CY:Number;
		private var CLengthSquared:Number;
		private var CLength:Number;
		
		private var invMass1:Number;
		private var invMass2:Number;
		private var invI1:Number;
		private var invI2:Number;
		
		private var uX:Number;
		private var uY:Number;
		private var k:Number;
		private var m:Number;
		
		
		public override function InitVelocityConstraints(step:b2TimeStep):void
		{
			tMat = m_bodyA.m_xf.R;
			r1X = m_localAnchor1.x - m_bodyA.m_sweep.localCenter.x;
			r1Y = m_localAnchor1.y - m_bodyA.m_sweep.localCenter.y;
			
			tX =  (tMat.col1.x * r1X + tMat.col2.x * r1Y);
			r1Y = (tMat.col1.y * r1X + tMat.col2.y * r1Y);
			r1X = tX;

			tMat = m_bodyB.m_xf.R;
			
			r2X = m_localAnchor2.x - m_bodyB.m_sweep.localCenter.x;
			r2Y = m_localAnchor2.y - m_bodyB.m_sweep.localCenter.y;
			
			tX =  (tMat.col1.x * r2X + tMat.col2.x * r2Y);
			r2Y = (tMat.col1.y * r2X + tMat.col2.y * r2Y);
			r2X = tX;

			m_mass.col1.x = m_bodyA.m_invMass + m_bodyB.m_invMass + r1Y * r1Y * m_bodyA.m_invI + r2Y * r2Y * m_bodyB.m_invI;
			m_mass.col2.x = -r1Y * r1X * m_bodyA.m_invI - r2Y * r2X * m_bodyB.m_invI;
			m_mass.col3.x = -r1Y * m_bodyA.m_invI - r2Y * m_bodyB.m_invI;
			m_mass.col1.y = m_mass.col2.x;
			m_mass.col2.y = m_bodyA.m_invMass + m_bodyB.m_invMass + r1X * r1X * m_bodyA.m_invI + r2X * r2X * m_bodyB.m_invI;
			m_mass.col3.y = r1X * m_bodyA.m_invI + r2X * m_bodyB.m_invI;
			m_mass.col1.z = m_mass.col3.x;
			m_mass.col2.z = m_mass.col3.y;
			m_mass.col3.z = m_bodyA.m_invI + m_bodyB.m_invI;

			m_motorMass = m_bodyA.m_invI + m_bodyB.m_invI;

			if (m_motorMass > 0)
			{
				m_motorMass = b2Settings.DECIMAL_ONE / m_motorMass;
			}

			if (m_enableMotor == false)
			{
				m_motorImpulse = b2Settings.DECIMAL_ZERO;
			}
			
			if (m_enableLimit)
			{
				jointAngle = m_bodyB.m_sweep.a - m_bodyA.m_sweep.a - m_referenceAngle;
				
				if (b2Math.Abs(m_upperAngle - m_lowerAngle) < 2.0 * b2Settings.b2_angularSlop)
				{
					m_limitState = e_equalLimits;
				}
				else if (jointAngle <= m_lowerAngle)
				{
					if (m_limitState != e_atLowerLimit)
					{
						m_impulse.z = b2Settings.DECIMAL_ZERO;
					}
					m_limitState = e_atLowerLimit;
				}
				else if (jointAngle >= m_upperAngle)
				{
					if (m_limitState != e_atUpperLimit)
					{
						m_impulse.z = b2Settings.DECIMAL_ZERO;
					}
					m_limitState = e_atUpperLimit;
				}
				else
				{
					m_limitState = e_inactiveLimit;
					m_impulse.z = b2Settings.DECIMAL_ZERO;
				}
			}
			else
			{
				m_limitState = e_inactiveLimit;
			}
			
			// Warm starting.
			if (step.warmStarting)
			{
				//Scale impulses to support a variable time step
				m_impulse.x *= step.dtRatio;
				m_impulse.y *= step.dtRatio;
				m_motorImpulse *= step.dtRatio;

				PX = m_impulse.x;
				PY = m_impulse.y;

				//bA->m_linearVelocity -= m1 * P;
				m_bodyA.m_linearVelocity.x -= m_bodyA.m_invMass * PX;
				m_bodyA.m_linearVelocity.y -= m_bodyA.m_invMass * PY;

				m_bodyA.m_angularVelocity -= m_bodyA.m_invI * ((r1X * PY - r1Y * PX) + m_motorImpulse + m_impulse.z);
				
				//bB->m_linearVelocity += m2 * P;
				m_bodyB.m_linearVelocity.x += m_bodyB.m_invMass * PX;
				m_bodyB.m_linearVelocity.y += m_bodyB.m_invMass * PY;

				m_bodyB.m_angularVelocity += m_bodyB.m_invI * ((r2X * PY - r2Y * PX) + m_motorImpulse + m_impulse.z);
			}
			else
			{
				m_impulse.x = b2Settings.DECIMAL_ZERO;
				m_impulse.y = b2Settings.DECIMAL_ZERO;
				m_impulse.z = b2Settings.DECIMAL_ZERO;
				
				m_motorImpulse = b2Settings.DECIMAL_ZERO;
			}
		}
		
		private var impulse3:b2Vec3 = new b2Vec3();
		private var impulse2:b2Vec2 = new b2Vec2();
		private var reduced:b2Vec2 = new b2Vec2();
	
		private var m22Det:Number = 0;
		
		private var m22bX:Number = 0;
		private var m22bY:Number = 0;
		
		
		[Inline]
		final public override function SolveVelocityConstraints(step:b2TimeStep):void
		{
			v1 = m_bodyA.m_linearVelocity;
			v2 = m_bodyB.m_linearVelocity;
			
			w1 = m_bodyA.m_angularVelocity;
			w2 = m_bodyB.m_angularVelocity;

			// Solve motor constraint.
			if(m_enableMotor && m_limitState != e_equalLimits)
			{
				Cdot = w2 - w1 - m_motorSpeed;
				impulse = m_motorMass * (-Cdot);
				oldImpulse = m_motorImpulse;
				maxImpulse = step.dt * m_maxMotorTorque;
				
				m_motorImpulse = b2Math.Clamp(m_motorImpulse + impulse, -maxImpulse, maxImpulse);
				impulse = m_motorImpulse - oldImpulse;
				
				w1 -= m_bodyA.m_invI * impulse;
				w2 += m_bodyB.m_invI * impulse;
			}
			
			// Solve limit constraint.
			if (m_enableLimit && m_limitState != e_inactiveLimit)
			{
				tMat = m_bodyA.m_xf.R;
				r1X = m_localAnchor1.x - m_bodyA.m_sweep.localCenter.x;
				r1Y = m_localAnchor1.y - m_bodyA.m_sweep.localCenter.y;
				tX =  tMat.col1.x * r1X + tMat.col2.x * r1Y;
				r1Y = tMat.col1.y * r1X + tMat.col2.y * r1Y;
				r1X = tX;

				tMat = m_bodyB.m_xf.R;
				r2X = m_localAnchor2.x - m_bodyB.m_sweep.localCenter.x;
				r2Y = m_localAnchor2.y - m_bodyB.m_sweep.localCenter.y;
				tX =  tMat.col1.x * r2X + tMat.col2.x * r2Y;
				r2Y = tMat.col1.y * r2X + tMat.col2.y * r2Y;
				r2X = tX;

				Cdot1X = v2.x + (-w2 * r2Y) - v1.x - (-w1 * r1Y);
				Cdot1Y = v2.y + (w2 * r2X) - v1.y - (w1 * r1X);
				Cdot2  = w2 - w1;
				
				m_mass.Solve33(impulse3, -Cdot1X, -Cdot1Y, -Cdot2);
				
				if (m_limitState == e_equalLimits)
				{
					m_impulse.x += impulse3.x;
					m_impulse.y += impulse3.y;
					m_impulse.z += impulse3.z;	
				}
				else if (m_limitState == e_atLowerLimit)
				{
					newImpulse = m_impulse.z + impulse3.z;

					if (newImpulse < b2Settings.DECIMAL_ZERO)
					{
						//m_mass.Solve22(reduced, -Cdot1X, -Cdot1Y); 
						
						
						// solving b2Mat33.Solve22 here to avoid function call
						m22bX = -Cdot1X;
						m22bY = -Cdot1Y;
						
						m22Det = m_mass.col1.x * m_mass.col2.y - m_mass.col2.x * m_mass.col1.y;
						
						if(m22Det != b2Settings.DECIMAL_ZERO)
						{
							m22Det = b2Settings.DECIMAL_ONE / m22Det;
						}
						
						reduced.x = m22Det * (m_mass.col2.y * m22bX - m_mass.col2.x * m22bY);
						reduced.y = m22Det * (m_mass.col1.x * m22bY - m_mass.col1.y * m22bX);
						
						// end of m_mass.Solve22(reduced, -Cdot1X, -Cdot1Y);
						

						impulse3.x = reduced.x;
						impulse3.y = reduced.y;
						impulse3.z = -m_impulse.z;
						m_impulse.x += reduced.x;
						m_impulse.y += reduced.y;
						m_impulse.z = b2Settings.DECIMAL_ZERO;
					}
				}
				else if (m_limitState == e_atUpperLimit)
				{
					newImpulse = m_impulse.z + impulse3.z;

					if (newImpulse > b2Settings.DECIMAL_ZERO)
					{
						
						//m_mass.Solve22(reduced, -Cdot1X, -Cdot1Y);
						

						// solving b2Mat33.Solve22 here to avoid function call
						m22bX = -Cdot1X;
						m22bY = -Cdot1Y;
						
						m22Det = m_mass.col1.x * m_mass.col2.y - m_mass.col2.x * m_mass.col1.y;
						
						if(m22Det != b2Settings.DECIMAL_ZERO)
						{
							m22Det = b2Settings.DECIMAL_ONE / m22Det;
						}
						
						reduced.x = m22Det * (m_mass.col2.y * m22bX - m_mass.col2.x * m22bY);
						reduced.y = m22Det * (m_mass.col1.x * m22bY - m_mass.col1.y * m22bX);
						
						// end of m_mass.Solve22(reduced, -Cdot1X, -Cdot1Y);
						

						impulse3.x = reduced.x;
						impulse3.y = reduced.y;
						impulse3.z = -m_impulse.z;
						m_impulse.x += reduced.x;
						m_impulse.y += reduced.y;
						m_impulse.z = b2Settings.DECIMAL_ZERO;
					}
				}
				
				v1.x -= m_bodyA.m_invMass * impulse3.x;
				v1.y -= m_bodyA.m_invMass * impulse3.y;
				w1 -= m_bodyA.m_invI * (r1X * impulse3.y - r1Y * impulse3.x + impulse3.z);
				
				v2.x += m_bodyB.m_invMass * impulse3.x;
				v2.y += m_bodyB.m_invMass * impulse3.y;
				w2 += m_bodyB.m_invI * (r2X * impulse3.y - r2Y * impulse3.x + impulse3.z);
			}
			else
			{
				//b2Vec2 r1 = b2Mul(bA->m_xf.R, m_localAnchor1 - bA->GetLocalCenter());
				tMat = m_bodyA.m_xf.R;
				r1X = m_localAnchor1.x - m_bodyA.m_sweep.localCenter.x;
				r1Y = m_localAnchor1.y - m_bodyA.m_sweep.localCenter.y;
				tX =  (tMat.col1.x * r1X + tMat.col2.x * r1Y);
				r1Y = (tMat.col1.y * r1X + tMat.col2.y * r1Y);
				r1X = tX;
				
				//b2Vec2 r2 = b2Mul(bB->m_xf.R, m_localAnchor2 - bB->GetLocalCenter());
				tMat = m_bodyB.m_xf.R;

				r2X = m_localAnchor2.x - m_bodyB.m_sweep.localCenter.x;
				r2Y = m_localAnchor2.y - m_bodyB.m_sweep.localCenter.y;
				
				tX =  (tMat.col1.x * r2X + tMat.col2.x * r2Y);
				r2Y = (tMat.col1.y * r2X + tMat.col2.y * r2Y);

				r2X = tX;

				CdotX = v2.x + (-w2 * r2Y) - v1.x - (-w1 * r1Y);
				CdotY = v2.y + (w2 * r2X) - v1.y - (w1 * r1X);

				
				
				//m_mass.Solve22(impulse2, -CdotX, -CdotY);				
				m22bX = -CdotX;
				m22bY = -CdotY;
				
				m22Det = m_mass.col1.x * m_mass.col2.y - m_mass.col2.x * m_mass.col1.y;
				
				if(m22Det != b2Settings.DECIMAL_ZERO)
				{
					m22Det = b2Settings.DECIMAL_ONE / m22Det;
				}
				
				impulse2.x = m22Det * (m_mass.col2.y * m22bX - m_mass.col2.x * m22bY);
				impulse2.y = m22Det * (m_mass.col1.x * m22bY - m_mass.col1.y * m22bX);
				
				// end of m_mass.Solve22(impulse2, -CdotX, -CdotY);
				
				
				m_impulse.x += impulse2.x;
				m_impulse.y += impulse2.y;
				
				v1.x -= m_bodyA.m_invMass * impulse2.x;
				v1.y -= m_bodyA.m_invMass * impulse2.y;

				//w1 -= i1 * b2Cross(r1, impulse2); 
				w1 -= m_bodyA.m_invI * (r1X * impulse2.y - r1Y * impulse2.x);
				
				v2.x += m_bodyB.m_invMass * impulse2.x;
				v2.y += m_bodyB.m_invMass * impulse2.y;

				//w2 += i2 * b2Cross(r2, impulse2); 
				w2 += m_bodyB.m_invI * (r2X * impulse2.y - r2Y * impulse2.x);
			}

			m_bodyA.m_linearVelocity.x = v1.x;
			m_bodyA.m_linearVelocity.y = v1.y;
			
			m_bodyA.m_angularVelocity = w1;

			m_bodyB.m_linearVelocity.x = v2.x;
			m_bodyB.m_linearVelocity.y = v2.y;
			
			m_bodyB.m_angularVelocity = w2;
		}
		
		private static var tImpulse:b2Vec2 = new b2Vec2();
		
		[Inline]
		final public override function SolvePositionConstraints(baumgarte:Number):Boolean
		{
			angularError = b2Settings.DECIMAL_ZERO;
			positionError = b2Settings.DECIMAL_ZERO;
			
			// Solve angular limit constraint.
			if (m_enableLimit && m_limitState != e_inactiveLimit)
			{
				angle = m_bodyB.m_sweep.a - m_bodyA.m_sweep.a - m_referenceAngle;
				limitImpulse = b2Settings.DECIMAL_ZERO;
				
				if (m_limitState == e_equalLimits)
				{
					// Prevent large angular corrections
					C = b2Math.Clamp(angle - m_lowerAngle, -b2Settings.b2_maxAngularCorrection, b2Settings.b2_maxAngularCorrection);
					limitImpulse = -m_motorMass * C;
					angularError = b2Math.Abs(C);
				}
				else if (m_limitState == e_atLowerLimit)
				{
					C = angle - m_lowerAngle;
					angularError = -C;
					
					// Prevent large angular corrections and allow some slop.
					C = b2Math.Clamp(C + b2Settings.b2_angularSlop, -b2Settings.b2_maxAngularCorrection, b2Settings.DECIMAL_ZERO);
					limitImpulse = -m_motorMass * C;
				}
				else if (m_limitState == e_atUpperLimit)
				{
					C = angle - m_upperAngle;
					angularError = C;
					
					// Prevent large angular corrections and allow some slop.
					C = b2Math.Clamp(C - b2Settings.b2_angularSlop, b2Settings.DECIMAL_ZERO, b2Settings.b2_maxAngularCorrection);
					limitImpulse = -m_motorMass * C;
				}
				
				m_bodyA.m_sweep.a -= m_bodyA.m_invI * limitImpulse;
				m_bodyB.m_sweep.a += m_bodyB.m_invI * limitImpulse;
				
				m_bodyA.SynchronizeTransform();
				m_bodyB.SynchronizeTransform();
			}
			
			// Solve point-to-point constraint
			{
				//b2Vec2 r1 = b2Mul(bA->m_xf.R, m_localAnchor1 - bA->GetLocalCenter());
				tMat = m_bodyA.m_xf.R;
				r1X = m_localAnchor1.x - m_bodyA.m_sweep.localCenter.x;
				r1Y = m_localAnchor1.y - m_bodyA.m_sweep.localCenter.y;
				
				tX =  (tMat.col1.x * r1X + tMat.col2.x * r1Y);
				r1Y = (tMat.col1.y * r1X + tMat.col2.y * r1Y);
				
				r1X = tX;
				
				tMat = m_bodyB.m_xf.R;
				r2X = m_localAnchor2.x - m_bodyB.m_sweep.localCenter.x;
				r2Y = m_localAnchor2.y - m_bodyB.m_sweep.localCenter.y;
				tX =  (tMat.col1.x * r2X + tMat.col2.x * r2Y);
				r2Y = (tMat.col1.y * r2X + tMat.col2.y * r2Y);
				r2X = tX;
				
				//b2Vec2 C = bB->m_sweep.c + r2 - bA->m_sweep.c - r1;
				CX = m_bodyB.m_sweep.c.x + r2X - m_bodyA.m_sweep.c.x - r1X;
				CY = m_bodyB.m_sweep.c.y + r2Y - m_bodyA.m_sweep.c.y - r1Y;

				CLengthSquared = CX * CX + CY * CY;
				CLength = Math.sqrt(CLengthSquared);

				positionError = CLength;
				
				invMass1 = m_bodyA.m_invMass;
				invMass2 = m_bodyB.m_invMass;
				invI1 = m_bodyA.m_invI;
				invI2 = m_bodyB.m_invI;

				if (CLengthSquared > k_allowedStretch * k_allowedStretch)
				{
					uX = CX / CLength;
					uY = CY / CLength;
					m = invMass1 + invMass2;

					if (m > 0)
					{
						m = b2Settings.DECIMAL_ONE / m;
					}

					impulseX = m * (-CX);
					impulseY = m * (-CY);

					m_bodyA.m_sweep.c.x -= k_beta * invMass1 * impulseX;
					m_bodyA.m_sweep.c.y -= k_beta * invMass1 * impulseY;
					m_bodyB.m_sweep.c.x += k_beta * invMass2 * impulseX;
					m_bodyB.m_sweep.c.y += k_beta * invMass2 * impulseY;
					
					//C = bB->m_sweep.c + r2 - bA->m_sweep.c - r1;
					CX = m_bodyB.m_sweep.c.x + r2X - m_bodyA.m_sweep.c.x - r1X;
					CY = m_bodyB.m_sweep.c.y + r2Y - m_bodyA.m_sweep.c.y - r1Y;
				}
				
				//b2Mat22 K1;
				K1.col1.x = invMass1 + invMass2;	K1.col2.x = b2Settings.DECIMAL_ZERO;
				K1.col1.y = b2Settings.DECIMAL_ZERO;					K1.col2.y = invMass1 + invMass2;
				
				//b2Mat22 K2;
				K2.col1.x =  invI1 * r1Y * r1Y;	K2.col2.x = -invI1 * r1X * r1Y;
				K2.col1.y = -invI1 * r1X * r1Y;	K2.col2.y =  invI1 * r1X * r1X;
				
				//b2Mat22 K3;
				K3.col1.x =  invI2 * r2Y * r2Y;		K3.col2.x = -invI2 * r2X * r2Y;
				K3.col1.y = -invI2 * r2X * r2Y;		K3.col2.y =  invI2 * r2X * r2X;
				
				//b2Mat22 K = K1 + K2 + K3;
				K.SetM(K1);
				K.AddM(K2);
				K.AddM(K3);
				//b2Vec2 impulse = K.Solve(-C);
				K.Solve(tImpulse, -CX, -CY);
				impulseX = tImpulse.x;
				impulseY = tImpulse.y;
				
				//bA.m_sweep.c -= bA.m_invMass * impulse;
				m_bodyA.m_sweep.c.x -= m_bodyA.m_invMass * impulseX;
				m_bodyA.m_sweep.c.y -= m_bodyA.m_invMass * impulseY;
				//bA.m_sweep.a -= bA.m_invI * b2Cross(r1, impulse);
				m_bodyA.m_sweep.a -= m_bodyA.m_invI * (r1X * impulseY - r1Y * impulseX);
				
				//bB.m_sweep.c += bB.m_invMass * impulse;
				m_bodyB.m_sweep.c.x += m_bodyB.m_invMass * impulseX;
				m_bodyB.m_sweep.c.y += m_bodyB.m_invMass * impulseY;
				//bB.m_sweep.a += bB.m_invI * b2Cross(r2, impulse);
				m_bodyB.m_sweep.a += m_bodyB.m_invI * (r2X * impulseY - r2Y * impulseX);
				
				m_bodyA.SynchronizeTransform();
				m_bodyB.SynchronizeTransform();
			}
	
			return positionError <= b2Settings.b2_linearSlop && angularError <= b2Settings.b2_angularSlop;
		}

		private static const k_beta:Number = 0.5;
		private static const k_allowedStretch:Number = 10.0 * b2Settings.b2_linearSlop;
		
		public var m_localAnchor1:b2Vec2 = new b2Vec2(); // relative
		public var m_localAnchor2:b2Vec2 = new b2Vec2();

		private var m_impulse:b2Vec3 = new b2Vec3();
		private var m_motorImpulse:Number;
	
		private var m_mass:b2Mat33 = new b2Mat33();		// effective mass for point-to-point constraint.
		private var m_motorMass:Number;	// effective mass for motor/limit angular constraint.
		public var m_enableMotor:Boolean;

		public var m_maxMotorTorque:Number;
		public var m_motorSpeed:Number;
	
		public var m_enableLimit:Boolean;
		private var m_referenceAngle:Number;

		public var m_lowerAngle:Number;
		public var m_upperAngle:Number;
		
		private var m_limitState:uint;
	};
}
