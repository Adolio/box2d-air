/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

//Ported to AS3 by Allan Bishop http://allanbishop.com
//Version 1.01
//
//Changes
//---------
//*Fixed bug when anchor is not set to centre and gravity is set to 0

package Box2D.Dynamics.Joints
{
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Mat22;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2TimeStep;
	
	/// A rope joint enforces a maximum distance between two points
	/// on two bodies. It has no other effect.
	/// Warning: if you attempt to change the maximum length during
	/// the simulation you will get some non-physical behavior.
	/// A model that would allow you to dynamically modify the length
	/// would have some sponginess, so I chose not to implement it
	/// that way. See b2DistanceJoint if you want to dynamically
	/// control length.
	
	// Limit:
	// C = norm(pB - pA) - L
	// u = (pB - pA) / norm(pB - pA)
	// Cdot = dot(u, vB + cross(wB, rB) - vA - cross(wA, rA))
	// J = [-u -cross(rA, u) u cross(rB, u)]
	// K = J * invM * JT
	//   = invMassA + invIA * cross(rA, u)^2 + invMassB + invIB * cross(rB, u)^2
	
	
	public class b2RopeJoint extends b2Joint
	{
		/** @inheritDoc */
		public override function GetAnchorA(result:b2Vec2):void
		{
			m_bodyA.GetWorldPoint(m_localAnchor1, result);
		}
		
		/** @inheritDoc */
		public override function GetAnchorB(result:b2Vec2):void
		{
			m_bodyB.GetWorldPoint(m_localAnchor2, result);
		}
		
		/** @inheritDoc */
		public override function GetReactionForce(inv_dt:Number, result:b2Vec2):void
		{
			//b2Vec2 F = (m_inv_dt * m_impulse) * m_u;
			//return F;
			result.x = inv_dt * m_impulse * m_u.x;
			result.y = inv_dt * m_impulse * m_u.y;
		}
	
		/** @inheritDoc */
		[Inline]
		final public override function GetReactionTorque(inv_dt:Number):Number
		{
			//B2_NOT_USED(inv_dt);
			return 0.0;
		}
		
		/// Set the natural length
		[Inline]
		final public function GetMaxLength():Number
		{
			return m_maxLength;
		}
		
		[Inline]
		final public function GetLimitState():int
		{
			return m_state;
		}
		
		//--------------- Internals Below -------------------
	
		/** @private */
		public function b2RopeJoint(def:b2RopeJointDef)
		{
			super(def);
			
			var tMat:b2Mat22;
			var tX:Number;
			var tY:Number;
			m_localAnchor1.SetV(def.localAnchorA);
			m_localAnchor2.SetV(def.localAnchorB);
			
			m_length = 0;
			m_mass = 0;
			m_maxLength = def.maxLength;
			m_impulse = 0.0;
			m_state = e_inactiveLimit;
		}
	
		private var bA:b2Body;
		private var bB:b2Body;
		
		private var tMat:b2Mat22;
		private var tX:Number;
		
		private var r1X:Number;
		private var r1Y:Number;
		
		private var r2X:Number;
		private var r2Y:Number;			
		private var c:Number;
		
		private var crA:Number;
		private var crB:Number;
		
		private var invMass:Number;
		private var PX:Number;
		private var PY:Number;

		private var v1X:Number;
		private var v1Y:Number;
		
		private var v2X:Number;
		private var v2Y:Number;
		
		private var C:Number;
		private var Cdot:Number;
		
		private var impulse:Number;
		private var oldImpulse:Number;
		
		private var dX:Number;
		private var dY:Number;
		private var length:Number;
		
		
		public override function InitVelocityConstraints(step:b2TimeStep):void
		{
			
			bA = m_bodyA;
			bB = m_bodyB;				
				
			tMat = bA.m_xf.R;
			r1X = m_localAnchor1.x - bA.m_sweep.localCenter.x;
			r1Y = m_localAnchor1.y - bA.m_sweep.localCenter.y;
			tX =  (tMat.col1.x * r1X + tMat.col2.x * r1Y);
			r1Y = (tMat.col1.y * r1X + tMat.col2.y * r1Y);
			r1X = tX;
			
			tMat = bB.m_xf.R;
			
			r2X = m_localAnchor2.x - bB.m_sweep.localCenter.x;
			r2Y = m_localAnchor2.y - bB.m_sweep.localCenter.y;
			
			tX =  (tMat.col1.x * r2X + tMat.col2.x * r2Y);
			r2Y = (tMat.col1.y * r2X + tMat.col2.y * r2Y);
			r2X = tX;
			
			//m_u = bB->m_sweep.c + r2 - bA->m_sweep.c - r1;
			m_u.x = bB.m_sweep.c.x + r2X - bA.m_sweep.c.x - r1X;
			m_u.y = bB.m_sweep.c.y + r2Y - bA.m_sweep.c.y - r1Y;
			
			m_length = Math.sqrt(m_u.x*m_u.x + m_u.y*m_u.y);
			
			c = m_length-m_maxLength;
			
			if(c>0)
			{
				 m_state = e_atUpperLimit;
			}
			else
			{
				m_state = e_inactiveLimit;
			}
			
			if(m_length > b2Settings.b2_linearSlop)
			{
				m_u.Multiply( 1.0 / m_length );
			}
			else
			{
				m_u.SetZero();
				m_mass = 0.0;
				m_impulse = 0.0;
				return;
			}

			crA = (r1X * m_u.y - r1Y * m_u.x);
			crB = (r2X * m_u.y - r2Y * m_u.x);
			
			invMass = bA.m_invMass + bA.m_invI * crA * crA + bB.m_invMass + bB.m_invI * crB * crB;
			m_mass = invMass != 0.0 ? 1.0 / invMass : 0.0;
			
			if (step.warmStarting)
			{
				// Scale the impulse to support a variable time step
				m_impulse *= step.dtRatio;
				
				//b2Vec2 P = m_impulse * m_u;
				PX = m_impulse * m_u.x;
				PY = m_impulse * m_u.y;
				//bA->m_linearVelocity -= bA->m_invMass * P;
				bA.m_linearVelocity.x -= bA.m_invMass * PX;
				bA.m_linearVelocity.y -= bA.m_invMass * PY;
				//bA->m_angularVelocity -= bA->m_invI * b2Cross(r1, P);
				bA.m_angularVelocity -= bA.m_invI * (r1X * PY - r1Y * PX);
				//bB->m_linearVelocity += bB->m_invMass * P;
				bB.m_linearVelocity.x += bB.m_invMass * PX;
				bB.m_linearVelocity.y += bB.m_invMass * PY;
				//bB->m_angularVelocity += bB->m_invI * b2Cross(r2, P);
				bB.m_angularVelocity += bB.m_invI * (r2X * PY - r2Y * PX);
			}
			else
			{
				//m_impulse = 0;
			}
		}
		
		
		public override function SolveVelocityConstraints(step:b2TimeStep):void
		{
			bA = m_bodyA;
			bB = m_bodyB;

			tMat = bA.m_xf.R;
			r1X = m_localAnchor1.x - bA.m_sweep.localCenter.x;
			r1Y = m_localAnchor1.y - bA.m_sweep.localCenter.y;
			tX =  (tMat.col1.x * r1X + tMat.col2.x * r1Y);
			r1Y = (tMat.col1.y * r1X + tMat.col2.y * r1Y);
			r1X = tX;

			tMat = bB.m_xf.R;

			r2X = m_localAnchor2.x - bB.m_sweep.localCenter.x;
			r2Y = m_localAnchor2.y - bB.m_sweep.localCenter.y;

			tX =  (tMat.col1.x * r2X + tMat.col2.x * r2Y);
			r2Y = (tMat.col1.y * r2X + tMat.col2.y * r2Y);
			r2X = tX;

			v1X = bA.m_linearVelocity.x + (-bA.m_angularVelocity * r1Y);
			v1Y = bA.m_linearVelocity.y + (bA.m_angularVelocity * r1X);

			v2X = bB.m_linearVelocity.x + (-bB.m_angularVelocity * r2Y);
			v2Y = bB.m_linearVelocity.y + (bB.m_angularVelocity * r2X);
			
			C = m_length-m_maxLength;
			Cdot = (m_u.x * (v2X - v1X) + m_u.y * (v2Y - v1Y));

			// Predictive contraint.
			if(C < 0)
			{
				Cdot+= step.inv_dt*C;
			}
			
			impulse = -m_mass*Cdot;
			oldImpulse = m_impulse;
			m_impulse = b2Math.Min(0,m_impulse+impulse);
			impulse = m_impulse - oldImpulse;

			PX = impulse * m_u.x;
			PY = impulse * m_u.y;

			bA.m_linearVelocity.x -= bA.m_invMass * PX;
			bA.m_linearVelocity.y -= bA.m_invMass * PY;

			bA.m_angularVelocity -= bA.m_invI * (r1X * PY - r1Y * PX);

			bB.m_linearVelocity.x += bB.m_invMass * PX;
			bB.m_linearVelocity.y += bB.m_invMass * PY;

			bB.m_angularVelocity += bB.m_invI * (r2X * PY - r2Y * PX);
		}
		
		public override function SolvePositionConstraints(baumgarte:Number):Boolean
		{
			bA = m_bodyA;
			bB = m_bodyB;

			tMat = bA.m_xf.R;
			r1X = m_localAnchor1.x - bA.m_sweep.localCenter.x;
			r1Y = m_localAnchor1.y - bA.m_sweep.localCenter.y;
			tX =  (tMat.col1.x * r1X + tMat.col2.x * r1Y);
			r1Y = (tMat.col1.y * r1X + tMat.col2.y * r1Y);
			r1X = tX;

			tMat = bB.m_xf.R;
			r2X = m_localAnchor2.x - bB.m_sweep.localCenter.x;
			r2Y = m_localAnchor2.y - bB.m_sweep.localCenter.y;
			tX =  (tMat.col1.x * r2X + tMat.col2.x * r2Y);
			r2Y = (tMat.col1.y * r2X + tMat.col2.y * r2Y);
			r2X = tX;

			dX = bB.m_sweep.c.x + r2X - bA.m_sweep.c.x - r1X;
			dY = bB.m_sweep.c.y + r2Y - bA.m_sweep.c.y - r1Y;

			length = Math.sqrt(dX*dX + dY*dY);
			
			if(length == 0)
			{
				length=1;
			}

			dX /= length;
			dY /= length;

			C = length - m_maxLength;
			C = b2Math.Clamp(C, 0, b2Settings.b2_maxLinearCorrection);
			
			impulse = -m_mass * C;

			m_u.Set(dX, dY);

			PX = impulse * m_u.x;
			PY = impulse * m_u.y;
			
			//bA->m_sweep.c -= bA->m_invMass * P;
			bA.m_sweep.c.x -= bA.m_invMass * PX;
			bA.m_sweep.c.y -= bA.m_invMass * PY;
			//bA->m_sweep.a -= bA->m_invI * b2Cross(r1, P);
			bA.m_sweep.a -= bA.m_invI * (r1X * PY - r1Y * PX);
			//bB->m_sweep.c += bB->m_invMass * P;
			bB.m_sweep.c.x += bB.m_invMass * PX;
			bB.m_sweep.c.y += bB.m_invMass * PY;
			//bB->m_sweep.a -= bB->m_invI * b2Cross(r2, P);
			bB.m_sweep.a += bB.m_invI * (r2X * PY - r2Y * PX);
			
			bA.SynchronizeTransform();
			bB.SynchronizeTransform();
			
			return length-m_maxLength <b2Settings.b2_linearSlop;
			
		}
	
		private var m_localAnchor1:b2Vec2 = new b2Vec2();
		private var m_localAnchor2:b2Vec2 = new b2Vec2();
		private var m_u:b2Vec2 = new b2Vec2();
		private var m_impulse:Number;
		private var m_mass:Number;
		private var m_length:Number;
		private var m_maxLength:Number
		private var m_state:int;
	}
}
