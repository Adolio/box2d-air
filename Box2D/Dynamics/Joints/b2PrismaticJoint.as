﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Dynamics.Joints
{
	import Box2D.Common.b2Pool;
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Mat22;
	import Box2D.Common.Math.b2Mat33;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Transform;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Common.Math.b2Vec3;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2TimeStep;

	
	/**
	* A prismatic joint. This joint provides one degree of freedom: translation
	* along an axis fixed in body1. Relative rotation is prevented. You can
	* use a joint limit to restrict the range of motion and a joint motor to
	* drive the motion or to model joint friction.
	* @see b2PrismaticJointDef
	*/
	public class b2PrismaticJoint extends b2Joint
	{
		/** @inheritDoc */
		public override function GetAnchorA(result:b2Vec2):void
		{
			return m_bodyA.GetWorldPoint(m_localAnchor1, result);
		}
		
		/** @inheritDoc */
		public override function GetAnchorB(result:b2Vec2):void
		{
			return m_bodyB.GetWorldPoint(m_localAnchor2, result);
		}
		/** @inheritDoc */
		public override function GetReactionForce(inv_dt:Number, result:b2Vec2):void
		{
			//return inv_dt * (m_impulse.x * m_perp + (m_motorImpulse + m_impulse.z) * m_axis);
			result.x = inv_dt * (m_impulse.x * m_perp.x + (m_motorImpulse + m_impulse.z) * m_axis.x);
			result.y = inv_dt * (m_impulse.x * m_perp.y + (m_motorImpulse + m_impulse.z) * m_axis.y);
		}
	
		/** @inheritDoc */
		public override function GetReactionTorque(inv_dt:Number) : Number
		{
			return inv_dt * m_impulse.y;
		}
		
		
		
		private static var p1:b2Vec2 = new b2Vec2();			
		private static var p2:b2Vec2 = new b2Vec2();
		private static var axis:b2Vec2 = new b2Vec2();
		
		
		private var dX:Number;
		private var dY:Number;
		private var translation:Number;
		
		
		private var tMat:b2Mat22;
		
		private var r1X:Number;
		private var r1Y:Number;
		private var tX:Number;
		private var r2X:Number;
		private var r2Y:Number;
		
		private var p1X:Number;
		private var p1Y:Number;
		
		private var p2X:Number;
		private var p2Y:Number;		
		
		private var v1:b2Vec2;
		private var v2:b2Vec2;
		private var w1:Number;
		private var w2:Number;
		
		private var speed:Number;
		
		private var xf1:b2Transform;
		private var xf2:b2Transform;
		
		private var m1:Number;
		private var m2:Number;
		private var i1:Number;
		private var i2:Number;
		
		private var jointTransition:Number;
		
		private var PX:Number;
		private var PY:Number;
		private var L1:Number;
		private var L2:Number;
		
		private var Cdot:Number;
		private var impulse:Number;
		private var oldImpulse:Number;
		private var maxImpulse:Number;
		
		private var Cdot1X:Number; 
		private var Cdot1Y:Number;
		
		
		private var Cdot2:Number; 
		private var _f1:b2Vec3 = new b2Vec3(0, 0, 0);
		
		private var _df:b2Vec3 = new b2Vec3(0, 0, 0);
		
		private var bX:Number;
		private var bY:Number;
		
		private var _f2r:b2Vec2 = new b2Vec2(0, 0);
		
		private var _df2:b2Vec2 = new b2Vec2(0, 0);
		
		
		private var c1:b2Vec2;
		private var a1:Number;
		
		private var c2:b2Vec2;
		private var a2:Number;

		private var linearError:Number;
		private var angularError:Number;
		private var active:Boolean;
		private var C2:Number;
		
		private var limitC:Number;
		private var oldLimitImpulse:Number;
		
		private var R1:b2Mat22 = new b2Mat22();
		private var R2:b2Mat22 = new b2Mat22();

		private var gravity:b2Vec2;
		
		private var impulse3:b2Vec3;  
		
		private var C1X:Number;
		private var C1Y:Number;
		
		
		private var k11:Number;
		private var k12:Number;
		private var k22:Number; 
		private var impulse1:b2Vec2;
		
		
		/**
		* Get the current joint translation, usually in meters.
		*/
		[Inline]
		final public function GetJointTranslation():Number
		{
			m_bodyA.GetWorldPoint(m_localAnchor1, p1);
			m_bodyB.GetWorldPoint(m_localAnchor2, p2);

			dX = p2.x - p1.x;
			dY = p2.y - p1.y;

			// m_bodyA.GetWorldVector(m_localXAxis1, axis);
			axis.x = m_bodyA.m_xf.R.col1.x * m_localXAxis1.x + m_bodyA.m_xf.R.col2.x * m_localXAxis1.y;
			axis.y = m_bodyA.m_xf.R.col1.y * m_localXAxis1.x + m_bodyA.m_xf.R.col2.y * m_localXAxis1.y;
			
			translation = (axis.x * dX) + (axis.y * dY);

			return translation;
		}
		
		/**
		* Get the current joint translation speed, usually in meters per second.
		*/
		[Inline]
		final public function GetJointSpeed():Number
		{
			var bA:b2Body = m_bodyA;
			var bB:b2Body = m_bodyB;

			tMat = bA.m_xf.R;
			
			r1X = m_localAnchor1.x - bA.m_sweep.localCenter.x;
			r1Y = m_localAnchor1.y - bA.m_sweep.localCenter.y;
			tX =  (tMat.col1.x * r1X + tMat.col2.x * r1Y);
			
			r1Y = (tMat.col1.y * r1X + tMat.col2.y * r1Y);
			r1X = tX;

			tMat = bB.m_xf.R;
			
			r2X = m_localAnchor2.x - bB.m_sweep.localCenter.x;
			r2Y = m_localAnchor2.y - bB.m_sweep.localCenter.y;
			
			tX =  (tMat.col1.x * r2X + tMat.col2.x * r2Y);
			r2Y = (tMat.col1.y * r2X + tMat.col2.y * r2Y);
			r2X = tX;

			p1X = bA.m_sweep.c.x + r1X;
			p1Y = bA.m_sweep.c.y + r1Y;

			p2X = bB.m_sweep.c.x + r2X;
			p2Y = bB.m_sweep.c.y + r2Y;

			dX = p2X - p1X;
			dY = p2Y - p1Y;
			
			bA.GetWorldVector(m_localXAxis1, axis);		
	
			v1 = bA.m_linearVelocity;
			v2 = bB.m_linearVelocity;
			w1 = bA.m_angularVelocity;
			w2 = bB.m_angularVelocity;

			speed = (dX*(-w1 * axis.y) + dY*(w1 * axis.x)) + (axis.x * ((( v2.x + (-w2 * r2Y)) - v1.x) - (-w1 * r1Y)) + axis.y * ((( v2.y + (w2 * r2X)) - v1.y) - (w1 * r1X)));
	
			return speed;
		}
		
		/**
		* Is the joint limit enabled?
		*/
		[Inline]
		final public function IsLimitEnabled():Boolean
		{
			return m_enableLimit;
		}

		/**
		* Enable/disable the joint limit.
		*/
		public function EnableLimit(flag:Boolean):void
		{
			m_bodyA.SetAwake(true);
			m_bodyB.SetAwake(true);
			m_enableLimit = flag;
		}

		/**
		* Get the lower joint limit, usually in meters.
		*/
		[Inline]
		final public function GetLowerLimit():Number
		{
			return m_lowerTranslation;
		}

		/**
		* Get the upper joint limit, usually in meters.
		*/
		[Inline]
		final public function GetUpperLimit():Number
		{
			return m_upperTranslation;
		}
		
		/**
		* Set the joint limits, usually in meters.
		*/
		public function SetLimits(lower:Number, upper:Number) : void
		{
			//b2Settings.b2Assert(lower <= upper);
			m_bodyA.SetAwake(true);
			m_bodyB.SetAwake(true);
			m_lowerTranslation = lower;
			m_upperTranslation = upper;
		}
		/**
		* Is the joint motor enabled?
		*/
		[Inline]
		final public function IsMotorEnabled():Boolean
		{
			return m_enableMotor;
		}

		/**
		* Enable/disable the joint motor.
		*/
		public function EnableMotor(flag:Boolean) : void
		{
			m_bodyA.SetAwake(true);
			m_bodyB.SetAwake(true);
			m_enableMotor = flag;
		}
		/**
		* Set the motor speed, usually in meters per second.
		*/
		public function SetMotorSpeed(speed:Number) : void
		{
			m_bodyA.SetAwake(true);
			m_bodyB.SetAwake(true);
			m_motorSpeed = speed;
		}
		/**
		* Get the motor speed, usually in meters per second.
		*/
		[Inline]
		final public function GetMotorSpeed():Number
		{
			return m_motorSpeed;
		}
		
		/**
		* Set the maximum motor force, usually in N.
		*/
		[Inline]
		final public function SetMaxMotorForce(force:Number):void
		{
			m_bodyA.SetAwake(true);
			m_bodyB.SetAwake(true);
			m_maxMotorForce = force;
		}
		
		public function GetMaxMotorForce():Number
		{
			return m_maxMotorForce;
		}
		
		
		/**
		* Get the current motor force, usually in N.
		*/
		public function GetMotorForce():Number
		{
			return m_motorImpulse;
		}
		
	
		//--------------- Internals Below -------------------
	
		/** @private */
		public function b2PrismaticJoint(def:b2PrismaticJointDef)
		{
			super(def);
			
			var tMat:b2Mat22;
			var tX:Number;
			var tY:Number;
			
			m_localAnchor1.SetV(def.localAnchorA);
			m_localAnchor2.SetV(def.localAnchorB);
			m_localXAxis1.SetV(def.localAxisA);
			
			//m_localYAxisA = b2Cross(1.0f, m_localXAxisA);
			m_localYAxis1.x = -m_localXAxis1.y;
			m_localYAxis1.y = m_localXAxis1.x;
			
			m_refAngle = def.referenceAngle;
			
			m_impulse.SetZero();
			m_motorMass = 0.0;
			m_motorImpulse = 0.0;
			
			m_lowerTranslation = def.lowerTranslation;
			m_upperTranslation = def.upperTranslation;
			m_maxMotorForce = def.maxMotorForce;
			m_motorSpeed = def.motorSpeed;
			m_enableLimit = def.enableLimit;
			m_enableMotor = def.enableMotor;
			m_limitState = e_inactiveLimit;
			
			m_axis.SetZero();
			m_perp.SetZero();
		}
	
		[Inline]
		final public override function InitVelocityConstraints(step:b2TimeStep):void
		{
			var bA:b2Body = m_bodyA;
			var bB:b2Body = m_bodyB;

			m_localCenterA.SetV(bA.GetLocalCenter());
			m_localCenterB.SetV(bB.GetLocalCenter());
	
			xf1 = bA.GetTransform();
			xf2 = bB.GetTransform();

			tMat = bA.m_xf.R;
			r1X = m_localAnchor1.x - m_localCenterA.x;
			r1Y = m_localAnchor1.y - m_localCenterA.y;
			tX =  (tMat.col1.x * r1X + tMat.col2.x * r1Y);
			r1Y = (tMat.col1.y * r1X + tMat.col2.y * r1Y);
			r1X = tX;

			tMat = bB.m_xf.R;
			r2X = m_localAnchor2.x - m_localCenterB.x;
			r2Y = m_localAnchor2.y - m_localCenterB.y;
			tX =  (tMat.col1.x * r2X + tMat.col2.x * r2Y);
			r2Y = (tMat.col1.y * r2X + tMat.col2.y * r2Y);
			r2X = tX;

			dX = bB.m_sweep.c.x + r2X - bA.m_sweep.c.x - r1X;
			dY = bB.m_sweep.c.y + r2Y - bA.m_sweep.c.y - r1Y;
	
			m_invMassA = bA.m_invMass;
			m_invMassB = bB.m_invMass;
			m_invIA = bA.m_invI;
			m_invIB = bB.m_invI;
	
			// Compute motor Jacobian and effective mass.
			{
				b2Math.MulMV(xf1.R, m_localXAxis1, m_axis);
	
				//m_a1 = b2Math.b2Cross(d + r1, m_axis);
				m_a1 = (dX + r1X) * m_axis.y - (dY + r1Y) * m_axis.x;
				//m_a2 = b2Math.b2Cross(r2, m_axis);
				m_a2 = r2X * m_axis.y - r2Y * m_axis.x;
				
				m_motorMass = m_invMassA + m_invMassB + m_invIA * m_a1 * m_a1 + m_invIB * m_a2 * m_a2; 
				if(m_motorMass > Number.MIN_VALUE)
					m_motorMass = 1.0 / m_motorMass;
			}
	
			// Prismatic constraint.
			{
				b2Math.MulMV(xf1.R, m_localYAxis1, m_perp);
				//m_s1 = b2Math.b2Cross(d + r1, m_perp);
				m_s1 = (dX + r1X) * m_perp.y - (dY + r1Y) * m_perp.x;
				//m_s2 = b2Math.b2Cross(r2, m_perp);
				m_s2 = r2X * m_perp.y - r2Y * m_perp.x;
				
				m1 = m_invMassA;
				m2 = m_invMassB;
				i1 = m_invIA;
				i2 = m_invIB;
				
				m_K.col1.x = m1 + m2 + i1 * m_s1 * m_s1 + i2 * m_s2 * m_s2;
	 	  	  	m_K.col1.y = i1 * m_s1 + i2 * m_s2;
	 	  	  	m_K.col1.z = i1 * m_s1 * m_a1 + i2 * m_s2 * m_a2;
				m_K.col2.x = m_K.col1.y;
	 	  	  	m_K.col2.y = i1 + i2;
	 	  	  	m_K.col2.z = i1 * m_a1 + i2 * m_a2;
				m_K.col3.x = m_K.col1.z;
				m_K.col3.y = m_K.col2.z;
	 	  	  	m_K.col3.z = m1 + m2 + i1 * m_a1 * m_a1 + i2 * m_a2 * m_a2; 
			}
			
			// Compute motor and limit terms
			if (m_enableLimit)
			{ 
				jointTransition = m_axis.x * dX + m_axis.y * dY;

				if (b2Math.Abs(m_upperTranslation - m_lowerTranslation) < 2.0 * b2Settings.b2_linearSlop)
				{
					m_limitState = e_equalLimits;
				}
				else if (jointTransition <= m_lowerTranslation)
				{
					if (m_limitState != e_atLowerLimit)
					{
						m_limitState = e_atLowerLimit;
						m_impulse.z = 0.0;
					}
				}
				else if (jointTransition >= m_upperTranslation)
				{
					if (m_limitState != e_atUpperLimit)
					{
						m_limitState = e_atUpperLimit;
						m_impulse.z = 0.0;
					}
				}
				else
				{
					m_limitState = e_inactiveLimit;
					m_impulse.z = 0.0;
				}
			}
			else
			{
				m_limitState = e_inactiveLimit;
			}
			
			if (m_enableMotor == false)
			{
				m_motorImpulse = 0.0;
			}
	
			if (step.warmStarting)
			{
				// Account for variable time step.
				m_impulse.x *= step.dtRatio;
				m_impulse.y *= step.dtRatio;
				m_motorImpulse *= step.dtRatio;

				PX = m_impulse.x * m_perp.x + (m_motorImpulse + m_impulse.z) * m_axis.x;
				PY = m_impulse.x * m_perp.y + (m_motorImpulse + m_impulse.z) * m_axis.y;
				L1 = m_impulse.x * m_s1 + m_impulse.y + (m_motorImpulse + m_impulse.z) * m_a1;
				L2 = m_impulse.x * m_s2 + m_impulse.y + (m_motorImpulse + m_impulse.z) * m_a2;
	
				//bA->m_linearVelocity -= m_invMassA * P;
				bA.m_linearVelocity.x -= m_invMassA * PX;
				bA.m_linearVelocity.y -= m_invMassA * PY;
				//bA->m_angularVelocity -= m_invIA * L1;
				bA.m_angularVelocity -= m_invIA * L1;
	
				//bB->m_linearVelocity += m_invMassB * P;
				bB.m_linearVelocity.x += m_invMassB * PX;
				bB.m_linearVelocity.y += m_invMassB * PY;
				//bB->m_angularVelocity += m_invIB * L2;
				bB.m_angularVelocity += m_invIB * L2;
			}
			else
			{
				m_impulse.SetZero();
				m_motorImpulse = 0.0;
			}
		}
		
		private var m22Det:Number = 0;
		
		private var m22bX:Number = 0;
		private var m22bY:Number = 0;
		

		[Inline]
		final public override function SolveVelocityConstraints(step:b2TimeStep):void
		{
			v1 = m_bodyA.m_linearVelocity;
			w1 = m_bodyA.m_angularVelocity;
			v2 = m_bodyB.m_linearVelocity;
			w2 = m_bodyB.m_angularVelocity;
			
			// Solve linear motor constraint
			if (m_enableMotor && m_limitState != e_equalLimits)
			{
				//float32 Cdot = b2Dot(m_axis, v2 - v1) + m_a2 * w2 - m_a1 * w1; 
				Cdot = m_axis.x * (v2.x -v1.x) + m_axis.y * (v2.y - v1.y) + m_a2 * w2 - m_a1 * w1;
				impulse = m_motorMass * (m_motorSpeed - Cdot);
				oldImpulse = m_motorImpulse;
				maxImpulse = step.dt * m_maxMotorForce;
				
				m_motorImpulse = b2Math.Clamp(m_motorImpulse + impulse, -maxImpulse, maxImpulse);
				impulse = m_motorImpulse - oldImpulse;
				
				PX = impulse * m_axis.x;
				PY = impulse * m_axis.y;
				L1 = impulse * m_a1;
				L2 = impulse * m_a2;
				
				v1.x -= m_invMassA * PX;
				v1.y -= m_invMassA * PY;
				w1 -= m_invIA * L1;
				
				v2.x += m_invMassB * PX;
				v2.y += m_invMassB * PY;
				w2 += m_invIB * L2;
			}
			
			//Cdot1.x = b2Dot(m_perp, v2 - v1) + m_s2 * w2 - m_s1 * w1; 
			Cdot1X = m_perp.x * (v2.x - v1.x) + m_perp.y * (v2.y - v1.y) + m_s2 * w2 - m_s1 * w1; 
			Cdot1Y = w2 - w1;
	
			if (m_enableLimit && m_limitState != e_inactiveLimit)
			{ 
				Cdot2 = m_axis.x * (v2.x - v1.x) + m_axis.y * (v2.y - v1.y) + m_a2 * w2 - m_a1 * w1;
				
				_f1.x = m_impulse.x;
				_f1.y = m_impulse.y;
				_f1.z = m_impulse.z;
					
				m_K.Solve33(_df, -Cdot1X, -Cdot1Y, -Cdot2);
				
				m_impulse.x += _df.x;
				m_impulse.y += _df.y;
				m_impulse.z += _df.z;
				
				if (m_limitState == e_atLowerLimit)
				{
					m_impulse.z = b2Math.Max(m_impulse.z, b2Settings.DECIMAL_ZERO);
				}
				else if (m_limitState == e_atUpperLimit)
				{
					m_impulse.z = b2Math.Min(m_impulse.z, b2Settings.DECIMAL_ZERO);
				}
 
				bX = -Cdot1X - (m_impulse.z - _f1.z) * m_K.col3.x;
				bY = -Cdot1Y - (m_impulse.z - _f1.z) * m_K.col3.y;	
				
				m_K.Solve22(_f2r, bX, bY);				
					
				_f2r.x += _f1.x;
				_f2r.y += _f1.y;
				
				m_impulse.x = _f2r.x;
				m_impulse.y = _f2r.y;
				
				_df.x = m_impulse.x - _f1.x;
				_df.y = m_impulse.y - _f1.y;
				_df.z = m_impulse.z - _f1.z;
				
				PX = _df.x * m_perp.x + _df.z * m_axis.x;
				PY = _df.x * m_perp.y + _df.z * m_axis.y;
				L1 = _df.x * m_s1 + _df.y + _df.z * m_a1;
				L2 = _df.x * m_s2 + _df.y + _df.z * m_a2;
				
				v1.x -= m_invMassA * PX;
				v1.y -= m_invMassA * PY;
				w1 -= m_invIA * L1;
				
				v2.x += m_invMassB * PX;
				v2.y += m_invMassB * PY;
				w2 += m_invIB * L2;
			}
			else
			{ 	

				//m_K.Solve22(_df2, -Cdot1X, -Cdot1Y);				
				m22bX = -Cdot1X;
				m22bY = -Cdot1Y;
				
				m22Det = m_K.col1.x * m_K.col2.y - m_K.col2.x * m_K.col1.y;
				
				if(m22Det != b2Settings.DECIMAL_ZERO)
				{
					m22Det = b2Settings.DECIMAL_ONE / m22Det;
				}
				
				_df2.x = m22Det * (m_K.col2.y * m22bX - m_K.col2.x * m22bY);
				_df2.y = m22Det * (m_K.col1.x * m22bY - m_K.col1.y * m22bX);
				// end of m_K.Solve22(_df2, -Cdot1X, -Cdot1Y);
				
				
				
				
				m_impulse.x += _df2.x;
				m_impulse.y += _df2.y;
				
				PX = _df2.x * m_perp.x;
				PY = _df2.x * m_perp.y;
				L1 = _df2.x * m_s1 + _df2.y;
				L2 = _df2.x * m_s2 + _df2.y;
				
				v1.x -= m_invMassA * PX;
				v1.y -= m_invMassA * PY;
				w1 -= m_invIA * L1;
				
				v2.x += m_invMassB * PX;
				v2.y += m_invMassB * PY;
				w2 += m_invIB * L2;
			}
	
			m_bodyA.m_linearVelocity.x = v1.x;
			m_bodyA.m_linearVelocity.y = v1.y;
			
			m_bodyA.m_angularVelocity = w1;
			
			m_bodyB.m_linearVelocity.x = v2.x;
			m_bodyB.m_linearVelocity.y = v2.y;
			
			m_bodyB.m_angularVelocity = w2;
		}
		
		[Inline]
		final public override function SolvePositionConstraints(baumgarte:Number):Boolean
		{
			//B2_NOT_USED(baumgarte);
			
			m_bodyA = m_bodyA;
			m_bodyB = m_bodyB;
			
			c1 = m_bodyA.m_sweep.c;
			a1 = m_bodyA.m_sweep.a;
			
			c2 = m_bodyB.m_sweep.c;
			a2 = m_bodyB.m_sweep.a;

			linearError = 0.0;
			angularError = 0.0;
			active = false;
			C2 = 0.0;
			
			R1.Set(a1);
			R2.Set(a2);
			
			tMat = R1;
			
			r1X = m_localAnchor1.x - m_localCenterA.x;
			r1Y = m_localAnchor1.y - m_localCenterA.y;
			
			tX =  (tMat.col1.x * r1X + tMat.col2.x * r1Y);
			
			r1Y = (tMat.col1.y * r1X + tMat.col2.y * r1Y);
			r1X = tX;

			tMat = R2;
			
			r2X = m_localAnchor2.x - m_localCenterB.x;
			r2Y = m_localAnchor2.y - m_localCenterB.y;
			
			tX =  (tMat.col1.x * r2X + tMat.col2.x * r2Y);
			r2Y = (tMat.col1.y * r2X + tMat.col2.y * r2Y);
			r2X = tX;
			
			dX = c2.x + r2X - c1.x - r1X;
			dY = c2.y + r2Y - c1.y - r1Y;
			
			if (m_enableLimit)
			{
				b2Math.MulMV(R1, m_localXAxis1, m_axis);
	
				//m_a1 = b2Math.b2Cross(d + r1, m_axis);
				m_a1 = (dX + r1X) * m_axis.y - (dY + r1Y) * m_axis.x;
				//m_a2 = b2Math.b2Cross(r2, m_axis);
				m_a2 = r2X * m_axis.y - r2Y * m_axis.x;
				
				translation = m_axis.x * dX + m_axis.y * dY;
				gravity = GetBodyA().GetWorld().GetGravity(); 
	
				if (b2Math.Abs(m_upperTranslation - m_lowerTranslation) < 2.0 * b2Settings.b2_linearSlop)
				{
					// Prevent large angular corrections.
					C2 = b2Math.Clamp(translation, -b2Settings.b2_maxLinearCorrection, b2Settings.b2_maxLinearCorrection);
					linearError = b2Math.Abs(translation);
					active = true;
				}
				else if(translation <= m_lowerTranslation)
				{
					// Prevent large angular corrections and allow some slop.
					C2 = b2Math.Clamp(translation - m_lowerTranslation + b2Settings.b2_linearSlop, -b2Settings.b2_maxLinearCorrection, 0.0);
					linearError = m_lowerTranslation - translation;
	
					if(gravity.x == 0)
					{
						if(gravity.y < 0)
						{
							active = false;
						}
						else
						{
							active = true;
						}
					}
					else
					{
						if(gravity.x < 0)
						{
							active = false;
						}
						else
						{
							active = true;
						}
					}
				}
				else if (translation >= m_upperTranslation)
				{
					// Prevent large angular corrections and allow some slop.
					C2 = b2Math.Clamp(translation - m_upperTranslation + b2Settings.b2_linearSlop, 0.0, b2Settings.b2_maxLinearCorrection);
					linearError = translation - m_upperTranslation;
	
					if(gravity.x == 0)
					{
						if(gravity.y > 0)
						{
							active = false;
						}
						else
						{
							active = true;
						}	
					}
					else
					{
						if(gravity.x > 0)
						{
							active = false;
						}
						else
						{
							active = true;
						}
					}
				}
			}
	
			b2Math.MulMV(R1, m_localYAxis1, m_perp);
	
			//m_s1 = b2Cross(d + r1, m_perp); 
			m_s1 = (dX + r1X) * m_perp.y - (dY + r1Y) * m_perp.x;
			//m_s2 = b2Cross(r2, m_perp); 
			m_s2 = r2X * m_perp.y - r2Y * m_perp.x;
			
			impulse3 = b2Pool.getVector3(0, 0, 0);  
				
			C1X = m_perp.x * dX + m_perp.y * dY;
			C1Y = a2 - a1 - m_refAngle;
			
			linearError = b2Math.Max(linearError, b2Math.Abs(C1X));
			angularError = b2Math.Abs(C1Y);
	
			if(active)
			{
				m1 = m_invMassA;
				m2 = m_invMassB;
				i1 = m_invIA;
				i2 = m_invIB;
				
				m_K.col1.x = m1 + m2 + i1 * m_s1 * m_s1 + i2 * m_s2 * m_s2;
	 	  	  	m_K.col1.y = i1 * m_s1 + i2 * m_s2;
	 	  	  	m_K.col1.z = i1 * m_s1 * m_a1 + i2 * m_s2 * m_a2;
				m_K.col2.x = m_K.col1.y;
	 	  	  	m_K.col2.y = i1 + i2;
	 	  	  	m_K.col2.z = i1 * m_a1 + i2 * m_a2;
				m_K.col3.x = m_K.col1.z;
				m_K.col3.y = m_K.col2.z;
	 	  	  	m_K.col3.z = m1 + m2 + i1 * m_a1 * m_a1 + i2 * m_a2 * m_a2;
				
				m_K.Solve33(impulse3, -C1X, -C1Y, -C2);
			}
			else
			{
				m1 = m_invMassA;
				m2 = m_invMassB;
				i1 = m_invIA;
				i2 = m_invIB;
				
				k11  = m1 + m2 + i1 * m_s1 * m_s1 + i2 * m_s2 * m_s2;
				k12 = i1 * m_s1 + i2 * m_s2;
				k22 = i1 + i2; 
				
				m_K.col1.Set(k11, k12, 0.0);
				m_K.col2.Set(k12, k22, 0.0);
				
				impulse1 = b2Pool.getVector2(0, 0); 
					
				m_K.Solve22(impulse1, -C1X, -C1Y);
				
				impulse3.x = impulse1.x;
				impulse3.y = impulse1.y;
				impulse3.z = 0.0;
				
				b2Pool.putVector2(impulse1);
			}
			
			PX = impulse3.x * m_perp.x + impulse3.z * m_axis.x;
			PY = impulse3.x * m_perp.y + impulse3.z * m_axis.y;
			L1 = impulse3.x * m_s1 + impulse3.y + impulse3.z * m_a1;
			L2 = impulse3.x * m_s2 + impulse3.y + impulse3.z * m_a2;
			
			b2Pool.putVector3(impulse3);
			
			c1.x -= m_invMassA * PX;
			c1.y -= m_invMassA * PY;
			a1 -= m_invIA * L1;
			
			c2.x += m_invMassB * PX;
			c2.y += m_invMassB * PY;
			a2 += m_invIB * L2;
			
			// TODO_ERIN remove need for this
			//bA.m_sweep.c = c1;	//Already done by reference
			m_bodyA.m_sweep.a = a1;
			//bB.m_sweep.c = c2;	//Already done by reference
			m_bodyB.m_sweep.a = a2;
			
			
			m_bodyA.SynchronizeTransform();
			m_bodyB.SynchronizeTransform(); 
			
			return linearError <= b2Settings.b2_linearSlop && angularError <= b2Settings.b2_angularSlop;
			
		}
	
		public var m_localAnchor1:b2Vec2 = new b2Vec2();
		public var m_localAnchor2:b2Vec2 = new b2Vec2();
		public var m_localXAxis1:b2Vec2 = new b2Vec2();

		private var m_localYAxis1:b2Vec2 = new b2Vec2();
		private var m_refAngle:Number;
	
		private var m_axis:b2Vec2 = new b2Vec2();
		private var m_perp:b2Vec2 = new b2Vec2();
		private var m_s1:Number;
		private var m_s2:Number;
		private var m_a1:Number;
		private var m_a2:Number;
		
		private var m_K:b2Mat33 = new b2Mat33();
		private var m_impulse:b2Vec3 = new b2Vec3();
	
		private var m_motorMass:Number;			// effective mass for motor/limit translational constraint.
		private var m_motorImpulse:Number;
	
		public var m_lowerTranslation:Number;
		public var m_upperTranslation:Number;
		
		public var m_maxMotorForce:Number;
		public var m_motorSpeed:Number;
		
		public var m_enableLimit:Boolean;
		public var m_enableMotor:Boolean;
		
		private var m_limitState:uint;
	};
}
