package Box2D.Dynamics
{
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	import Box2D.Common.Math.b2Transform;
	import Box2D.Common.Math.b2Vec2;

	public interface IBox2dDebugDraw
	{
		function SetFlags(flags:uint):void;
		function GetFlags():uint;
		function AppendFlags(flags:uint):void;
		function ClearFlags(flags:uint):void 
		function SetSprite(sprite:Sprite):void 
		function GetSprite():Sprite;
		function SetDrawScale(drawScale:Number):void;
		function GetDrawScale():Number;
		function SetLineThickness(lineThickness:Number):void ;
		function GetLineThickness():Number;
		function SetAlpha(alpha:Number):void ;
		function GetAlpha():Number;
		function SetFillAlpha(alpha:Number):void;
		function GetFillAlpha():Number;
		function SetXFormScale(xformScale:Number):void;
		function GetXFormScale():Number;

		function clear():void;
		function DrawPolygon(vertices:Array, vertexCount:int, color:uint):void;
		function DrawSolidPolygon(vertices:Vector.<b2Vec2>, vertexCount:int, color:uint, dimensions:Rectangle):void;
		function DrawCircle(center:b2Vec2, radius:Number, color:uint):void;
		function DrawSolidCircle(center:b2Vec2, radius:Number, axis:b2Vec2, color:uint):void;
		function DrawSegment(p1:b2Vec2, p2:b2Vec2, color:uint):void;
		function DrawTransform(xf:b2Transform):void;
	}
}