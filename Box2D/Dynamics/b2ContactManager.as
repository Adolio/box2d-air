﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Dynamics
{
	import Box2D.Collision.IBroadPhase;
	import Box2D.Collision.IBroadPhaseUpdatePairCallback;
	import Box2D.Collision.b2ContactPoint;
	import Box2D.Collision.b2DynamicTreeBroadPhase;
	import Box2D.Dynamics.Contacts.b2Contact;
	import Box2D.Dynamics.Contacts.b2ContactFactory;
	import Box2D.Dynamics.Joints.b2JointEdge;


	// Delegate of b2World.
	/**
	* @private
	*/
	public class b2ContactManager implements IBroadPhaseUpdatePairCallback
	{
		public function b2ContactManager()
		{
			m_world = null;
			m_contactCount = 0;
			m_contactFilter = b2ContactFilter.b2_defaultFilter;
			m_contactListener = b2ContactListener.b2_defaultListener;
			m_contactFactory = new b2ContactFactory();
			m_broadPhase = new b2DynamicTreeBroadPhase();
		};

		private static var ap_contact:b2Contact;

		private static var ap_bodyA:b2Body;
		private static var ap_bodyB:b2Body;

		private static var shouldBodyCollide:Boolean;
		private static var sc_jointEdge:b2JointEdge;

		private static var sc_filter1:b2FilterData;
		private static var sc_filter2:b2FilterData;


		// This is a callback from the broadphase when two AABB proxies begin
		// to overlap. We create a b2Contact to manage the narrow phase.
		[Inline]
		final public function updatePairsCallback(ap_fixtureA:b2Fixture, ap_fixtureB:b2Fixture):void
		{
			ap_bodyA = ap_fixtureA.m_body;
			ap_bodyB = ap_fixtureB.m_body;

			// Does a joint override collision? Is at least one body dynamic?
			if (!ap_bodyB.ShouldCollide(ap_bodyA))
			{
				return;
			}

			// Check user filtering
			if (!m_contactFilter.ShouldCollide(ap_fixtureA, ap_fixtureB))
			{
				return;
			}

			// Call the factory.
			ap_contact = m_contactFactory.Create(ap_fixtureA, ap_fixtureB);

			// Contact creation may swap shapes.
			ap_fixtureA = ap_contact.m_fixtureA;
			ap_fixtureB = ap_contact.m_fixtureB;

			ap_bodyA = ap_fixtureA.m_body;
			ap_bodyB = ap_fixtureB.m_body;

			// Insert into the world.
			ap_contact.m_prev = null;
			ap_contact.m_next = m_world.m_contactList;

			if (m_world.m_contactList != null)
			{
				m_world.m_contactList.m_prev = ap_contact;
			}

			m_world.m_contactList = ap_contact;


			// Connect to island graph.

			// Connect to body A
			ap_contact.m_nodeA.contact = ap_contact;
			ap_contact.m_nodeA.other = ap_bodyB;

			ap_contact.m_nodeA.prev = null;
			ap_contact.m_nodeA.next = ap_bodyA.m_contactList;

			if(ap_bodyA.m_contactList != null)
			{
				ap_bodyA.m_contactList.prev = ap_contact.m_nodeA;
			}

			ap_bodyA.m_contactList = ap_contact.m_nodeA;

			// Connect to body 2
			ap_contact.m_nodeB.contact = ap_contact;
			ap_contact.m_nodeB.other = ap_bodyA;

			ap_contact.m_nodeB.prev = null;
			ap_contact.m_nodeB.next = ap_bodyB.m_contactList;

			if (ap_bodyB.m_contactList != null)
			{
				ap_bodyB.m_contactList.prev = ap_contact.m_nodeB;
			}

			ap_bodyB.m_contactList = ap_contact.m_nodeB;

			++m_world.m_contactCount;
		}

		[Inline]
		final public function FindNewContacts():void
		{
			m_broadPhase.UpdatePairs(this);
		}

		static private const s_evalCP:b2ContactPoint = new b2ContactPoint();

		private var dc_fixtureA:b2Fixture;
		private var dc_fixtureB:b2Fixture;

		private var dc_bodyA:b2Body;
		private var dc_bodyB:b2Body;

		public function Destroy(c:b2Contact):void
		{
			dc_fixtureA = c.GetFixtureA();
			dc_fixtureB = c.GetFixtureB();

			dc_bodyA = dc_fixtureA.GetBody();
			dc_bodyB = dc_fixtureB.GetBody();

			if (c.IsTouching())
			{
				m_contactListener.EndContact(c);
			}

			// Remove from the world.
			if (c.m_prev)
			{
				c.m_prev.m_next = c.m_next;
			}

			if (c.m_next)
			{
				c.m_next.m_prev = c.m_prev;
			}

			if (c == m_world.m_contactList)
			{
				m_world.m_contactList = c.m_next;
			}

			// Remove from body A
			if (c.m_nodeA.prev)
			{
				c.m_nodeA.prev.next = c.m_nodeA.next;
			}

			if (c.m_nodeA.next)
			{
				c.m_nodeA.next.prev = c.m_nodeA.prev;
			}

			if (dc_bodyA && c.m_nodeA == dc_bodyA.m_contactList)
			{
				dc_bodyA.m_contactList = c.m_nodeA.next;
			}

			// Remove from body 2
			if (c.m_nodeB.prev)
			{
				c.m_nodeB.prev.next = c.m_nodeB.next;
			}

			if (c.m_nodeB.next)
			{
				c.m_nodeB.next.prev = c.m_nodeB.prev;
			}

			if (dc_bodyB && c.m_nodeB == dc_bodyB.m_contactList)
			{
				dc_bodyB.m_contactList = c.m_nodeB.next;
			}

			// Call the factory.
			m_contactFactory.Destroy(c);
			--m_contactCount;
		}


		// This is the top level collision call for the time step. Here
		// all the narrow phase collision is processed for the world
		// contact list.

		private var co_contact:b2Contact;
		private var co_nukeContact:b2Contact;

		private var co_fixtureA:b2Fixture;
		private var co_fixtureB:b2Fixture;

		private var co_bodyA:b2Body;
		private var co_bodyB:b2Body;

		private var co_overlap:Boolean;

		[Inline]
		final public function Collide():void
		{
			// Update awake contacts.
			co_contact = m_world.m_contactList;

			while(co_contact)
			{
				co_fixtureA = co_contact.m_fixtureA;
				co_fixtureB = co_contact.m_fixtureB;

				co_bodyA = co_fixtureA.m_body;
				co_bodyB = co_fixtureB.m_body;

				if(!co_bodyA.m_isAwake && !co_bodyB.m_isAwake)
				{
					co_contact = co_contact.m_next;
					continue;
				}

				// Is this contact flagged for filtering?
				if(co_contact.m_isFiltering)
				{
					// Should these bodies collide?
					if (!co_bodyB.ShouldCollide(co_bodyA))
					{
						co_nukeContact = co_contact;
						co_contact = co_nukeContact.m_next;
						Destroy(co_nukeContact);
						continue;
					}

					// Check user filtering.
					if (!m_contactFilter.ShouldCollide(co_fixtureA, co_fixtureB))
					{
						co_nukeContact = co_contact;
						co_contact = co_nukeContact.m_next;
						Destroy(co_nukeContact);
						continue;
					}

					// Clear the filtering flag
					co_contact.m_isFiltering = false;
				}

				// m_broadPhase.TestDynamicTreeNodeOverlap(co_fixtureA.m_proxy, co_fixtureB.m_proxy);
				co_overlap = co_fixtureA.m_proxy.aabb.TestOverlap(co_fixtureB.m_proxy.aabb);

				// Here we destroy contacts that cease to overlap in the broadphase
				if(co_overlap == false)
				{
					co_nukeContact = co_contact;
					co_contact = co_nukeContact.m_next;
					Destroy(co_nukeContact);
					continue;
				}

				co_contact.Update(m_contactListener);
				co_contact = co_contact.m_next;

			}// end of while(co_contact)
		}

		public var m_world:b2World;
		public var m_broadPhase:IBroadPhase;

		public var m_contactList:b2Contact;
		public var m_contactCount:int;
		public var m_contactFilter:b2ContactFilter;
		public var m_contactListener:b2ContactListener;
		public var m_contactFactory:b2ContactFactory;
	};
}
