﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Dynamics
{

	import flash.geom.Rectangle;
	
	import Box2D.Collision.IB2WorldRayCastCallback;
	import Box2D.Collision.IBroadPhase;
	import Box2D.Collision.IBroadPhaseRayCastCallback;
	import Box2D.Collision.b2AABB;
	import Box2D.Collision.b2DynamicTreeBroadPhase;
	import Box2D.Collision.b2DynamicTreeNode;
	import Box2D.Collision.b2RayCastInput;
	import Box2D.Collision.b2RayCastOutput;
	import Box2D.Collision.b2TOIInput;
	import Box2D.Collision.b2TimeOfImpact;
	import Box2D.Collision.b2WorldQueryPointBroadPhaseCallback;
	import Box2D.Collision.b2WorldQueryShapeBroadPhaseCallback;
	import Box2D.Collision.Shapes.b2CircleShape;
	import Box2D.Collision.Shapes.b2EdgeShape;
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Common.b2Color;
	import Box2D.Common.b2Pool;
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Sweep;
	import Box2D.Common.Math.b2Transform;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.Contacts.b2Contact;
	import Box2D.Dynamics.Contacts.b2ContactEdge;
	import Box2D.Dynamics.Contacts.b2ContactSolver;
	import Box2D.Dynamics.Controllers.b2Controller;
	import Box2D.Dynamics.Controllers.b2ControllerEdge;
	import Box2D.Dynamics.Joints.b2Joint;
	import Box2D.Dynamics.Joints.b2JointDef;
	import Box2D.Dynamics.Joints.b2JointEdge;
	import Box2D.Dynamics.Joints.b2PulleyJoint;
	
	
	/**
	* The world class manages all physics entities, dynamic simulation,
	* and asynchronous queries. 
	*/
	public class b2World implements IBroadPhaseRayCastCallback
	{
		
		// Construct a world object.
		/**
		* @param gravity the world gravity vector.
		* @param doSleep improve performance by not simulating inactive bodies.
		*/
		public function b2World(gravity:b2Vec2, doSleep:Boolean)
		{			
			m_destructionListener = null;
			m_debugDraw = null;

			m_bodyList = null;
			m_contactList = null;
			m_jointList = null;
			m_controllerList = null;

			m_bodyCount = 0;
			m_contactCount = 0;
			m_jointCount = 0;

			m_warmStarting = true;
			m_continuousPhysics = true;
			
			m_allowSleep = doSleep;
			m_gravity = gravity;
			
			m_inv_dt0 = b2Settings.DECIMAL_ZERO;
			
			m_contactManager.m_world = this;
			
			s_TOI_Timestep = new b2TimeStep();

			s_b2World_timestep = new b2TimeStep()
			s_b2World_timestep.dt = 1/60; // 60 frames per second

			s_backupA = new b2Sweep();
			s_backupB = new b2Sweep();
			s_queue = new Vector.<b2Body>;

			m_groundBody = CreateBody(new b2BodyDef());
		}
	
		/**
		* Destruct the world. All physics entities are destroyed and all heap memory is released.
		*/
		//~b2World();
	
		/**
		* Register a destruction listener.
		*/
		public function SetDestructionListener(listener:b2DestructionListener) : void{
			m_destructionListener = listener;
		}
	
		/**
		* Register a contact filter to provide specific control over collision.
		* Otherwise the default filter is used (b2_defaultFilter).
		*/
		public function SetContactFilter(filter:b2ContactFilter) : void{
			m_contactManager.m_contactFilter = filter;
		}
	
		/**
		* Register a contact event listener
		*/
		public function SetContactListener(listener:b2ContactListener) : void{
			m_contactManager.m_contactListener = listener;
		}
		
		[Inline]
		final public function GetContactListener():b2ContactListener
		{
			return m_contactManager.m_contactListener;
		}
	
		/**
		* Register a routine for debug drawing. The debug draw functions are called
		* inside the b2World::Step method, so make sure your renderer is ready to
		* consume draw commands when you call Step().
		*/
		public function SetDebugDraw(debugDraw:IBox2dDebugDraw):void{
			m_debugDraw = debugDraw;
		}
		
		public function GetDebugDraw():IBox2dDebugDraw{
			return m_debugDraw;
		}
		
		/**
		 * Use the given object as a broadphase.
		 * The old broadphase will not be cleanly emptied.
		 * @warning It is not recommended you call this except immediately after constructing the world.
		 * @warning This function is locked during callbacks.
		 */
		public function SetBroadPhase(broadPhase:IBroadPhase):void
		{
			var oldBroadPhase:IBroadPhase = m_contactManager.m_broadPhase;
				
			m_contactManager.m_broadPhase = broadPhase;
			
			for(var b:b2Body = m_bodyList; b; b = b.m_next)
			{
				for(var f:b2Fixture = b.m_fixtureList; f; f = f.m_next)
				{
					f.m_proxy = broadPhase.CreateDynamicTreeNodeProxyForFixture(oldBroadPhase.GetFatAABB(f.m_proxy), f);
				}
			}
		}
		
		/**
		* Perform validation of internal data structures.
		*/
		public function Validate() : void
		{
			m_contactManager.m_broadPhase.Validate();
		}
		
		/**
		* Get the number of broad-phase proxies.
		*/
		public function GetProxyCount() : int
		{
			return m_contactManager.m_broadPhase.GetProxyCount();
		}
		
		/**
		* Create a rigid body given a definition. No reference to the definition
		* is retained.
		* @warning This function is locked during callbacks.
		*/
		public function CreateBody(def:b2BodyDef):b2Body
		{
			// b2Settings.b2Assert(IsLocked() == false);
			if(IsLocked())
			{
				throw Error('Can create body because world is locked');
			}
			
			var b:b2Body = new b2Body(def, this);
			
			// Add to world doubly linked list.
			b.m_prev = null;
			b.m_next = m_bodyList;
	
			if(m_bodyList)
			{
				m_bodyList.m_prev = b;
			}
	
			m_bodyList = b;
			++m_bodyCount;
			
			return b;
		}
	
		/**
		* Destroy a rigid body given a definition. No reference to the definition
		* is retained. This function is locked during callbacks.
		* @warning This automatically deletes all associated shapes and joints.
		* @warning This function is locked during callbacks.
		*/
		public function DestroyBody(b:b2Body):void
		{
			// b2Settings.b2Assert(IsLocked() == false);
			if(IsLocked())
			{
				throw Error('Can destroy body because world is locked');
			}
			
			// Delete the attached joints.
			var jn:b2JointEdge = b.m_jointList;
			var jn0:b2JointEdge;

			while(jn)
			{
				jn0 = jn;
				jn = jn.next;

				if (m_destructionListener)
				{
					m_destructionListener.SayGoodbyeJoint(jn0.joint);
				}

				DestroyJoint(jn0.joint);
			}
			
			// Detach controllers attached to this body
			var coe:b2ControllerEdge = b.m_controllerList;
			var coe0:b2ControllerEdge = coe;
			
			while(coe)
			{
				coe0 = coe;
				coe = coe.nextController;
				coe0.controller.RemoveBody(b);
			}
			
			// Delete the attached contacts.
			var ce:b2ContactEdge = b.m_contactList;
			var ce0:b2ContactEdge;
			
			while (ce)
			{
				ce0 = ce;
				ce = ce.next;
				m_contactManager.Destroy(ce0.contact);
			}

			b.m_contactList = null;
			
			// Delete the attached fixtures. This destroys broad-phase
			// proxies.
			var f:b2Fixture = b.m_fixtureList;
			var f0:b2Fixture;
			
			while(f)
			{
				f0 = f;
				f = f.m_next;

				if(m_destructionListener)
				{
					m_destructionListener.SayGoodbyeFixture(f0);
				}
				
				f0.DestroyProxy(m_contactManager.m_broadPhase);

				b.DestroyFixture(f0);		
			}

			// Remove world body list.
			if (b.m_prev)
			{
				b.m_prev.m_next = b.m_next;
			}
			
			if (b.m_next)
			{
				b.m_next.m_prev = b.m_prev;
			}
			
			if (b == m_bodyList)
			{
				m_bodyList = b.m_next;
			}
			
			b.Destroy();
			--m_bodyCount;
		}
	
		
		private var joint:b2Joint;
		private var bodyA:b2Body;
		private var bodyB:b2Body;
		private var jointEdge:b2ContactEdge;
		
		/**
		* Create a joint to constrain bodies together. No reference to the definition
		* is retained. This may cause the connected bodies to cease colliding.
		* @warning This function is locked during callbacks.
		*/

		public function CreateJoint(def:b2JointDef):b2Joint
		{
			//b2Settings.b2Assert(m_lock == false);
			
			joint = b2Joint.Create(def, null);
			
			if(def.simulateInactive == true)
			{
				joint.SetSimulateInactive(true);			
			}
			else
			{
				joint.SetSimulateInactive(false);
			}
	
			// check if list is not empty
			if(m_jointList)
			{
				if(m_jointList.m_last)
				{
					joint.m_prev = m_jointList.m_last;
					m_jointList.m_last.m_next = joint;
				}
				else
				{
					joint.m_prev = m_jointList; 
					m_jointList.m_next = joint;
				}
				
				m_jointList.m_last = joint;
			}
			else
			{
				m_jointList = joint;
			}

			++m_jointCount;

			// Connect to the bodies' doubly linked lists.
			joint.m_edgeAList.joint = joint;
			joint.m_edgeAList.other = joint.m_bodyB;
			
			
			joint.m_edgeAList.prev = null;
			joint.m_edgeAList.next = joint.m_bodyA.m_jointList;
			
			if (joint.m_bodyA.m_jointList)
			{
				joint.m_bodyA.m_jointList.prev = joint.m_edgeAList;
			}
			
			joint.m_bodyA.m_jointList = joint.m_edgeAList;
			
			joint.m_edgeBList.joint = joint;
			joint.m_edgeBList.other = joint.m_bodyA;
			joint.m_edgeBList.prev = null;
			joint.m_edgeBList.next = joint.m_bodyB.m_jointList;
			
			if(joint.m_bodyB.m_jointList)
			{
				joint.m_bodyB.m_jointList.prev = joint.m_edgeBList;
			}
			
			joint.m_bodyB.m_jointList = joint.m_edgeBList;
			
			bodyA = def.bodyA;
			bodyB = def.bodyB;

			// If the joint prevents collisions, then flag any contacts for filtering.
			if(def.collideConnected == false)
			{
				jointEdge = bodyB.GetContactList();

				while(jointEdge)
				{
					if(jointEdge.other == bodyA)
					{
						// Flag the contact for filtering at the next time step (where either
						// body is awake).
						jointEdge.contact.FlagForFiltering();
					}
	
					jointEdge = jointEdge.next;
				}
			}
			
			// Note: creating a joint doesn't wake the bodies.
			
			return joint;
		}
	
		private var collideConnected:Boolean
		
		/**
		* Destroy a joint. This may cause the connected bodies to begin colliding.
		* @warning This function is locked during callbacks.
		*/
		public function DestroyJoint(j:b2Joint):void
		{
			//b2Settings.b2Assert(m_lock == false);
			
			collideConnected = j.m_collideConnected;

			// check if there is any element in the list
			if(m_jointList)
			{
				// are we removing first element
				if(m_jointList == j)
				{
					if(j.m_next)
					{
						j.m_next.m_last = m_jointList.m_last;
						j.m_next.m_prev = null;
					}

					m_jointList = j.m_next;
				}
				// are we removing last element
				else if(m_jointList.m_last == j)
				{
					m_jointList.m_last = j.m_prev;
					m_jointList.m_last.m_next = null;
				}
				// element in between must have both m_next and m_prev
				// and if it does not it means it was removed earlier
				else if(j.m_next && j.m_prev)
				{
					j.m_prev.m_next = j.m_next;
					j.m_next.m_prev = j.m_prev;
				}
			}			

			j.m_next = null;
			j.m_prev = null;
			j.m_last = null;
			
			// Disconnect from island graph.
			bodyA = j.m_bodyA;
			bodyB = j.m_bodyB;
			
			// Wake up connected bodies.
			bodyA.SetAwake(true);
			bodyB.SetAwake(true);
			
			
			// Remove from body 1.
			if(j.m_edgeAList.prev)
			{
				j.m_edgeAList.prev.next = j.m_edgeAList.next;
			}
			
			if(j.m_edgeAList.next)
			{
				j.m_edgeAList.next.prev = j.m_edgeAList.prev;
			}
			
			if(j.m_edgeAList == bodyA.m_jointList)
			{
				bodyA.m_jointList = j.m_edgeAList.next;
			}
			
			j.m_edgeAList.prev = null;
			j.m_edgeAList.next = null;
			
			// Remove from body 2
			if(j.m_edgeBList.prev)
			{
				j.m_edgeBList.prev.next = j.m_edgeBList.next;
			}
			
			if(j.m_edgeBList.next)
			{
				j.m_edgeBList.next.prev = j.m_edgeBList.prev;
			}
			
			if(j.m_edgeBList == bodyB.m_jointList)
			{
				bodyB.m_jointList = j.m_edgeBList.next;
			}
			
			j.m_edgeBList.prev = null;
			j.m_edgeBList.next = null;


			//b2Settings.b2Assert(m_jointCount > 0);
			--m_jointCount;

			// If the joint prevents collisions, then flag any contacts for filtering.
			if(collideConnected == false)
			{
				jointEdge = bodyB.m_contactList;

				while(jointEdge)
				{
					if (jointEdge.other == bodyA)
					{
						// Flag the contact for filtering at the next time step (where either
						// body is awake).
						jointEdge.contact.FlagForFiltering();
					}

					jointEdge = jointEdge.next;
				}
			}
		}

		/**
		 * Add a controller to the world list
		 */
		public function AddController(c:b2Controller):b2Controller
		{
			// check if list is not empty
			if(m_controllerList)
			{
				if(m_controllerList.m_last)
				{
					c.m_prev = m_controllerList.m_last;
					m_controllerList.m_last.m_next = c;
				}
				else
				{
					c.m_prev = m_controllerList; 
					m_controllerList.m_next = c;
				}
				
				m_controllerList.m_last = c;
			}
			else
			{
				m_controllerList = c;
			}

			c.m_world = this;

			return c;
		}

		public function RemoveController(c:b2Controller):void
		{
			// check if there is any controller in the list
			if(m_controllerList)
			{
				// are we removing first element
				if(m_controllerList == c)
				{
					if(c.m_next)
					{
						c.m_next.m_last = m_controllerList.m_last;
						c.m_next.m_prev = null;
					}

					m_controllerList = c.m_next;
				}
					// are we removing last element
				else if(m_controllerList.m_last == c)
				{
					m_controllerList.m_last = c.m_prev;
					m_controllerList.m_last.m_next = null;
				}
				// element in between must have both m_next and m_prev
				// and if it does not it means it was removed earlier
				else if(c.m_next && c.m_prev)
				{
					c.m_prev.m_next = c.m_next;
					c.m_next.m_prev = c.m_prev;
				}

				c.m_next = null;
				c.m_prev = null;
				c.m_last = null;

				c.m_world = null;
			}
		}
		
		/**
		* Enable/disable warm starting. For testing.
		*/
		public function SetWarmStarting(flag: Boolean) : void { m_warmStarting = flag; }
	
		/**
		* Enable/disable continuous physics. For testing.
		*/
		public function SetContinuousPhysics(flag: Boolean) : void { m_continuousPhysics = flag; }
		
		/**
		* Get the number of bodies.
		*/
		public function GetBodyCount():int
		{
			return m_bodyCount;
		}
		
		/**
		* Get the number of joints.
		*/
		public function GetJointCount():int
		{
			return m_jointCount;
		}
		
		/**
		* Get the number of contacts (each may have 0 or more contact points).
		*/
		public function GetContactCount():int
		{
			return m_contactCount;
		}
		
		/**
		* Change the global gravity vector.
		*/
		public function SetGravity(gravity: b2Vec2): void
		{
			m_gravity = gravity;
		}
	
		/**
		* Get the global gravity vector.
		*/
		[Inline]
		final public function GetGravity():b2Vec2
		{
			return m_gravity;
		}
		
		final public function GetWorldTimeStep():b2TimeStep
		{
			return s_b2World_timestep;
		}
	
		/**
		* The world provides a single static ground body with no collision shapes.
		* You can use this to simplify the creation of joints and static shapes.
		*/
		public function GetGroundBody() : b2Body{
			return m_groundBody;
		}

		/**
		* Take a time step. This performs collision detection, integration,
		* and constraint solution.
		* @param timeStep the amount of time to simulate, this should not vary.
		* @param velocityIterations for the velocity constraint solver.
		* @param positionIterations for the position constraint solver.
		*/
		public function Step(dt:Number, velocityIterations:int, positionIterations:int):void
		{
			if (m_flags & e_newFixture)
			{
				m_contactManager.FindNewContacts();
				m_flags &= ~e_newFixture;
			}

			m_flags |= e_locked;

			s_b2World_timestep.dt = dt;
			s_b2World_timestep.velocityIterations = velocityIterations;
			s_b2World_timestep.positionIterations = positionIterations;
			
			if (dt > b2Settings.DECIMAL_ZERO)
			{
				s_b2World_timestep.inv_dt = b2Settings.DECIMAL_ONE / dt;
			}
			else
			{
				s_b2World_timestep.inv_dt = b2Settings.DECIMAL_ZERO;
			}
			
			s_b2World_timestep.dtRatio = m_inv_dt0 * dt;
			
			s_b2World_timestep.warmStarting = m_warmStarting;
			
			// Update contacts.
			m_contactManager.Collide();
			
			// Integrate velocities, solve velocity constraints, and integrate positions.
			if(s_b2World_timestep.dt > b2Settings.DECIMAL_ZERO)
			{
				Solve(s_b2World_timestep);
			}

			// Handle TOI events.
			if(m_continuousPhysics && s_b2World_timestep.dt > b2Settings.DECIMAL_ZERO)
			{
				SolveTOI(s_b2World_timestep);
			}
			
			if(s_b2World_timestep.dt > b2Settings.DECIMAL_ZERO)
			{
				m_inv_dt0 = s_b2World_timestep.inv_dt;
			}
	
			m_flags &= ~e_locked;
		}
		
		private var cf_body:b2Body;
		
		/**
		 * Call this after you are done with time steps to clear the forces. You normally
		 * call this after each call to Step, unless you are performing sub-steps.
		 */
		[Inline]
		final public function ClearForces():void
		{
			for(cf_body = m_bodyList; cf_body; cf_body = cf_body.m_next)
			{
				cf_body.m_force.x = b2Settings.DECIMAL_ZERO;
				cf_body.m_force.y = b2Settings.DECIMAL_ZERO;
				
				cf_body.m_torque = b2Settings.DECIMAL_ZERO;
			}
		}
		
		static private var s_xf:b2Transform = new b2Transform();
		static private var debugDrawColor:b2Color = new b2Color(0, 0, 0);
		
		private static var _vertice:b2Vec2;
		private static var dd_vs:Array = new Array(new b2Vec2(),new b2Vec2(),new b2Vec2(),new b2Vec2());
		
		/**
		 * Call this to draw shapes and other debug draw data.
		 */
		public function DrawDebugData():void
		{	
			if(m_debugDraw == null)
			{
				return;
			}
			
			m_debugDraw.clear();
	
			var flags:uint = m_debugDraw.GetFlags();
			
			var i:int;
			var b:b2Body;
			var f:b2Fixture;
			var s:b2Shape;
			var j:b2Joint;
			var bp:IBroadPhase;
			
			var xf:b2Transform;
			
			var b1:b2AABB = b2Pool.getAABB();
			var b2:b2AABB = b2Pool.getAABB();
				
			if(flags & b2DebugDraw.e_shapeBit)
			{
				for(b = m_bodyList; b; b = b.m_next)
				{
					xf = b.m_xf;

					for(f = b.m_fixtureList; f; f = f.m_next)
					{
						s = f.m_shape;

						if(!b.m_isActive)
						{
							debugDrawColor.Set(0.5, 0.5, 0.3);
							DrawShape(s, xf, debugDrawColor);
						}
						else if (b.m_type == b2Body.b2_staticBody)
						{
							debugDrawColor.Set(0.5, 0.9, 0.5);
							DrawShape(s, xf, debugDrawColor);
						}
						else if (b.m_type == b2Body.b2_kinematicBody)
						{
							debugDrawColor.Set(0.5, 0.5, 0.9);
							DrawShape(s, xf, debugDrawColor);
						}
						else if (b.IsAwake() == false)
						{
							debugDrawColor.Set(0.6, 0.6, 0.6);
							DrawShape(s, xf, debugDrawColor);
						}
						else
						{
							debugDrawColor.Set(0.9, 0.7, 0.7);
							DrawShape(s, xf, debugDrawColor);
						}
					}
				}
			}
			
			if(flags & b2DebugDraw.e_jointBit)
			{
				for(j = m_jointList; j; j = j.m_next)
				{
					DrawJoint(j);
				}
			}
			
			if(flags & b2DebugDraw.e_controllerBit)
			{
				for(var c:b2Controller = m_controllerList; c; c = c.m_next)
				{
					c.Draw(m_debugDraw);
				}
			}
			
			if(flags & b2DebugDraw.e_pairBit)
			{
				debugDrawColor.Set(0.3, 0.9, 0.9);
				
				var cA:b2Vec2 = b2Pool.getVector2(0, 0);
				var cB:b2Vec2 = b2Pool.getVector2(0, 0);
				
				for (var contact:b2Contact = m_contactManager.m_contactList; contact; contact = contact.m_next)
				{
					var fixtureA:b2Fixture = contact.GetFixtureA();
					var fixtureB:b2Fixture = contact.GetFixtureB();
	
					fixtureA.GetAABB().GetCenter(cA);
					fixtureB.GetAABB().GetCenter(cB);
	
					m_debugDraw.DrawSegment(cA, cB, debugDrawColor.color);
				}
				
				b2Pool.putVector2(cA);
				b2Pool.putVector2(cB);
			}
			
			if (flags & b2DebugDraw.e_aabbBit)
			{
				bp = m_contactManager.m_broadPhase;
				
				for(b = m_bodyList; b; b = b.m_next)
				{
					if(!b.m_isActive)
					{
						continue;
					}

					for (f = b.m_fixtureList; f; f = f.m_next)
					{
						var aabb:b2AABB = bp.GetFatAABB(f.m_proxy);
						
						_vertice = dd_vs[0];
						_vertice.x = aabb.lowerBound.x;
						_vertice.y = aabb.lowerBound.y;
						
						
						_vertice = dd_vs[1];
						_vertice.x = aabb.upperBound.x;
						_vertice.y = aabb.lowerBound.y;
						
						_vertice = dd_vs[2];
						_vertice.x = aabb.upperBound.x;
						_vertice.y = aabb.upperBound.y;
						
						_vertice = dd_vs[3];
						_vertice.x = aabb.lowerBound.x;
						_vertice.y = aabb.upperBound.y;

						
						m_debugDraw.DrawPolygon(dd_vs, 4, debugDrawColor.color);
					}
				}
			}
			
			if (flags & b2DebugDraw.e_centerOfMassBit)
			{
				for (b = m_bodyList; b; b = b.m_next)
				{
					xf = s_xf;
					xf.R = b.m_xf.R;
					xf.position = b.GetWorldCenter();
					m_debugDraw.DrawTransform(xf);
				}
			}

			b2Pool.putAABB(b1);
			b2Pool.putAABB(b2);
		}
	
		/**
		 * Query the world for all fixtures that potentially overlap the
		 * provided AABB.
		 * @param callback a user implemented callback class. It should match signature
		 * <code>function Callback(fixture:b2Fixture):Boolean</code>
		 * Return true to continue to the next fixture.
		 * @param aabb the query box.
		 */
		public function QueryAABB(callback:Function, aabb:b2AABB):void
		{
			/*var broadPhase:IBroadPhase = m_contactManager.m_broadPhase;
			function WorldQueryWrapper(proxy:*):Boolean
			{
				return callback(broadPhase.GetUserData(proxy));
			}
			broadPhase.Query(WorldQueryWrapper, aabb);*/
		}
		
		private static var _queryShapeAABB:b2AABB = new b2AABB();
		private static var queryShapeDefaultTransform:b2Transform = new b2Transform();

		private static var _queryShapeBroadPhaseCallback:b2WorldQueryShapeBroadPhaseCallback = new b2WorldQueryShapeBroadPhaseCallback();
		
		/**
		 * Query the world for all fixtures that precisely overlap the
		 * provided transformed shape.
		 * @param callback a user implemented callback class. It should match signature
		 * <code>function Callback(fixture:b2Fixture):Boolean</code>
		 * Return true to continue to the next fixture.
		 * @asonly
		 */
		public function QueryShape(fixtureFoundCallback:Function,
								   shape:b2Shape,
								   transform:b2Transform = null,
								   nothingIsFoundCallback:Function = null):void
		{
			if(transform == null)
			{
				transform = queryShapeDefaultTransform;
				transform.SetIdentity();
			}

			_queryShapeBroadPhaseCallback.broadPhase = m_contactManager.m_broadPhase;
			_queryShapeBroadPhaseCallback.shape = shape;
			_queryShapeBroadPhaseCallback.transform = transform;
			_queryShapeBroadPhaseCallback.isFixtureFound = false;
			_queryShapeBroadPhaseCallback.fixtureFoundCallback = fixtureFoundCallback;

			shape.ComputeAABB(_queryShapeAABB, transform);

			_queryShapeBroadPhaseCallback.broadPhase.Query(_queryShapeBroadPhaseCallback, _queryShapeAABB);

			if(_queryShapeBroadPhaseCallback.isFixtureFound == false && nothingIsFoundCallback is Function)
			{
				nothingIsFoundCallback();
			}
		}
		
		private static var _queryPointBroadPhaseCallback:b2WorldQueryPointBroadPhaseCallback = new b2WorldQueryPointBroadPhaseCallback();
		
		/**
		 * Query the world for all fixtures that contain a point.
		 * @param callback a user implemented callback class. It should match signature
		 * <code>function Callback(fixture:b2Fixture):Boolean</code>
		 * @param callback a user implemented callback class. It should match signature
		 * Return true to continue to the next fixture.
		 * @asonly
		 */
		
		private  var _queryPointAABB:b2AABB = new b2AABB();

		public function QueryPoint(fixtureFoundCallback:Function,
								   point:b2Vec2,
								   nothingIsFoundCallback:Function = null,
								   shouldExcludeFixtureCallback:Function = null):void
		{
			_queryPointBroadPhaseCallback.broadPhase = m_contactManager.m_broadPhase;
			_queryPointBroadPhaseCallback.point = point;
			_queryPointBroadPhaseCallback.isFixtureFound = false;
			_queryPointBroadPhaseCallback.fixtureFoundCallback = fixtureFoundCallback;
			_queryPointBroadPhaseCallback.shouldExcludeFixtureCallback = shouldExcludeFixtureCallback;

			// Make a small box.
			_queryPointAABB.lowerBound.x = point.x - b2Settings.b2_linearSlop;
			_queryPointAABB.lowerBound.y = point.y - b2Settings.b2_linearSlop;

			_queryPointAABB.upperBound.x = point.x + b2Settings.b2_linearSlop;
			_queryPointAABB.upperBound.y = point.y + b2Settings.b2_linearSlop;

			_queryPointBroadPhaseCallback.broadPhase.Query(_queryPointBroadPhaseCallback, _queryPointAABB);

			if(_queryPointBroadPhaseCallback.isFixtureFound == false && nothingIsFoundCallback is Function)
			{
				nothingIsFoundCallback();
			}
		}
		
		private var _rayCastContactPoint:b2Vec2 = new b2Vec2(0, 0);
		private var _rayCastInput:b2RayCastInput = new b2RayCastInput();
		private var _rayCastOutput:b2RayCastOutput = new b2RayCastOutput();
		private var _currentRayCastPointA:b2Vec2;
		private var _currentRayCastPointB:b2Vec2;
		private var _currentRayCastBroadPhase:b2DynamicTreeBroadPhase;
		private var _currentRayCastCallback:IB2WorldRayCastCallback;

		private var rayCastHit:Boolean;
		
		[Inline]
		final public function RayCastCallback(input:b2RayCastInput, proxy:b2DynamicTreeNode):Number
		{
			rayCastHit = proxy.userData.RayCast(_rayCastOutput, input);
			
			if(rayCastHit)
			{	
				_rayCastContactPoint.x = (b2Settings.DECIMAL_ONE - _rayCastOutput.fraction) * _currentRayCastPointA.x + _rayCastOutput.fraction * _currentRayCastPointB.x;
				_rayCastContactPoint.y = (b2Settings.DECIMAL_ONE - _rayCastOutput.fraction) * _currentRayCastPointA.y + _rayCastOutput.fraction * _currentRayCastPointB.y;
				
				return _currentRayCastCallback.rayCastCallback(
					proxy.userData,
					_rayCastContactPoint,
					_rayCastOutput.normal,
					_rayCastOutput.fraction
				);
			}
			
			return input.maxFraction;
		}
	
		/**
		 * Ray-cast the world for all fixtures in the path of the ray. Your callback
		 * Controls whether you get the closest point, any point, or n-points
		 * The ray-cast ignores shapes that contain the starting point
		 * @param callback A callback function which must be of signature:
		 * <code>function Callback(fixture:b2Fixture,    // The fixture hit by the ray
		 * point:b2Vec2,         // The point of initial intersection
		 * normal:b2Vec2,        // The normal vector at the point of intersection
		 * fraction:Number       // The fractional length along the ray of the intersection
		 * ):Number
		 * </code>
		 * Callback should return the new length of the ray as a fraction of the original length.
		 * By returning 0, you immediately terminate.
		 * By returning 1, you continue wiht the original ray.
		 * By returning the current fraction, you proceed to find the closest point.
		 * @param point1 the ray starting point
		 * @param point2 the ray ending point
		 */
		[Inline]
		final public function RayCast(callback:IB2WorldRayCastCallback, point1:b2Vec2, point2:b2Vec2):void
		{
			_currentRayCastCallback = callback;
			_currentRayCastPointA = point1;
			_currentRayCastPointB = point2;
			_currentRayCastBroadPhase = m_contactManager.m_broadPhase as b2DynamicTreeBroadPhase;
	
			_rayCastInput.p1.x = point1.x;
			_rayCastInput.p1.y = point1.y;
			
			_rayCastInput.p2.x = point2.x;
			_rayCastInput.p2.y = point2.y;
	
			_currentRayCastBroadPhase.m_tree.RayCast(this, _rayCastInput);
		}
		
		public function RayCastOne(point1:b2Vec2, point2:b2Vec2):b2Fixture
		{
			var result:b2Fixture;
	
			/*function RayCastOneWrapper(fixture:b2Fixture, point:b2Vec2, normal:b2Vec2, fraction:Number):Number
			{
				result = fixture;
				return fraction;
			}
			
			RayCast(RayCastOneWrapper, point1, point2);*/
	
			return result;
		}
		
		public function RayCastAll(point1:b2Vec2, point2:b2Vec2):Vector.<b2Fixture>
		{
			var result:Vector.<b2Fixture> = new Vector.<b2Fixture>();
			/*function RayCastAllWrapper(fixture:b2Fixture, point:b2Vec2, normal:b2Vec2, fraction:Number):Number
			{
				result[result.length] = fixture;
				return 1;
			}
			RayCast(RayCastAllWrapper, point1, point2);*/
			return result;
		}
	
		/**
		* Get the world body list. With the returned body, use b2Body::GetNext to get
		* the next body in the world list. A NULL body indicates the end of the list.
		* @return the head of the world body list.
		*/
		[Inline]
		final public function GetBodyList():b2Body
		{
			return m_bodyList;
		}
	
		/**
		* Get the world joint list. With the returned joint, use b2Joint::GetNext to get
		* the next joint in the world list. A NULL joint indicates the end of the list.
		* @return the head of the world joint list.
		*/
		[Inline]
		final public function GetJointList():b2Joint
		{
			return m_jointList;
		}
	
		/**
		 * Get the world contact list. With the returned contact, use b2Contact::GetNext to get
		 * the next contact in the world list. A NULL contact indicates the end of the list.
		 * @return the head of the world contact list.
		 * @warning contacts are 
		 */
		
		[Inline]
		final public function GetContactList():b2Contact
		{
			return m_contactList;
		}
		
		/**
		 * Is the world locked (in the middle of a time step).
		 */
		[Inline]
		final public function IsLocked():Boolean
		{
			return (m_flags & e_locked) > 0;
		}
	
		//--------------- Internals Below -------------------
		// Internal yet public to make life easier.
	
		// Find islands, integrate and solve constraints, solve position constraints
		private var s_stack:Vector.<b2Body> = new Vector.<b2Body>;
		
		private var s_controller:b2Controller;
		private var s_contact:b2Contact;
		private var s_joint:b2Joint;

		private var s_stackCount:int;
		
		private var s_seedBody:b2Body;
		
		private var s_body:b2Body;
		private var s_otherBody:b2Body;
		private var c_contactEdge:b2ContactEdge;
		private var s_jointEdge:b2JointEdge;
		
		private var s_i:int, si_len:int;
		
		[Inline]
		final public function Solve(step:b2TimeStep):void
		{
			// Step all controllers
			for(s_controller = m_controllerList; s_controller; s_controller = s_controller.m_next)
			{
				s_controller.Step(step);
			}
			
			m_island.Initialize(
				m_bodyCount,
				m_contactCount,
				m_jointCount,
				m_contactManager.m_contactListener,
				m_contactSolver
			);
			
			// Clear all the island flags.
			for(s_body = m_bodyList; s_body; s_body = s_body.m_next)
			{
				s_body.m_isIsland = false;
			}
	
			for (s_contact = m_contactList; s_contact; s_contact = s_contact.m_next)
			{
				s_contact.m_isIsland = false;
			}
	
			for(s_joint = m_jointList; s_joint; s_joint = s_joint.m_next)
			{
				s_joint.m_islandFlag = false;
			}
			
			for(s_seedBody = m_bodyList; s_seedBody; s_seedBody = s_seedBody.m_next)
			{
				if(s_seedBody.m_isIsland)
				{
					continue;
				}
				
				if(!s_seedBody.m_isAwake || !s_seedBody.m_isActive)
				{
					continue;
				}
				
				// The seed can be dynamic or kinematic.
				if(s_seedBody.m_type == b2Body.b2_staticBody)
				{
					continue;
				}
				
				// Reset island and stack.				
				m_island.m_bodyCount = 0;
				m_island.m_contactCount = 0;
				m_island.m_jointCount = 0;
				
				s_stackCount = 0;
				
				s_stack[s_stackCount++] = s_seedBody;

				s_seedBody.m_isIsland = true;
				
				// Perform a depth first search (DFS) on the constraint graph.
				while(s_stackCount > 0)
				{
					// Grab the next body off the stack and add it to the island.
					s_body = s_stack[--s_stackCount];
					
					//b2Assert(b.IsActive() == true);
					m_island.AddBody(s_body);
					
					// Make sure the body is awake.
					if(!s_body.m_isAwake)
					{
						s_body.SetAwake(true);
					}
					
					// To keep islands as small as possible, we don't
					// propagate islands across static bodies.
					if (s_body.m_type == b2Body.b2_staticBody)
					{
						continue;
					}
					
					s_otherBody = null;

					// Search all contacts connected to this body.
					for(c_contactEdge = s_body.m_contactList; c_contactEdge; c_contactEdge = c_contactEdge.next)
					{
						// Has this contact already been added to an island?
						if(c_contactEdge.contact.m_isIsland)
						{
							continue;
						}
						
						// Is this contact solid and touching?
						if(c_contactEdge.contact.m_isSensor ||
						  !c_contactEdge.contact.m_isEnabled ||
						  !c_contactEdge.contact.m_isTouching)
						{
							continue;
						}
						
						m_island.AddContact(c_contactEdge.contact);
						c_contactEdge.contact.m_isIsland = true;
						
						//var other:b2Body = ce.other;
						s_otherBody = c_contactEdge.other;
						
						// Was the other body already added to this island?
						if (s_otherBody.m_isIsland)
						{
							continue;
						}
						
						//b2Settings.b2Assert(stackCount < stackSize);
						s_stack[s_stackCount++] = s_otherBody;

						s_otherBody.m_isIsland = true;
					}
					
					// Search all joints connect to this body.
					for(s_jointEdge = s_body.m_jointList; s_jointEdge; s_jointEdge = s_jointEdge.next)
					{
						if(s_jointEdge.joint.m_islandFlag == true)
						{
							continue;
						}
						
						s_otherBody = s_jointEdge.other;
	
						if(!s_otherBody.m_isActive)
						{
							// check to see if we should simulate inactive joints.
							if(s_jointEdge.joint.m_simulateInactive == false)
							{
								continue;
							}
						}
						
						m_island.AddJoint(s_jointEdge.joint);
						s_jointEdge.joint.m_islandFlag = true;
						
						if(s_otherBody.m_isIsland)
						{
							continue;
						}
						
						//b2Settings.b2Assert(stackCount < stackSize);
						s_stack[s_stackCount++] = s_otherBody;

						s_otherBody.m_isIsland = true;
					}
				}
				
				m_island.Solve(step, m_gravity, m_allowSleep);
				
				// Post solve cleanup.
				for(s_i = 0; s_i < m_island.m_bodyCount; ++s_i)
				{
					// Allow static bodies to participate in other islands.
					s_body = m_island.m_bodies[s_i];
					
					if(s_body.m_type == b2Body.b2_staticBody)
					{
						s_body.m_isIsland = false;
					}
				}
			}

			for(s_i = 0, si_len = s_stack.length; s_i < si_len; ++s_i)
			{
				if(!s_stack[s_i])
				{
					break;
				}
				
				s_stack[s_i] = null;
			}
			
			// Synchronize fixutres, check for out of range bodies.
			for(s_body = m_bodyList; s_body; s_body = s_body.m_next)
			{
				if(!s_body.m_isAwake || !s_body.m_isActive)
				{
					continue;
				}
				
				if(s_body.m_type == b2Body.b2_staticBody)
				{
					continue;
				}
				
				// Update fixtures (for broad-phase).
				s_body.SynchronizeFixtures();
			}
			
			// Look for new contacts.
			m_contactManager.FindNewContacts();
		}
		
		private var st_toi:Number;
		private var st_minTOI:Number;
		private var st_t0:Number;
		
		private var st_queueStart:int;
		private var st_queueSize:int;
		
		private var st_body:b2Body;
		private var st_otherBody:b2Body;
		
		private var st_fixtureA:b2Fixture;
		private var st_fixtureB:b2Fixture;
		
		private var st_bodyA:b2Body;
		private var st_bodyB:b2Body;
		
		private var st_contactEdge:b2ContactEdge;
		private var st_joint:b2Joint;
		private var st_contact:b2Contact;
		private var st_minContact:b2Contact;
		private var st_seedBody:b2Body;

		private var st_jointEdge:b2JointEdge
		
		private var st_i:int;
		
		private static const TOI_CONST:Number = b2Settings.DECIMAL_ONE - 100.0 * Number.MIN_VALUE;
		public var stoi_input:b2TOIInput = new b2TOIInput();

		
		// Find TOI contacts and solve them.
		[Inline]
		final public function SolveTOI(step:b2TimeStep):void
		{
			m_island.Initialize(
				m_bodyCount,
				b2Settings.b2_maxTOIContactsPerIsland,
				b2Settings.b2_maxTOIJointsPerIsland,
				m_contactManager.m_contactListener,
				m_contactSolver
			);
			
			for(st_body = m_bodyList; st_body; st_body = st_body.m_next)
			{
				st_body.m_isIsland = false;
				st_body.m_sweep.t0 = b2Settings.DECIMAL_ZERO;
			}

			for(st_contact = m_contactList; st_contact; st_contact = st_contact.m_next)
			{
				// Invalidate TOI
				//st_contact.m_flags &= ~(b2Contact.e_toiFlag | b2Contact.e_islandFlag);
				st_contact.m_isIsland = false;
				st_contact.m_isTOI = false;
			}
			
			for(st_joint = m_jointList; st_joint; st_joint = st_joint.m_next)
			{
				st_joint.m_islandFlag = false;
			}
			
			st_toi = b2Settings.DECIMAL_ONE;
			
			// Find TOI events and solve them.
			for(;;)
			{
				// Find the first TOI.
				st_minContact = null;
				st_minTOI = b2Settings.DECIMAL_ONE;
				
				for(st_contact = m_contactList; st_contact; st_contact = st_contact.m_next)
				{
					// Can this contact generate a solid TOI contact?
	 				if(st_contact.m_isSensor ||
					   !st_contact.m_isEnabled ||
					   !st_contact.m_isContinuous)
					{
						continue;
					}
					
					// TODO_ERIN keep a counter on the contact, only respond to M TOIs per contact.
					
					st_toi = b2Settings.DECIMAL_ONE;
	
					if(st_contact.m_isTOI)
					{
						// This contact has a valid cached TOI.
						st_toi = st_contact.m_toi;
					}
					else
					{
						// Compute the TOI for this contact.
						st_bodyA = st_contact.m_fixtureA.m_body;
						st_bodyB = st_contact.m_fixtureB.m_body;
						
						if((st_bodyA.m_type != b2Body.b2_dynamicBody || !st_bodyA.m_isAwake) &&
							(st_bodyB.m_type != b2Body.b2_dynamicBody || !st_bodyB.m_isAwake))
						{
							continue;
						}
						
						// Put the sweeps onto the same time interval.
						st_t0 = st_bodyA.m_sweep.t0;
						
						if (st_bodyA.m_sweep.t0 < st_bodyB.m_sweep.t0)
						{
							st_t0 = st_bodyB.m_sweep.t0;
							st_bodyA.m_sweep.Advance(st_t0);
						}
						else if (st_bodyB.m_sweep.t0 < st_bodyA.m_sweep.t0)
						{
							st_t0 = st_bodyA.m_sweep.t0;
							st_bodyB.m_sweep.Advance(st_t0);
						}
						
						//b2Settings.b2Assert(t0 < 1.0f);
						
						// Compute the time of impact.
						//st_toi = st_contact.ComputeTOI(st_bodyA.m_sweep, st_bodyB.m_sweep);

						stoi_input.proxyA.Set(st_contact.m_fixtureA.m_shape);
						stoi_input.proxyB.Set(st_contact.m_fixtureB.m_shape);
						stoi_input.sweepA = st_bodyA.m_sweep;
						stoi_input.sweepB = st_bodyB.m_sweep;
						stoi_input.tolerance = b2Settings.b2_linearSlop;
						
						st_toi = b2TimeOfImpact.TimeOfImpact(stoi_input);
						
						//b2Settings.b2Assert(b2Settings.DECIMAL_ZERO <= st_toi && st_toi <= b2Settings.DECIMAL_ONE);
						if(!(b2Settings.DECIMAL_ZERO <= st_toi && st_toi <= b2Settings.DECIMAL_ONE))
						{
							throw Error('TOI is invalid');
						}
						
						// If the TOI is in range ...
						if (st_toi > b2Settings.DECIMAL_ZERO && st_toi < b2Settings.DECIMAL_ONE)
						{
							st_toi = (b2Settings.DECIMAL_ONE - st_toi) * st_t0 + st_toi;
							if (st_toi > 1) st_toi = 1;
						}
						
						
						st_contact.m_toi = st_toi;
						st_contact.m_isTOI = true;
					}
					
					if(Number.MIN_VALUE < st_toi && st_toi < st_minTOI)
					{
						// This is the minimum TOI found so far.
						st_minContact = st_contact;
						st_minTOI = st_toi;
					}
				}
				
				if (st_minContact == null || TOI_CONST < st_minTOI)
				{
					// No more TOI events. Done!
					break;
				}
				
				// Advance the bodies to the TOI.
				st_bodyA = st_minContact.m_fixtureA.m_body;
				st_bodyB = st_minContact.m_fixtureB.m_body;
				
				s_backupA.Set(st_bodyA.m_sweep);
				s_backupB.Set(st_bodyB.m_sweep);

				st_bodyA.Advance(st_minTOI);
				st_bodyB.Advance(st_minTOI);
				
				// The TOI contact likely has some new contact points.
				st_minContact.Update(m_contactManager.m_contactListener);
				st_minContact.m_isTOI = false;
				
				// Is the contact solid?
				if(st_minContact.m_isSensor || !st_minContact.m_isEnabled)
				{
					// Restore the sweeps
					st_bodyA.m_sweep.Set(s_backupA);
					st_bodyB.m_sweep.Set(s_backupB);
					
					st_bodyA.SynchronizeTransform();
					st_bodyB.SynchronizeTransform();
					
					continue;
				}
				
				// Did numerical issues prevent contact point from being generated
				if(st_minContact.IsTouching() == false)
				{
					// Give up on this TOI
					continue;
				}
				
				// Build the TOI island. We need a dynamic seed.
				st_seedBody = st_bodyA;
	
				if(st_seedBody.m_type != b2Body.b2_dynamicBody)
				{
					st_seedBody = st_bodyB;
				}
				
				// Reset island and queue.
				m_island.Clear();
	
				st_queueStart = 0;	//start index for queue
				st_queueSize = 0;	//elements in queue
	
				s_queue[st_queueStart + st_queueSize++] = st_seedBody;
				st_seedBody.m_isIsland = true;
				
				// Perform a breadth first search (BFS) on the contact graph.
				while(st_queueSize > 0)
				{
					// Grab the next body off the stack and add it to the island.
					st_body = s_queue[st_queueStart++];
					--st_queueSize;
					
					m_island.AddBody(st_body);
					
					// Make sure the body is awake.
					if(!st_body.m_isAwake)
					{
						st_body.SetAwake(true);
					}
					
					// To keep islands as small as possible, we don't
					// propagate islands across static or kinematic bodies.
					if(st_body.m_type != b2Body.b2_dynamicBody)
					{
						continue;
					}
					
					// Search all contacts connected to this body.
					for(st_contactEdge = st_body.m_contactList; st_contactEdge; st_contactEdge = st_contactEdge.next)
					{
						// Does the TOI island still have space for contacts?
						if(m_island.m_contactCount == m_island.m_contactCapacity)
						{
							break;
						}
						
						// Has this contact already been added to an island?
						if(st_contactEdge.contact.m_isIsland)
						{
							continue;
						}
						
						// Skip sperate, sensor, or disabled contacts.
						if (st_contactEdge.contact.m_isSensor ||
							!st_contactEdge.contact.m_isEnabled ||
							!st_contactEdge.contact.m_isTouching)
						{
							continue;
						}
						
						m_island.AddContact(st_contactEdge.contact);
						st_contactEdge.contact.m_isIsland = true;
						
						// Update other body.
						st_otherBody = st_contactEdge.other;
						
						// Was the other body already added to this island?
						if(st_otherBody.m_isIsland)
						{
							continue;
						}
						
						// Synchronize the connected body.
						if(st_otherBody.m_type != b2Body.b2_staticBody)
						{
							st_otherBody.Advance(st_minTOI);
							st_otherBody.SetAwake(true);
						}
						
						//b2Settings.b2Assert(queueStart + queueSize < queueCapacity);
						s_queue[st_queueStart + st_queueSize] = st_otherBody;
						++st_queueSize;
						
						st_otherBody.m_isIsland = true;
					}
					
					for(st_jointEdge = st_body.m_jointList; st_jointEdge; st_jointEdge = st_jointEdge.next) 
					{
						if(m_island.m_jointCount == m_island.m_jointCapacity)
						{
							continue;							
						}
						
						if(st_jointEdge.joint.m_islandFlag)
						{
							continue;							
						}
						
						st_otherBody = st_jointEdge.other;

						if(!st_otherBody.m_isActive)
						{
							continue;
						}
						
						m_island.AddJoint(st_jointEdge.joint);
						st_jointEdge.joint.m_islandFlag = true;
						
						if(st_otherBody.m_isIsland)
						{
							continue;							
						}
							
						// Synchronize the connected body.
						if (st_otherBody.m_type != b2Body.b2_staticBody)
						{
							st_otherBody.Advance(st_minTOI);
							st_otherBody.SetAwake(true);
						}

						s_queue[st_queueStart + st_queueSize] = st_otherBody;
						++st_queueSize;

						st_otherBody.m_isIsland = true;
					}
				}

				s_TOI_Timestep.warmStarting = false;
				s_TOI_Timestep.dt = (b2Settings.DECIMAL_ONE - st_minTOI) * step.dt;
				s_TOI_Timestep.inv_dt = b2Settings.DECIMAL_ONE / s_TOI_Timestep.dt;
				s_TOI_Timestep.dtRatio = b2Settings.DECIMAL_ZERO;
				s_TOI_Timestep.velocityIterations = step.velocityIterations;
				s_TOI_Timestep.positionIterations = step.positionIterations;
				
				m_island.SolveTOI(s_TOI_Timestep);
				
				// Post solve cleanup.
				for(st_i = 0; st_i < m_island.m_bodyCount; ++st_i)
				{
					// Allow bodies to participate in future TOI islands.
					st_body = m_island.m_bodies[st_i];
					st_body.m_isIsland = false;
					
					if(!st_body.m_isAwake)
					{
						continue;
					}
					
					if(st_body.m_type != b2Body.b2_dynamicBody)
					{
						continue;
					}
					
					// Update fixtures (for broad-phase).
					st_body.SynchronizeFixtures();
					
					// Invalidate all contact TOIs associated with this body. Some of these
					// may not be in the island because they were not touching.
					for(st_contactEdge = st_body.m_contactList; st_contactEdge; st_contactEdge = st_contactEdge.next)
					{
						st_contactEdge.contact.m_isTOI = false;
					}
				}
				
				for(st_i = 0; st_i < m_island.m_contactCount; ++st_i)
				{
					// Allow contacts to participate in future TOI islands.
					st_contact = m_island.m_contacts[st_i];
					//st_contact.m_flags &= ~(b2Contact.e_toiFlag | b2Contact.e_islandFlag);
					st_contact.m_isIsland = false;
					st_contact.m_isTOI = false;
				}
				
				for(st_i = 0; st_i < m_island.m_jointCount;++st_i)
				{
					// Allow joints to participate in future TOI islands
					st_joint = m_island.m_joints[st_i];
					st_joint.m_islandFlag = false;
				}

				// Commit fixture proxy movements to the broad-phase so that new contacts are created.
				// Also, some contacts can be destroyed.
				m_contactManager.FindNewContacts();

			} // END OF for(;;)

		}
		
		static private var s_jointColor:b2Color = new b2Color(0.5, 0.8, 0.8);
		//
		public function DrawJoint(joint:b2Joint):void
		{
			var b1:b2Body = joint.GetBodyA();
			var b2:b2Body = joint.GetBodyB();
			var xf1:b2Transform = b1.m_xf;
			var xf2:b2Transform = b2.m_xf;
			
			if(xf1 == null || xf1.position == null || xf2 == null || xf2.position == null)
			{
				return;
			}
			
			var x1:b2Vec2 = xf1.position;
			var x2:b2Vec2 = xf2.position;
			var p1:b2Vec2 = b2Pool.getVector2(0, 0);
			
			joint.GetAnchorA(p1);
			
			var p2:b2Vec2 = b2Pool.getVector2(0, 0);
			
			joint.GetAnchorB(p2);
			
			//b2Color color(0.5f, 0.8f, 0.8f);
			var color:b2Color = s_jointColor;
			
			switch (joint.m_type)
			{
				case b2Joint.e_distanceJoint:
					m_debugDraw.DrawSegment(p1, p2, color.color);
				break;
			
				case b2Joint.e_pulleyJoint:
				
					var pulley:b2PulleyJoint = (joint as b2PulleyJoint);
					var s1:b2Vec2 = b2Pool.getVector2(0, 0);
					
					pulley.GetGroundAnchorA(s1);
					
					var s2:b2Vec2 = b2Pool.getVector2(0, 0);
					
					pulley.GetGroundAnchorB(s2);
					
					m_debugDraw.DrawSegment(s1, p1, color.color);
					m_debugDraw.DrawSegment(s2, p2, color.color);
					m_debugDraw.DrawSegment(s1, s2, color.color);
					
					b2Pool.putVector2(s1);
					b2Pool.putVector2(s2);
				
				break;
			
				case b2Joint.e_mouseJoint:
					m_debugDraw.DrawSegment(p1, p2, color.color);
				break;
			
				default:
					
					if(b1 != m_groundBody)
					{
						m_debugDraw.DrawSegment(x1, p1, color.color);					
					}
					
					m_debugDraw.DrawSegment(p1, p2, color.color);
					
					if (b2 != m_groundBody)
					{
						m_debugDraw.DrawSegment(x2, p2, color.color);
					}
	
				break;
			}
			
			b2Pool.putVector2(p1);
			b2Pool.putVector2(p2);
		}
		
		protected var _debuDrawPolygonBounds:Rectangle = new Rectangle(0, 0, 0, 0);
		protected var _debuDrawPolygonVertices:Vector.<b2Vec2> = new Vector.<b2Vec2>;
		
		public function DrawShape(shape:b2Shape, xf:b2Transform, color:b2Color):void
		{
			switch (shape.m_type)
			{
				case b2Shape.e_circleShape:
					
					var circle:b2CircleShape = (shape as b2CircleShape);
					
					var center:b2Vec2 = b2Pool.getVector2(0, 0);
					
					b2Math.MulX(xf, circle.m_p, center);
					
					var radius:Number = circle.m_radius;
					var axis:b2Vec2 = b2Pool.getVector2(xf.R.col1.x, xf.R.col1.y);
					
					m_debugDraw.DrawSolidCircle(center, radius, axis, color.color);
					
					b2Pool.putVector2(center);
					b2Pool.putVector2(axis);
					
				break;
				
				case b2Shape.e_polygonShape:
		
						var i:int;
						var poly:b2PolygonShape = (shape as b2PolygonShape);
						var vertexCount:int = poly.GetVertexCount();
						var localVertices:Array = poly.GetVertices();
				
						var minX:Number = Number.MAX_VALUE;
						var maxX:Number = 0;
						
						var minY:Number = Number.MAX_VALUE;
						var maxY:Number = 0;
						
						var currentX:Number;
						var currentY:Number;
						var calculatedVector:b2Vec2;
						
						for(i = 0; i < vertexCount; ++i)
						{
							calculatedVector = b2Pool.getVector2(0, 0);
							
							b2Math.MulX(xf, localVertices[i], calculatedVector);
						
							_debuDrawPolygonVertices.push(calculatedVector);
							
							currentX = calculatedVector.x;
							currentY = calculatedVector.y;
							
							minX = Math.min(minX, currentX);
							maxX = Math.max(maxX, currentX);
							
							minY = Math.min(minY, currentY);
							maxY = Math.max(maxY, currentY);
						}
						
						var width:Number = maxX - minX;
						var height:Number = maxY - minY;
						
						_debuDrawPolygonBounds.x = minX;
						_debuDrawPolygonBounds.y = minY;
						_debuDrawPolygonBounds.width = width;
						_debuDrawPolygonBounds.height = height;
						
						m_debugDraw.DrawSolidPolygon(_debuDrawPolygonVertices, vertexCount, color.color, _debuDrawPolygonBounds);
						
						i = _debuDrawPolygonVertices.length;
						
						while(--i > -1)
						{
							b2Pool.putVector2(_debuDrawPolygonVertices.pop());
						}
		
				break;
				
				case b2Shape.e_edgeShape:
					
					var edge: b2EdgeShape = shape as b2EdgeShape;
					var v1:b2Vec2 = b2Pool.getVector2(0, 0);
					var v2:b2Vec2 = b2Pool.getVector2(0, 0);
					
					b2Math.MulX(xf, edge.GetVertex1(), v1)
					b2Math.MulX(xf, edge.GetVertex2(), v2)
					
					m_debugDraw.DrawSegment(
						v1,
						v2,
						color.color
					);
					
					b2Pool.putVector2(v1);
					b2Pool.putVector2(v2);
	
				break;
			}
		}
		
		public var m_flags:int;
	
		public var m_contactManager:b2ContactManager = new b2ContactManager();
		
		public var m_bodyList:b2Body;
		public var m_contactList:b2Contact;
		public var m_contactCount:int;
		
		// These two are stored purely for efficiency purposes, they don't maintain
		// any data outside of a call to Step
		private var m_contactSolver:b2ContactSolver = new b2ContactSolver();
		private var m_island:b2Island = new b2Island();
	
		private var m_jointList:b2Joint;
		private var m_controllerList:b2Controller;
	
		private var m_bodyCount:int;
		private var m_jointCount:int;
		
		public var m_gravity:b2Vec2;
		private var m_allowSleep:Boolean;
	
		public var m_groundBody:b2Body;
	
		private var m_destructionListener:b2DestructionListener;
		private var m_debugDraw:IBox2dDebugDraw;
	
		// This is used to compute the time step ratio to support a variable time step.
		private var m_inv_dt0:Number;
	
		private var s_TOI_Timestep:b2TimeStep;
		public  var s_b2World_timestep:b2TimeStep;
		
		private var s_backupA:b2Sweep;
		private var s_backupB:b2Sweep;
		private var s_queue:Vector.<b2Body>;
		
		// This is for debugging the solver.
		private var m_warmStarting:Boolean;
	
		// This is for debugging the solver.
		private var m_continuousPhysics:Boolean;
		
		// m_flags
		public static const e_newFixture:int = 0x0001;
		public static const e_locked:int = 0x0002;
		
	};
}
