﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Dynamics.Controllers 
{
	import Box2D.Common.b2Pool;
	import Box2D.Dynamics.IBox2dDebugDraw;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2TimeStep;
	import Box2D.Dynamics.b2World;
		
	/**
	 * Base class for controllers. Controllers are a convience for encapsulating common
	 * per-step functionality.
	 */
	public class b2Controller 
	{
		
		public function b2Controller()
		{	
		}
		
		public function Step(step:b2TimeStep):void {}
			
		public function Draw(debugDraw:IBox2dDebugDraw):void { }
		
		private var controllerEdge:b2ControllerEdge;

		public function AddBody(body:b2Body):void 
		{
			controllerEdge = b2Pool.getControllerEdge();
			controllerEdge.controller = this;
			controllerEdge.body = body;

			controllerEdge.nextBody = m_bodyList;
			controllerEdge.prevBody = null;
			m_bodyList = controllerEdge;

			if(controllerEdge.nextBody)
			{
				controllerEdge.nextBody.prevBody = controllerEdge;				
			}

			m_bodyCount++;

			controllerEdge.nextController = body.m_controllerList;
			controllerEdge.prevController = null;
			body.m_controllerList = controllerEdge;

			if(controllerEdge.nextController)
			{
				controllerEdge.nextController.prevController = controllerEdge;
			}

			body.m_controllerCount++;
		}

		public function RemoveBody(body:b2Body):void
		{
			controllerEdge = body.m_controllerList;

			// find controller edge for this body
			while(controllerEdge && controllerEdge.controller != this)
			{
				controllerEdge = controllerEdge.nextController;
			}

			//Attempted to remove a body that was not attached?
			//b2Settings.b2Assert(bEdge != null);
			if(controllerEdge.prevBody)
			{
				controllerEdge.prevBody.nextBody = controllerEdge.nextBody;
			}

			if(controllerEdge.nextBody)
			{
				controllerEdge.nextBody.prevBody = controllerEdge.prevBody;
			}

			if(controllerEdge.nextController)
			{
				controllerEdge.nextController.prevController = controllerEdge.prevController;
			}

			if(controllerEdge.prevController)
			{
				controllerEdge.prevController.nextController = controllerEdge.nextController;
			}

			if(m_bodyList == controllerEdge)
			{
				m_bodyList = controllerEdge.nextBody;
			}

			if(body.m_controllerList == controllerEdge)
			{
				body.m_controllerList = controllerEdge.nextController;
			}

			b2Pool.putb2ControllerEdge(controllerEdge);

			body.m_controllerCount--;
			m_bodyCount--;

			//b2Settings.b2Assert(body.m_controllerCount >= 0);
			//b2Settings.b2Assert(m_bodyCount >= 0);
		}
		
		public function Clear():void
		{
			while(m_bodyList)
			{
				RemoveBody(m_bodyList.body);
			}
		}

		public function GetNext():b2Controller{return m_next;}
		public function GetLast():b2Controller{return m_last;}
		
		public function GetWorld():b2World { return m_world; }
		public function GetBodyList() :b2ControllerEdge{return m_bodyList;}

		public var m_next:b2Controller;
		public var m_prev:b2Controller;
		public var m_last:b2Controller;
		
		public var m_world:b2World;

		public var m_bodyList:b2ControllerEdge;
		public var m_bodyCount:int;		
	}
}