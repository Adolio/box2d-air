﻿/*
* Copyright (c) 2006-2007 Adam Newgas
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Dynamics.Controllers
{

	import Box2D.Common.b2Pool;
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.IBox2dDebugDraw;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2Fixture;
	import Box2D.Dynamics.b2TimeStep;
	
	
	/**
	 * Calculates buoyancy forces for fluids in the form of a half plane
	 */
	public class b2BuoyancyController extends b2Controller
	{
		public function b2BuoyancyController()
		{
		}
	
		/**
		 * The outer surface normal
		 */
		public var normal:b2Vec2 = new b2Vec2(0, -1);
		/**
		 * The height of the fluid surface along the normal
		 */
		public var offset:Number = 0;
		/**
		 * The fluid density
		 */
		public var density:Number = 0;
		/**
		 * Fluid velocity, for drag calculations
		 */
		public var velocity:b2Vec2 = new b2Vec2(0,0);
		/**
		 * Linear drag co-efficient
		 */
		public var linearDrag:Number = 2;
		/**
		 * Angular drag co-efficient
		 */
		public var angularDrag:Number = 1;
		/**
		 * 
		 * If false, bodies are assumed to be uniformly dense, otherwise use the shapes densities
		 *  False by default to prevent a gotcha and currently is NOT IMPLEMENTED
		 * 
		 */
		public var useDensity:Boolean = false; 
		/**
		 * If true, gravity is taken from the world instead of the gravity parameter.
		 */
		public var useWorldGravity:Boolean = true;
		/**
		 * Gravity vector, if the world's gravity is not used
		 */
		public var gravity:b2Vec2 = new b2Vec2();
		
			
		private static var areac:b2Vec2 = new b2Vec2();
		private static var massc:b2Vec2 = new b2Vec2();
		
		private static var sc:b2Vec2 = new b2Vec2();
		private static var buoyancyForce:b2Vec2 = new b2Vec2();
		
		private static var dragForce:b2Vec2 = new b2Vec2();
		
		private static var bodySubmergedArea:Number;
		private static var bodySubmergedMass:Number;
		
		private static var fixtureSubmergedArea:Number;
		
		private static var fixture:b2Fixture;
		private static var body:b2Body;
		
		private static var controller:b2ControllerEdge;

		public override function Step(step:b2TimeStep):void
		{
			if(!m_bodyList)
			{	
				return;
			}
	
			if(useWorldGravity)
			{
				gravity.x = m_world.m_gravity.x;
				gravity.y = m_world.m_gravity.y;
			}
			
			for(controller = m_bodyList; controller; controller = controller.nextBody)
			{
				body = controller.body;

				if(!body.m_isAwake)
				{
					//Buoyancy force is just a function of position,
					//so unlike most forces, it is safe to ignore sleeping bodes
					continue;
				}
				
				areac.x = 0;
				areac.y = 0;

				massc.x = 0;
				massc.y = 0;

				bodySubmergedArea = b2Settings.DECIMAL_ZERO;
				bodySubmergedMass = b2Settings.DECIMAL_ZERO;

				for(fixture = body.m_fixtureList; fixture; fixture = fixture.m_next)
				{
					fixtureSubmergedArea = fixture.m_shape.ComputeSubmergedArea(
						normal,
						offset,
						body.m_xf,
						sc
					);

					bodySubmergedArea += fixtureSubmergedArea;

					areac.x += fixtureSubmergedArea * sc.x;
					areac.y += fixtureSubmergedArea * sc.y;
					
					bodySubmergedMass += fixtureSubmergedArea;

					massc.x += fixtureSubmergedArea * sc.x;
					massc.y += fixtureSubmergedArea * sc.y;
				}
				
				//trace('Body: ', body.m_xf.position, 'Area: ', bodySubmergedArea, ' ', body.m_xf);
				
				// object is not submerged into liquid so continue to next object
				if(bodySubmergedArea < Number.MIN_VALUE)
				{
					continue;
				}

				areac.x /= bodySubmergedArea;
				areac.y /= bodySubmergedArea;

				massc.x /= bodySubmergedMass;
				massc.y /= bodySubmergedMass;

				//Buoyancy
				gravity.GetNegative(buoyancyForce);

				buoyancyForce.Multiply(density * bodySubmergedArea)

				//trace(massc.x, ' ', massc.y, ' --- BODY MASA: ', bodySubmergedMass, ' Fixture MASS: ', fixtureSubmergedArea );

				body.ApplyForce(buoyancyForce, massc);	
	
				//Linear drag
				body.GetLinearVelocityFromWorldPoint(areac, dragForce);
	
				dragForce.Subtract(velocity);
				dragForce.Multiply(-linearDrag * bodySubmergedArea);
	
				body.ApplyForce(dragForce, areac);
				//Angular drag
	
				//TODO: Something that makes more physical sense?
				body.ApplyTorque(
					-body.m_I / // GetInertia()
					body.m_mass * // m_mass
					bodySubmergedArea * 
					body.m_angularVelocity * // GetAngularVelocity() 
					angularDrag
				);
				
			}// end of main for
		}
		
		public override function Draw(debugDraw:IBox2dDebugDraw):void
		{
			var r:Number = 1000;
			
			//Would like to draw a semi-transparent box
			//But debug draw doesn't support that
			var p1:b2Vec2 = b2Pool.getVector2(0, 0);
			var p2:b2Vec2 = b2Pool.getVector2(0, 0);
			
			p1.x = normal.x * offset + normal.y * r;
			p1.y = normal.y * offset - normal.x * r;
			p2.x = normal.x * offset - normal.y * r;
			p2.y = normal.y * offset + normal.x * r;
			
			debugDraw.DrawSegment(p1, p2, 0x0000ff);
			
			b2Pool.putVector2(p1);
			b2Pool.putVector2(p2);
		}
	}
}