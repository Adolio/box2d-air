﻿/*
* Copyright (c) 2006-2007 Erin Catto http://www.gphysics.com
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

package Box2D.Dynamics
{
	import Box2D.Collision.IBroadPhase;
	import Box2D.Collision.b2AABB;
	import Box2D.Collision.b2DynamicTreeNode;
	import Box2D.Collision.b2RayCastInput;
	import Box2D.Collision.b2RayCastOutput;
	import Box2D.Collision.Shapes.b2MassData;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Common.b2Pool;
	import Box2D.Common.b2Settings;
	import Box2D.Common.Math.b2Transform;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.Contacts.b2Contact;
	import Box2D.Dynamics.Contacts.b2ContactEdge;
	
	
	/**
	 * A fixture is used to attach a shape to a body for collision detection. A fixture
	 * inherits its transform from its parent. Fixtures hold additional non-geometric data
	 * such as friction, collision filters, etc.
	 * Fixtures are created via b2Body::CreateFixture.
	 * @warning you cannot reuse fixtures.
	 */
	public class b2Fixture
	{
		/**
		 * Get the type of the child shape. You can use this to down cast to the concrete shape.
		 * @return the shape type.
		 */
		[Inline]	
		final public function GetType():uint
		{
			return m_shape.GetType();
		}
		
		/**
		 * Get the child shape. You can modify the child shape, however you should not change the
		 * number of vertices because this will crash some collision caching mechanisms.
		 */
		[Inline]	
		final public function GetShape():b2Shape
		{
			return m_shape;
		}
		
		private static var edgeContactList:b2ContactEdge;
		private static var contact:b2Contact;
		private static var fixtureA:b2Fixture;
		private static var fixtureB:b2Fixture;
		
		/**
		 * Set if this fixture is a sensor.
		 */
		public function SetSensor(sensor:Boolean):void
		{
			if(m_isSensor == sensor)
			{
				return;				
			}

			m_isSensor = sensor;

			if(m_body == null)
			{
				return;				
			}

			edgeContactList = m_body.m_contactList;

			while(edgeContactList)
			{
				contact = edgeContactList.contact;
				fixtureA = contact.m_fixtureA;
				fixtureB = contact.m_fixtureB;

				if(fixtureA == this || fixtureB == this)
				{
					contact.SetSensor(fixtureA.m_isSensor || fixtureB.m_isSensor);				
				}

				edgeContactList = edgeContactList.next;
			}
		}

		/**
		 * Is this fixture a sensor (non-solid)?
		 * @return the true if the shape is a sensor.
		 */
		[Inline]	
		final public function IsSensor():Boolean
		{
			return m_isSensor;
		}
		
		/**
		 * Set the contact filtering data. This will not update contacts until the next time
		 * step when either parent body is active and awake.
		 */
		public function SetFilterData(filter:b2FilterData):void
		{
			m_filter.categoryBits = filter.categoryBits;
			m_filter.maskBits = filter.maskBits;
			m_filter.groupIndex = filter.groupIndex;
			
			if(!m_body)
			{
				return;				
			}
				
			var edge:b2ContactEdge = m_body.GetContactList();

			while(edge)
			{
				var contact:b2Contact = edge.contact;
				var fixtureA:b2Fixture = contact.GetFixtureA();
				var fixtureB:b2Fixture = contact.GetFixtureB();
				
				if(fixtureA == this || fixtureB == this)
				{
					contact.FlagForFiltering();					
				}

				edge = edge.next;
			}
		}
		
		/**
		 * Get the contact filtering data.
		 */
		[Inline]	
		final public function GetFilterData():b2FilterData
		{
			return m_filter;
		}
		
		/**
		 * Get the parent body of this fixture. This is NULL if the fixture is not attached.
		 * @return the parent body.
		 */
		[Inline]	
		final public function GetBody():b2Body
		{
			return m_body;
		}
		
		/**
		 * Get the next fixture in the parent body's fixture list.
		 * @return the next shape.
		 */
		[Inline]	
		final public function GetNext():b2Fixture
		{
			return m_next;
		}
		
		/**
		 * Get the user data that was assigned in the fixture definition. Use this to
		 * store your application specific data.
		 */
		[Inline]	
		final public function GetUserData():*
		{
			return m_userData;
		}
		
		/**
		 * Set the user data. Use this to store your application specific data.
		 */
		public function SetUserData(data:*):void
		{
			m_userData = data;
		}
		
		/**
		 * Test a point for containment in this fixture.
		 * @param xf the shape world transform.
		 * @param p a point in world coordinates.
		 */
		public function TestPoint(p:b2Vec2):Boolean
		{
			return m_shape.TestPoint(m_body.m_xf, p);
		}
		
		/**
		 * Perform a ray cast against this shape.
		 * @param output the ray-cast results.
		 * @param input the ray-cast input parameters.
		 */
		[Inline]
		final public function RayCast(output:b2RayCastOutput, input:b2RayCastInput):Boolean
		{
			if(!m_shape || !m_body)
			{
				return false;
			}

			return m_shape.RayCast(output, input, m_body.m_xf);
		}
		
		/**
		 * Get the mass data for this fixture. The mass data is based on the density and
		 * the shape. The rotational inertia is about the shape's origin. This operation may be expensive
		 * @param massData
		 * @note if the input is null then you must get the return value.
		 */
		[Inline]
		final public function CalculatetMassData(massData:b2MassData):void
		{
			m_shape.ComputeMass(massData, m_density);
		}
		
		/**
		 * Set the density of this fixture. This will _not_ automatically adjust the mass
		 * of the body. You must call b2Body::ResetMassData to update the body's mass.
		 * @param	density
		 */
		public function SetDensity(density:Number):void
		{
			//b2Settings.b2Assert(b2Math.b2IsValid(density) && density >= 0.0);
			m_density = density;
		}
		
		/**
		 * Get the density of this fixture.
		 * @return density
		 */
		[Inline]
		final public function GetDensity():Number
		{
			return m_density;
		}
		
		/**
		 * Get the coefficient of friction.
		 */
		[Inline]
		final public function GetFriction():Number
		{
			return m_friction;
		}
		
		/**
		 * Set the coefficient of friction.
		 */
		public function SetFriction(friction:Number):void
		{
			m_friction = friction;
		}
		
		/**
		 * Get the coefficient of restitution.
		 */
		[Inline]
		final public function GetRestitution():Number
		{
			return m_restitution;
		}
		
		/**
		 * Get the coefficient of restitution.
		 */
		public function SetRestitution(restitution:Number):void
		{
			m_restitution = restitution;
		}
		
		/**
		 * Get the fixture's AABB. This AABB may be enlarge and/or stale.
		 * If you need a more accurate AABB, compute it using the shape and
		 * the body transform.
		 * @return
		 */
		public function GetAABB():b2AABB
		{
			return m_aabb;
		}
		
		/**
		 * @private
		 */
		
		public function b2Fixture()
		{
			this.Initialize();
		}
		
		public function Initialize():void
		{
			if(isInitialized == false)
			{
				syncAABB1 = b2Pool.getAABB();
				syncAABB2 = b2Pool.getAABB();
				s_displacement = b2Pool.getVector2(0, 0);
				
				m_aabb = b2Pool.getAABB();
				
				m_filter = b2Pool.getFilterData();
					
				m_userData = null;
				m_body = null;
				m_next = null;

				m_shape = null;
				
				m_density = b2Settings.DECIMAL_ZERO;
				
				m_friction = b2Settings.DECIMAL_ZERO;
				m_restitution = b2Settings.DECIMAL_ZERO;
				
				isInitialized = true;
			}
		}
		
		/**
		 * the destructor cannot access the allocator (no destructor arguments allowed by C++).
		 *  We need separation create/destroy functions from the constructor/destructor because
		 */
		public function Create(body:b2Body, xf:b2Transform, def:b2FixtureDef):void
		{
			m_userData = def.userData;
			m_friction = def.friction;
			m_restitution = def.restitution;
			
			m_body = body;
			m_next = null;

			m_filter.categoryBits = def.filter.categoryBits;
			m_filter.maskBits = def.filter.maskBits;
			m_filter.groupIndex = def.filter.groupIndex;
			
			m_isSensor = def.isSensor;
			
			m_shape = def.shape;
			
			m_density = def.density;
		}
		
		/**
		 * the destructor cannot access the allocator (no destructor arguments allowed by C++).
		 *  We need separation create/destroy functions from the constructor/destructor because
		 */
		public function Destroy():void
		{
			b2Pool.putVector2(s_displacement);
			
			b2Pool.putFilterData(m_filter);
			b2Pool.putAABB(m_aabb);
			b2Pool.putAABB(syncAABB1);
			b2Pool.putAABB(syncAABB2);
			
			s_displacement = null;

			// Free the child shape
			m_shape = null;
		    m_filter = null;
			m_body = null;
			m_userData = null;
			m_next = null;
			
			m_proxy = null;
			m_aabb = null;

			syncAABB1 = null;
			syncAABB2 = null;

			isInitialized = false;
		}
		
		/**
		 * This supports body activation/deactivation.
		 */ 
		public function CreateProxy(broadPhase:IBroadPhase, xf:b2Transform):void
		{
			//b2Assert(m_proxyId == b2BroadPhase::e_nullProxy);
			
			// Create proxy in the broad-phase.
			m_shape.ComputeAABB(m_aabb, xf);
			m_proxy = broadPhase.CreateDynamicTreeNodeProxyForFixture(m_aabb, this);
		}
		
		/**
		 * This supports body activation/deactivation.
		 */
		public function DestroyProxy(broadPhase:IBroadPhase):void
		{
			if(m_proxy == null)
			{
				return;
			}
			
			// Destroy proxy in the broad-phase.
			broadPhase.DestroyDynamicTreeNodeProxy(m_proxy);

			m_proxy = null;
		}
		
		private var syncAABB1:b2AABB;
		private var syncAABB2:b2AABB;
		private var s_displacement:b2Vec2;

		[Inline]
		final public function Synchronize(broadPhase:IBroadPhase, transform1:b2Transform, transform2:b2Transform):void
		{
			if(!m_proxy) return;
	
			m_shape.ComputeAABB(syncAABB1, transform1);
			m_shape.ComputeAABB(syncAABB2, transform2);
			
			// b2AABB.Combine(aabb1:b2AABB, aabb2:b2AABB, resultAABB:b2AABB):void

			// b2AABB.Combine(syncAABB1, syncAABB2, m_aabb);
			m_aabb.lowerBound.x = syncAABB1.lowerBound.x < syncAABB2.lowerBound.x ? syncAABB1.lowerBound.x : syncAABB2.lowerBound.x;
			m_aabb.lowerBound.y = syncAABB1.lowerBound.y < syncAABB2.lowerBound.y ? syncAABB1.lowerBound.y : syncAABB2.lowerBound.y;
			m_aabb.upperBound.x = syncAABB1.upperBound.x > syncAABB2.upperBound.x ? syncAABB1.upperBound.x : syncAABB2.upperBound.x;
			m_aabb.upperBound.y = syncAABB1.upperBound.y > syncAABB2.upperBound.y ? syncAABB1.upperBound.y : syncAABB2.upperBound.y;

			s_displacement.x = transform2.position.x - transform1.position.x;
			s_displacement.y = transform2.position.y - transform1.position.y;

			broadPhase.MoveDynamicTreeNodeProxy(m_proxy, m_aabb, s_displacement);
		}
		
		private var isInitialized:Boolean;

		public var m_aabb:b2AABB;
		public var m_density:Number;
		public var m_next:b2Fixture;
		public var m_body:b2Body;
		public var m_shape:b2Shape;
		
		public var m_friction:Number;
		public var m_restitution:Number;
		
		public var m_proxy:b2DynamicTreeNode;
		public var m_filter:b2FilterData;
		
		public var m_isSensor:Boolean;
		
		public var m_userData:*;
	};
}
